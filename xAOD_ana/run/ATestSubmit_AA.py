#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()


# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
sh.setMetaString( "nc_grid_filter", "*" );

#################################
# mc15 PbPb 
#standard HI reco
#ROOT.SH.scanRucio (sh, "data17_hi.00338037.physics_MinBias.merge.AOD.r11399_p3845");
# low pT track reco or doHIP mode??
ROOT.SH.scanRucio (sh, "data17_hi.00338037.physics_MinBias.merge.AOD.f900_m1912");

sh.setMetaString( "nc_grid_filter", "*" );
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here
from AnaAlgorithm.DualUseConfig import addPrivateTool

# add the GRL tool to the algorithm
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
#fullGRLFilePath = "$WorkDir_DIR/data/MyAnalysis/minbias_20.7.xml"

GRLpaths = []
# 2017 XeXe GRL
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data17_hi.periodAllYear_DetStatus-v97-pro21-14_PHYS_StandardGRL_All_Good.xml")

alg.grlTool.GoodRunsListVec = GRLpaths 
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# add the InDet selection tool to the algorithm
addPrivateTool( alg, 'idSelectTool', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool.CutLevel  = "HILoose"
alg.idSelectTool.minPt  = 300.
addPrivateTool( alg, 'idSelectTool_2', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool_2.CutLevel  = "HITight"
alg.idSelectTool_2.minPt  = 300.

alg.getTowers     = 1;
alg.getClusters   = 0;
alg.getTracks     = 1;
alg.getTruth      = 0;
alg.getFcal_sumEt = 1;
alg.getGaps       = 1;
alg.triggerSet    = 3;
alg.cluster_pt_low      = 0.2;
alg.cluster_pt_high     = 1000.0;
alg.cluster_abs_eta_low = 0.0;
alg.tower_ptCut  = 0;
alg.tower_pt_low  = -1000000;
alg.tower_pt_high = 1000000;
alg.tower_abs_eta_low = 3.0;


# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
job.options().setString( ROOT.EL.Job.optXaodAccessMode, 'athena') # Only use this line when you are running on an xAOD instead of a DAOD

# Run the job using the direct driver.
driver = ROOT.EL.PrunDriver()
#driver.options ().setString(ROOT.EL.Job.optGridSite,"BNL-OSG2_SCRATCHDISK");
driver.options().setDouble(ROOT.EL.Job.optGridNFilesPerJob, 1);
driver.options().setDouble(ROOT.EL.Job.optGridMaxNFilesPerJob, 1);
driver.options().setString("nc_outputSampleName", "user.bseidlit.ppDecorrelation_XeXe_37.%in:name[2]%.%in:name[6]%");
driver.submitOnly( job, options.submission_dir )
