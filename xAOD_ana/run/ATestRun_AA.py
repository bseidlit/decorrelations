#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()


# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
#inputFilePath = '/gpfs/mnt/atlasgpfs01/usatlas/data/bseidlit/localAODs/user.rbi.epos.peripheral.diffractive.AOD.210709.v2_EXT0/'
#inputFilePath = '/eos/atlas/atlascerngroupdisk/phys-hi/example_hi_AOD/data17_hi.00338037.physics_MinBias.merge.AOD.f900_m1912/'
inputFilePath = '/gpfs/mnt/atlasgpfs01/usatlas/data/bseidlit/localAODs/data17_hi/'
ROOT.SH.ScanDir().filePattern( 'data17_hi*' ).scan( sh, inputFilePath )

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 5 )
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here
from AnaAlgorithm.DualUseConfig import addPrivateTool

# add the GRL tool to the algorithm
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
GRLpaths = []
#GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data15_13TeV.periodABC50ns_DetStatus-v105-pro22-13_MERGED_PHYS_StandardGRL_All_Good_tolerable_L1CALmisconfigSatBCID.xml")
#GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardModel_MinimuBias2010_tolerable_L1CALmisconfigSatBCID.xml")
# 2017 5 TeV pp GRL
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml"
# 2017 13 TeV pp GRL
#GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data17_13TeV.periodN_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml")
# 2015 13 TeV pp low-mul GRL
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data15_13TeV.periodABC50ns_DetStatus-v105-pro22-13_MERGED_PHYS_StandardGRL_All_Good_tolerable_L1CALmisconfigSatBCID.xml"
# 2018 13 TeV pp GRL
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data18_13TeV.periodG4J_DetStatus-v105-pro22-13_MERGED_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml"
# 2017 XeXe GRL
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data17_hi.periodAllYear_DetStatus-v97-pro21-14_PHYS_StandardGRL_All_Good.xml")
# 2018 PbPb GRL
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml"
#fullGRLFilePath = "$WorkDir_DIR/data/MyAnalysis/data17_13TeV.periodN_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml"
alg.grlTool.GoodRunsListVec = GRLpaths
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# add the InDet selection tool to the algorithm
alg.OutputLevel = 1;
addPrivateTool( alg, 'idSelectTool', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool.CutLevel  = "HILoose"
alg.idSelectTool.minPt  = 300.
addPrivateTool( alg, 'idSelectTool_2', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool_2.CutLevel  = "HITight"
alg.idSelectTool_2.minPt  = 300.

alg.getTowers     = 1;
alg.getClusters   = 0;
alg.getTracks     = 1;
alg.getTruth      = 0;
alg.getFcal_sumEt = 1;
alg.getGaps       = 1;
alg.triggerSet    = 3;
alg.cluster_pt_low      = 0.2;
alg.cluster_pt_high     = 1000.0;
alg.cluster_abs_eta_low = 0.0;
alg.tower_ptCut  = 0;
alg.tower_pt_low  = -1000;
alg.tower_pt_high = 1000;
alg.tower_abs_eta_low = 3.0;

# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Only use this line when you are running on an xAOD instead of a DAOD
#job.options().setString( ROOT.EL.Job.optXaodAccessMode, 'athena') 

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
