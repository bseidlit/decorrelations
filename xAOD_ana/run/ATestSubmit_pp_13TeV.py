#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()


# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
sh.setMetaString( "nc_grid_filter", "*" );


# 2017 5 TeV data
#ROOT.SH.scanRucio (sh, "data17_5TeV.00340973.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV.0034*.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340910.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341184.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340814.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341027.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340925.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340718.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340683.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340644.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340697.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340849.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340850.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341123.physics_MinBias.merge.AOD.r11215_p3764");

#################################
# 13 TeV 
# 2017 period N 00341*
# 2015
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267*.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.003*.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data18_13TeV.0036169*.physics_MinBias.merge.AOD.f995_m2032");

#2015
#ROOT.SH.scanRucio (sh, "data15_13TeV:data15_13TeV.00277025.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV:data15_13TeV.00277081.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267358.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267359.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267360.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267367.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267385.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data15_13TeV.00267599.physics_MinBias.merge.AOD.r12571_p4507");

#2016
#ROOT.SH.scanRucio (sh, "data16_13TeV:data16_13TeV.00299390.physics_MinBias.merge.AOD.r12571_p4507"); 
#ROOT.SH.scanRucio (sh, "data16_13TeV:data16_13TeV.00300287.physics_MinBias.merge.AOD.r12571_p4507"); 
ROOT.SH.scanRucio (sh, "data16_13TeV.00305359.physics_MinBias.merge.AOD.r12572_p4507"); 
#ROOT.SH.scanRucio (sh, "data16_13TeV:data16_13TeV.00309314.physics_MinBias.merge.AOD.r12571_p4507"); 
#ROOT.SH.scanRucio (sh, "data16_13TeV:data16_13TeV.00309346.physics_MinBias.merge.AOD.r12571_p4507"); 
#ROOT.SH.scanRucio (sh, "data16_13TeV:data16_13TeV.00310216.physics_MinBias.merge.AOD.r12571_p4507"); 

#2017
#ROOT.SH.scanRucio (sh, "data17_13TeV.00329542.physics_MinBias.merge.AOD.r12571_p4507 ");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00330857.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00330875.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00331020.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00336505.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00341294.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00341312.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00341419.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00341534.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00341615.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data17_13TeV.00341649.physics_MinBias.merge.AOD.r12571_p4507");
###2018

#2018
#ROOT.SH.scanRucio (sh, "data18_13TeV.00354176.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data18_13TeV.00354311.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data18_13TeV.00361689.physics_MinBias.merge.AOD.r12571_p4507");
#ROOT.SH.scanRucio (sh, "data18_13TeV.00361690.physics_MinBias.merge.AOD.f995_m2032");
#ROOT.SH.scanRucio (sh, "data18_13TeV.00361696.physics_MinBias.merge.AOD.f995_m2032");


#####################################
# minbias 13 TeV pythia  
#ROOT.SH.scanRucio (sh, "mc15_13TeV.361203.Pythia8_A2_MSTW2008LO_ND_minbias.merge.AOD.e3639_s2601_s2132_r6616_r6270_tid05411175_00");


sh.setMetaString( "nc_grid_filter", "*" );
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here
from AnaAlgorithm.DualUseConfig import addPrivateTool

# add the GRL tool to the algorithm
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
#fullGRLFilePath = "$WorkDir_DIR/data/MyAnalysis/minbias_20.7.xml"

GRLpaths = []
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardModel_MinimuBias2010_tolerable_L1CALmisconfigSatBCID.xml")
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data17_13TeV.periodN_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml")
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data18_13TeV_self.xml")
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data16_13TeV.periodAFP_DetStatus-v84-pro20-16_DQDefects-00-02-04_PHYS_StandardModel_MinimuBias2010_Ignore_BS.xml")
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data18_13TeV.periodG4J_DetStatus-v105-pro22-13_MERGED_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMUewq.xml")

alg.grlTool.GoodRunsListVec = GRLpaths 
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# add the InDet selection tool to the algorithm
addPrivateTool( alg, 'idSelectTool', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool.CutLevel  = "HILoose"
alg.idSelectTool.minPt  = 300.
addPrivateTool( alg, 'idSelectTool_2', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool_2.CutLevel  = "HITight"
alg.idSelectTool_2.minPt  = 300.

alg.getTowers     = 0;
alg.getClusters   = 1;
alg.getTracks     = 1;
alg.getTruth      = 0;
alg.getFcal_sumEt = 1;
alg.getGaps       = 0;
alg.triggerSet    = 1;
alg.cluster_pt_low      = 0.3;
alg.cluster_pt_high     = 5.0;
alg.cluster_abs_eta_low = 3.5;


# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.PrunDriver()
#driver.options ().setString(ROOT.EL.Job.optGridSite,"BNL-OSG2_SCRATCHDISK");
driver.options().setDouble(ROOT.EL.Job.optGridNFilesPerJob, 10);
driver.options().setDouble(ROOT.EL.Job.optGridMaxNFilesPerJob, 1);
driver.options().setString("nc_outputSampleName", "user.bseidlit.ppDecorrelation_run2_trkClus_13TeV_06.%in:name[2]%.%in:name[6]%");
driver.submitOnly( job, options.submission_dir )
