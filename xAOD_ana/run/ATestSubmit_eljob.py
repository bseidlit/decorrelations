#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()


# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
sh.setMetaString( "nc_grid_filter", "*" );

#////////////////////////////////////////
# data sets

# 2017 5 TeV data
#ROOT.SH.scanRucio (sh, "data17_5TeV.00340973.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV.0034*.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340910.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341184.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340814.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341027.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340925.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340718.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340683.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340644.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340697.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340849.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340850.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341123.physics_MinBias.merge.AOD.r11215_p3764");

# QCD non-dif MC
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420010.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0R04.merge.AOD.e4108_s3238_r10441_r10210");
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420011.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1R04.merge.AOD.e4108_s3238_r10441_r10210");

#################################
# 2017 13 TeV 
ROOT.SH.scanRucio (sh, "data17_13TeV.00341*.physics_MinBias.merge.AOD.r12571_p4507");

# 2017 XeXe 
#ROOT.SH.scanRucio (sh, "data17_hi.00338037.physics_MinBias.merge.AOD.r11399_p3845");
# HIJING min bias
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e4962_s3531_s3519_r11875_tid21043668_00");
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e4962_s3530_s3519_r11875_tid21043673_00");
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e4962_s3520_s3519_r11875_tid21043679_00");
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e4962_s3528_s3519_r11875_tid21043681_00");
#ROOT.SH.scanRucio (sh, "mc16_5TeV.420000.Hijing_PbPb_5p02TeV_MinBias_Flow_JJFV6.recon.AOD.e4962_s3529_s3519_r11875_tid21043684_00");

#####################################
# 2018 PbPb 
# pp reco
#ROOT.SH.scanRucio (sh, "data18_hi.00366919.physics_PC.merge.AOD.f1028_m2048");
#HI reco
#ROOT.SH.scanRucio (sh, "data18_hi.00366919.physics_PC.merge.AOD.f1030_m2048");
#ROOT.SH.scanRucio (sh, "data18_hi.00366919.physics_CC.merge.AOD.f1030_m2048");


sh.setMetaString( "nc_grid_filter", "*" );
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here
from AnaAlgorithm.DualUseConfig import addPrivateTool

# add the GRL tool to the algorithm
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
#fullGRLFilePath = "$WorkDir_DIR/data/MyAnalysis/minbias_20.7.xml"
# 2017 5 TeV pp GRL
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml"
# 2017 13 TeV pp GRL
fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data17_13TeV.periodN_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml"
# 2017 XeXe 
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data17_hi.periodAllYear_DetStatus-v97-pro21-14_PHYS_StandardGRL_All_Good.xml"
# 2018 PbPb GRL
#fullGRLFilePath = "$UserAnalysis_DIR/data/MyAnalysis/data18_hi.periodAllYear_DetStatus-v106-pro22-14_Unknown_PHYS_HeavyIonP_All_Good_ignore_TOROIDSTATUS.xml"
alg.grlTool.GoodRunsListVec = [ fullGRLFilePath ]
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# add the InDet selection tool to the algorithm
addPrivateTool( alg, 'idSelectTool', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool.CutLevel  = "MinBias"
alg.idSelectTool.minPt  = 300.
alg.idSelectTool.maxAbsEta  = 2.5


# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.PrunDriver()
#driver.options ().setString(ROOT.EL.Job.optGridSite,"BNL-OSG2_SCRATCHDISK");
driver.options().setDouble(ROOT.EL.Job.optGridNFilesPerJob, 1);
driver.options().setDouble(ROOT.EL.Job.optGridMaxNFilesPerJob, 1);
driver.options().setString("nc_outputSampleName", "user.bseidlit.ppDecorrelation_2017_13TeV_periodN_32.%in:name[2]%.%in:name[6]%");
driver.submitOnly( job, options.submission_dir )
