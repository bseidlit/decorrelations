#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()


# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
sh.setMetaString( "nc_grid_filter", "*" );


# 2017 5 TeV data
#ROOT.SH.scanRucio (sh, "data17_5TeV.00340973.physics_MinBias.merge.AOD.r11215_p3764");
ROOT.SH.scanRucio (sh, "data17_5TeV.0034*.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340910.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341184.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340814.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341027.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340925.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340718.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340683.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340644.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340697.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340849.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00340850.physics_MinBias.merge.AOD.r11215_p3764");
#ROOT.SH.scanRucio (sh, "data17_5TeV:data17_5TeV.00341123.physics_MinBias.merge.AOD.r11215_p3764");


sh.setMetaString( "nc_grid_filter", "*" );
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# later on we'll add some configuration options for our algorithm that go here
from AnaAlgorithm.DualUseConfig import addPrivateTool

# add the GRL tool to the algorithm
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
#fullGRLFilePath = "$WorkDir_DIR/data/MyAnalysis/minbias_20.7.xml"

GRLpaths = []
GRLpaths.append("$UserAnalysis_DIR/data/MyAnalysis/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml")

alg.grlTool.GoodRunsListVec = GRLpaths 
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# add the InDet selection tool to the algorithm
addPrivateTool( alg, 'idSelectTool', 'InDet::InDetTrackSelectionTool' )
alg.idSelectTool.CutLevel  = "MinBias"
alg.idSelectTool.minPt  = 300.
alg.idSelectTool.maxAbsEta  = 2.5

alg.triggerSet    = 2;
alg.getTowers     = 1;
alg.getClusters   = 1;
alg.getTracks     = 1;
alg.getTruth      = 0;
alg.getFcal_sumEt = 1;
alg.getGaps       = 0;
alg.cluster_pt_low      = 0.5;
alg.cluster_pt_high     = 5.0;
alg.cluster_abs_eta_low = 4.0;
alg.tower_pt_low        = 0.5;
alg.tower_pt_high       = 5.0;
alg.tower_abs_eta_low   = 4.0;
alg.tower_ptCut   = 0;


# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.PrunDriver()
#driver.options ().setString(ROOT.EL.Job.optGridSite,"BNL-OSG2_SCRATCHDISK");
driver.options().setDouble(ROOT.EL.Job.optGridNFilesPerJob, 1);
driver.options().setDouble(ROOT.EL.Job.optGridMaxNFilesPerJob, 1);
driver.options().setString("nc_outputSampleName", "user.bseidlit.ppDecorrelation_mb_2017_5TeV_33.%in:name[2]%.%in:name[6]%");
driver.submitOnly( job, options.submission_dir )
