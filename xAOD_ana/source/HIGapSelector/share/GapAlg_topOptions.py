theApp.EvtMax = 5
#import AthenaPoolCnvSvc.ReadAthenaPool
import AthenaRootComps.ReadAthenaxAODHybrid
svcMgr.EventSelector.InputCollections = ['AOD.pool.root']

from AthenaCommon.AppMgr import ToolSvc

from HIGapSelector.HIGapSelectorConf import UPC__TopoClusterSelectionTool
tc_selection_tool=UPC__TopoClusterSelectionTool("TopoClusterSelectionTool_UPCGap")
tc_selection_tool.OutputLevel=VERBOSE
tc_selection_tool.MinPt=200.*MeV
tc_selection_tool.ApplySignificanceCuts=True
tc_selection_tool.CalibFile="HIGapSelector/TCSigCuts.root"
tc_selection_tool.CalibHisto="h1_TC_sig_cuts"
ToolSvc += tc_selection_tool

from InDetTrackSelectionTool.InDetTrackSelectionToolConf import InDet__InDetTrackSelectionTool
track_selection_tool=InDet__InDetTrackSelectionTool("InDetTrackSelectionTool_UPCGap");  
track_selection_tool.CutLevel="NoCut"
track_selection_tool.minPt=200.*MeV
ToolSvc += track_selection_tool

algSeq = CfgMgr.AthSequencer("AthAlgSeq")
algSeq += CfgMgr.GapCalculationAlg("GapAlg",OutputLevel=VERBOSE)
algSeq.GapAlg.ClusterContainerName="CaloCalTopoClusters"
algSeq.GapAlg.TrackContainerName="InDetTrackParticles"
algSeq.GapAlg.ClusterSelectionTool=tc_selection_tool
algSeq.GapAlg.TrackSelectionTool=track_selection_tool
algSeq.GapAlg.GapMinimum=0.5
