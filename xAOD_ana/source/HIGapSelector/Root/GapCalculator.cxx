#include "HIGapSelector/GapCalculator.h"

namespace UPC
{
  float getEdgeGapPos(const std::set<float>& eta_vals)
  {
    float s_detector_max_eta = 4.9;
    float eta_max=-s_detector_max_eta;
    auto itr=eta_vals.rbegin();
    if(itr!=eta_vals.rend()) eta_max=(*itr);
    return s_detector_max_eta-eta_max;
  }
  float getEdgeGapNeg(const std::set<float>& eta_vals)
  {
    float s_detector_max_eta = 4.9;
    float eta_min=s_detector_max_eta;
    auto itr=eta_vals.begin();
    if(itr!=eta_vals.end()) eta_min=(*itr);
    return s_detector_max_eta+eta_min;
  }
  float getSumGap(const std::set<float>& eta_vals, float min_delta)
  {
    float s_detector_max_eta = 4.9;
    std::set<float> eta_vals_copy = eta_vals;
    eta_vals_copy.insert(s_detector_max_eta);
    eta_vals_copy.insert(-s_detector_max_eta);
    float sumGap=0;
    auto itr1=eta_vals_copy.rbegin();
    if(itr1==eta_vals_copy.rend()) return sumGap;
    auto itr2=itr1;
    itr2++;
    for(; itr2!=eta_vals_copy.rend(); itr1++, itr2++)
    {
      float delta_eta=(*itr1)-(*itr2);
      if(delta_eta > min_delta) sumGap+=delta_eta;
    }
    return sumGap;
  }

  float getSumGapPos(const std::set<float>& eta_vals, float jet_eta_max, float min_delta)
  {
    float s_detector_max_eta = 4.9;
    std::set<float> eta_vals_copy = eta_vals;
    eta_vals_copy.insert(s_detector_max_eta);
    eta_vals_copy.insert(-s_detector_max_eta);
    eta_vals_copy.insert(1e-10);
    float sumGap_pos=0;
    auto itr1=eta_vals_copy.rbegin(); // Start looping at the beginning, with reverse ordering!
    if(itr1==eta_vals_copy.rend()) return sumGap_pos; // If there are < 2 elements, terminate.
    auto itr2=itr1; // Come up with a second iterator for the other gap
    itr2++; // Place the second iterator one ~behind~ of the other.
    for(; itr2!=eta_vals_copy.rend(); itr1++, itr2++)
    {
      if( (*itr2) < jet_eta_max) continue; // Skip any pair which starts at an eta value below the highest jet eta
      float delta_eta=(*itr1)-(*itr2); // Compute the pair gap from our two iterators
      if(delta_eta > min_delta) { 
        sumGap_pos+=delta_eta; // Add it to the sum if it passes the threshold
      }
    }
    return sumGap_pos;
  }

  float getSumGapNeg(const std::set<float>& eta_vals, float jet_eta_min, float min_delta)
  {
    float s_detector_max_eta = 4.9;
    std::set<float> eta_vals_copy = eta_vals;
    eta_vals_copy.insert(s_detector_max_eta);
    eta_vals_copy.insert(-s_detector_max_eta);
    eta_vals_copy.insert(-1e-10);
    float sumGap_neg=0;
    auto itr1=eta_vals_copy.begin(); // Start looping at beginning
    if(itr1==eta_vals_copy.end()) return sumGap_neg; // If there are < 2 elements, terminate.
    auto itr2=itr1; // Come up with a second iterator for the other gap
    itr2++; // Place the second iterator one ahead of the other
    for(; itr2!=eta_vals_copy.end(); itr1++, itr2++)
    {
      if( (*itr2) > jet_eta_min) continue; // Skip any pair which ends at an eta value above the lowest jet eta
      float delta_eta=(*itr2)-(*itr1); // Computer the pair gap from our two iterators
      if(delta_eta > min_delta) {
        sumGap_neg+=delta_eta; // Add it to the sum if it passes the threshold
      }
    }
    return sumGap_neg;
  }


}
