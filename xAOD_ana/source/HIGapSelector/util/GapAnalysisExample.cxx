//#include <AsgMessaging/MessageCheck.h>
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODRootAccess/tools/ReturnCheck.h>
#include <xAODRootAccess/tools/TFileAccessTracer.h>
#include <InDetTrackSelectionTool/InDetTrackSelectionTool.h>
#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODTracking/TrackParticleContainer.h>

#include "HIGapSelector/TopoClusterSelectionTool.h"
#include "HIGapSelector/GapCalculator.h"
#include <TFile.h>
#include <iomanip>
#include <iostream>

int main(int argc, char* argv[])
{
  std::string m_cluster_container_name("CaloCalTopoClusters");
  std::string m_track_container_name("InDetTrackParticles");

  std::string appName("GapAnalysisExample");
  std::string inputFileName("AOD.pool.root");
  if(argc > 0) appName=argv[0];
  if(argc > 1) inputFileName=argv[1];

  //Turn off spyware
  xAOD::TFileAccessTracer::enableDataSubmission(false);

  //other minimal stuff needed to read an AOD
  xAOD::TReturnCode::enableFailure();
  RETURN_CHECK(appName.c_str(),xAOD::Init("Initializing"));
  xAOD::TEvent eventstore(xAOD::TEvent::kClassAccess);
  RETURN_CHECK(appName.c_str(),eventstore.readFrom(TFile::Open(inputFileName.c_str())));
  ///


  InDet::InDetTrackSelectionTool* m_track_selection_tool=new InDet::InDetTrackSelectionTool("TrackSelector_GapAnalysis");
  m_track_selection_tool->setProperty("CutLevel","NoCut");
  m_track_selection_tool->setProperty("minPt",200.);
  if(m_track_selection_tool->initialize().isFailure())
  {
    std::cerr <<"Could not initialize InDetTrackSelectionTool" << std::endl;
    return 0;
  }
  
  UPC::TopoClusterSelectionTool* m_tc_selection_tool=new UPC::TopoClusterSelectionTool("TCSelector_GapAnalysis");
  m_tc_selection_tool->setProperty("MinPt",200.);
  m_tc_selection_tool->setProperty("ApplySignificanceCuts",true);
  m_tc_selection_tool->setProperty("CalibFile","HIGapSelector/TCSigCuts.root");
  m_tc_selection_tool->setProperty("CalibHisto","h1_TC_sig_cuts");
  if(m_tc_selection_tool->initialize().isFailure())
  {
    std::cerr << "Could not initialize TopoClusterSelectionTool" << std::endl;
    return 0;
  }
  //

  float gapMin=0.5;
  ///
  Long64_t max_entries=eventstore.getEntries();
  for (Long64_t jentry=0; jentry<max_entries;jentry++)
  {
    int nbytes=eventstore.getEntry(jentry);
    if(nbytes < 0)
    {
      std::cerr << "Could not read tree for event " << jentry << std::endl;
      continue;
    }
    //auto eta_vals=gct->getOrderedEtaVals();
    //
    const xAOD::CaloClusterContainer* tc=0;
    if(eventstore.retrieve(tc,m_cluster_container_name).isFailure())
    {
      std::cerr << "Could not find CaloClusterContainer object w/ name" << m_cluster_container_name << std::endl;
      return 1;
    }

    std::set<float> eta_vals;
    for(auto cl : *tc)
    {
      if(m_tc_selection_tool->accept(*cl)) eta_vals.insert(cl->eta());
    }
    ///
    const xAOD::TrackParticleContainer* track_container=nullptr;
    if(eventstore.retrieve(track_container,m_track_container_name).isFailure())
    {
      std::cerr << "Could not retrieve TrackParticleContainer object w/ name " << m_track_container_name << std::endl;
      return 1;
    }
    //could use version of track selection with explicit reference to Primary Vertex
    for(auto track : *track_container)
    {
      //if(m_track_selection_tool->accept(*track,pv)) eta_vals.insert(track->eta());
      if(m_track_selection_tool->accept(*track)) eta_vals.insert(track->eta());
    }
    float jet_eta1=1.2;
    float jet_eta2=-0.2;
    std::cout << "Event " << jentry << std::endl;
    std::cout << std::setw(15) << UPC::getEdgeGapPos(eta_vals)<< std::endl;
    std::cout << std::setw(15) << UPC::getEdgeGapNeg(eta_vals)<< std::endl;
    std::cout << std::setw(15) << UPC::getSumGap(eta_vals,gapMin)<< std::endl;
    std::cout << std::setw(15) << UPC::getSumGapPos(eta_vals,jet_eta1,gapMin)<< std::endl;
    std::cout << std::setw(15) << UPC::getSumGapNeg(eta_vals,jet_eta2,gapMin)<< std::endl;
    //

  }
  //
}
