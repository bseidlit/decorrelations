#ifndef UPC_GAPCALCULATOR_H
#define UPC_GAPCALCULATOR_H

#include <set>

namespace UPC
{
//   float s_detector_max_eta;
  float getEdgeGapPos(const std::set<float>& eta_vals);
  float getEdgeGapNeg(const std::set<float>& eta_vals);
  float getSumGap(const std::set<float>& eta_vals, float min_delta);
  float getSumGapPos(const std::set<float>& eta_vals, float jet_eta_max, float min_delta);
  float getSumGapNeg(const std::set<float>& eta_vals, float jet_eta_min, float min_delta);
}

#endif
