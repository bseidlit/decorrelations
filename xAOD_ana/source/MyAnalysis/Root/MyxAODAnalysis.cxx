#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include <EventLoop/Worker.h>
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
// truth info
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
// EventShape
#include "xAODHIEvent/HIEventShapeContainer.h"
// online tracking
#include "xAODTrigMinBias/TrigVertexCountsContainer.h"
//#include "CaloGeoHelpers/CaloSampling.h"
#include <TSystem.h>
// Level1 energy
#include "xAODTrigger/EnergySumRoI.h"

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator), 
    m_grl ("GoodRunsListSelectionTool/grl", this), 
    m_InDetTrackSelectionTool("InDet::InDetTrackSelectionTool/TrackSelectionTool", this), 
    m_InDetTrackSelectionTool_2("InDet::InDetTrackSelectionTool/TrackSelectionTool", this), 
    m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool"), 
    m_trigConfigTool("TrigConf::xAODConfigTool/xAODConfigTool"), 
    m_muonSelection ("CP::MuonSelectionTool", this), 
    m_muonCalibrationAndSmearingTool ("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool",this), 
    m_matchTool ("Trig::MatchingTool", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.
  
  declareProperty ("grlTool", m_grl, "the GRL tool");
  declareProperty ("idSelectTool", m_InDetTrackSelectionTool);
  declareProperty ("idSelectTool_2", m_InDetTrackSelectionTool_2);

  declareProperty ("getTowers"    ,m_getTowers,"getTowers");
  declareProperty ("getClusters"  ,m_getClusters,"getClusters");
  declareProperty ("getTracks"    ,m_getTracks,"getTracks");
  declareProperty ("getTruth"     ,m_getTruth,"getTruth");
  declareProperty ("getFcal_sumEt",m_getFcal_sumEt,"getFcal_sumEt");
  declareProperty ("getGaps"      ,m_getGaps,"getGaps");
  declareProperty ("triggerSet"   ,m_triggerSet,"triggerSet");
  declareProperty ("cluster_pt_low"     ,m_cluster_pt_low,"svae clusters above this cut");
  declareProperty ("cluster_pt_high"    ,m_cluster_pt_high,"svae clusters below this cut");
  declareProperty ("cluster_abs_eta_low",m_cluster_abs_eta_low,"svae clusters abs eta above this cut");
  declareProperty ("tower_pt_low"     ,m_tower_pt_low ,"svae towers above this cut");
  declareProperty ("tower_pt_high"    ,m_tower_pt_high,"svae towers below this cut");
  declareProperty ("tower_abs_eta_low",m_tower_abs_eta_low,"svae towers abs eta above this cut");
  declareProperty ("tower_ptCut",m_tower_ptCut,"bool for tower pt cut");

  //sleep(600);

}

void MyxAODAnalysis :: ClearVector() {

  m_TriggerObject_chain.clear();
  m_TriggerObject_ps.clear();

  m_Vertex_nTrk.clear();
  m_Vertex_sumPt.clear();
  m_Vertex_sumPt2.clear();
  m_Vertex_type.clear();
  m_Vertex_x.clear();
  m_Vertex_y.clear();
  m_Vertex_z.clear();

  m_Track_pt. clear();
  m_Track_eta.clear();
  m_Track_phi.clear();
  m_Track_d0Signif.clear();
  m_Track_z0SinTheta.clear();
  m_Track_truthMatchingProb.clear();
  m_Track_truthPt.clear();
  m_Track_truthEta.clear();
  m_Track_truthPhi.clear();
  m_Track_truthType.clear();
  m_Track_truthOrig.clear();
  m_Track_truthPdgID.clear();
  m_Track_truthz.clear();
  m_Track_truthnIn.clear();
  m_Track_truthBarcode.clear();
  m_Track_quality1.clear();
  m_Track_quality2.clear();

  m_Cluster_et. clear();
  m_Cluster_eta.clear();
  m_Cluster_phi.clear();
  m_Cluster_size.clear();
  m_Cluster_status.clear();
  m_Cluster_cellSigSamp.clear();
  m_Cluster_e.clear();
  m_b_tower_pt.clear();
  m_b_tower_phi.clear();
  m_b_tower_eta.clear();
  m_b_tower_m.clear();
  m_b_tower_E.clear();
  m_truth_pt.clear();
  m_truth_eta.clear();
  m_truth_phi.clear();
  m_truth_pid.clear();
  m_truth_barcode.clear();
  m_truth_isCharged.clear();

}


StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_DEBUG ("in initialize");

  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* m_tree = tree ("analysis");
  //m_tree->SetDirectory (outputFile);

  ANA_CHECK (book (TH1F ("h_Pt", "h_jetPt", 100, 0, 500)));
  //ANA_CHECK (book (TH2F ("h2_evtDis", "", 10, 4, 5,64,-TMath::Pi(),TMath::Pi())));

  m_tree->Branch("RunNumber",  &m_RunNumber);
  m_tree->Branch("EventNumber",&m_EventNumber);
  m_tree->Branch("LumiBlock",  &m_LumiBlock);
  m_tree->Branch("AvgMu",  &m_AvgMu);
  m_tree->Branch("ActMu",  &m_ActMu);
  m_tree->Branch("fcalet_A",  &b_fcalet_A);
  m_tree->Branch("fcalet_C",  &b_fcalet_C);
  m_tree->Branch("PV_x",       &m_PV_x);
  m_tree->Branch("PV_y",       &m_PV_y);
  m_tree->Branch("PV_z",       &m_PV_z);
  m_tree->Branch("nVtx",       &m_nVtx);
  m_tree->Branch("L1_MBTS_1",  &m_MBTS_1);
  m_tree->Branch("Ntrk_MB",    &m_Ntrk_MB);
  m_tree->Branch("N_barrelClus",    &m_N_barrelClus);
  m_tree->Branch("L1_energyT",    &m_L1_energyT);
  m_tree->Branch("sumGapAC",    &m_sumGapAC);

  // trigger
  m_tree->Branch("TriggerObject_Chain", &m_TriggerObject_chain);
  m_tree->Branch("TriggerObject_Ps", &m_TriggerObject_ps);
  m_tree->Branch("nTrkOnline", &nTrkOnline);
  m_tree->Branch("nTrk_AR", &nTrk);

  // vertex
  m_tree->Branch("Vertex_nTrk",   &m_Vertex_nTrk);
  m_tree->Branch("Vertex_sumPt",  &m_Vertex_sumPt);
  m_tree->Branch("Vertex_sumPt2", &m_Vertex_sumPt2);
  m_tree->Branch("Vertex_type",   &m_Vertex_type);
  m_tree->Branch("Vertex_x",      &m_Vertex_x);
  m_tree->Branch("Vertex_y",      &m_Vertex_y);
  m_tree->Branch("Vertex_z",      &m_Vertex_z);

  // tracks
  if (m_getTracks){
    m_tree->Branch("Track_pt",   &m_Track_pt);
    m_tree->Branch("Track_eta",  &m_Track_eta);
    m_tree->Branch("Track_phi",  &m_Track_phi);
    m_tree->Branch("Track_quality1",  &m_Track_quality1);
    m_tree->Branch("Track_quality2",  &m_Track_quality2);
    //m_tree->Branch("Track_d0Signif",    &m_Track_d0Signif);
    //m_tree->Branch("Track_z0SinTheta",  &m_Track_z0SinTheta);
    if (m_getTruth){
      m_tree->Branch("Track_truthMatchingProb",  &m_Track_truthMatchingProb);
      m_tree->Branch("Track_truthPt", &m_Track_truthPt);
      m_tree->Branch("Track_truthEta", &m_Track_truthEta);
      m_tree->Branch("Track_truthPhi", &m_Track_truthPhi);
      m_tree->Branch("Track_truthType", &m_Track_truthType);
      m_tree->Branch("Track_truthOrig", &m_Track_truthOrig);
      m_tree->Branch("Track_truthPdgID", &m_Track_truthPdgID);
      m_tree->Branch("Track_truthz", &m_Track_truthz);
      m_tree->Branch("Track_truthnIn", &m_Track_truthnIn);
      m_tree->Branch("Track_truthBarcode", &m_Track_truthBarcode);
    }
  }

  // clusters
  if (m_getClusters){
    m_tree->Branch("Cluster_et",   &m_Cluster_et);
    m_tree->Branch("Cluster_eta",  &m_Cluster_eta);
    m_tree->Branch("Cluster_phi",  &m_Cluster_phi);
    //m_tree->Branch("Cluster_e", &m_Cluster_e);
    //m_tree->Branch("Cluster_size", &m_Cluster_size);
    //m_tree->Branch("Cluster_status", &m_Cluster_status);
    //m_tree->Branch("Cluster_cellSigSamp", &m_Cluster_cellSigSamp);
  }
  if (m_getTruth){
    // truth particles
    m_tree->Branch("truth_pt",&m_truth_pt) ;
    m_tree->Branch("truth_eta",&m_truth_eta);
    m_tree->Branch("truth_phi",&m_truth_phi);
    m_tree->Branch("truth_pid",&m_truth_pid);
    m_tree->Branch("truth_barcode",&m_truth_barcode);
    m_tree->Branch("truth_isCharged",&m_truth_isCharged);
  }
  /// towers
  if(m_getTowers){
    m_tree->Branch("ntower", &m_b_ntower, "ntower/I");
    m_tree->Branch("tower_pt", &m_b_tower_pt);
    //m_tree->Branch("tower_eta", &m_b_tower_eta);
    //m_tree->Branch("tower_phi", &m_b_tower_phi);
    //m_tree->Branch("tower_m", &m_b_tower_m);
    //m_tree->Branch("tower_E", &m_b_tower_E);
  }


  ANA_MSG_DEBUG ("in initialize");
  ANA_MSG_DEBUG( "OutputFileName = " << m_OutputFileName );
 
  ANA_CHECK (m_grl.retrieve());
  //ANA_CHECK (m_InDetTrackSelectionTool.retrieve());

  // Initialize and configure trigger tools
  ANA_CHECK (m_trigConfigTool.initialize());
  ANA_CHECK (m_trigDecisionTool.setProperty ("ConfigTool", m_trigConfigTool.getHandle())); // connect the TrigDecisionTool to the ConfigTool
  ANA_CHECK (m_trigDecisionTool.setProperty ("TrigDecisionKey", "xTrigDecision"));
  ANA_CHECK (m_trigDecisionTool.initialize());

  ///////////////////////
  // truth classifier
  m_truthClassifier  = new MCTruthClassifier("TruthClassifier");
  ANA_CHECK(m_truthClassifier->initialize());

  ////////////////////////////////
  //cluster selector tool for rapidity gaps  
  m_clusterSelectionTool = new UPC::TopoClusterSelectionTool("TopoClusterSelectionTool");
  ANA_CHECK(m_clusterSelectionTool->setProperty("MinPt",200.0));
  ANA_CHECK(m_clusterSelectionTool->setProperty("ApplySignificanceCuts",false));
  ANA_CHECK(m_clusterSelectionTool->setProperty("CalibFile","HIGapSelector/TCSigCuts.root"));
  ANA_CHECK(m_clusterSelectionTool->setProperty("CalibHisto","h1_TC_sig_cuts"));
  ANA_CHECK(m_clusterSelectionTool->initialize());


  m_AnalysisTriggers.clear();
 // m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE10");
  
/*
  // 2015 low-mu run trigger list
  m_AnalysisTriggers.push_back("HLT_mb_sptrk");
  m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_1");
  m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1MBTS_1_1");
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10");

  // 2017 low-mu run trigger list
  //m_AnalysisTriggers.push_back("HLT_mb_sptrk");
  m_AnalysisTriggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10");
  m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20");
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30");
  m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40");
  m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50");

  // 2017 5 TeV run trigger list
  // no pileup suppression
  // mb trigger HLT_mb_sptrk
  m_AnalysisTriggers.push_back("HLT_mb_sptrk");
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE25");
  m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE20");
  m_AnalysisTriggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE15");
  m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE10");
  m_AnalysisTriggers.push_back("HLT_mb_sp600_trk40_hmt_L1TE5");
  // 2017 13 TeV HMTs
  m_AnalysisTriggers.push_back("HLT_mb_sptrk"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE10"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10");
  m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30"); 
  m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30");
  m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40");
  m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30");
  m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40");
  m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50");
  m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40");
  m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50");
*/

  // 2017 XeXe 5 TeV trigger list
  if (m_triggerSet == 3){
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1TE4");
    m_AnalysisTriggers.push_back("HLT_mb_sptrk_L1MBTS_1_VTE4");
  }
/*
  // 2018 PbPb PC and CC triggers
  m_AnalysisTriggers.push_back("HLT_mb_sptrk_L1ZDC_A_C_VTE50");
  m_AnalysisTriggers.push_back("HLT_noalg_pc_L1TE50_VTE600.0ETA49");
  m_AnalysisTriggers.push_back("HLT_noalg_cc_L1TE600.0ETA49");
*/
  if (m_triggerSet == 1){

    // Minbias trigger
    m_AnalysisTriggers.push_back("HLT_mb_sptrk");            
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_1");   
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_2");   
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_1_1"); 
    // HMT trigger
    m_AnalysisTriggers.push_back("HLT_mb_sp400_trk40_hmt_L1MBTS_1_1");
    m_AnalysisTriggers.push_back("HLT_mb_sp600_trk40_hmt_L1TE5");  
    m_AnalysisTriggers.push_back("HLT_mb_sp600_trk40_hmt_L1TE10");    
    m_AnalysisTriggers.push_back("HLT_mb_sp600_pusup300_trk40_hmt_L1TE5");   
    m_AnalysisTriggers.push_back("HLT_mb_sp600_pusup300_trk40_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp600_trk45_hmt_L1MBTS_1_1"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1MBTS_1_1");  
    m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE10");   
    m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk50_hmt_L1TE5");  
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10");  
    m_AnalysisTriggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE5");   
    m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk50_hmt_L1TE5");
    m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp700_trk55_hmt_L1MBTS_1_1");  
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1MBTS_1_1");  
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE5");   
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE10");  
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE20");   
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE10");
    m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE5");
    m_AnalysisTriggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp900_trk65_hmt_L1MBTS_1_1");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE5");
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE10");
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24");    
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10");
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5");
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1"); 
    m_AnalysisTriggers.push_back("HLT_mb_sptrk_trk80_L1MBTS_2");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE10");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE15");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk90_hmt_L1TE5");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25");
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE15");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE20");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE30");    
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE40");    
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE5");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk100_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk100_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE10");
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE15"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE20");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE25");
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE40"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE5");          
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE50");      
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15");
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50");
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE50");         
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE60");
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24");          
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE15"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE20");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE25");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE30");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10");
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE10");   
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE40");       
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE50");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE60");
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE70"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE10");    
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE15");    
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE20");    
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE25"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE30");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60");
    m_AnalysisTriggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE20");   
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE40");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE50"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE60"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE70");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE15"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE20");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE25");   
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE30");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60");
    m_AnalysisTriggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE20"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE30");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE40");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE50");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE60");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE70"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2500_trk140_hmt_L1TE40");  
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60");
    m_AnalysisTriggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10");
    m_AnalysisTriggers.push_back("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE30");   
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE50");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE60");    
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE70"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2700_trk150_hmt_L1TE40"); 
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60");
    m_AnalysisTriggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70");
    m_AnalysisTriggers.push_back("HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24");
    m_AnalysisTriggers.push_back("HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40");       
    m_AnalysisTriggers.push_back("HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40");
    m_AnalysisTriggers.push_back("HLT_mb_sp2900_trk160_hmt_L1TE40");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1500_hmtperf_L1TE10");  
    m_AnalysisTriggers.push_back("HLT_mb_sp1800_hmtperf_L1TE5");  
    m_AnalysisTriggers.push_back("HLT_mb_sptrk_pt2_L1MBTS_2");  
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1TE5");  
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1TE10");  
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1TE20");  
  }  


  if (m_triggerSet == 2){

    // Minbias trigger
    m_AnalysisTriggers.push_back("HLT_mb_sptrk");
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_1");
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_2");
    m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_1_1");
  }


  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  bool isMC = false;
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true;
  }

  std::set<float> etavals;

  // GRL
  if (!isMC) {
    if (!m_grl->passRunLB(*eventInfo)) {
      ANA_MSG_DEBUG ("drop event: GRL");
      return StatusCode::SUCCESS;
    }
  }

  ClearVector();

  /////////////////////////////////////////////////////////////////
  // Trigger
  /////////////////////////////////////////////////////////////////
  ANA_MSG_DEBUG("-------------------");
  bool _passTrig = false;
  char name[200];
  for (unsigned int anaTrig = 0; anaTrig < m_AnalysisTriggers.size(); ++anaTrig) {
     auto chainGroup = m_trigDecisionTool->getChainGroup(m_AnalysisTriggers.at(anaTrig));
     for (auto &trig : chainGroup->getListOfTriggers()) {
        auto cg = m_trigDecisionTool->getChainGroup(trig);
        if (cg->isPassed()){
           ANA_MSG_DEBUG(trig);
           TString TrigName(trig);
           m_TriggerObject_chain.push_back(TrigName);
           m_TriggerObject_ps.push_back(cg->getPrescale());
           _passTrig = true;
        }
     }
  }

  if (!_passTrig && !isMC) {
    ANA_MSG_DEBUG ("no trigger fired");
    return StatusCode::SUCCESS;
  }


  const unsigned int triggerbits = m_trigDecisionTool->isPassedBits("L1_MBTS_1");
  m_MBTS_1 = triggerbits&TrigDefs::L1_isPassedBeforePrescale;

/*
   for (unsigned int anaTrig = 0; anaTrig < m_AnalysisTriggers.size(); ++anaTrig) {
     sprintf(name,"%s",m_AnalysisTriggers.at(anaTrig).c_str());
     //const unsigned int triggerbits = m_trigDecisionTool->isPassed(m_AnalysisTriggers.at(anaTrig));
     //bool passL1 = triggerbits & TrigDefs::L1_isPassedBeforePrescale;
     bool passL1 = m_trigDecisionTool->getChainGroup(name)->isPassedBits() & TrigDefs::L1_isPassedBeforePrescale;   
     bool passRaw = m_trigDecisionTool->getChainGroup(name)->isPassed(TrigDefs::allowResurrectedDecision);   
    // if (passL1) ANA_MSG_DEBUG("pass L1 before ps " <<m_AnalysisTriggers.at(anaTrig) );
     //if (passRaw) ANA_MSG_DEBUG("passed Raw " <<m_AnalysisTriggers.at(anaTrig) );
   }
*/
   bool m_rel21 = true;
   m_L1_energyT = -1;
   // L1 TE
   if (m_rel21){
   const xAOD::EnergySumRoI* m_lvl1EnergySumRoI = 0;
   ANA_CHECK(evtStore()->retrieve( m_lvl1EnergySumRoI,"LVL1EnergySumRoI"));
   m_L1_energyT = m_lvl1EnergySumRoI->energyT(); 
   }
  ////////////////////////////////////////////
  //HIEventShape
  ////////////////////////////////////////////
  b_fcalet_A      = 0;
  b_fcalet_C      = 0;
  if (m_getFcal_sumEt){
    const xAOD::HIEventShapeContainer *hiue = 0;
    ANA_CHECK(evtStore()->retrieve( hiue, "HIEventShape"));
  
    xAOD::HIEventShapeContainer::const_iterator hiue_itr = hiue->begin();
    xAOD::HIEventShapeContainer::const_iterator hiue_end = hiue->end();
  
    for ( ; hiue_itr != hiue_end ; hiue_itr++) {
      double et = (*hiue_itr)->et() * 1e-3;
      int layer = (*hiue_itr)->layer();
      double eta = (*hiue_itr)->etaMin();
  
      if (layer == 21 || layer == 22 || layer == 23) {
        if (eta > 0) {
          b_fcalet_A += et;
        }
        else {
          b_fcalet_C += et;
        }
      }
    }
  }

  float sumFCalEt = b_fcalet_C + b_fcalet_A;
  bool tooBig = sumFCalEt > 1000;

  ////////////////////////////////////////////////////////////
  // Vertex
  ////////////////////////////////////////////////////////////////
  const xAOD::VertexContainer* primaryVertexCollection = 0;
  const xAOD::Vertex* pvtx = 0;
  bool _findPV = false;
  m_nVtx = 0;
  ANA_CHECK( evtStore()->retrieve(primaryVertexCollection,"PrimaryVertices") );
  for (auto vertex: *primaryVertexCollection) {
    m_nVtx++;
    if (vertex->vertexType() == xAOD::VxType::PriVtx && !_findPV) {
      pvtx = vertex;
      m_PV_x = vertex->x();
      m_PV_y = vertex->y();
      m_PV_z = vertex->z();
      //ANA_MSG_DEBUG(" pri vtx type =" << vertex->vertexType());
      _findPV = true;
    }

    float _sumPt = 0;
    float _sumPt2 = 0;
    for (size_t itrk=0; itrk < vertex->nTrackParticles(); itrk++) {
      const xAOD::TrackParticle* trk = vertex->trackParticle(itrk);
      _sumPt +=  trk->pt();
      _sumPt2 += trk->pt()*trk->pt();
    }

    m_Vertex_nTrk.push_back(vertex->nTrackParticles());
    m_Vertex_sumPt.push_back(_sumPt);
    m_Vertex_sumPt2.push_back(_sumPt2);
    m_Vertex_type.push_back(vertex->vertexType());
    m_Vertex_x.push_back(vertex->x());
    m_Vertex_y.push_back(vertex->y());
    m_Vertex_z.push_back(vertex->z());

  }
  if (pvtx == 0) {
    ANA_MSG_DEBUG("No primary vertex");
    return StatusCode::SUCCESS;
  }
  //ANA_MSG_DEBUG("#vtx=" << m_Vertex_z.size());

  m_RunNumber   = eventInfo->runNumber();
  m_EventNumber = eventInfo->eventNumber();
  m_LumiBlock   = eventInfo->lumiBlock();
  m_ActMu   = eventInfo->actualInteractionsPerCrossing(); // per a lumi block average of mu for specific BCID
  m_AvgMu   = eventInfo->averageInteractionsPerCrossing(); // per a lumi block average AND averaged over BCID


  /////////////////////////////////////////////////////////
  // tracks 
  //////////////////////////////////////////////////////////
  m_Ntrk_MB = 0;
  std::pair<unsigned int, unsigned int> res;
  const xAOD::TrackParticleContainer* tracks = 0;

  ANA_CHECK( evtStore()->retrieve( tracks, "InDetTrackParticles" ) );
  for (const auto* track : *tracks) {

    if ( !m_InDetTrackSelectionTool  ->accept(*track, pvtx) &&
         !m_InDetTrackSelectionTool_2->accept(*track, pvtx) ) continue;
    if (!tooBig) etavals.insert(track->eta());
    if (m_InDetTrackSelectionTool  ->accept(*track, pvtx) && track->pt() > 400.0) m_Ntrk_MB++; 
    //if (track->pt() <= 400.0) continue;
    if (std::fabs(track->eta()) > 2.5) continue;
    

    m_Track_pt. push_back(track->pt()/1e3 );
    m_Track_eta.push_back(track->eta());
    m_Track_phi.push_back(track->phi());
    m_Track_quality1.push_back(m_InDetTrackSelectionTool  ->accept(*track, pvtx));
    m_Track_quality2.push_back(m_InDetTrackSelectionTool_2->accept(*track, pvtx));

    //ANA_MSG_DEBUG("track Delta z0 " << track->z0() );
    //ANA_MSG_DEBUG("track theta " << track->theta() );

    float _d0sig = xAOD::TrackingHelpers::d0significance( track, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
    float _z0sinTheta = TMath::Sin(track->theta())*fabs(track->z0() + track->vz() - pvtx->z());
    m_Track_d0Signif.  push_back(_d0sig);
    m_Track_z0SinTheta.push_back(_z0sinTheta);

    if (track->isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink")) {
      ElementLink<xAOD::TruthParticleContainer> link = track->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
      if (link.isValid()) {
        m_Track_truthMatchingProb.push_back(track->auxdataConst<float>("truthMatchProbability"));
        
        const xAOD::TruthParticle* thePart = xAOD::TruthHelpers::getTruthParticle( *(track) );
        if (thePart){
          m_Track_truthPt  .push_back(thePart->pt()/1000.);
          m_Track_truthEta .push_back(thePart->eta());
          m_Track_truthPhi .push_back(thePart->phi());

          res = m_truthClassifier->particleTruthClassifier( thePart );
          m_Track_truthType    .push_back( short(res.first));
          m_Track_truthOrig    .push_back( short(res.second));
          m_Track_truthPdgID   .push_back( short(thePart->pdgId()));
          m_Track_truthBarcode .push_back( thePart->barcode()); 
        } else {
          m_Track_truthPt      .push_back(-999);
          m_Track_truthEta     .push_back(-999);
          m_Track_truthPhi     .push_back(-999);
          m_Track_truthType    .push_back(-999);
          m_Track_truthOrig    .push_back(-999);
          m_Track_truthPdgID   .push_back(-999);
          m_Track_truthz       .push_back(-999);
          m_Track_truthnIn     .push_back(-999);
          m_Track_truthBarcode .push_back(-999);
        }
      }else{
        m_Track_truthMatchingProb.push_back(0);
  
      } // if link is valid
    } // if element link exist
  }// track loop

  //ANA_MSG_DEBUG("m_EventNumber " << m_EventNumber << " Nch " << m_Ntrk_MB);




  /////////////////////////////////////////////
  // clusters
  //////////////////////////////////////////// 
  m_N_barrelClus = 0;
  if((m_getGaps || m_getClusters) && !tooBig){
    const xAOD::CaloClusterContainer* CaloClusterCollection = 0;
    ANA_CHECK( evtStore()->retrieve(CaloClusterCollection,"CaloCalTopoClusters") );
    for (auto cluster: *CaloClusterCollection) {

      if(m_clusterSelectionTool->accept(*cluster)) etavals.insert(cluster->eta());      
      if (cluster->et()/1e3 < m_cluster_pt_low) continue;
      if (cluster->et()/1e3 > m_cluster_pt_high) continue;
      if (fabs(cluster->eta()) < 2.5) m_N_barrelClus++;
      if (fabs(cluster->eta()) < m_cluster_abs_eta_low) continue;


      m_Cluster_et.push_back(cluster->et()/1e3);
      m_Cluster_eta.push_back(cluster->eta());
      m_Cluster_phi.push_back(cluster->phi());
      //m_Cluster_size.push_back(cluster->clusterSize());
      //m_Cluster_status.push_back(cluster->signalState());
      //m_Cluster_e.push_back(cluster->rawE()/1e3);
      //m_Cluster_cellSigSamp.push_back(cluster->auxdata< float >("CELL_SIG_SAMPLING"));

    }
  } 

  ////////////////////////////////////////////////////////
  // Towers 
  //////////////////////////////////////////////////////////

  if(m_getTowers){
    const xAOD::CaloClusterContainer* ccl=0;
    if(evtStore()->retrieve(ccl,"HIClusters").isFailure()){
      ATH_MSG_ERROR("Could not retrieve input CaloClusterContainer HIClusters ");
      return 1;
    }

    m_b_ntower = 0;    
    float eScaleFactor = 1e-3;
    //ANA_MSG_DEBUG("--------------"); 
    for(xAOD::CaloClusterContainer::const_iterator itr=ccl->begin(); itr!=ccl->end(); itr++){
      const xAOD::CaloCluster* cl=*itr;
      if(fabs(cl->eta0()) < m_tower_abs_eta_low) continue;
      if(m_tower_ptCut && cl->pt()/1e3 < m_tower_pt_low) continue;
      if(m_tower_ptCut && cl->pt()/1e3 > m_tower_pt_high) continue;
      
      m_b_tower_pt.push_back(cl->pt()*eScaleFactor);
      //m_b_tower_eta.push_back(cl->eta0());
      //m_b_tower_phi.push_back(cl->phi0());
      /*
      m_b_tower_m.push_back(cl->m()*eScaleFactor);
      */
      //m_b_tower_E.push_back(cl->rawE()*eScaleFactor);
      ++m_b_ntower;
      std::cout << cl->phi0() << "," ;
      
    }
    //std::cout << std::endl;
  }

   ////////////////////////////////////////////////////////
   // Rapidity gaps
   ///////////////////////////////////////////////////////
   m_sumGapAC = -999;
   //ANA_MSG_DEBUG("sum gap " << UPC::getSumGap(etavals,0.5) << "  big? " << tooBig );
   if (!tooBig) m_sumGapAC = UPC::getSumGap(etavals,0.5);

  /////////////////////////////////////////////////////////
  // truth tracks
  /////////////////////////////////////////////////////////
  if (isMC){
    const xAOD::TruthParticleContainer* truthParticleCollection = 0;
    ANA_CHECK( evtStore()->retrieve(truthParticleCollection,"TruthParticles") );
    for (auto truthParticle: *truthParticleCollection) {
         if (truthParticle->status() != 1) continue;
         //if (!truthParticle->isCharged()) continue;
        // if (truthParticle->pt() < 300) continue;
         if (fabs(truthParticle->eta()) > 5) continue;

	 m_truth_pt .push_back(truthParticle->pt()/1000) ;
	 m_truth_eta.push_back(truthParticle->eta());
	 m_truth_phi.push_back(truthParticle->phi());
	 m_truth_pid.push_back(truthParticle->pdgId());
	 m_truth_barcode.push_back(truthParticle->barcode());
	 m_truth_isCharged.push_back(truthParticle->isCharged());
      }
   }

/*
  //------------------------------------------------------
  // Online Tracking
  //------------------------------------------------------
  std::vector<unsigned int> vtxNtrk;
  std::vector<float> vtxTrkPt;
  const xAOD::TrigVertexCountsContainer *ptrOnTrkCon = 0;
  ANA_CHECK(evtStore()->retrieve(ptrOnTrkCon,"HLT_xAOD__TrigVertexCountsContainer_vertexcounts"));
  for(const auto *ptrOnTrk : *ptrOnTrkCon)
    {
      vtxNtrk = ptrOnTrk->vtxNtrks();
      vtxTrkPt = ptrOnTrk->vtxTrkPtSqSum();
    }
  nTrkOnline = 0;
  float maxSumPt = 0;
  for(int iV=0; iV<vtxNtrk.size(); iV++)
    {
      if(vtxTrkPt.at(iV)>maxSumPt)
        {
          maxSumPt = vtxTrkPt.at(iV);
          nTrkOnline = vtxNtrk.at(iV);
        }
    }



  //------------------------------------------------------
  // Reconstructed Tracks
  //------------------------------------------------------
  nTrk=0;
  nTrk_raw=0;
  nTrk_eff=0;
  for(int i=0; i<nTrkMax; i++)
    {
      trkPt[i] = 251;
      trkEta[i] = 251;
      trkPhi[i] = 251;
      trkChg[i] = true;
      trkZ0[i] = 251;
      trkD0[i] = 251;
      trkPixHit[i] = 0;
      trkSctHit[i] = 0;
      trkTrtHit[i] = 0;
      trkVtx[i] = true;
      trkQual[i] = 0;
  }



  const double pTmin = 0.4;
  const xAOD::Vertex *ptrVtx = 0;
  const xAOD::TrackParticleContainer *ptrRecTrkCon = 0;
  ANA_CHECK(evtStore()->retrieve(ptrRecTrkCon,"InDetTrackParticles"));
  for(const auto* ptrRecTrk : *ptrRecTrkCon)
    {
      float zVtx = m_PV_z;
      ptrVtx = vertexParticle(ptrRecTrk);
      //if(!ptrVtx) trkVtx[nTrk] = false;
      //else if(fabs(ptrVtx->z()-zVtx)>0.0001) trkVtx[nTrk] = false;
      if(!ptrVtx) continue;
      else if(fabs(ptrVtx->z()-zVtx)>0.0001) continue;

      float pt,eta,phi,Z0,D0;
      UChar_t pix,sct,trt;
      UChar_t iblExp,iblHit,n2iblExp,n2iblHit;
      pt=ptrRecTrk->pt();
      eta=ptrRecTrk->eta();
      phi=ptrRecTrk->phi();
      Z0=sin(ptrRecTrk->theta())*(ptrRecTrk->z0()+ptrRecTrk->vz()-zVtx);
      D0=ptrRecTrk->d0();
      pix=ptrRecTrk->auxdata<unsigned char>("numberOfPixelHits");
      sct=ptrRecTrk->auxdata<unsigned char>("numberOfSCTHits");
      trt=ptrRecTrk->auxdata<unsigned char>("numberOfTRTHits");
      iblExp=ptrRecTrk->auxdata<unsigned char>("expectInnermostPixelLayerHit");
      iblHit=ptrRecTrk->auxdata<unsigned char>("numberOfInnermostPixelLayerHits");
      n2iblExp=ptrRecTrk->auxdata<unsigned char>("expectNextToInnermostPixelLayerHit");
      n2iblHit=ptrRecTrk->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerHits");

      //Main Track Cuts:
      //if(pix<1) continue;
      bool ibl_flag=false;
      if(iblExp>0 && iblHit>0) ibl_flag=true;
      else if(iblExp==0 && (n2iblExp>0 && n2iblHit>0)) ibl_flag=true;
      else if(iblExp==0 && n2iblExp==0) ibl_flag=true;
      else ibl_flag=false;
      if(ibl_flag==false) continue;


      trkPt[nTrk_raw] = cvtPt(pt);
      trkEta[nTrk_raw] = cvtEta(eta);
      trkPhi[nTrk_raw] = cvtPhi(phi);
      if(ptrRecTrk->charge()>0) trkChg[nTrk_raw] = true;
      else if(ptrRecTrk->charge()<0) trkChg[nTrk_raw] = false;
      trkZ0[nTrk_raw] = cvtZ0(Z0);
      trkD0[nTrk_raw] = cvtD0(D0);
      trkPixHit[nTrk_raw] = pix;
      trkSctHit[nTrk_raw] = sct;
      trkTrtHit[nTrk_raw] = trt;
      trkIblExp[nTrk_raw] = iblExp;
      trkIblHit[nTrk_raw] = iblHit;
      trkN2IblExp[nTrk_raw] = n2iblExp;
      trkN2IblHit[nTrk_raw] = n2iblHit;

      nTrk_raw++;

      if(pt/1000. < 0.3){
        if(sct < 2) continue;
      }
      else if(pt/1000. < 0.4){
        if(sct < 4) continue;
      }
      else{
        if(sct < 6) continue;
      }

      if(fabs(eta) > etaMax) continue;
      if(pt/1000. < pTmin) continue;

      if(fabs(D0) >= 1.5) continue;
      if(fabs(Z0) >= 1.5) continue;

      //Fill track histograms
   //   hTrkPt->Fill(pt/1000.);
   //   hTrkEta->Fill(eta);
   //   hTrkPhi->Fill(phi);
   //   hTrkZ0->Fill(Z0);
   //   hTrkD0->Fill(D0);
   //   hTrkPixHit->Fill(pix);
   //   hTrkSctHit->Fill(sct);
   //   hTrkTrtHit->Fill(trt);

      nTrk++;
   }



  ANA_MSG_DEBUG("online Ntrk " << nTrkOnline  << " Blair " << m_Ntrk_MB << " Arabinda " << nTrk);
*/

  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  ATH_MSG_INFO ("finalize() successful");

  return StatusCode::SUCCESS;
}


const xAOD::Vertex *MyxAODAnalysis::vertexParticle(const xAOD::TrackParticle *ptrTrk)
{
  typedef ElementLink<xAOD::VertexContainer> Link_t;
  static const char* NAME = "vertexLink";
  if( ! ptrTrk->isAvailable< Link_t >( NAME ) ) {
    return 0;
  }
  const Link_t& link = ptrTrk->auxdata< Link_t >( NAME );
  if( ! link.isValid() ) {
    return 0;
  }

  return *link;
}

int MyxAODAnalysis::cvtPt(double pT)
{
  int flagPt = int(pT/100.);
  if(flagPt<0) flagPt = 0;
  if(flagPt>=250) flagPt = 250;
  return flagPt;
}
int MyxAODAnalysis::cvtEta(double eta)
{
  double etaMax = 2.5;
  int flagEta = int((eta+etaMax)/2/etaMax*250);
  if(flagEta<0) flagEta = 0;
  if(flagEta>=250) flagEta = 250;
  return flagEta;
}
int MyxAODAnalysis::cvtPhi(double phi)
{
  int flagPhi = int((phi+TMath::Pi())/2/TMath::Pi()*250);
  if(flagPhi<0) flagPhi = 0;
  if(flagPhi>=250) flagPhi = 250;
  return flagPhi;
}
int MyxAODAnalysis::cvtZ0(double z0)
{
  int flagZ0 = int((z0+10)/20*200);
  if(flagZ0<0) flagZ0 = 0;
  if(flagZ0>=200) flagZ0 = 200;
  return flagZ0;
}
int MyxAODAnalysis::cvtD0(double d0)
{
  int flagD0 = int((d0+10)/20*200);
  if(flagD0<0) flagD0 = 0;
  if(flagD0>=200) flagD0 = 200;
  return flagD0;
}


