#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
// GRL
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/ToolHandle.h>
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "xAODTracking/VertexContainer.h"
// Truth tools
#include <MCTruthClassifier/MCTruthClassifier.h>
#include "HIGapSelector/GapCalculator.h"
#include "HIGapSelector/TopoClusterSelectionTool.h"

#include <TTree.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

/*
namespace InDet {
    class IInDetTrackSelectionTool;
}
*/

class MyxAODAnalysis : public EL::AnaAlgorithm
{


public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  virtual void ClearVector();
  virtual const xAOD::Vertex* vertexParticle(const xAOD::TrackParticle *ptrTrk);
  virtual int cvtD0(double);
  virtual int cvtZ0(double);
  virtual int cvtPhi(double);
  virtual int cvtEta(double);
  virtual int cvtPt(double);

  ToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  ToolHandle<InDet::IInDetTrackSelectionTool> m_InDetTrackSelectionTool; //!
  ToolHandle<InDet::IInDetTrackSelectionTool> m_InDetTrackSelectionTool_2; //!

  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool; //!
  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; //!

  // Muon Tools 
  asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection; //!
  asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool; //!
  asg::AnaToolHandle<Trig::IMatchingTool> m_matchTool; //!

  // cluster selector tool
  UPC::TopoClusterSelectionTool * m_clusterSelectionTool; //!

private:
  // Configuration, and any other types of variables go here.
  //TTree* m_tree; //!
  //TFile* outputFile; //!
  const float etaMax = 2.5;
  int    m_RunNumber; //!
  Long64_t    m_EventNumber; //!
  int    m_LumiBlock; //!
  float  m_AvgMu; //!
  float  m_ActMu; //!
  float  m_PV_z; //!
  float  m_PV_x; //!
  float  m_PV_y; //!
  int    m_nVtx; //!
  bool   m_MBTS_1; //!
  int    m_Ntrk_MB; //!
  float b_fcalet_A;//!
  float b_fcalet_C;//!
  int m_N_barrelClus;//!
  int m_L1_energyT;//! 
  float m_sumGapAC;//!

  std::vector<float> m_Track_pt; //!
  std::vector<float> m_Track_eta; //!
  std::vector<float> m_Track_phi; //!
  std::vector<float> m_Track_d0Signif; //!
  std::vector<float> m_Track_z0SinTheta; //!
  std::vector<float> m_Track_truthMatchingProb; //!
  std::vector<bool> m_Track_quality1; //!
  std::vector<bool> m_Track_quality2; //!

  std::vector<float> m_Track_truthPt      ;//!
  std::vector<float> m_Track_truthEta     ;//!
  std::vector<float> m_Track_truthPhi     ;//!
  std::vector<float> m_Track_truthType    ;//!
  std::vector<float> m_Track_truthOrig    ;//!
  std::vector<float> m_Track_truthPdgID   ;//!
  std::vector<float> m_Track_truthz       ;//!
  std::vector<float> m_Track_truthnIn     ;//!
  std::vector<float> m_Track_truthBarcode ;//!


  std::vector<float> m_truth_pt;
  std::vector<float> m_truth_eta;
  std::vector<float> m_truth_phi;
  std::vector<int> m_truth_pid;
  std::vector<int> m_truth_barcode;
  std::vector<bool> m_truth_isCharged;

  std::vector<int>   m_Vertex_nTrk; //!
  std::vector<float> m_Vertex_sumPt;//!
  std::vector<float> m_Vertex_sumPt2;//!
  std::vector<int>   m_Vertex_type; //!
  std::vector<float> m_Vertex_x; //!
  std::vector<float> m_Vertex_y; //!
  std::vector<float> m_Vertex_z; //!

  std::vector<float> m_Cluster_et; //!
  std::vector<float> m_Cluster_eta; //!
  std::vector<float> m_Cluster_phi; //!
  std::vector<float> m_Cluster_e; //!
  std::vector<float> m_Cluster_size; //!
  std::vector<int>   m_Cluster_status; //!
  std::vector<int>   m_Cluster_cellSigSamp; //!

  // Arabinda tracking vars
  static const int nTrkMax=1000;

UInt_t  nTrk;
UInt_t  nTrk_raw;


UChar_t trkPt[nTrkMax];
UChar_t trkEta[nTrkMax];
bool    trkChg[nTrkMax];
UChar_t trkPhi[nTrkMax];
UChar_t trkZ0[nTrkMax];
UChar_t trkD0[nTrkMax];

UChar_t trkPixHit[nTrkMax];
UChar_t trkSctHit[nTrkMax];
UChar_t trkTrtHit[nTrkMax];
bool    trkVtx[nTrkMax];
UChar_t trkQual[nTrkMax];
//New
Float_t trkChi2[nTrkMax];
UChar_t trkIblExp[nTrkMax];
UChar_t trkIblHit[nTrkMax];
UChar_t trkN2IblExp[nTrkMax];
UChar_t trkN2IblHit[nTrkMax];
UInt_t  nTrk_eff;
UInt_t  bcID; 


  // truth particles
  MCTruthClassifier* m_truthClassifier; //!

  int nTrkOnline;//!
  bool m_getTowers=0;//!
  bool m_getClusters=1;//!
  bool m_getTracks=1;//!
  bool m_getTruth=0;//!
  bool m_getFcal_sumEt=1;//!
  bool m_getGaps=0;//!
  int  m_triggerSet=0;//!
  float m_cluster_pt_low;//!
  float m_cluster_pt_high;//!
  float m_cluster_abs_eta_low;//!
  float m_tower_pt_low = -10000000;//!
  float m_tower_pt_high = 1000000;//!
  float m_tower_abs_eta_low = 4.0;//!
  bool  m_tower_ptCut = 1;//!

  int m_b_ntower;//!
  std::vector<float> m_b_tower_pt;//!
  std::vector<float> m_b_tower_eta;//!
  std::vector<float> m_b_tower_phi;//!
  std::vector<float> m_b_tower_m;//!
  std::vector<float> m_b_tower_E;//!

  std::vector< std::string > m_AnalysisTriggers; //!

  std::vector<TString> m_TriggerObject_chain; //!
  std::vector<float>   m_TriggerObject_ps; //!
  std::vector<TString> m_TriggerObject_beforePS; //!

  std::string m_OutputFileName;
};

#endif
