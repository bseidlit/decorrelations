//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Feb 23 13:45:33 2020 by ROOT version 6.18/04
// from TTree analysis/My analysis ntuple
// found on file: ../RacfFile/user.qhu.20651067._000013.ANALYSIS.root
//////////////////////////////////////////////////////////

#ifndef CorrelationTrackCluster_h
#define CorrelationTrackCluster_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class CorrelationTrackCluster {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   int syst = 0;

// Fixed size dimensions of array or collections stored in the TTree if any.

   vector<string>  Triggers;
   vector<int>     runList = {267358,267359,267360,267367,267385,267599,277025,277081,329542,330857,330875,331020,341294,341312,341419,341534,341615,341649};

   // Declaration of leaf types
   Int_t           RunNumber;
   Long64_t        EventNumber;
   Int_t           LumiBlock;
   Float_t         AvgMu;
   Float_t         ActMu;
   Float_t         PV_x;
   Float_t         PV_y;
   Float_t         PV_z;
   Int_t           nVtx;
   Bool_t          L1_MBTS_1;
   Int_t           Ntrk_MB;
   Int_t           N_barrelClus;
   vector<TString> *TriggerObject_Chain;
   vector<float>   *TriggerObject_Ps;
   vector<int>     *Vertex_nTrk;
   vector<float>   *Vertex_sumPt;
   vector<float>   *Vertex_sumPt2;
   vector<int>     *Vertex_type;
   vector<float>   *Vertex_x;
   vector<float>   *Vertex_y;
   vector<float>   *Vertex_z;
   vector<float>   *Track_pt;
   vector<float>   *Track_eta;
   vector<float>   *Track_phi;
   //vector<float>   *Track_eff;
   vector<float>   *Track_d0Signif;
   vector<float>   *Track_z0SinTheta;
   vector<float>   *Cluster_et;
   vector<float>   *Cluster_eta;
   vector<float>   *Cluster_phi;
   vector<float>   *Cluster_size;
   vector<int>     *Cluster_status;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_LumiBlock;   //!
   TBranch        *b_AvgMu;   //!
   TBranch        *b_ActMu;   //!
   TBranch        *b_PV_x;   //!
   TBranch        *b_PV_y;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_L1_MBTS_1;   //!
   TBranch        *b_Ntrk_MB;   //!
   TBranch        *b_TriggerObject_Chain;   //!
   TBranch        *b_TriggerObject_Ps;   //!
   TBranch        *b_Vertex_nTrk;   //!
   TBranch        *b_Vertex_sumPt;   //!
   TBranch        *b_Vertex_sumPt2;   //!
   TBranch        *b_Vertex_type;   //!
   TBranch        *b_Vertex_x;   //!
   TBranch        *b_Vertex_y;   //!
   TBranch        *b_Vertex_z;   //!
   TBranch        *b_Track_pt;   //!
   TBranch        *b_Track_eta;   //!
   TBranch        *b_Track_phi;   //!
   TBranch        *b_Track_eff;   //!
   TBranch        *b_Track_d0Signif;   //!
   TBranch        *b_Track_z0SinTheta;   //!
   TBranch        *b_Cluster_et;   //!
   TBranch        *b_Cluster_eta;   //!
   TBranch        *b_Cluster_phi;   //!
   TBranch        *b_Cluster_size;   //!
   TBranch        *b_Cluster_status;   //!
   TBranch        *b_N_barrelClus;   //!

   CorrelationTrackCluster(TTree *tree=0);
   virtual ~CorrelationTrackCluster();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual float    trkEff(float,float);
   virtual bool     trigReq(TString,int);
   virtual int      TrigToInt(TString);
   virtual int      runToInt(int);

   TH2F *h2_trkEff;
};

#endif

#ifdef CorrelationTrackCluster_cxx
CorrelationTrackCluster::CorrelationTrackCluster(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
/*   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../input/user.qhu.20651067._000002.ANALYSIS.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../../input/user.qhu.20651067._000002.ANALYSIS.root");
      }
      f->GetObject("analysis",tree);

   } */
   Init(tree);
}

CorrelationTrackCluster::~CorrelationTrackCluster()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t CorrelationTrackCluster::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t CorrelationTrackCluster::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void CorrelationTrackCluster::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   TriggerObject_Chain = 0;
   TriggerObject_Ps = 0;
   Vertex_nTrk = 0;
   Vertex_sumPt = 0;
   Vertex_sumPt2 = 0;
   Vertex_type = 0;
   Vertex_x = 0;
   Vertex_y = 0;
   Vertex_z = 0;
   Track_pt = 0;
   Track_eta = 0;
   Track_phi = 0;
   //Track_eff = 0;
   Track_d0Signif = 0;
   Track_z0SinTheta = 0;
   Cluster_et = 0;
   Cluster_eta = 0;
   Cluster_phi = 0;
   Cluster_size = 0;
   Cluster_status = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("LumiBlock", &LumiBlock, &b_LumiBlock);
   fChain->SetBranchAddress("AvgMu", &AvgMu, &b_AvgMu);
   fChain->SetBranchAddress("ActMu", &ActMu, &b_ActMu);
   fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
   fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("L1_MBTS_1", &L1_MBTS_1, &b_L1_MBTS_1);
   fChain->SetBranchAddress("Ntrk_MB", &Ntrk_MB, &b_Ntrk_MB);
   fChain->SetBranchAddress("TriggerObject_Chain", &TriggerObject_Chain, &b_TriggerObject_Chain);
   fChain->SetBranchAddress("TriggerObject_Ps", &TriggerObject_Ps, &b_TriggerObject_Ps);
   fChain->SetBranchAddress("Vertex_nTrk", &Vertex_nTrk, &b_Vertex_nTrk);
   fChain->SetBranchAddress("Vertex_sumPt", &Vertex_sumPt, &b_Vertex_sumPt);
   fChain->SetBranchAddress("Vertex_sumPt2", &Vertex_sumPt2, &b_Vertex_sumPt2);
   fChain->SetBranchAddress("Vertex_type", &Vertex_type, &b_Vertex_type);
   fChain->SetBranchAddress("Vertex_x", &Vertex_x, &b_Vertex_x);
   fChain->SetBranchAddress("Vertex_y", &Vertex_y, &b_Vertex_y);
   fChain->SetBranchAddress("Vertex_z", &Vertex_z, &b_Vertex_z);
   fChain->SetBranchAddress("Track_pt", &Track_pt, &b_Track_pt);
   fChain->SetBranchAddress("Track_eta", &Track_eta, &b_Track_eta);
   fChain->SetBranchAddress("Track_phi", &Track_phi, &b_Track_phi);
   //fChain->SetBranchAddress("Track_eff", &Track_eff, &b_Track_eff);
   fChain->SetBranchAddress("Track_d0Signif", &Track_d0Signif, &b_Track_d0Signif);
   fChain->SetBranchAddress("Track_z0SinTheta", &Track_z0SinTheta, &b_Track_z0SinTheta);
   fChain->SetBranchAddress("Cluster_et", &Cluster_et, &b_Cluster_et);
   fChain->SetBranchAddress("Cluster_eta", &Cluster_eta, &b_Cluster_eta);
   fChain->SetBranchAddress("Cluster_phi", &Cluster_phi, &b_Cluster_phi);
   fChain->SetBranchAddress("Cluster_size", &Cluster_size, &b_Cluster_size);
   fChain->SetBranchAddress("Cluster_status", &Cluster_status, &b_Cluster_status);
   fChain->SetBranchAddress("N_barrelClus", &N_barrelClus, &b_N_barrelClus);

   TFile* f_trkEff = new TFile("trkEff_nominal.root");
   h2_trkEff = (TH2F*) f_trkEff->Get("eff_eta_pt");


    Triggers.push_back("HLT_mb_sptrk");            
    Triggers.push_back("HLT_noalg_mb_L1MBTS_1");   
    Triggers.push_back("HLT_noalg_mb_L1MBTS_2");   
    Triggers.push_back("HLT_noalg_mb_L1MBTS_1_1"); 
    Triggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24");
    Triggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5");  
    Triggers.push_back("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1"); 
    Triggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24");    
    Triggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE5");
    Triggers.push_back("HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24");
    Triggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10");
    Triggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5");
    Triggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE10");
    Triggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10");  
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24");  
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24");  
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20");  
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp1200_trk100_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1"); 
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE10");  
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE15");  
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp1200_trk90_hmt_L1TE5");   
    Triggers.push_back("HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24");  
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24");  
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15"); 
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24");  
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24");  
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40");  
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5");  
    Triggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp1400_trk100_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24");  
    Triggers.push_back("HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25"); 
    Triggers.push_back("HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1");  
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10");   
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24");  
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25");
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE15");  
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24");   
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE20");  
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24");   
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE30");    
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE40");    
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE5");   
    Triggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15");
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50");
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE10");
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE15"); 
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE20");  
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE25");
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24");  
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE40"); 
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE5");          
    Triggers.push_back("HLT_mb_sp1600_trk100_hmt_L1TE50");      
    Triggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10");
    Triggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50"); 
    Triggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60"); 
    Triggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE50");         
    Triggers.push_back("HLT_mb_sp1700_trk110_hmt_L1TE60");
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24");          
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE15"); 
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE20");  
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24");  
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE25");  
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE30");   
    Triggers.push_back("HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24"); 
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50"); 
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60");
    Triggers.push_back("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70");
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE10");   
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE40");       
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE50");  
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE60");
    Triggers.push_back("HLT_mb_sp1900_trk120_hmt_L1TE70"); 
    Triggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50"); 
    Triggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60");
    Triggers.push_back("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15");
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25");
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE10");    
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE15");    
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24");
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE20");    
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE25"); 
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE30");  
    Triggers.push_back("HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24");  
    Triggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE20");   
    Triggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE30"); 
    Triggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE40");  
    Triggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE50"); 
    Triggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE60"); 
    Triggers.push_back("HLT_mb_sp2100_trk130_hmt_L1TE70");
    Triggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50");
    Triggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60");
    Triggers.push_back("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70"); 
    Triggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE30");  
    Triggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE40");  
    Triggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE50");
    Triggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE60");  
    Triggers.push_back("HLT_mb_sp2200_trk140_hmt_L1TE70"); 
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24");
    Triggers.push_back("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5");
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE15"); 
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE20");  
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24");  
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE25");   
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24");  
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE30");  
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24");
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24");  
    Triggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30");
    Triggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50");
    Triggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60");
    Triggers.push_back("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70");
    Triggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE20");
    Triggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE30");   
    Triggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE50");
    Triggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE60");    
    Triggers.push_back("HLT_mb_sp2400_trk150_hmt_L1TE70"); 
    Triggers.push_back("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10");
    Triggers.push_back("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24");
    Triggers.push_back("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24");
    Triggers.push_back("HLT_mb_sp2500_trk140_hmt_L1TE40");  
    Triggers.push_back("HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24");
    Triggers.push_back("HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40");       
    Triggers.push_back("HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24"); 
    Triggers.push_back("HLT_mb_sp2700_trk150_hmt_L1TE40"); 
    Triggers.push_back("HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40");
    Triggers.push_back("HLT_mb_sp2900_trk160_hmt_L1TE40");  
    Triggers.push_back("HLT_mb_sp400_trk40_hmt_L1MBTS_1_1");
    Triggers.push_back("HLT_mb_sp600_pusup300_trk40_hmt_L1TE10"); 
    Triggers.push_back("HLT_mb_sp600_pusup300_trk40_hmt_L1TE5");   
    Triggers.push_back("HLT_mb_sp600_trk40_hmt_L1TE10");    
    Triggers.push_back("HLT_mb_sp600_trk40_hmt_L1TE5");  
    Triggers.push_back("HLT_mb_sp600_trk45_hmt_L1MBTS_1_1"); 
    Triggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10");  
    Triggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE5");   
    Triggers.push_back("HLT_mb_sp700_trk50_hmt_L1MBTS_1_1");  
    Triggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE10");   
    Triggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE20"); 
    Triggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE5"); 
    Triggers.push_back("HLT_mb_sp700_trk55_hmt_L1MBTS_1_1");  
    Triggers.push_back("HLT_mb_sp900_pusup400_trk50_hmt_L1TE5");  
    Triggers.push_back("HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24");    
    Triggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE10");
    Triggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE20");  
    Triggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE5");
    Triggers.push_back("HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24");    
    Triggers.push_back("HLT_mb_sp900_trk50_hmt_L1TE5");  
    Triggers.push_back("HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24");   
    Triggers.push_back("HLT_mb_sp900_trk60_hmt_L1MBTS_1_1");  
    Triggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE10");  
    Triggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE20");   
    Triggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE5");   
    Triggers.push_back("HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24");  
    Triggers.push_back("HLT_mb_sp900_trk65_hmt_L1MBTS_1_1");  

   Notify();
}

float CorrelationTrackCluster::trkEff(float eta,float pt){
  int bin  = h2_trkEff->FindBin(eta,pt);
  float track_efficiency = h2_trkEff->GetBinContent(bin);
  return track_efficiency;
}

bool CorrelationTrackCluster::trigReq(TString trig,int Ntrk){
  
    if (trig.EqualTo("HLT_mb_sptrk") && Ntrk >= 0) return true;  
    if (trig.EqualTo("HLT_noalg_mb_L1MBTS_1") && Ntrk >= 0) return true;   
    //if (trig.EqualTo("HLT_noalg_mb_L1MBTS_2") && Ntrk >= 0) return true;   
    if (trig.EqualTo("HLT_noalg_mb_L1MBTS_1_1") && Ntrk >= 10) return true; 
    if (trig.EqualTo("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE10_0ETA24") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_pusup450_trk70_hmt_L1TE5_0ETA24") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_trk70_hmt_L1MBTS_1_1") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_trk70_hmt_L1TE10") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_trk70_hmt_L1TE10_0ETA24") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_trk70_hmt_L1TE5") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1000_trk70_hmt_L1TE5_0ETA24") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE10") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE30") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE5") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_trk70_hmt_L1TE10") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_trk70_hmt_L1TE20") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_trk70_hmt_L1TE30") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1100_trk70_hmt_L1TE5") && Ntrk >= 70) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk100_hmt_L1TE5_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE10_0ETA24") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE15_0ETA24") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE20") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE30") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk80_hmt_L1TE5_0ETA24") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1200_pusup500_trk90_hmt_L1TE5_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk100_hmt_L1TE5") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk100_hmt_L1TE5_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk75_hmt_L1MBTS_1_1") && Ntrk >= 75) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE10") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE10_0ETA24") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE15") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE15_0ETA24") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE20") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE30") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE5") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE5_0ETA24") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk90_hmt_L1TE5") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1200_trk90_hmt_L1TE5_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE10_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE15_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE20_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE40") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE5_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk100_hmt_L1TE20") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk100_hmt_L1TE20_0ETA25") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk80_hmt_L1MBTS_1_1") && Ntrk >= 80) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE10") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE10_0ETA25") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE15") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE15_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE20") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE20_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE30") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE40") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE5") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE5_0ETA24") && Ntrk >= 90) return true;
    if (trig.EqualTo("HLT_mb_sp1500_pusup700_trk100_hmt_L1TE20_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE10_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE15_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE20_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE25_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE30") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE5") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE50") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE10") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE10_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE15") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE15_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE20") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE20_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE25") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE25_0ETA24") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE30") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE40") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE5") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1600_trk100_hmt_L1TE50") && Ntrk >= 100) return true;
    if (trig.EqualTo("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE10") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE20") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE30") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE40") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE60") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_trk110_hmt_L1TE10") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_trk110_hmt_L1TE20") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_trk110_hmt_L1TE30") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_trk110_hmt_L1TE40") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_trk110_hmt_L1TE50") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1700_trk110_hmt_L1TE60") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE10_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE15_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE20_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE25_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_pusup700_trk110_hmt_L1TE30_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE10") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE10_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE15") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE15_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE20") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE20_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE25") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE25_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE30") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1800_trk110_hmt_L1TE30_0ETA24") && Ntrk >= 110) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE10") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE20") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE30") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE40") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE50") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE60") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_pusup700_trk120_hmt_L1TE70") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE10") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE20") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE30") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE40") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE50") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE60") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp1900_trk120_hmt_L1TE70") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE20") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE30") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE40") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE50") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE60") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup750_trk130_hmt_L1TE70") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE10_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE15_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE20_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE25_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE30_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_pusup900_trk120_hmt_L1TE5") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE10") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE10_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE15") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE15_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE20") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE20_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE25") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE25_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE30") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk120_hmt_L1TE30_0ETA24") && Ntrk >= 120) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk130_hmt_L1TE20") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk130_hmt_L1TE30") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk130_hmt_L1TE40") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk130_hmt_L1TE50") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk130_hmt_L1TE60") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2100_trk130_hmt_L1TE70") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE20") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE30") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE40") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE50") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE60") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_pusup800_trk140_hmt_L1TE70") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_trk140_hmt_L1TE20") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_trk140_hmt_L1TE30") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_trk140_hmt_L1TE40") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_trk140_hmt_L1TE50") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_trk140_hmt_L1TE60") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2200_trk140_hmt_L1TE70") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE15_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE20_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE25_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE30_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE40_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_pusup1000_trk130_hmt_L1TE5") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE15") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE15_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE20") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE20_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE25") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE25_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE30") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE30_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE40") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2300_trk130_hmt_L1TE40_0ETA24") && Ntrk >= 130) return true;
    if (trig.EqualTo("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE20") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE30") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE40") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE50") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE60") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_pusup850_trk150_hmt_L1TE70") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_trk150_hmt_L1TE20") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_trk150_hmt_L1TE30") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_trk150_hmt_L1TE40") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_trk150_hmt_L1TE50") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_trk150_hmt_L1TE60") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2400_trk150_hmt_L1TE70") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE10") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE20_0ETA24") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2500_pusup1100_trk140_hmt_L1TE40") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2500_trk140_hmt_L1TE20_0ETA24") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2500_trk140_hmt_L1TE40") && Ntrk >= 140) return true;
    if (trig.EqualTo("HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE20_0ETA24") && Ntrk >= 150) return true;
    if (trig.EqualTo("HLT_mb_sp2700_pusup1200_trk150_hmt_L1TE40") && Ntrk >= 150) return true;  
    if (trig.EqualTo("HLT_mb_sp2700_trk150_hmt_L1TE20_0ETA24") && Ntrk >= 150) return true;  
    if (trig.EqualTo("HLT_mb_sp2700_trk150_hmt_L1TE40") && Ntrk >= 150) return true;  
    if (trig.EqualTo("HLT_mb_sp2900_pusup1300_trk160_hmt_L1TE40") && Ntrk >= 160) return true;  
    if (trig.EqualTo("HLT_mb_sp2900_trk160_hmt_L1TE40") && Ntrk >= 160) return true;  
    if (trig.EqualTo("HLT_mb_sp400_trk40_hmt_L1MBTS_1_1") && Ntrk >= 40) return true;  
    if (trig.EqualTo("HLT_mb_sp600_pusup300_trk40_hmt_L1TE10") && Ntrk >= 40) return true;  
    if (trig.EqualTo("HLT_mb_sp600_pusup300_trk40_hmt_L1TE5") && Ntrk >= 40) return true;  
    if (trig.EqualTo("HLT_mb_sp600_trk40_hmt_L1TE10") && Ntrk >= 40) return true;  
    if (trig.EqualTo("HLT_mb_sp600_trk40_hmt_L1TE5") && Ntrk >= 40) return true;  
    if (trig.EqualTo("HLT_mb_sp600_trk45_hmt_L1MBTS_1_1") && Ntrk >= 45) return true;  
    if (trig.EqualTo("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10") && Ntrk >= 50) return true;  
    if (trig.EqualTo("HLT_mb_sp700_pusup350_trk50_hmt_L1TE20") && Ntrk >= 50) return true;  
    if (trig.EqualTo("HLT_mb_sp700_pusup350_trk50_hmt_L1TE5") && Ntrk >= 50) return true;  
    if (trig.EqualTo("HLT_mb_sp700_trk50_hmt_L1MBTS_1_1") && Ntrk >= 50) return true;  
    if (trig.EqualTo("HLT_mb_sp700_trk50_hmt_L1TE10") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp700_trk50_hmt_L1TE20") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp700_trk50_hmt_L1TE5") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp700_trk55_hmt_L1MBTS_1_1") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp900_pusup400_trk50_hmt_L1TE5") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp900_pusup400_trk50_hmt_L1TE5_0ETA24") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp900_pusup400_trk60_hmt_L1TE10") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_pusup400_trk60_hmt_L1TE20") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_pusup400_trk60_hmt_L1TE5") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_pusup400_trk60_hmt_L1TE5_0ETA24") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk50_hmt_L1TE5") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk50_hmt_L1TE5_0ETA24") && Ntrk >= 50) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk60_hmt_L1MBTS_1_1") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk60_hmt_L1TE10") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk60_hmt_L1TE20") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk60_hmt_L1TE5") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk60_hmt_L1TE5_0ETA24") && Ntrk >= 60) return true;
    if (trig.EqualTo("HLT_mb_sp900_trk65_hmt_L1MBTS_1_1") && Ntrk >= 65) return true;

    return false;
  }
  
int CorrelationTrackCluster::TrigToInt(TString trig){
   int it=0;
   for (; it<Triggers.size(); it++)
     if (trig.EqualTo(Triggers.at(it))) return it;
   
   it++;
   return it;
}

int CorrelationTrackCluster::runToInt(int run){
   int ir=0;
   for (; ir<runList.size(); ir++)
     if (run == runList[ir]) return ir;

   ir++;
   return ir;
}


Bool_t CorrelationTrackCluster::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void CorrelationTrackCluster::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t CorrelationTrackCluster::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef CorrelationTrackCluster_cxx
