#define CorrelationTrackCluster_cxx
#include "CorrelationTrackCluster.h"
#include <TH2.h>
#include <TH3.h>
#include <TStyle.h>
#include <TCanvas.h>

// trigger particle: tracks of eta selection -2.5 to 2.5
// associated particle: clusters of eta selection 4.0 to 4.9

void CorrelationTrackCluster::Loop() {
    if (fChain == 0) return;
    
    std::cout.imbue(std::locale(""));



    // Basic setting
    const float PVzCut = 100.;
    const unsigned int NchCut = 200;
    unsigned int Nch_interval = 10;
    float ActMuCut_low  = 0.0;// 0.54 split
    float ActMuCut_high = 0.75;//2.5 max  0.452 split
    float cluster_pt_low  = 0.5;
    const float cluster_pt_high = 5.0;
    const float cluster_eta_low = 4.0;
    const int poolSize = 20; // pool size
    float dPVz_interval = 5;// default 20mm

    // default
    bool use_trkEff = 1;
    bool reCalcNtrk = 1;
    bool req_Vtx = 1; bool spec_1Vtx = 1;

    // varation
    bool weightClusterPt = 0;
    bool splitData = 0; const int splitFac = 1;
    bool poisFluc = 0;
    bool randSplit = 0; int randIntSplitFac = 1;
    bool nearbyVertexSyst = 0;  const float nearbyVertexCut = 15;//mm
    bool doCorr = 1;
    bool rej_HM = 0;
    bool reqTrigger = 1;
    float energyConv = 1;
    bool setRunZero = 0;

    // 5TEV pp
    if (0){
      energyConv = 1e3;
      setRunZero = 1;
      req_Vtx = 0;
      ActMuCut_high = 5;
    }

    ////////////////////////////////////
    // Systematics
    string outputName = "";

    if (syst==0){
       outputName = "corr_nominal";
    }
    if (syst==1){
       outputName = "corr_NoEff";
       use_trkEff = 0;
    }
    if (syst==2){
       outputName = "corr_split0";
       randSplit = 1; randIntSplitFac = 0;
    }
    if (syst==3){
       outputName = "corr_split1";
       randSplit = 1; randIntSplitFac = 1;
    }
    if (syst==4){
       outputName = "corr_Mu";
       ActMuCut_low  = 0.0;
       ActMuCut_high = 0.4;
    }
    if (syst==5){
       outputName = "corr_MuHigh";
       ActMuCut_low  = 1.25;//1.25 for 13TeV  0.5 5TeV
       ActMuCut_high = 3.0;  //3 for 13TeV   0.8 5TeV
    }
    if (syst==6){
       //outputName = "corr_1Vtx";
       //req_Vtx = 1; spec_1Vtx = 1;
       outputName = "corr_MuMed";
       ActMuCut_low  = 0.4;
       ActMuCut_high = 1.25;
    }
    if (syst==7){
       outputName = "corr_2PlusVtx";
       req_Vtx = 1; spec_1Vtx = 0;
    }
    if (syst==8){
       outputName = "corr_NBV";
       nearbyVertexSyst = 1;
       randSplit = 1; randIntSplitFac = 1;
    }
    if (syst==9){
       outputName = "corr_200mmVtxZMix";
       dPVz_interval = 50;//mm 
    }
    if (syst==10){
       outputName = "corr_20TrackMix";
       Nch_interval = 5;
    }
    if (syst==11){
       outputName = "skim";
       doCorr     = 0;
       energyConv = 1;
    }
    if (syst==12){
       outputName = "corr_ENW";
       weightClusterPt = true;
    }
    if (syst==13){
       outputName = "corr_HighMuRef";
       rej_HM = true;
    }
    if (syst==14){
       outputName = "corr_clus_pt3";
       cluster_pt_low  = 0.3;
    }
    if (syst==15){
       outputName = "corr_PC_pbpb";
      reCalcNtrk = 0;
      reqTrigger = 0;
      energyConv = 1;
    }
    bool debug = 0; 

    cout << "Running variation " << outputName << endl;
    TFile* fout = new TFile((outputName + ".root").c_str(),"RECREATE");



    // 2PC dphi and deta binning for correlation funciton
    const int Nbins_dPhi = 60;
    float dPhi_range[Nbins_dPhi+1] = {0};
    float dphi_width = 4*TMath::PiOver2() / Nbins_dPhi;
    float deta_width = 0.2;
    const int Nbins_deta = 50;
    float dEta_range[Nbins_deta+1] = {0};
    for (unsigned int i=0; i<Nbins_deta+1; i++) {
        dEta_range[i] = -5 + deta_width*i;
    }
    for (unsigned int i=0; i<Nbins_dPhi+1; i++) {
        dPhi_range[i] = -1*TMath::PiOver2() + dphi_width*i;
    }


    //-----------------------------------------------------------------------------

    const unsigned int Nbins_Nch = NchCut / Nch_interval;
    double dNch_range[Nbins_Nch+1];
    for (unsigned int i=0; i<Nbins_Nch+1; i++) {
        dNch_range[i] = i*Nch_interval;
    }


    // Nch slices
    TH1F* hNch_binning = new TH1F("hNch_binning","",Nbins_Nch,dNch_range);


    // pt slices for trigger particles; track
    const int Nbins_pt = 4;
    double dpt_range[Nbins_pt+1] = {0.3,0.4,0.5,2,5};
    //double dpt_range[Nbins_pt+1] = {0.3,0.4,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5};
    TH1F* hpt_binning = new TH1F("hpt_binning","",Nbins_pt,dpt_range);

    // eta slices for trigger particles; track
    const int Nbins_eta = 10;
    double deta_range[Nbins_eta+1];
    float deta_interval = 5./Nbins_eta;
    for (unsigned int ib=0; ib< Nbins_eta+1; ib++) deta_range[ib] = -2.5+ib*deta_interval;
    TH1F* heta_binning = new TH1F("heta_binning","",Nbins_eta,deta_range);

    const int Nbins_PVz = 2*PVzCut/dPVz_interval;

    //-----------------------------------------------------------------------------
    // mixing pool
    //-----------------------------------------------------------------------------
    typedef vector< vector< float>> basic_buffer;
    // vectors for event mixing
    basic_buffer evtMixBuffer_eta[Nbins_PVz][Nbins_Nch];
    basic_buffer evtMixBuffer_phi[Nbins_PVz][Nbins_Nch];
    //-----------------------------------------------------------------------------


    // 2D correlation functions as a function of Nch and pT
    TH2F* h2pc_sig[Nbins_Nch][Nbins_pt][Nbins_eta];
    TH2F* h2pc_mix[Nbins_Nch][Nbins_pt][Nbins_eta];
    for (unsigned int i=0; i<Nbins_Nch; i++) {
        for (unsigned int j=0; j<Nbins_pt; j++) {
            for (unsigned int k=0; k<Nbins_eta; k++) {
                h2pc_sig[i][j][k] = new TH2F(Form("h2pc_sig_Nch%d_pt%d_eta%d",i,j,k),"", Nbins_deta, dEta_range, Nbins_dPhi, dPhi_range);
                h2pc_mix[i][j][k] = new TH2F(Form("h2pc_mix_Nch%d_pt%d_eta%d",i,j,k),"", Nbins_deta, dEta_range, Nbins_dPhi, dPhi_range);
            }
        }
    }

    TH2F* h2_nsig_raw = new TH2F("h2_nsig_raw", "", Nbins_pt, dpt_range, Nbins_Nch, dNch_range);
    TH3F* h3_nsig_raw = new TH3F("h3_nsig_raw", "", Nbins_pt, dpt_range, Nbins_Nch, dNch_range,Nbins_eta,deta_range);
    TH1F* h_Nch_raw = new TH1F("h_Nch_raw", "", 450, 0, 450);
    TH1F* h_Nvtx = new TH1F("h_Nvtx", "",10, 0,10);
    TH1F* h_Nvtx_beforeCuts = new TH1F("h_Nvtx_beforeCuts", "",10, 0,10);
     TH1F* h_ActMu = new TH1F("h_ActMu","",5000,0,5);
     TH2F* h2_Nch_ActMu = new TH2F("h_Mch_ActMu","",500,0,5,200,0,200);
     TH1F* h_NBV = new TH1F("h_NBV","",3e4,0,300);
     TH2F* h2_Nch_NclusBarrel = new TH2F("h2_Nch_NclusBarrel","",200,0,200,500,0,500);
     TH2F* h2_Nch_NclusFcal = new TH2F("h2_Nch_NclusFcal","",200,0,200,200,0,200);
     TH1F* h_triggerChains = new TH1F();
     h_triggerChains->SetName("h_triggerChains"); 

     TH1F* h_triggerChains_PSW = new TH1F();
     h_triggerChains_PSW->SetName("h_triggerChains_PSW"); 

     TH1F* h_runs = new TH1F();
     h_runs->SetName("h_runs"); 

     TH1F* h_runs_evtSel = new TH1F();
     h_runs_evtSel->SetName("h_runs_evtSel"); 

     const int numTrig = Triggers.size() + 1;
     const int numRun = runList.size() + 1;
     TH1F* h_Nch_run_trig[numRun][numTrig];
     TH1F* h_mu_run[numRun];
     TH1F* h_Nch_run[numRun];
     for (int ir=0; ir<numRun; ir++) {
       h_mu_run[ir] = new TH1F(Form("h_num_run%d",ir),"",100,0,5);
       h_Nch_run[ir] = new TH1F(Form("h_Nch_run%d",ir),"",200,0,200);
       for (int it=0; it<numTrig; it++) {
         h_Nch_run_trig[ir][it] = new TH1F(Form("h_Nch_run%d_trig%d",ir,it),"",500,0,500);
       }
     }

     TH1F* h_clus_pt = new TH1F("h_clus_pt","",500,0,5);

    //-----------------------------------------------------------
    Long64_t nentries = fChain->GetEntries();
    //Long64_t nentries = 10000; //for testing
    Long64_t nbytes = 0, nb = 0;

    vector<float> toFillBufferTracks_eta;
    vector<float> toFillBufferTracks_phi;

    //////////////////////////////////////////
    // tools 
    TFile* fin_eff = new TFile("trkEff_nominal.root","READ");
    TH2F* eff_eta_pt = (TH2F*) fin_eff->Get("eff_eta_pt");



    cout << "total event number: " << nentries << endl;
    cout << "Event mixing strategy: " << endl;
    cout << "  PV_z interval: " << dPVz_interval << endl;
    cout << "  Nch interval:  " << Nch_interval << endl;
    cout << "  Mix pool size: " << poolSize -1 << endl;


    //////////////////////////////////////////////////////////////////////
   //Track roconstuction efficiency tool
   // poisson fluctuation for synthetic data
   TRandom3 rnd = TRandom3();
   rnd.SetSeed();
   if (randSplit) rnd.SetSeed(nentries); 

    /////////////////////////////////////////
    // Event loop
    cout << "total event number: " << nentries << endl;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        //if (jentry > 1e4) continue;

        // split data for systematic checks
        if (splitData && jentry%2 != splitFac) continue;
        //if (jentry > 4.*nentries/4. || jentry < 3.*nentries/4.)continue;
        int randInt = rnd.Integer(2);
        if (randSplit &&  randInt != randIntSplitFac) continue;

        nb = fChain->GetEntry(jentry);   nbytes += nb;
      	if (jentry%100000 == 0)  cout << "------------------running event " << jentry << "------------------" << endl;
       if (reCalcNtrk){
         Ntrk_MB = 0;
         for (unsigned int itrk=0; itrk<Track_pt->size(); itrk++){
            if (Track_pt->at(itrk)/energyConv > 0.4) Ntrk_MB++;
         }
       }

        ///////////////////////////////
        // bad events 

       ////////////////////////////////////
       // Event level quantities 
       float min_Del_r = 100000;
       for (unsigned int iv=0; iv<Vertex_x->size(); iv++){
         if ( Vertex_type->at(iv) <= 1 ) continue;
         float dR = sqrt( pow(PV_x-Vertex_x->at(iv),2) +  pow(PV_y-Vertex_y->at(iv),2) + pow(PV_z-Vertex_z->at(iv),2));
         if ( dR < min_Del_r && Vertex_type->at(iv) > 1) min_Del_r = dR;
       }
        
        ///////////////////////////////
        // trigger selection
       if (debug) cout << "looking at triggers" << endl;

       h_runs->Fill(Form("%d",RunNumber),1);
       int runInt = runToInt(RunNumber);
       if (setRunZero) runInt =0;
       h_mu_run[runInt]->Fill(ActMu);
        for (unsigned int itrig = 0; itrig < TriggerObject_Chain->size(); itrig++) {
           h_triggerChains->Fill(TriggerObject_Chain->at(itrig).Data(),1);
           h_triggerChains_PSW->Fill(TriggerObject_Chain->at(itrig).Data(),TriggerObject_Ps->at(itrig));
           int trigInt= TrigToInt(TriggerObject_Chain->at(itrig));
           h_Nch_run_trig[runInt][trigInt]->Fill(Ntrk_MB,TriggerObject_Ps->at(itrig));
        }

        bool _passTrig = false;
        bool passTrig = false; 
        for (unsigned int itrig = 0; itrig < TriggerObject_Chain->size(); itrig++) {
           passTrig = trigReq(TriggerObject_Chain->at(itrig),Ntrk_MB);
           if (passTrig) break;
        }
        for (unsigned int itrig = 0; itrig < TriggerObject_Chain->size(); itrig++) {
            if ((TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sptrk"))||
                 (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE25") && Ntrk_MB >= 90) || 
                 (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE20") && Ntrk_MB >= 80) || 
                 (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp1100_trk70_hmt_L1TE15") && Ntrk_MB >= 70) || 
                 ((TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp700_trk50_hmt_L1TE10") || 
                 TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10")) && Ntrk_MB >= 50) || 
                 (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp600_trk40_hmt_L1TE5") && Ntrk_MB >= 40))
               _passTrig = true;
       }
       if (!passTrig && reqTrigger) continue;
       
       h_Nch_run[runInt]->Fill(Ntrk_MB);
        h_Nvtx_beforeCuts->Fill(Vertex_x->size());

       //////////////////////////////////
       // event selection
       if (debug) cout<< "imposing event cuts" << endl;
        if (req_Vtx && ((nVtx == 2) != spec_1Vtx) ) continue;
        if (fabs(PV_z) >= PVzCut) continue;
        if ( (unsigned int) Ntrk_MB >= NchCut) continue;
        if (Ntrk_MB == 0) continue;
        if (ActMu > ActMuCut_high || ActMu <  ActMuCut_low) continue;
        if (nearbyVertexSyst && min_Del_r < nearbyVertexCut) continue;
        if (rej_HM && Ntrk_MB > 39) continue;

        //////////////////////////////////////////////////////
        // generate random number of resamplings for bootstrap
        unsigned int poisEvtW = 1;
        if (poisFluc) poisEvtW = (unsigned int) rnd.Poisson(1);

        ///////////////////////////////
        // event binning
        int Vz_bin = (int)((PV_z + PVzCut)/dPVz_interval);
        int Nch_bin = hNch_binning->FindBin(Ntrk_MB)-1;
    

        ///////////////////////////////
        // fill event-level histograms 
        h_Nch_raw->Fill(Ntrk_MB);
        h_ActMu->Fill(ActMu);
        h_NBV->Fill(min_Del_r);
        h2_Nch_NclusFcal->Fill(Ntrk_MB,Cluster_et->size());
        h2_Nch_NclusBarrel->Fill(Ntrk_MB,N_barrelClus);
        h_runs_evtSel->Fill(Form("%d",RunNumber),1);
        h2_Nch_ActMu->Fill(Ntrk_MB,ActMu);
        h_Nvtx->Fill(Vertex_x->size());

        for (size_t iclus = 0; iclus < Cluster_et->size(); iclus++) {
           if (fabs(Cluster_eta->at(iclus)) < cluster_eta_low) continue;
           h_clus_pt->Fill(Cluster_et->at(iclus));
        }

        ///////////////////////////////
        // 2-Particle correlation 
        if (!doCorr) continue;
        vector<float> Track_eff;

        // get tracking eff 
        for (size_t itrk = 0; itrk < Track_pt->size(); itrk++) {
            int bin = eff_eta_pt->FindBin(Track_eta->at(itrk),Track_pt->at(itrk));
            float eff = eff_eta_pt->GetBinContent(bin);
            if (eff < 0.001 || eff > 1.) eff = 1;
            Track_eff.push_back(eff);
        }

        for (size_t itrk = 0; itrk < Track_pt->size(); itrk++) {
            int pt_bin = hpt_binning->FindBin(Track_pt->at(itrk)/energyConv)-1;
            if (pt_bin < 0 || pt_bin >= Nbins_pt) continue;

	    int eta_bin = heta_binning->FindBin(Track_eta->at(itrk))-1;
            if (eta_bin < 0 || eta_bin >= Nbins_eta) continue;

            h2_nsig_raw->Fill(Track_pt->at(itrk)/energyConv, Ntrk_MB);
            h3_nsig_raw->Fill(Track_pt->at(itrk)/energyConv, Ntrk_MB,Track_eta->at(itrk));

            for (size_t jtrk = 0; jtrk < Track_pt->size(); jtrk++) {
              if (itrk == jtrk) continue;

              //int pt_bin_aso = hpt_binning->FindBin(Track_pt->at(itrk)/energyConv)-1;
              //if (pt_bin_aso < 0 || pt_bin_aso >= Nbins_pt) continue;
              if (Track_pt->at(jtrk)/energyConv > cluster_pt_high) continue;
              if (Track_pt->at(jtrk)/energyConv < cluster_pt_low) continue;

              int eta_bin_aso = heta_binning->FindBin(Track_eta->at(jtrk))-1;
             // if (eta_bin_aso != eta_bin ) continue;


              float dphi = Track_phi->at(itrk) - Track_phi->at(jtrk);
              float deta = Track_eta->at(itrk) - Track_eta->at(jtrk);
              while (dphi >  3*TMath::PiOver2()) dphi -= 2*TMath::Pi();
              while (dphi < -1*TMath::PiOver2()) dphi += 2*TMath::Pi();

              float weight = 1;
              if (use_trkEff) weight /= Track_eff.at(itrk);//*Track_eff.at(jtrk);
              // record event as if recorded multiple time  poisEvtW = 1 if no bootstrap
              h2pc_sig[Nch_bin][pt_bin][eta_bin]->Fill(deta, dphi,weight);

              if (debug) cout << jtrk << " " << itrk << endl;
            }

            if ( evtMixBuffer_eta[Vz_bin][Nch_bin].size() > poolSize ) continue; 
            /////////////////
            // mixed event
            for (size_t ibuffer = 0; ibuffer < evtMixBuffer_eta[Vz_bin][Nch_bin].size(); ibuffer++) {
              vector<float> toMixTracks_eta = evtMixBuffer_eta[Vz_bin][Nch_bin].at(ibuffer);
              vector<float> toMixTracks_phi = evtMixBuffer_phi[Vz_bin][Nch_bin].at(ibuffer);

              for (size_t iclus = 0; iclus < toMixTracks_eta.size(); iclus++) {


                  int eta_bin_aso = heta_binning->FindBin(toMixTracks_eta.at(iclus))-1;
                  //if (eta_bin_aso != eta_bin ) continue;

                  float deta = Track_eta->at(itrk) - toMixTracks_eta.at(iclus);
                  float dphi = Track_phi->at(itrk) - toMixTracks_phi.at(iclus);
           	  while (dphi >  3*TMath::PiOver2()) dphi -= 2*TMath::Pi();
           	  while (dphi < -1*TMath::PiOver2()) dphi += 2*TMath::Pi();

                  // track, cluster, and event weights
                  float weight = 1;
                  if (use_trkEff) weight /= Track_eff.at(itrk);
                  if (weightClusterPt) weight *= toMixTracks_eta.at(iclus);

                  h2pc_mix[Nch_bin][pt_bin][eta_bin]->Fill(deta, dphi,weight);
               }//aso mix particle
            } // mixed event paring
        } // trigger particle loop

        // Update the pool with the current event at the end
        for (size_t iclus = 0; iclus < Track_pt->size(); iclus++) {
            if (Track_pt->at(iclus)/energyConv > cluster_pt_high) continue;
            if (Track_pt->at(iclus)/energyConv < cluster_pt_low) continue;
            toFillBufferTracks_eta.push_back(Track_eta->at(iclus));
            toFillBufferTracks_phi.push_back(Track_phi->at(iclus));
        }

        if (evtMixBuffer_eta[Vz_bin][Nch_bin].size() < poolSize) {
            evtMixBuffer_eta[Vz_bin][Nch_bin].push_back(toFillBufferTracks_eta);
            evtMixBuffer_phi[Vz_bin][Nch_bin].push_back(toFillBufferTracks_phi);
        } else if (evtMixBuffer_eta[Vz_bin][Nch_bin].size() == poolSize) {
            evtMixBuffer_eta[Vz_bin][Nch_bin].erase(evtMixBuffer_eta[Vz_bin][Nch_bin].begin());
            evtMixBuffer_phi[Vz_bin][Nch_bin].erase(evtMixBuffer_phi[Vz_bin][Nch_bin].begin());

            evtMixBuffer_eta[Vz_bin][Nch_bin].push_back(toFillBufferTracks_eta);
            evtMixBuffer_phi[Vz_bin][Nch_bin].push_back(toFillBufferTracks_phi);
        } else {
            cout << "problems with mix buffer size " << evtMixBuffer_eta[Vz_bin][Nch_bin].size() << endl;
        }
        toFillBufferTracks_eta.clear();
        toFillBufferTracks_phi.clear();
    }// event loop

    fout->mkdir("Correlation_2D_raw");
    fout->cd("Correlation_2D_raw");
    for (unsigned int m=0; m<Nbins_Nch; m++) {
        for (unsigned int n=0; n<Nbins_pt; n++) {
            for (unsigned int l=0; l<Nbins_eta; l++) {
                h2pc_sig[m][n][l]->Write();
                h2pc_mix[m][n][l]->Write();
            }
        }
    }

    fout->cd();
    hNch_binning->Write();
    hpt_binning->Write();
    heta_binning->Write();
    h_Nch_raw->Write();
    h_NBV->Write();
    h2_nsig_raw->Write();
    h3_nsig_raw->Write();
    h_ActMu->Write();
    h2_Nch_ActMu->Write();
    h2_Nch_NclusBarrel->Write();
    h2_Nch_NclusFcal->Write();
    h_triggerChains->Write();
    h_triggerChains_PSW->Write();
    h_runs->Write();
    h_runs_evtSel->Write();
    h_Nvtx->Write();
    h_Nvtx_beforeCuts->Write();
    h_clus_pt->Write();

    fout->mkdir("h_Nch_run_trig");
    fout->cd("h_Nch_run_trig");
    if (syst==11){
    for (int ir=0; ir<numRun; ir++) {
       h_mu_run[ir]->Write();
       h_Nch_run[ir]->Write();
       for (int it=0; it<numTrig; it++) {
         h_Nch_run_trig[ir][it]->Write();
       }
    }
    }
    fout->Close();


}
