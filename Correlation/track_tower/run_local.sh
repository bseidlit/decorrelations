#!/bin/bash

export TargetDir="$PWD"


if [ -d ${TargetDir} ]; then
  rm -rf ${TargetDir}/testOutDir*
else
  mkdir ${TargetDir}
fi


j=1
tot_files=$( cat inputdata_test.txt | wc -l )
echo "total files: $tot_files"
rem=$(( $tot_files%$j ))
files_per_job=$(( $tot_files/$j ))
njob=$j
if [ $rem -ne 0 ]; then
  files_per_job=$(( $files_per_job+1 ))
fi
rem2=$(( $tot_files%$files_per_job ))
njob=$(( $tot_files/$files_per_job ))
if [ $rem2 -ne 0 ]; then
  njob=$(( ($tot_files/$files_per_job)+1 ))
fi
echo "files per job: $files_per_job"
echo "njob: $njob"

for((i=0;i<$njob;i++));
do

  mkdir ${TargetDir}/testOutDir$i
  export WorkDir="${TargetDir}/testOutDir$i"
  echo "WorkDir:" ${WorkDir}
  start_file=$(( $i*$files_per_job+1 ))
  end_file=$(( $start_file+$files_per_job-1 ))
  echo "start file: $start_file   end file: $end_file"

  sed -n $start_file\,${end_file}p inputdata_test.txt > tmp.txt
  mv tmp.txt ${WorkDir}/inputdata.txt

  pushd ${WorkDir}

  cp "$PWD"/../LocalRun.sh CondorRunTC$i.sh
  cp -v "$PWD"/../runMaker .
  cp "$PWD"/../trkEff_nominal.root .
  cp "$PWD"/../pp_tower_calib_cuts.root .

   source CondorRunTC$i.sh

  popd
done
