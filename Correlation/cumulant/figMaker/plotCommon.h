// Common plotting frames and strings


static TH1F *h_frame_r2;
static TH1F *h_frame_v22;
static TH1F *h_frame_v22_dCent;
static TH1F *h_frame_r2_ratio;
static TH1F *h_frame_v22_ratio;
static TH1F *h_frame_v22_dCent_ratio;

static TGraph* line1;
static TGraph* line0;

string etaAString = "#||{#it{#eta}^{a}}";

void initStyle(){
    h_frame_r2 = new TH1F("h_frame_r2","",100,0,10);
    h_frame_r2->GetXaxis()->SetRangeUser(0,2.5);
    h_frame_r2->GetXaxis()->SetTitle(etaAString.c_str());
    h_frame_r2->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_r2->GetYaxis()->SetRangeUser(0.67,1.3);
    h_frame_r2->GetYaxis()->SetTitle("#it{r}_{2}");
    h_frame_r2->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_r2->GetYaxis()->SetTitleSize(0.09);
    h_frame_r2->GetYaxis()->SetTitleOffset(0.7);
    h_frame_r2->GetYaxis()->SetLabelSize(0.07);


    h_frame_v22 = new TH1F("h_frame_v22","",100,-5,5);
    h_frame_v22->GetYaxis()->SetRangeUser(0.0,0.015);
    h_frame_v22->GetXaxis()->SetRangeUser(-2.5,2.5);
    h_frame_v22->GetXaxis()->SetTitle("#it{#eta^{a}}");
    h_frame_v22->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_v22->GetYaxis()->SetTitle("#it{v}_{2,2}");
    h_frame_v22->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22->GetYaxis()->SetTitleSize(0.09);
    h_frame_v22->GetYaxis()->SetLabelSize(0.07);
    h_frame_v22->GetYaxis()->SetTitleOffset(0.9);
    h_frame_v22->GetXaxis()->SetNdivisions(510,kTRUE);

    h_frame_v22_dCent = new TH1F("h_frame_v22_dCent","",100,0,100);
    h_frame_v22_dCent->GetYaxis()->SetRangeUser(-0.005,0.025);
    h_frame_v22_dCent->GetXaxis()->SetRangeUser(0,100);
    h_frame_v22_dCent->GetXaxis()->SetTitle("Centrality [%]");
    h_frame_v22_dCent->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_v22_dCent->GetYaxis()->SetTitle("#it{v}_{2,2}");
    h_frame_v22_dCent->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22_dCent->GetYaxis()->SetTitleSize(0.09);
    h_frame_v22_dCent->GetYaxis()->SetLabelSize(0.07);
    h_frame_v22_dCent->GetYaxis()->SetTitleOffset(0.9);
    h_frame_v22_dCent->GetXaxis()->SetNdivisions(510,kTRUE);


    line1 = new TGraph(2);
    line1->SetPoint(0,-100,1);
    line1->SetPoint(1,500,1);
    line1->SetLineStyle(2);
    line1->SetLineColor(1);
    line1->SetLineWidth(1);

    line0 = new TGraph(2);
    line0->SetPoint(0,-100,0);
    line0->SetPoint(1,500,0);
    line0->SetLineStyle(2);
    line0->SetLineColor(1);
    line0->SetLineWidth(1);

    h_frame_r2_ratio = new TH1F("h_frame_r2_ratio","",100,0,10);
    h_frame_r2_ratio->GetYaxis()->SetRangeUser(0.65,1.6);
    h_frame_r2_ratio->GetXaxis()->SetRangeUser(0,2.5);

    h_frame_r2_ratio->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_r2_ratio->GetYaxis()->SetLabelSize(0.14);
    h_frame_r2_ratio->GetYaxis()->SetTitleSize(0.13);
    h_frame_r2_ratio->GetYaxis()->CenterTitle(kTRUE);
    h_frame_r2_ratio->GetYaxis()->SetTitle("ratio");
    h_frame_r2_ratio->GetYaxis()->SetTitleOffset(0.6);

    h_frame_r2_ratio->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_r2_ratio->GetXaxis()->SetLabelSize(0.16);
    h_frame_r2_ratio->GetXaxis()->SetTickLength(0.10);
    h_frame_r2_ratio->GetXaxis()->SetTitleSize(0.13);
    h_frame_r2_ratio->GetXaxis()->SetTitleOffset(0.83);
    h_frame_r2_ratio->GetXaxis()->SetTitle(Form("%s      ",etaAString.c_str()));


    h_frame_v22_ratio = new TH1F("h_frame_v22_ratio","",100,-5,5);
    h_frame_v22_ratio->GetYaxis()->SetRangeUser(0.75,1.25);
    h_frame_v22_ratio->GetXaxis()->SetRangeUser(-2.5,2.5);

    h_frame_v22_ratio->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22_ratio->GetYaxis()->SetLabelSize(0.14);
    h_frame_v22_ratio->GetYaxis()->SetTitleSize(0.13);
    h_frame_v22_ratio->GetYaxis()->CenterTitle(kTRUE);
    h_frame_v22_ratio->GetYaxis()->SetTitle("ratio");
    h_frame_v22_ratio->GetYaxis()->SetTitleOffset(0.6);
    h_frame_v22_ratio->GetYaxis()->SetTitleSize(0.12);

    h_frame_v22_ratio->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_v22_ratio->GetXaxis()->SetLabelSize(0.16);
    h_frame_v22_ratio->GetXaxis()->SetTickLength(0.10);
    h_frame_v22_ratio->GetXaxis()->SetTitleSize(0.13);
    h_frame_v22_ratio->GetXaxis()->SetTitleOffset(1.03);
    h_frame_v22_ratio->GetXaxis()->SetTitle("#it{#eta^{a}}");

    h_frame_v22_dCent_ratio = new TH1F("h_frame_v22_dCent_ratio","",100,0,100);
    h_frame_v22_dCent_ratio->GetYaxis()->SetRangeUser(0.50,1.10);
    h_frame_v22_dCent_ratio->GetXaxis()->SetRangeUser(0,100);
    h_frame_v22_dCent_ratio->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22_dCent_ratio->GetYaxis()->SetLabelSize(0.14);
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitleSize(0.13);
    h_frame_v22_dCent_ratio->GetYaxis()->CenterTitle(kTRUE);
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitle("ratio to raw");
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitleOffset(0.6);
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitleSize(0.12);
    h_frame_v22_dCent_ratio->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_v22_dCent_ratio->GetXaxis()->SetLabelSize(0.16);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTickLength(0.10);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTitleSize(0.13);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTitleOffset(1.03);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTitle("Centrality [%]");

  }




  TGraphAsymmErrors* ratioHistAbsErr(TH1* nom,TH1* den){
    TGraphAsymmErrors* gr_ratio = new TGraphAsymmErrors();
    string name = nom->GetName();
    gr_ratio->SetName(Form("%s%s",name.c_str(),"_ratioAbsErr"));

    for (int ip=0; ip<nom->GetNbinsX(); ip++){
      float nomVal = nom->GetBinContent(ip+1);
      float denVal = den->GetBinContent(ip+1);
      float nomErr = nom->GetBinError(ip+1);
      float denErr = den->GetBinError(ip+1);
      // calc ratio
      float ratio = nomVal/denVal;
      float x = nom->GetBinCenter(ip+1);
      // calc error
      float sum = sqrt(pow(nomErr,2)+pow(denErr,2));
      float ratio_low  = fabs((nomVal-sum)/denVal-ratio);
      float ratio_high = fabs((nomVal+sum)/denVal-ratio);
      gr_ratio->SetPoint(ip,x,ratio);
      gr_ratio->SetPointError(ip,0,0,ratio_low,ratio_high);
    }
    return gr_ratio;
  }


  void shiftGraph(TGraphAsymmErrors* gr, float shift){
    for (int ig=0; ig<gr->GetN(); ig++) {
           gr->GetX()[ig] += shift;
           gr->GetEXlow ()[ig] = 0.0;
           gr->GetEXhigh()[ig] = 0.0;
    }
    return;
  }

  void shiftGraphNoErr(TGraphAsymmErrors* gr, float shift){
    for (int ig=0; ig<gr->GetN(); ig++) {
           gr->GetX()[ig] += shift;
           gr->GetEXlow ()[ig] = 0.0;
           gr->GetEXhigh()[ig] = 0.0;
           gr->GetEYlow ()[ig] = 0.0;
           gr->GetEYhigh()[ig] = 0.0;
    }
    return;
  }


  /////////////////////////////////////////////////////////////
  // generate a synthetic histagram data based on errors and
  // values.  Guassin uncorrelated

  class BlairGenSynHist{
  public:
    void init();
    TH1F* genHist(TH1F*,int);
  private:
    TRandom3* rand;
  };

  void BlairGenSynHist::init(){
    rand = new TRandom3(0);
  }

  TH1F* BlairGenSynHist::genHist(TH1F* hist, int index){
    int numBins = hist->GetNbinsX();
    TH1F* histMC = (TH1F*) hist->Clone(Form("%s_MC%d",hist->GetName(),index));
    histMC->Reset();

    for(int ib=1; ib<numBins+1; ib++){
      float val = rand->Gaus(hist->GetBinContent(ib),hist->GetBinError(ib));
      histMC->SetBinContent(ib,val);
      histMC->SetBinError(ib,hist->GetBinError(ib));
    }

    return histMC;
  }

  float NtrkToCent(float Ntrk){
    TFile* fin = new TFile("../../Correlation/Correlation_XeXe/Ntrk_cent.root","READ");
    TGraph* gr_cent_ntrk = (TGraph*) fin->Get("gr_cent_ntrk");
    float cent = gr_cent_ntrk->Eval(Ntrk);
    return cent;
  }

class calcF{
  public:
    void init();
    void calc(TH1F*);
    float f_val;
    float f_err;
    TF1* f_f;
  private:
};

void calcF::init(){
  f_f =  new TF1("f_f","1-2*[0]*x",0,2.5);
}

void calcF::calc(TH1F* hist){
  cout << endl << "Fitting " << hist->GetName() << endl;
  hist->Fit(f_f);
  f_val = f_f->GetParameter(0);
  f_err = f_f->GetParError(0);
}

/*
class fitJetY{
public:
  static double_t fitf(double_t*,double_t*);
  void init(int);
private:
  TFile* fin;
  static TGraphErrors* gr;
};

void fitJetY::init(int i){
  fin = new TFile("../../centrality/rootFiles/JetY_cent.root");
  if (i==0) {
    gr = (TGraphErrors*) fin->Get("gr_npart_ncollOnpart2_pfx");
  }
  if (i==1){
    gr = (TGraphErrors*) fin->Get("gr_npart_1Oncoll_pfx");
  }
}

double_t fitJetY::fitf(double_t* x,double_t* par){
  double_t val = gr->Eval(*x);
  double_t fitval = par[0]*val;
  return val;
}
*/

double_t fitf_1ONcall(double_t* x,double_t* par){
  TFile fin("../../centrality/rootFiles/JetY_cent.root");
  TGraphErrors* gr = (TGraphErrors*) fin.Get("gr_npart_1Oncoll_pfx");
  double_t val = gr->Eval(*x);
  double_t fitval = par[0]*val;
  return fitval;
}

double_t fitf_NcollONpart2(double_t* x,double_t* par){
  TFile fin("../../centrality/rootFiles/JetY_cent.root");
  TGraphErrors* gr2 = (TGraphErrors*) fin.Get("gr_npart_ncollOnpart2_pfx");
  double_t val2= gr2->Eval(*x);
  double_t fitval = par[0]*val2;
  return fitval;
}
