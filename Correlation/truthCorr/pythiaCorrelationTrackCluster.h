//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Feb 23 13:45:33 2020 by ROOT version 6.18/04
// from TTree analysis/My analysis ntuple
// found on file: ../RacfFile/user.qhu.20651067._000013.ANALYSIS.root
//////////////////////////////////////////////////////////

#ifndef CorrelationTrackCluster_h
#define CorrelationTrackCluster_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class CorrelationTrackCluster {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t truth_n          ;
   float truth_eta [1000];
   float truth_phi [1000];
   float truth_pt  [1000];

   vector<float> *truth_eta_in;
   vector<float> *truth_phi_in;
   vector<float> *truth_pt_in ;
   vector<float> *truth_pid_in;

   // List of branches
   TBranch        *b_truth_pid ;   //!
   TBranch        *b_truth_eta ;   //!
   TBranch        *b_truth_phi ;   //!
   TBranch        *b_truth_pt  ;   //!

   CorrelationTrackCluster(TTree *tree=0);
   virtual ~CorrelationTrackCluster();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef CorrelationTrackCluster_cxx
CorrelationTrackCluster::CorrelationTrackCluster(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../input/user.qhu.20651067._000002.ANALYSIS.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../../input/user.qhu.20651067._000002.ANALYSIS.root");
      }
      f->GetObject("jewel",tree);

   }
   Init(tree);
}

CorrelationTrackCluster::~CorrelationTrackCluster()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t CorrelationTrackCluster::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t CorrelationTrackCluster::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void CorrelationTrackCluster::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   truth_eta_in  =0;
   truth_pid_in  =0;
   truth_phi_in  =0;
   truth_pt_in   =0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   //fChain->SetBranchAddress("truth_n", &truth_n , &b_truth_n );
   fChain->SetBranchAddress("trk_eta", &truth_eta_in , &b_truth_eta );
   fChain->SetBranchAddress("trk_phi", &truth_phi_in , &b_truth_phi );
   fChain->SetBranchAddress("trk_pt" ,  &truth_pt_in  , &b_truth_pt  );
   fChain->SetBranchAddress("trk_ped", &truth_pid_in , &b_truth_pid );

   Notify();
}


Bool_t CorrelationTrackCluster::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void CorrelationTrackCluster::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t CorrelationTrackCluster::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef CorrelationTrackCluster_cxx
