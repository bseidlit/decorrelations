#define CorrelationTrackCluster_cxx
#include "pythiaCorrelationTrackCluster.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

// trigger particle: tracks of eta selection -2.5 to 2.5
// associated particle: clusters of eta selection 4.0 to 4.9

void CorrelationTrackCluster::Loop() {
    if (fChain == 0) return;
    
    std::cout.imbue(std::locale(""));


    TFile* fout = new TFile("correlation_pythia.root","RECREATE");

    // Basic setting
    const float PVzCut = 100.;
    const int NchCut = 150;
    const float ActMuCut_high = 2.5;
    const float ActMuCut_low = 0.0;
    const float cluster_pt_low  = 0.5;
    const float cluster_pt_high = 5.0;
    const float cluster_eta_low = 4.0;

    // default

    // varation
    const bool req_1vtx = 0;
    const bool weightClusterPt = 0;
    const bool splitData = 0; const int splitFac = 9;
    const bool poisFluc = 0;


    // dphi and deta binning for correlation funciton
    float dPhi_range[31] = {0};
    float dEta_range[145] = {0};
    float dphi_width = 4*TMath::PiOver2() / 30;
    float deta_width = 2.0;
    const int Nbins_deta = 8;
    for (int i=0; i<Nbins_deta+1; i++) {
        dEta_range[i] = -8 + deta_width*i;
    }
    for (int i=0; i<31; i++) {
        dPhi_range[i] = -1*TMath::PiOver2() + dphi_width*i;
    }

    //-----------------------------------------------------------------------------
    // event mixing categories
    const int poolSize = 6;

    const int Nch_interval = 10;
    const int Nbins_Nch = NchCut / Nch_interval;
    double dNch_range[Nbins_Nch+1];
    for (int i=0; i<Nbins_Nch+1; i++) {
        dNch_range[i] = i*Nch_interval;
    }


    // Nch slices
    TH1F* hNch_binning = new TH1F("hNch_binning","",Nbins_Nch,dNch_range);


    // pt slices for trigger particles; track
     const int Nbins_pt = 11;
    double dpt_range[Nbins_pt+1] = {0.3,0.4,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5};
    TH1F* hpt_binning = new TH1F("hpt_binning","",Nbins_pt,dpt_range);

    // eta slices for trigger particles; track
    const int Nbins_eta = 20;
    double deta_range[Nbins_eta+1];
    float deta_interval = 5./Nbins_eta;
    for (unsigned int ib=0; ib< Nbins_eta+1; ib++) deta_range[ib] = -2.5+ib*deta_interval;

    TH1F* heta_binning = new TH1F("heta_binning","",Nbins_eta,deta_range);

    const float dPVz_interval = 5;
    const int Nbins_PVz = 2*PVzCut/dPVz_interval;

    //-----------------------------------------------------------------------------
    // mixing pool
    //-----------------------------------------------------------------------------
    typedef vector< vector< float>> basic_buffer;
    typedef vector< int> basic_buffer_index;
    // vectors for event mixing
    basic_buffer evtMixBuffer_eta[Nbins_PVz][Nbins_Nch];
    basic_buffer evtMixBuffer_phi[Nbins_PVz][Nbins_Nch];
    //-----------------------------------------------------------------------------


    // 2D correlation functions as a function of Nch and pT
    TH2F* h2pc_sig_C[Nbins_Nch][Nbins_pt][Nbins_eta];
    TH2F* h2pc_sig_A[Nbins_Nch][Nbins_pt][Nbins_eta];
    TH2F* h2pc_mix_C[Nbins_Nch][Nbins_pt][Nbins_eta];
    TH2F* h2pc_mix_A[Nbins_Nch][Nbins_pt][Nbins_eta];
    for (int i=0; i<Nbins_Nch; i++) {
        for (int j=0; j<Nbins_pt; j++) {
            for (int k=0; k<Nbins_eta; k++) {
                h2pc_sig_A[i][j][k] = new TH2F(Form("h2pc_sig_A_Nch%d_pt%d_eta%d",i,j,k),"", Nbins_deta, dEta_range, 30, dPhi_range);
                h2pc_sig_C[i][j][k] = new TH2F(Form("h2pc_sig_C_Nch%d_pt%d_eta%d",i,j,k),"", Nbins_deta, dEta_range, 30, dPhi_range);
                h2pc_mix_A[i][j][k] = new TH2F(Form("h2pc_mix_A_Nch%d_pt%d_eta%d",i,j,k),"", Nbins_deta, dEta_range, 30, dPhi_range);
                h2pc_mix_C[i][j][k] = new TH2F(Form("h2pc_mix_C_Nch%d_pt%d_eta%d",i,j,k),"", Nbins_deta, dEta_range, 30, dPhi_range);
            }
        }
    }

    TH2F* h2_nsig_raw = new TH2F("h2_nsig_raw", "", Nbins_pt, dpt_range, Nbins_Nch, dNch_range);
    TH1F* h_Nch_raw = new TH1F("h_Nch_raw", "", 450, 0, 450);

    //////////////////////////////////////////////////////////////////////
   //Track roconstuction efficiency tool
   // poisson fluctuation for synthetic data
   TRandom3 rnd = TRandom3();
   rnd.SetSeed();


    //-----------------------------------------------------------------------------

    Long64_t nentries = fChain->GetEntries();
    //Long64_t nentries = 10000; //for testing
    Long64_t nbytes = 0, nb = 0;

    vector<float> toFillBufferClusters_eta;
    vector<float> toFillBufferClusters_phi;

    cout << "total event number: " << nentries << endl;
    cout << "Event mixing strategy: " << endl;
    cout << "  PV_z interval: " << dPVz_interval << endl;
    cout << "  Nch interval:  " << Nch_interval << endl;
    cout << "  Mix pool size: " << poolSize -1 << endl;


    cout << "total event number: " << nentries << endl;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        //if (jentry > 1e4) continue;

        // split data for systematic checks
        if (splitData && jentry%10 != splitFac) continue;
        //if (jentry > 4.*nentries/4. || jentry < 3.*nentries/4.)continue;

        nb = fChain->GetEntry(jentry);   nbytes += nb;
      	if (jentry%50000 == 0)  cout << "------------------running event " << jentry << "------------------" << endl;

        truth_n=0;
        for (int ip=0; ip<truth_pt_in->size() ; ip++){
          truth_pt[ip] = truth_pt_in->at(ip);
          truth_eta[ip] = truth_eta_in->at(ip);
          //truth_pid[ip] = truth_pid_in->at(ip);
          truth_phi[ip] = truth_phi_in->at(ip);
          truth_n++;
        }

        float PV_z = 0;
        float Ntrk_MB = 0;
        for (int ip=0; ip<truth_n ; ip++){
           if (fabs(truth_eta [ip]) < 2.5 && truth_pt [ip] > 0.4) Ntrk_MB++;
        }


        if (Ntrk_MB >= NchCut) continue;
        if (Ntrk_MB == 0) continue;

        //////////////////////////////////////////////////////
        // generate random number of resamplings for bootstrap
        int poisEvtW = 1;
        if (poisFluc) poisEvtW = (int) rnd.Poisson(1);

        ///////////////////////////////
        // event binning
        int Vz_bin = (int)((PV_z + PVzCut)/dPVz_interval);
        int Nch_bin = hNch_binning->FindBin(Ntrk_MB)-1;
        h_Nch_raw->Fill(Ntrk_MB);


        for (size_t itrk = 0; itrk < truth_n ; itrk++) {

            int pt_bin = hpt_binning->FindBin(truth_pt [itrk])-1;
            if (pt_bin < 0 || pt_bin >= Nbins_pt) continue;

	    int eta_bin = heta_binning->FindBin(truth_eta [itrk])-1;
            if (eta_bin < 0 || eta_bin >= Nbins_eta) continue;

            h2_nsig_raw->Fill(truth_pt [itrk], Ntrk_MB);

            //////////////////////
            // same event
            for (size_t iclus = 0; iclus < truth_n ; iclus++) {

		 if (fabs(truth_eta[iclus]) < cluster_eta_low) continue;

                if (truth_pt[iclus] < cluster_pt_low || truth_pt[iclus] > cluster_pt_high) continue;

           	float dphi = truth_phi[itrk] - truth_phi[iclus];
           	float deta = truth_eta[itrk] - truth_eta[iclus];
           	while (dphi >  3*TMath::PiOver2()) dphi -= 2*TMath::Pi();
           	while (dphi < -1*TMath::PiOver2()) dphi += 2*TMath::Pi();

                float weight = 1;
                if (weightClusterPt) weight *=truth_pt [iclus];

                // record event as if recorded multiple time  poisEvtW = 1 if no bootstrap
                for (int isamp=0; isamp< poisEvtW; isamp++){
                    // 2D signal pair distribution
                    if ( truth_eta[iclus] > 0)
                          h2pc_sig_A[Nch_bin][pt_bin][eta_bin]->Fill(deta, dphi,weight);
                    if ( truth_eta[iclus] < 0)
                           h2pc_sig_C[Nch_bin][pt_bin][eta_bin]->Fill(deta, dphi,weight);
                }

            }

            /////////////////
            // mixed event
            for (size_t ibuffer = 0; ibuffer < evtMixBuffer_eta[Vz_bin][Nch_bin].size(); ibuffer++) {
           	    vector<float> toMixClusters_eta = evtMixBuffer_eta[Vz_bin][Nch_bin].at(ibuffer);
           	    vector<float> toMixClusters_phi = evtMixBuffer_phi[Vz_bin][Nch_bin].at(ibuffer);

                for (size_t iclus = 0; iclus < toMixClusters_eta.size(); iclus++) {

		        if (fabs(toMixClusters_eta.at(iclus)) < cluster_eta_low) continue;

           	        float deta = truth_eta[itrk] - toMixClusters_eta.at(iclus);
           	        float dphi = truth_phi[itrk] - toMixClusters_phi.at(iclus);
           	        while (dphi >  3*TMath::PiOver2()) dphi -= 2*TMath::Pi();
           	        while (dphi < -1*TMath::PiOver2()) dphi += 2*TMath::Pi();

                    // track, cluster, and event weights
                    float weight = 1;
                    if (weightClusterPt) weight *= toMixClusters_eta.at(iclus);

                    // record event as if recorded multiple time  poisEvtW = 1 if no bootstrap
                    for (int isamp=0; isamp< poisEvtW; isamp++){
                        // 2D mixed pair distribution
                        if ( toMixClusters_eta.at(iclus) > 0)
                           h2pc_mix_A[Nch_bin][pt_bin][eta_bin]->Fill(deta, dphi);
                        if ( toMixClusters_eta.at(iclus) < 0)
                           h2pc_mix_C[Nch_bin][pt_bin][eta_bin]->Fill(deta, dphi);
                    }// bootstrap lloop 
           	    } // cluster loop
            } // mixed event paring
        } // trigger particle loop


        // Update the pool with the current event at the end
        for (size_t iclus = 0; iclus < truth_n ; iclus++) {
            if (truth_pt [iclus] > cluster_pt_high ||truth_pt [iclus]< cluster_pt_low) continue;
            if (fabs(truth_eta [iclus]) < cluster_eta_low) continue;
            toFillBufferClusters_eta.push_back(truth_eta [iclus]);
            toFillBufferClusters_phi.push_back(truth_phi [iclus]);
        }

        if (evtMixBuffer_eta[Vz_bin][Nch_bin].size() < poolSize) {
            evtMixBuffer_eta[Vz_bin][Nch_bin].push_back(toFillBufferClusters_eta);
            evtMixBuffer_phi[Vz_bin][Nch_bin].push_back(toFillBufferClusters_phi);
        } else if (evtMixBuffer_eta[Vz_bin][Nch_bin].size() == poolSize) {
            evtMixBuffer_eta[Vz_bin][Nch_bin].erase(evtMixBuffer_eta[Vz_bin][Nch_bin].begin());
            evtMixBuffer_phi[Vz_bin][Nch_bin].erase(evtMixBuffer_phi[Vz_bin][Nch_bin].begin());

            evtMixBuffer_eta[Vz_bin][Nch_bin].push_back(toFillBufferClusters_eta);
            evtMixBuffer_phi[Vz_bin][Nch_bin].push_back(toFillBufferClusters_phi);
        } else {
            cout << "problems with mix buffer size " << evtMixBuffer_eta[Vz_bin][Nch_bin].size() << endl;
        }
        toFillBufferClusters_eta.clear();
        toFillBufferClusters_phi.clear();

    }// event loop

    fout->mkdir("Correlation_2D_raw");
    fout->cd("Correlation_2D_raw");
    for (int m=0; m<Nbins_Nch; m++) {
        for (int n=0; n<Nbins_pt; n++) {
            for (int l=0; l<Nbins_eta; l++) {
                h2pc_sig_C[m][n][l]->Write();
                h2pc_sig_A[m][n][l]->Write();
                h2pc_mix_C[m][n][l]->Write();
                h2pc_mix_A[m][n][l]->Write();
            }
        }
    }

    fout->cd();
    hNch_binning->Write();
    hpt_binning->Write();
    heta_binning->Write();
    h_Nch_raw->Write();
    h2_nsig_raw->Write();
    fout->Close();
}
