#include "plotCommon.h"

void shiftLogGraph(TGraphAsymmErrors* gr, float shift,float x_err){
  for (int ig=1; ig<gr->GetN(); ig++) {
         gr->GetX()[ig] += shift*gr->GetX()[ig]/25.;
         gr->GetEXlow ()[ig] =  x_err*gr->GetX()[ig]/25.;
         gr->GetEXhigh()[ig] =  x_err*gr->GetX()[ig]/25.;
  }
  return;
}

pair <TGraphAsymmErrors*,TGraphAsymmErrors*> mergeStatSyst(TGraphAsymmErrors* stat1,TGraphAsymmErrors* syst1, TGraphAsymmErrors* stat2, TGraphAsymmErrors* syst2){
  pair <TGraphAsymmErrors*,TGraphAsymmErrors*> result;
  result.first  = new TGraphAsymmErrors();
  result.second = new TGraphAsymmErrors();
  for (int ip=0; ip<stat1->GetN(); ip++){
    double x1,x2,y1,y2,el1,el2,eh1,eh2,exl,exh;
    stat1->GetPoint(ip,x1,y1);
    stat2->GetPoint(ip,x2,y2);
    result.first ->SetPoint(ip,(x1+x2)/2,(y1+y2)/2);
    result.second->SetPoint(ip,(x1+x2)/2,(y1+y2)/2);

    float estatl = quad(stat1->GetErrorYlow(ip) ,stat2->GetErrorYlow(ip))/2;
    float estath = quad(stat1->GetErrorYhigh(ip),stat2->GetErrorYhigh(ip))/2;
    result.first->SetPointError(ip,stat1->GetErrorXlow(ip),stat1->GetErrorXhigh(ip),estatl,estath);
    float valsystl = min(y1-syst1->GetErrorYlow(ip) ,y2-syst2->GetErrorYlow(ip));
    float valsysth = max(y1+syst1->GetErrorYhigh(ip),y2+syst2->GetErrorYhigh(ip));
    float esystl = (y1+y2)/2 - valsystl;
    float esysth = valsysth  - (y1+y2)/2;
    result.second->SetPointError(ip,syst1->GetErrorXlow(ip),syst1->GetErrorXhigh(ip),esystl,esysth);
  }
  return result;
}

void replaceXaxis(TGraphAsymmErrors* gr,float* newx){
  for (int ip=0; ip<gr->GetN(); ip++){
    cout << gr->GetX()[ip] << " ";
    gr->GetX()[ip] = newx[ip];
    cout << gr->GetX()[ip] << endl;
  }
  return;
}


void combFn() {
    initStyle();
    TFile* fin = new TFile("rootFiles/xexe/Results_withSyst.root","Read");

    TGraphAsymmErrors* gr_v11Raw_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v11Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v11Raw_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v11Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Sub_Fn_StatError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Sub_Fn_SystError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22SubD1_Fn_StatError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22SubD1_Fn_SystError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Crt_Fn_StatError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Crt_Fn_SystError");

    TFile* fin_nch_xexe =new TFile("../Correlation/Correlation_XeXe/rootFiles/corr_nominal_dNch.root");
    TH1F* h_nch_xexe_raw = (TH1F*) fin_nch_xexe->Get("h_Nch_raw");
    const int Nbins_out_nch_xexe = 20;
    float dnch_xexe_out_range[Nbins_out_nch_xexe+1] = {0, 18, 28, 41, 59, 82, 112, 150, 196, 254, 323, 404, 500, 612, 740, 889, 1061, 1261, 1496, 1783,2500};

    float nch_xexe_avg[Nbins_out_nch_xexe];
    calcAvgNch(h_nch_xexe_raw,Nbins_out_nch_xexe,dnch_xexe_out_range,nch_xexe_avg);

    replaceXaxis(gr_v11Raw_Fn_Stat_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v11Raw_Fn_Syst_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v22Raw_Fn_Stat_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v22Raw_Fn_Syst_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v22Sub_Fn_Stat_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v22Sub_Fn_Syst_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v22SubD1_Fn_Stat_xexe,nch_xexe_avg);
    replaceXaxis(gr_v22SubD1_Fn_Syst_xexe,nch_xexe_avg);
    replaceXaxis(gr_v22Crt_Fn_Stat_xexe  ,nch_xexe_avg);
    replaceXaxis(gr_v22Crt_Fn_Syst_xexe  ,nch_xexe_avg);


    float plotShift = 0.5;

    pair <TGraphAsymmErrors*,TGraphAsymmErrors*> gr_v22SubComb_xexe;
    gr_v22SubComb_xexe = mergeStatSyst(gr_v22Crt_Fn_Stat_xexe,gr_v22Crt_Fn_Syst_xexe, gr_v22SubD1_Fn_Stat_xexe,gr_v22SubD1_Fn_Syst_xexe);

    shiftLogGraph(gr_v22SubComb_xexe.first ,0,0);
    shiftLogGraph(gr_v22SubComb_xexe.second,0,1);


    shiftLogGraph(gr_v11Raw_Fn_Stat_xexe,0,0);
    shiftLogGraph(gr_v11Raw_Fn_Syst_xexe,0,1);
    shiftLogGraph(gr_v22Raw_Fn_Stat_xexe,0,0);
    shiftLogGraph(gr_v22Raw_Fn_Syst_xexe,0,1);
    shiftLogGraph(gr_v22Sub_Fn_Stat_xexe,0,0);
    shiftLogGraph(gr_v22Sub_Fn_Syst_xexe,0,1);
    shiftLogGraph(gr_v22SubD1_Fn_Stat_xexe,   plotShift,0);
    shiftLogGraph(gr_v22SubD1_Fn_Syst_xexe,   plotShift,1);
    shiftLogGraph(gr_v22Crt_Fn_Stat_xexe  ,-1*plotShift,0);
    shiftLogGraph(gr_v22Crt_Fn_Syst_xexe  ,-1*plotShift,1);


    gr_v22Crt_Fn_Stat_xexe->SetPoint(0,-8,0);
    gr_v22Crt_Fn_Syst_xexe->SetPoint(0,-8,0);
    gr_v22Sub_Fn_Stat_xexe->SetPoint(0,-8,0);
    gr_v22Sub_Fn_Syst_xexe->SetPoint(0,-8,0);



    TFile* fin_5TeV = new TFile("rootFiles/5TeV/Results_withSyst.root","Read");

    TGraphAsymmErrors* gr_v11Raw_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v11Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v11Raw_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v11Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Sub_Fn_StatError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Sub_Fn_SystError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Stat_5TeV = (TGraphAsymmErrors*) fin_5TeV->Get("g_v22SubD1_Fn_StatError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Syst_5TeV = (TGraphAsymmErrors*) fin_5TeV->Get("g_v22SubD1_Fn_SystError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Crt_Fn_StatError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Crt_Fn_SystError");


    TFile* fin_nch_5TeV = new TFile("../Correlation/track_cluster/rootFiles/corr_nominal.root");
    TH1F* h_nch_5TeV_raw = (TH1F*) fin_nch_5TeV->Get("h_Nch_raw");
    const int Nbins_out_nch_5TeV = 6;
    float dnch_5TeV_out_range[Nbins_out_nch_5TeV+1] = {0,10, 20 , 40,60,80,160};
    float nch_5TeV_avg[Nbins_out_nch_5TeV];
    calcAvgNch(h_nch_5TeV_raw,Nbins_out_nch_5TeV,dnch_5TeV_out_range,nch_5TeV_avg);

    replaceXaxis(gr_v11Raw_Fn_Stat_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v11Raw_Fn_Syst_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v22Raw_Fn_Stat_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v22Raw_Fn_Syst_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v22Sub_Fn_Stat_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v22Sub_Fn_Syst_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v22SubD1_Fn_Stat_5TeV,nch_5TeV_avg);
    replaceXaxis(gr_v22SubD1_Fn_Syst_5TeV,nch_5TeV_avg);
    replaceXaxis(gr_v22Crt_Fn_Stat_5TeV  ,nch_5TeV_avg);
    replaceXaxis(gr_v22Crt_Fn_Syst_5TeV  ,nch_5TeV_avg);


    pair <TGraphAsymmErrors*,TGraphAsymmErrors*> gr_v22SubComb_5TeV;
    gr_v22SubComb_5TeV = mergeStatSyst(gr_v22Crt_Fn_Stat_5TeV,gr_v22Crt_Fn_Syst_5TeV, gr_v22SubD1_Fn_Stat_5TeV,gr_v22SubD1_Fn_Syst_5TeV);

    shiftLogGraph(gr_v22SubComb_5TeV.first ,0,0);
    shiftLogGraph(gr_v22SubComb_5TeV.second,0,1);



    shiftLogGraph(gr_v11Raw_Fn_Stat_5TeV,0,0);
    shiftLogGraph(gr_v11Raw_Fn_Syst_5TeV,0,1);
    shiftLogGraph(gr_v22Raw_Fn_Stat_5TeV,0,0);
    shiftLogGraph(gr_v22Raw_Fn_Syst_5TeV,0,1);
    shiftLogGraph(gr_v22Sub_Fn_Stat_5TeV,0,0);
    shiftLogGraph(gr_v22Sub_Fn_Syst_5TeV,0,1);
    shiftLogGraph(gr_v22SubD1_Fn_Stat_5TeV,   plotShift,0);
    shiftLogGraph(gr_v22SubD1_Fn_Syst_5TeV,   plotShift,1);
    shiftLogGraph(gr_v22Crt_Fn_Stat_5TeV  ,-1*plotShift,0);
    shiftLogGraph(gr_v22Crt_Fn_Syst_5TeV  ,-1*plotShift,1);

    gr_v22SubComb_5TeV.first->SetPoint(0,-8,0);
    gr_v22SubComb_5TeV.first->SetPoint(1,-8,0);
    gr_v22SubComb_5TeV.first->SetPoint(2,-8,0);
    gr_v22SubComb_5TeV.first->SetPoint(3,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(0,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(1,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(2,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(3,-8,0);





    TFile* fin_13TeV = new TFile("rootFiles/13TeV/Results_withSyst.root","Read");

    TGraphAsymmErrors* gr_v11Raw_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v11Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v11Raw_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v11Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Sub_Fn_StatError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Sub_Fn_SystError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Stat_13TeV = (TGraphAsymmErrors*) fin_13TeV->Get("g_v22SubD1_Fn_StatError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Syst_13TeV = (TGraphAsymmErrors*) fin_13TeV->Get("g_v22SubD1_Fn_SystError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Crt_Fn_StatError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Crt_Fn_SystError");


    TFile* fin_nch_13TeV =new TFile("../Correlation/track_cluster/rootFiles/13TeV2/corr_nominal.root");
    TH1F* h_nch_13TeV_raw = (TH1F*) fin_nch_13TeV->Get("h_Nch_raw");
    const int Nbins_out_nch_13TeV = 7;
    float dnch_13TeV_out_range[Nbins_out_nch_13TeV+1] = {0,10, 20 , 40,60,70,90,160};
    float nch_13TeV_avg[Nbins_out_nch_13TeV];
    calcAvgNch(h_nch_13TeV_raw,Nbins_out_nch_13TeV,dnch_13TeV_out_range,nch_13TeV_avg);

    replaceXaxis(gr_v11Raw_Fn_Stat_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v11Raw_Fn_Syst_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v22Raw_Fn_Stat_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v22Raw_Fn_Syst_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v22Sub_Fn_Stat_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v22Sub_Fn_Syst_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v22SubD1_Fn_Stat_13TeV,nch_13TeV_avg);
    replaceXaxis(gr_v22SubD1_Fn_Syst_13TeV,nch_13TeV_avg);
    replaceXaxis(gr_v22Crt_Fn_Stat_13TeV  ,nch_13TeV_avg);
    replaceXaxis(gr_v22Crt_Fn_Syst_13TeV  ,nch_13TeV_avg);





    pair <TGraphAsymmErrors*,TGraphAsymmErrors*> gr_v22SubComb_13TeV;
    gr_v22SubComb_13TeV = mergeStatSyst(gr_v22Crt_Fn_Stat_13TeV,gr_v22Crt_Fn_Syst_13TeV, gr_v22SubD1_Fn_Stat_13TeV,gr_v22SubD1_Fn_Syst_13TeV);

    shiftLogGraph(gr_v22SubComb_13TeV.first ,0,0);
    shiftLogGraph(gr_v22SubComb_13TeV.second,0,1);



    shiftLogGraph(gr_v11Raw_Fn_Stat_13TeV,0,0);
    shiftLogGraph(gr_v11Raw_Fn_Syst_13TeV,0,1);
    shiftLogGraph(gr_v22Raw_Fn_Stat_13TeV,0,0);
    shiftLogGraph(gr_v22Raw_Fn_Syst_13TeV,0,1);
    shiftLogGraph(gr_v22Sub_Fn_Stat_13TeV,0,0);
    shiftLogGraph(gr_v22Sub_Fn_Syst_13TeV,0,1);
    shiftLogGraph(gr_v22SubD1_Fn_Stat_13TeV,   plotShift,0);
    shiftLogGraph(gr_v22SubD1_Fn_Syst_13TeV,   plotShift,1);
    shiftLogGraph(gr_v22Crt_Fn_Stat_13TeV  ,-1*plotShift,0);
    shiftLogGraph(gr_v22Crt_Fn_Syst_13TeV  ,-1*plotShift,1);


    gr_v22SubComb_13TeV.first->SetPoint(0,-8,0);
    gr_v22SubComb_13TeV.first->SetPoint(1,-8,0);
    gr_v22SubComb_13TeV.first->SetPoint(2,-8,0);
    gr_v22SubComb_13TeV.first->SetPoint(3,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(0,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(1,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(2,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(3,-8,0);


    TCanvas* c1 = new TCanvas("c1","New Canvas",50,50,700,600);
    h_frame_Fn->Draw("AXIS");
    h_frame_Fn->GetXaxis()->SetTitle("#it{N}_{ch}^{rec}");
    h_frame_Fn->GetYaxis()->SetTitle("#it{F}_{2}");
    h_frame_Fn->GetYaxis()->SetRangeUser(5e-3,0.5);
    h_frame_Fn->GetXaxis()->SetRangeUser(20,2500);

    float alphaF = 0.6;

/*
    gr_v11Raw_Fn_Syst_xexe->Draw("2");
    gr_v11Raw_Fn_Syst_xexe->SetFillColorAlpha(kMagenta-7,alphaF);
    gr_v11Raw_Fn_Syst_xexe->SetFillColorAlpha(kMagenta-7,alphaF);
    gr_v11Raw_Fn_Stat_xexe->Draw("PSAME");
    gr_v11Raw_Fn_Stat_xexe->SetMarkerColor(kMagenta+1);
    gr_v11Raw_Fn_Stat_xexe->SetLineColor(kMagenta+1);
    gr_v11Raw_Fn_Stat_xexe->SetMarkerStyle(21);
*/


    gr_v22Raw_Fn_Syst_xexe->Draw("2");
    gr_v22Raw_Fn_Syst_xexe->SetFillColorAlpha(kBlue-7,alphaF);
    gr_v22Raw_Fn_Syst_xexe->SetFillColorAlpha(kBlue-7,alphaF);
    gr_v22Raw_Fn_Stat_xexe->Draw("PSAME");
    gr_v22Raw_Fn_Stat_xexe->SetMarkerColor(kBlue+1);
    gr_v22Raw_Fn_Stat_xexe->SetLineColor(kBlue+1);
    gr_v22Raw_Fn_Stat_xexe->SetMarkerStyle(24);

    gr_v22Raw_Fn_Syst_5TeV->Draw("2");
    gr_v22Raw_Fn_Syst_5TeV->SetFillColorAlpha(kGray+1,alphaF);
    gr_v22Raw_Fn_Stat_5TeV->Draw("PSAME");
    gr_v22Raw_Fn_Stat_5TeV->SetMarkerColor(kBlack);
    gr_v22Raw_Fn_Stat_5TeV->SetLineColor  (kBlack);
    gr_v22Raw_Fn_Stat_5TeV->SetMarkerStyle(24);

    gr_v22Raw_Fn_Syst_13TeV->Draw("2");
    gr_v22Raw_Fn_Syst_13TeV->SetFillColorAlpha(kRed-7,alphaF);
    gr_v22Raw_Fn_Syst_13TeV->SetFillColorAlpha(kRed-7,alphaF);
    gr_v22Raw_Fn_Stat_13TeV->Draw("PSAME");
    gr_v22Raw_Fn_Stat_13TeV->SetMarkerColor(kRed+1);
    gr_v22Raw_Fn_Stat_13TeV->SetLineColor(kRed+1);
    gr_v22Raw_Fn_Stat_13TeV->SetMarkerStyle(24);


/*
    gr_v22Sub_Fn_Syst_xexe->Draw("2");
    gr_v22Sub_Fn_Syst_xexe->SetFillColorAlpha(kGreen-7,alphaF);
    gr_v22Sub_Fn_Syst_xexe->SetFillColorAlpha(kGreen-7,alphaF);
    gr_v22Sub_Fn_Stat_xexe->Draw("PSAME");
    gr_v22Sub_Fn_Stat_xexe->SetMarkerColor(kGreen+2);
    gr_v22Sub_Fn_Stat_xexe->SetLineColor(kGreen+2);
    gr_v22Sub_Fn_Stat_xexe->SetMarkerStyle(20);
    */
/*
    gr_v22Crt_Fn_Syst_xexe->Draw("2");
    gr_v22Crt_Fn_Syst_xexe->SetFillColorAlpha(kCyan-7,alphaF);
    gr_v22Crt_Fn_Syst_xexe->SetLineColor(kCyan-7);
    gr_v22Crt_Fn_Stat_xexe->Draw("PSAME");
    gr_v22Crt_Fn_Stat_xexe->SetMarkerColor(kCyan+2);
    gr_v22Crt_Fn_Stat_xexe->SetLineColor(kCyan+2);
    gr_v22Crt_Fn_Stat_xexe->SetMarkerStyle(22);
*/
    gr_v22SubComb_xexe.second->Draw("2");
    gr_v22SubComb_xexe.second->SetFillColorAlpha(kBlue-7,alphaF);
    gr_v22SubComb_xexe.second->SetLineColor(kBlue-7);
    gr_v22SubComb_xexe.first->Draw("PSAME");
    gr_v22SubComb_xexe.first->SetMarkerColor(kBlue+2);
    gr_v22SubComb_xexe.first->SetLineColor(kBlue+2);
    gr_v22SubComb_xexe.first->SetMarkerStyle(33);

    gr_v22SubComb_5TeV.second->Draw("2");
    gr_v22SubComb_5TeV.second->SetFillColorAlpha(kGray+1,alphaF);
    gr_v22SubComb_5TeV.second->SetLineColor(kGray);
    gr_v22SubComb_5TeV.first->Draw("PSAME");
    gr_v22SubComb_5TeV.first->SetMarkerColor(kBlack);
    gr_v22SubComb_5TeV.first->SetLineColor  (kBlack);
    gr_v22SubComb_5TeV.first->SetMarkerStyle(33);

    gr_v22SubComb_13TeV.second->Draw("2");
    gr_v22SubComb_13TeV.second->SetFillColorAlpha(kRed-7,alphaF);
    gr_v22SubComb_13TeV.second->SetLineColor(kRed-7);
    gr_v22SubComb_13TeV.first->Draw("PSAME");
    gr_v22SubComb_13TeV.first->SetMarkerColor(kRed+2);
    gr_v22SubComb_13TeV.first->SetLineColor(kRed+2);
    gr_v22SubComb_13TeV.first->SetMarkerStyle(33);


    h_frame_Fn->Draw("AXIS same");


    gPad->SetLogx();
    gPad->SetLogy();

    myText( 0.22,0.91,1,"#font[72]{ATLAS} Preliminary",0.04);
    myText( 0.22,0.86,1,"|#it{#eta}^{a}| < 2.5,  0.5 < #it{p}_{T}^{a} < 5.0",0.04);
    myText( 0.22,0.81,1,"4.0 < |#it{#eta}^{ref}| < 4.9",0.04);
    //myText( 0.20,0.20,1,"#it{v}_{2,2} = 1 + 2#times#it{F}_{n}#times#it{#eta}^{a}",0.04);
/*
    myMarkerLineText(0.22,0.40-0.0,1.5,kMagenta, 21,kMagenta, 1,"raw #it{F}_{1}", 0.04, true);
    myMarkerLineText(0.22,0.35-0.0,1.5,kRed    , 23,kRed    , 1,"raw #it{F}_{2}", 0.04, true);
    myMarkerLineText(0.22,0.30-0.0,1.5,kSpring , 20,kSpring, 1,"temp. fit #it{F}_{2}", 0.04, true);
    */
    myText          (0.65-0.05,0.90 ,1,"#it{pp} 5.02 TeV trk-clus",0.04);
    myMarkerLineText(0.65     ,0.85 ,1.5,kBlack, 24,kBlack, 1,"Raw Fourier", 0.04, true);
    myMarkerLineText(0.65     ,0.80 ,1.5,kBlack, 33,kBlack, 1,"Nonflow subtracted", 0.04, true);
    myText          (0.65-0.05,0.90-0.15 ,1,"#it{pp} 13 TeV trk-clus",0.04);
    myMarkerLineText(0.65     ,0.85-0.15 ,1.5,kRed+2, 24,kRed+2, 1,"Raw Fourier", 0.04, true);
    myMarkerLineText(0.65     ,0.80-0.15 ,1.5,kRed+2, 33,kRed+2, 1,"Nonflow subtracted", 0.04, true);
    myText          (0.65-0.05,0.90-0.30 ,1,"Xe+Xe 5.44 TeV trk-twr",0.04);
    myMarkerLineText(0.65     ,0.85-0.30 ,1.5,kBlue+2, 24,kBlue+2, 1,"Raw Fourier", 0.04, true);
    myMarkerLineText(0.65     ,0.80-0.30 ,1.5,kBlue+2, 33,kBlue+2, 1,"Nonflow subtracted", 0.04, true);


  c1->SaveAs("figures/comb/finalResults_Fn.pdf");





  TGraphAsymmErrors* gr_v22Raw_Fn_Stat_xexe_binx = (TGraphAsymmErrors*) gr_v22Raw_Fn_Stat_xexe->Clone("gr_v22Raw_Fn_Stat_xexe_binx");
  TGraphAsymmErrors* gr_v22SubComb_Fn_Stat_xexe_binx = (TGraphAsymmErrors*) gr_v22SubComb_xexe.first->Clone("gr_v22Raw_Fn_Stat_xexe_binx");
  TH1F* frame_binx = new TH1F("frame_binx","",20,-70,3);
  for (int ib=0; ib<20; ib++){
    gr_v22Raw_Fn_Stat_xexe_binx->GetX()[ib] = -1.*(90 - ib*4.5);
    gr_v22SubComb_Fn_Stat_xexe_binx->GetX()[ib] = -1.*(90 - ib*4.5);
  }



  TCanvas* c2 = new TCanvas("c2","New Canvas",50,50,700,600);
  frame_binx->Draw("AXIS");
  frame_binx->GetXaxis()->SetTitle("#it{N}_{ch}^{rec}");
  frame_binx->GetYaxis()->SetTitle("#it{F}_{2}");
  frame_binx->GetYaxis()->SetRangeUser(0,0.035);
  frame_binx->GetXaxis()->SetNdivisions(305);
//  frame_binx->GetXaxis()->SetRangeUser(0,10);
frame_binx->Draw("axis");


/*
  gr_v22Raw_Fn_Syst_xexe->Draw("2");
  gr_v22Raw_Fn_Syst_xexe->SetFillColorAlpha(kBlue-7,alphaF);
  gr_v22Raw_Fn_Syst_xexe->SetFillColorAlpha(kBlue-7,alphaF);
  gr_v22Raw_Fn_Stat_xexe->Draw("PSAME");
  gr_v22Raw_Fn_Stat_xexe->SetMarkerColor(kBlue+1);
  gr_v22Raw_Fn_Stat_xexe->SetLineColor(kBlue+1);
  gr_v22Raw_Fn_Stat_xexe->SetMarkerStyle(24);
*/

  gr_v22Raw_Fn_Stat_xexe_binx->Draw("p");
  gr_v22Raw_Fn_Stat_xexe_binx->SetMarkerColor(kBlack);
gr_v22Raw_Fn_Stat_xexe_binx->SetMarkerStyle(22);
gr_v22Raw_Fn_Stat_xexe_binx->SetLineColor(kBlack);
  //h_frame_Fn->Draw("AXIS same");

  gr_v22SubComb_Fn_Stat_xexe_binx->Draw("p");
  gr_v22SubComb_Fn_Stat_xexe_binx->SetMarkerColor(kBlue);
gr_v22SubComb_Fn_Stat_xexe_binx->SetMarkerStyle(33);

//  myText( 0.22,0.91,1,"#font[72]{ATLAS} Internal",0.04);
//  myText( 0.22,0.86,1,"|#it{#eta}^{a}| < 2.5,  0.5 < #it{p}_{T}^{a} < 5.0",0.04);
//  myText( 0.22,0.81,1,"4.0 < |#it{#eta}^{ref}| < 4.9",0.04);
  //myText( 0.20,0.20,1,"#it{v}_{2,2} = 1 + 2#times#it{F}_{n}#times#it{#eta}^{a}",0.04);
/*
  myMarkerLineText(0.22,0.40-0.0,1.5,kMagenta, 21,kMagenta, 1,"raw #it{F}_{1}", 0.04, true);
  myMarkerLineText(0.22,0.35-0.0,1.5,kRed    , 23,kRed    , 1,"raw #it{F}_{2}", 0.04, true);
  myMarkerLineText(0.22,0.30-0.0,1.5,kSpring , 20,kSpring, 1,"temp. fit #it{F}_{2}", 0.04, true);
  */
  myMarkerLineText(0.65     ,0.85-0.03,1.5,kBlack, 22,kBlack, 1,"Raw Fourier", 0.04, true);
  myMarkerLineText(0.65     ,0.80-0.03,1.5,kBlue, 33,kBlue, 1,"Nonflow subtracted", 0.04, true);
  /*
  myText          (0.65-0.05,0.90-0.15 ,1,"#it{pp} 13 TeV trk-clus",0.04);
  myMarkerLineText(0.65     ,0.85-0.15 ,1.5,kRed+2, 24,kRed+2, 1,"Raw Fourier", 0.04, true);
  myMarkerLineText(0.65     ,0.80-0.15 ,1.5,kRed+2, 33,kRed+2, 1,"Nonflow subtracted", 0.04, true);
  myText          (0.65-0.05,0.90-0.30 ,1,"Xe+Xe 5.44 TeV trk-twr",0.04);
  myMarkerLineText(0.65     ,0.85-0.30 ,1.5,kBlue+2, 24,kBlue+2, 1,"Raw Fourier", 0.04, true);
  myMarkerLineText(0.65     ,0.80-0.30 ,1.5,kBlue+2, 33,kBlue+2, 1,"Nonflow subtracted", 0.04, true);
*/

c2->SaveAs("figures/comb/comp_pub.pdf");

}
