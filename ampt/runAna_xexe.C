#include "../Analysis/CorrelationButton/CorrelationMaker.h"
#include "../Analysis/CorrelationButton/NonFlowSubtractor.C"


void runAna_xexe() {


        int low = 0; int high = 1;
        int lowLM = 0; int highLM = 0;

        bool f_pp = 1;
        bool f_xexe = 0;
        bool f_difLM = 0;

        TFile *f;
        if(f_xexe) f  = new TFile("rootFiles/superduper_decorr_ampt_xexe5440_0.root");
        if(f_pp)    f = new TFile("rootFiles/decorr_v2_ampt_pp5000_total.root");
        TFile *fLM ;
        if (f_difLM) fLM= new TFile("rootFiles/superduper_decorr_ampt_xexe5440_bgt11_0.root");
        if (!f_difLM) fLM=f;
        TFile *fout;
        if (f_xexe)fout  = new TFile("rootFiles/Results_xexe.root","RECREATE");
        if (f_pp)fout  = new TFile("rootFiles/Results_pp.root","RECREATE");

        bool Save = 1;

        // xe-xe case
        TGraph *xexebins = new TGraph();
        // xe-xe case has 103 bins in Nch
        for (int i=0; i<100; i++) {
                double mult_nch25 = 50.0* (double) (100-i);
                xexebins->SetPoint(i,i,mult_nch25);
        }
        xexebins->SetPoint(100,100,25.0);
        xexebins->SetPoint(101,101,5.0);

        //const int Nbins_out_Nch = 13;
        //int dNch_out_range[Nbins_out_Nch+1] =  {0,10,20,30,40,50,60,70,80,90,95,98,99,100};
        const int Nbins_out_Nch = 5;
        int dNch_out_range[Nbins_out_Nch+1] =  {0,1,2,3,4,5};

        string pt_string = "#it{p}_{T}^{ref,a} > 0.4 [GeV]";
        float _pt_trig_low  = 0.4;
        float _pt_trig_high = 5.0;
        float _pt_asso_low  = 0.4;
        float _pt_asso_high = 5.0;

        int whichcenthighLM = 4;
        int whichcentlowLM  = 4;

        const int Nbins_in_eta = 16;
        float etaA_width = 0.5;
        float etaA_low = -4.0;
        float dEtaA_in_range[Nbins_in_eta+1];
        float dEtaA_in_centers[Nbins_in_eta];

        for (int ib=0; ib<Nbins_in_eta+1; ib++) dEtaA_in_range[ib] = etaA_low + ib *0.5;
        for (int ib=0; ib<Nbins_in_eta; ib++) {
                dEtaA_in_centers[ib]   = etaA_low + etaA_width/2+ ib *0.5;
        }

        const int Neta = 8;
        float detapos_out_range[Neta+1];
        float detaneg_out_range[Neta+1];

        for (int ib=0; ib<Neta+1; ib++) {
                detapos_out_range[ib]   =  ib *0.5;
                detaneg_out_range[ib]   =  ib * -0.5;
        }


        // CONSTRUCT LM TH2
        TH2F *h2_LM;
        char namecorr[100];
        sprintf(namecorr,"hcorr_full_%d",whichcentlowLM);
        h2_LM = static_cast <TH2F *> (fLM->Get(namecorr));
        // if there are at least two centralities to add, then sum them...
        if (whichcentlowLM != whichcenthighLM) {
                for (int whichcent=whichcentlowLM+1; whichcent<=whichcenthighLM; whichcent++) {
                        sprintf(namecorr,"hcorr_full_%d",whichcent);
                        TH2F *hcorr_full_temp = static_cast <TH2F *> (fLM->Get(namecorr));
                        h2_LM->Add(hcorr_full_temp);
                }
        }

        for (int index_Nch = 0; index_Nch < Nbins_out_Nch; index_Nch++) {
                int whichcenthigh = dNch_out_range[index_Nch+1];
                int whichcentlow  = dNch_out_range[index_Nch];

                low  = (int) xexebins->Eval(whichcenthigh);
                high = (int) xexebins->Eval(whichcentlow);

                bool periphFile = high < 101;

                lowLM  = (int) xexebins->Eval(whichcenthighLM);
                highLM = (int) xexebins->Eval(whichcentlowLM);

                cout << "XeXe case:   low,high = " << low << " , " << high << endl;
                cout << "XeXe case:   low,high (LM) = " << lowLM << " , " << highLM << endl;

                string getOutputFigPath ;
                if (f_pp)   getOutputFigPath = "figures/pp/TemplateFigures";
                if (f_xexe) getOutputFigPath = "figures/xexe/TemplateFigures";

                TFile *fout;
                string LM_string;
                LM_string = "LM: " + std::to_string(lowLM) + " - " + std::to_string(highLM);
                string HM_string;
                HM_string = "HM: " + std::to_string(low) + " - " + std::to_string(high);

                // CONSTRUCT HM TH2
                TH2F *h2_HM;
                sprintf(namecorr,"hcorr_full_%d",whichcentlow);
                if (periphFile){ h2_HM = static_cast <TH2F *> (fLM->Get(namecorr));}
                else { h2_HM = static_cast <TH2F *> (f->Get(namecorr));}
                // if there are at least two centralities to add, then sum them...
                if (whichcentlow != whichcenthigh) {
                        for (int whichcent=whichcentlow+1; whichcent<=whichcenthigh; whichcent++) {
                                sprintf(namecorr,"hcorr_full_%d",whichcent);
                                TH2F *hcorr_full_temp;
                                if (periphFile){
                                  hcorr_full_temp = static_cast <TH2F *> (fLM->Get(namecorr));
                                }
                                else {
                                  hcorr_full_temp = static_cast <TH2F *> (f->Get(namecorr));
                                }
                                h2_HM->Add(hcorr_full_temp);
                        }
                }




                NonFlowSubtractor subTool_den;
                NonFlowSubtractor subTool_num;
                NonFlowSubtractor subTool_den_Zyam;
                NonFlowSubtractor subTool_num_Zyam;
                subTool_den_Zyam.setZYAM(1);
                subTool_num_Zyam.setZYAM(1);
                subTool_den.init();
                subTool_num.init();
                subTool_den_Zyam.init();
                subTool_num_Zyam.init();


                TH1F* h_den_v11Raw = new TH1F(Form("hNch%d_den_v11Raw_deta",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_den_v22Raw = new TH1F(Form("hNch%d_den_v22Raw_deta",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_den_v22Sub = new TH1F(Form("hNch%d_den_v22Sub_deta",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_den_v22Imp = new TH1F(Form("hNch%d_den_v22Imp_deta",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_den_v22Zyam = new TH1F(Form("hNch%d_den_v22Zyam_deta",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_den_v33Raw = new TH1F(Form("hNch%d_den_v33Raw_deta",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_den_v33Sub = new TH1F(Form("hNch%d_den_v33Sub_deta",index_Nch), "", Neta, detapos_out_range);

                TH1F* h_num_v11Raw = new TH1F(Form("hNch%d_num_v11Raw_deta",index_Nch), "", Neta, detaneg_out_range);
                TH1F* h_num_v22Raw = new TH1F(Form("hNch%d_num_v22Raw_deta",index_Nch), "", Neta, detaneg_out_range);
                TH1F* h_num_v22Sub = new TH1F(Form("hNch%d_num_v22Sub_deta",index_Nch), "", Neta, detaneg_out_range);
                TH1F* h_num_v22Imp = new TH1F(Form("hNch%d_num_v22Imp_deta",index_Nch), "", Neta, detaneg_out_range);
                TH1F* h_num_v22Zyam = new TH1F(Form("hNch%d_num_v22Zyam_deta",index_Nch), "", Neta, detaneg_out_range);
                TH1F* h_num_v33Sub = new TH1F(Form("hNch%d_num_v33Sub_deta",index_Nch), "", Neta, detaneg_out_range);
                TH1F* h_num_v33Raw = new TH1F(Form("hNch%d_num_v33Raw_deta",index_Nch), "", Neta, detaneg_out_range);

                TH1F* h_num_v11Raw1 = new TH1F(Form("hNch0_num_v11Raw_deta1"), "", Neta, detapos_out_range);
                TH1F* h_num_v22Raw1 = new TH1F(Form("hNch0_num_v22Raw_deta1"), "", Neta, detapos_out_range);
                TH1F* h_num_v22Sub1 = new TH1F(Form("hNch0_num_v22Sub_deta1"), "", Neta, detapos_out_range);
                TH1F* h_num_v22Imp1 = new TH1F(Form("hNch0_num_v22Imp_deta1"), "", Neta, detapos_out_range);
                TH1F* h_num_v22Zyam1 = new TH1F(Form("hNch0_num_v22Zyam_deta1"), "", Neta, detapos_out_range);
                TH1F* h_num_v33Raw1 = new TH1F(Form("hNch0_num_v33Raw_deta1"), "", Neta, detapos_out_range);
                TH1F* h_num_v33Sub1 = new TH1F(Form("hNch0_num_v33Sub_deta1"), "", Neta, detapos_out_range);


                for (int index_eta=0; index_eta<Neta; index_eta++) {

                        int ieA_num = 8 -index_eta;
                        TH1* hist_HM_num = (TH1*) h2_HM->ProjectionY(Form("h_HM_num_etaA%d",index_eta),ieA_num,ieA_num);
                        TH1* hist_LM_num = (TH1*) h2_LM->ProjectionY(Form("h_LM_num_etaA%d",index_eta),ieA_num,ieA_num);
                        hist_HM_num->Scale(1./ hist_HM_num->Integral());
                        hist_LM_num->Scale(1./ hist_LM_num->Integral());

                        int ieA_den = 8 + index_eta;
                        TH1* hist_HM_den = (TH1*) h2_HM->ProjectionY(Form("h_HM_den_etaA_%d",index_eta) ,ieA_den,ieA_den);
                        TH1* hist_LM_den = (TH1*) h2_LM->ProjectionY(Form("h_LM_den_etaA%d",index_eta) ,ieA_den,ieA_den);
                        hist_HM_den->Scale(1./ hist_HM_den->Integral());
                        hist_LM_den->Scale(1./ hist_LM_den->Integral());


                        // apply subtraction here
                        subResult theResult_num = subTool_num.templateFit(hist_LM_num, hist_HM_num, hist_LM_num);
                        subResult theResult_den = subTool_den.templateFit(hist_LM_den, hist_HM_den, hist_LM_den);
                        subResult theResult_num_Zyam = subTool_num_Zyam.templateFit(hist_LM_num, hist_HM_num, hist_LM_num);
                        subResult theResult_den_Zyam = subTool_den_Zyam.templateFit(hist_LM_den, hist_HM_den, hist_LM_den);


                        // make some plots here
                        if (index_eta==4){
                        TCanvas* c1fit = new TCanvas("c1fit","scaling",50,50, 600,800);
                        subTool_den.plotAtlas3pHM(c1fit);
                        c1fit->cd();
                        plotText( 0.2,0.94,1,"#bf{AMPT} XeXe #it{h-h}");
                        plotText( 0.2,0.90,1,Form("#it{#eta}^{ref} = [4.0,4.9] "));
                        plotText( 0.2,0.86,1,Form("#it{#eta}^{a} = [%0.1f,%0.1f]",detapos_out_range[index_eta],detapos_out_range[index_eta+1]));
                        plotText(0.2,0.37,1,LM_string.c_str());
                        plotText(0.2,0.33,1,HM_string.c_str());
                        plotText(0.2,0.29,1,pt_string.c_str());
                        if (Save) c1fit->SaveAs(Form("%s/templateFit_den_eta_%d_Nch%d.pdf",getOutputFigPath.c_str(), index_eta,index_Nch));
                        delete c1fit; c1fit = 0;

                        TCanvas* c2fit = new TCanvas("c2fit","scaling",50,50, 600,800);
                        subTool_num.plotAtlas3pHM(c2fit);
                        c2fit->cd();
                        plotText( 0.2,0.94,1,"#bf{AMPT} XeXe #it{h-h}");
                        plotText( 0.2,0.90,1,Form("#it{#eta}^{ref} = [4.0,4.9] "));
                        plotText( 0.2,0.86,1,Form("#it{#eta}^{a} = [%0.1f,%0.1f]",detaneg_out_range[index_eta],detaneg_out_range[index_eta+1]));
                        plotText(0.2,0.37,1,LM_string.c_str());
                        plotText(0.2,0.33,1,HM_string.c_str());
                        plotText(0.2,0.29,1,pt_string.c_str());
                        if (Save) c2fit->SaveAs(Form("%s/templateFit_num_eta_%d_Nch%d.pdf",getOutputFigPath.c_str(), index_eta,index_Nch));
                        delete c2fit; c2fit = 0;

                        TCanvas* c3fit = new TCanvas("c3fit","c2fit",50,50, 600,600);
                        subTool_den.plotAtlasLM(c3fit);
                        c3fit->cd();
                        plotText( 0.2,0.89,1,"#bf{AMPT}");
                        plotText( 0.2,0.84,1,"#it{XeXe} h-h correlation");
                        plotText( 0.2,0.79,1,LM_string.c_str());
                        plotText( 0.2,0.74,1,pt_string.c_str());
                        if (Save) c3fit->SaveAs(Form("%s/referenceFit_den_eta%d.pdf",getOutputFigPath.c_str(), index_eta));
                        delete c3fit; c3fit = 0;

                        TCanvas* c4fit = new TCanvas("c4fit","c2fit",50,50, 600,600);
                        subTool_num.plotAtlasLM(c4fit);
                        c4fit->cd();
                        plotText( 0.2,0.89,1,"#bf{AMPT}");
                        plotText( 0.2,0.84,1,"#it{XeXe} h-h correlation");
                        plotText( 0.2,0.79,1,LM_string.c_str());
                        plotText( 0.2,0.74,1,pt_string.c_str());
                        if (Save) c4fit->SaveAs(Form("%s/referenceFit_num_eta%d.pdf",getOutputFigPath.c_str(), index_eta));
                        delete c4fit; c4fit = 0;
                        }


                        h_den_v11Raw->SetBinContent (index_eta, theResult_den.getCoeffRawValue(1));
                        h_den_v11Raw->SetBinError   (index_eta, theResult_den.getCoeffRawError(1));
                        h_den_v22Raw->SetBinContent (index_eta, theResult_den.getV22RawValue());
                        h_den_v22Raw->SetBinError   (index_eta, theResult_den.getV22RawError());
                        h_den_v22Sub->SetBinContent (index_eta, theResult_den.getV22SubValue());
                        h_den_v22Sub->SetBinError   (index_eta, theResult_den.getV22SubError());
                        h_den_v22Imp->SetBinContent (index_eta, theResult_den_Zyam.getV22SubImpValue());
                        h_den_v22Imp->SetBinError   (index_eta, theResult_den_Zyam.getV22SubImpError());
                        h_den_v22Zyam->SetBinContent(index_eta, theResult_den_Zyam.getV22SubValue());
                        h_den_v22Zyam->SetBinError  (index_eta, theResult_den_Zyam.getV22SubError());

                        h_den_v33Raw->SetBinContent (index_eta, theResult_den.getV33RawValue());
                        h_den_v33Raw->SetBinError   (index_eta, theResult_den.getV33RawError());
                        h_den_v33Sub->SetBinContent (index_eta, theResult_den.getV33SubValue());
                        h_den_v33Sub->SetBinError   (index_eta, theResult_den.getV33SubError());

                        h_num_v11Raw->SetBinContent (index_eta, theResult_num.getCoeffRawValue(1));
                        h_num_v11Raw->SetBinError   (index_eta, theResult_num.getCoeffRawError(1));
                        h_num_v22Raw->SetBinContent(index_eta, theResult_num.getV22RawValue());
                        h_num_v22Raw->SetBinError  (index_eta, theResult_num.getV22RawError());
                        h_num_v22Sub->SetBinContent(index_eta, theResult_num.getV22SubValue());
                        h_num_v22Sub->SetBinError  (index_eta, theResult_num.getV22SubError());
                        h_num_v22Imp->SetBinContent(index_eta, theResult_num_Zyam.getV22SubImpValue());
                        h_num_v22Imp->SetBinError  (index_eta, theResult_num_Zyam.getV22SubImpError());
                        h_num_v22Zyam->SetBinContent(index_eta, theResult_num_Zyam.getV22SubValue());
                        h_num_v22Zyam->SetBinError  (index_eta, theResult_num_Zyam.getV22SubError());
                        h_num_v33Raw->SetBinContent(index_eta, theResult_num.getV33RawValue());
                        h_num_v33Raw->SetBinError  (index_eta, theResult_num.getV33RawError());
                        h_num_v33Sub->SetBinContent(index_eta, theResult_num.getV33SubValue());
                        h_num_v33Sub->SetBinError  (index_eta, theResult_num.getV33SubError());

                        h_num_v11Raw1->SetBinContent(index_eta, theResult_num.getCoeffRawValue(1));
                        h_num_v11Raw1->SetBinError  (index_eta, theResult_num.getCoeffRawError(1));
                        h_num_v22Raw1->SetBinContent(index_eta, theResult_num.getV22RawValue());
                        h_num_v22Raw1->SetBinError  (index_eta, theResult_num.getV22RawError());
                        h_num_v22Sub1->SetBinContent(index_eta, theResult_num.getV22SubValue());
                        h_num_v22Sub1->SetBinError  (index_eta, theResult_num.getV22SubError());
                        h_num_v22Imp1->SetBinContent(index_eta, theResult_num_Zyam.getV22SubImpValue());
                        h_num_v22Imp1->SetBinError  (index_eta, theResult_num_Zyam.getV22SubImpError());
                        h_num_v22Zyam1->SetBinContent(index_eta, theResult_num_Zyam.getV22SubValue());
                        h_num_v22Zyam1->SetBinError  (index_eta, theResult_num_Zyam.getV22SubError());
                        h_num_v33Raw1->SetBinContent(index_eta, theResult_num.getV33RawValue());
                        h_num_v33Raw1->SetBinError  (index_eta, theResult_num.getV33RawError());
                        h_num_v33Sub1->SetBinContent(index_eta, theResult_num.getV33SubValue());
                        h_num_v33Sub1->SetBinError  (index_eta, theResult_num.getV33SubError());
                }
                h_den_v11Raw->Write();
                h_den_v22Raw->Write();
                h_den_v22Sub->Write();
                h_den_v22Imp->Write();
                h_den_v22Zyam->Write();
                h_num_v22Raw->Write();
                h_num_v22Sub->Write();
                h_num_v22Imp->Write();
                h_num_v22Zyam->Write();
                h_den_v33Raw->Write();
                h_den_v33Sub->Write();

                h_num_v11Raw1->Write();
                h_num_v22Raw1->Write();
                h_num_v22Sub1->Write();
                h_num_v22Zyam1->Write();
                h_num_v33Sub1->Write();
                h_num_v33Raw1->Write();

                TH1F* h_deco_v11Raw = (TH1F*)h_num_v11Raw1->Clone(Form("Nch%d_deco_v11Raw",index_Nch));
                TH1F* h_deco_v22Raw = (TH1F*)h_num_v22Raw1->Clone(Form("Nch%d_deco_v22Raw",index_Nch));
                TH1F* h_deco_v22Sub = (TH1F*)h_num_v22Sub1->Clone(Form("Nch%d_deco_v22Sub",index_Nch));
                TH1F* h_deco_v22Imp = (TH1F*)h_num_v22Imp1->Clone(Form("Nch%d_deco_v22Imp",index_Nch));
                TH1F* h_deco_v22Zyam = (TH1F*)h_num_v22Zyam1->Clone(Form("Nch%d_deco_v22Zyam",index_Nch));
                TH1F* h_deco_v33Raw = (TH1F*)h_num_v33Raw1->Clone(Form("Nch%d_deco_v33Raw",index_Nch));
                TH1F* h_deco_v33Sub = (TH1F*)h_num_v33Sub1->Clone(Form("Nch%d_deco_v33Sub",index_Nch));

                h_deco_v11Raw->Divide(h_den_v11Raw);
                h_deco_v22Raw->Divide(h_den_v22Raw);
                h_deco_v22Sub->Divide(h_den_v22Sub);
                h_deco_v22Imp->Divide(h_den_v22Imp);
                h_deco_v22Zyam->Divide(h_den_v22Zyam);
                h_deco_v33Raw->Divide(h_den_v33Raw);
                h_deco_v33Sub->Divide(h_den_v33Sub);

                h_deco_v11Raw->Write();
                h_deco_v22Raw->Write();
                h_deco_v22Sub->Write();
                h_deco_v22Imp->Write();
                h_deco_v22Zyam->Write();
                h_deco_v33Raw->Write();
                h_deco_v33Sub->Write();

                // ===============================
                // now also include the geometric decorrelation ...
                // ===============================

                // e2vec*e2vec is somewhat equivalent to r2 and so plot it....
                // sum over the relevant HM bin
                sprintf(namecorr,"he2ae2bvec_%d",whichcentlow);
                TProfile *he2ae2bvec = static_cast <TProfile *> (f->Get(namecorr));
                sprintf(namecorr,"he3ae3bvec_%d",whichcentlow);
                TProfile *he3ae3bvec = static_cast <TProfile *> (f->Get(namecorr));
                // if there are at least two centralities to add, then sum them...
                if (whichcentlow != whichcenthigh) {
                  for (int whichcent=whichcentlow+1; whichcent<=whichcenthigh; whichcent++) {
                    sprintf(namecorr,"he2ae2bvec_%d",whichcent);
                    TProfile *he2ae2bvec_temp = static_cast <TProfile *> (f->Get(namecorr));
                    he2ae2bvec->Add(he2ae2bvec_temp);
                    sprintf(namecorr,"he3ae3bvec_%d",whichcent);
                    TProfile *he3ae3bvec_temp = static_cast <TProfile *> (f->Get(namecorr));
                    he3ae3bvec->Add(he3ae3bvec_temp);
                  }
                }

                sprintf(namecorr,"he2_%d",whichcentlow);
                TProfile *he2 = static_cast <TProfile *> (f->Get(namecorr));
                sprintf(namecorr,"he3_%d",whichcentlow);
                TProfile *he3 = static_cast <TProfile *> (f->Get(namecorr));
                // if there are at least two centralities to add, then sum them...
                if (whichcentlow != whichcenthigh) {
                  for (int whichcent=whichcentlow+1; whichcent<=whichcenthigh; whichcent++) {
                    sprintf(namecorr,"he3_%d",whichcent);
                    TProfile *he3_temp = static_cast <TProfile *> (f->Get(namecorr));
                    he3->Add(he3_temp);
                  }
                }

                int ngbins = 16;

                TH1F* h_geom_decor_2 = new TH1F(Form("h_geom_decor_2_Nch%d",index_Nch), "", Neta, detapos_out_range);
                TH1F* h_geom_decor_3 = new TH1F(Form("h_geom_decor_3_Nch%d",index_Nch), "", Neta, detapos_out_range);

                TGraph *geomdecorr = new TGraph();
                TGraph *geomdecorr3 = new TGraph();
                for (int i=0;i<ngbins/2;i++) {
                  int inum = ngbins/2-1 - i;
                  int iden = ngbins/2 + i;
                  //double eta = -3.75 + 0.5*(double) i;
                  double decorr_temp_den = he2ae2bvec->GetBinContent(iden+1) /
                    (he2->GetBinContent(iden+1) * he2->GetBinContent(ngbins+1));
                  double decorr_temp_num = he2ae2bvec->GetBinContent(inum+1) /
                    (he2->GetBinContent(inum+1) * he2->GetBinContent(ngbins+1));
                    h_geom_decor_2->SetBinContent(i+1,decorr_temp_num/decorr_temp_den);
                  h_geom_decor_2->SetBinError(i+1,0.0001);
                  //r3
                  double decorr3_temp_den = he3ae3bvec->GetBinContent(iden+1) /
                    (he3->GetBinContent(iden+1) * he3->GetBinContent(ngbins+1));
                  double decorr3_temp_num = he3ae3bvec->GetBinContent(inum+1) /
                    (he3->GetBinContent(inum+1) * he3->GetBinContent(ngbins+1));
                  h_geom_decor_3->SetBinContent(i+1,decorr3_temp_num/decorr3_temp_den);
                  h_geom_decor_3->SetBinError(i+1,0.000001);
                }
                h_geom_decor_2->Write();
                h_geom_decor_3->Write();


        }



}
