#include "plotCommon.h"

void Cluster_tower_comp() {
    initStyle();
    // outfile saves deviations
    TFile* fout = new TFile("../output/clus_tower_comp.root","RECREATE");
    // input files
    TFile* fin1 = new TFile("../output/Results_default.root","READ");
    TFile* fin2 = new TFile("../output/Results_trackTower.root","READ");


    TH1F* h_var_1 = (TH1F*)fin1->Get("h_deco_v22Sub");
    TH1F* h_var_2 = (TH1F*)fin2->Get("h_deco_v22Sub");


    TGraphAsymmErrors* g_var_1 = new TGraphAsymmErrors(h_var_1);
    TGraphAsymmErrors* g_var_2 = new TGraphAsymmErrors(h_var_2);

    shiftGraph(g_var_1,-0.01);
    shiftGraph(g_var_2, 0.01);

    TH1F* h_sys      = (TH1F*)h_var_1->Clone( Form("h_r2_sysSym") );
    TH1F* h_sys_low  = (TH1F*)h_var_1->Clone( Form("h_r2_sysLow") );
    TH1F* h_sys_high = (TH1F*)h_var_1->Clone( Form("h_r2_sysHigh") );
    h_sys     ->Reset();
    h_sys_low ->Reset();
    h_sys_high->Reset();

    for (int ig = 0; ig < h_var_1->GetNbinsX(); ig++) {
        float error_low = 0;
        float error_high = 0;

        float rel_error1 = h_var_1->GetBinContent(ig+1) - h_var_2->GetBinContent(ig+1);
        if (rel_error1 > 0) {
            error_high = fabs(rel_error1);
        } else {
            error_low = fabs(rel_error1);
        }
        // set rel_error2 if there is a second variation
        float rel_error2 = 0;
        if (rel_error2 > 0) {
            error_high = TMath::Max(fabs(rel_error2), error_high);
        } else {
            error_low = TMath::Max(fabs(rel_error2), error_low);
        }

        float error = TMath::Max(fabs(rel_error1), fabs(rel_error2));

        // fill absolute error
        h_sys->SetBinContent(ig+1, error);
        h_sys->SetBinError  (ig+1, 0);
        h_sys_low->SetBinContent(ig+1, error_low);
        h_sys_low->SetBinError  (ig+1, 0);
        h_sys_high->SetBinContent(ig+1, error_high);
        h_sys_high->SetBinError  (ig+1, 0);
    }

    fout->cd();
    h_sys->Write();
    h_sys_low->Write();
    h_sys_high->Write();
    delete h_sys;       h_sys=0;
    delete h_sys_low;   h_sys_low=0;
    delete h_sys_high;  h_sys_high=0;


    TH1F* h_ratio = (TH1F*)h_var_1->Clone(Form("h_ratio"));
    h_ratio->Divide(h_var_2);

    TGraphAsymmErrors* gr_ratio = (TGraphAsymmErrors*) ratioHistAbsErr(h_var_1,h_var_2);

    TF1* fit_var1 = new TF1("fit_var1","1-2*[0]*x",0,2.5);
    fit_var1->SetLineColor(kBlue);
    fit_var1->SetLineStyle(2);
    g_var_1->Fit(fit_var1);
    float f2_var1 = fit_var1->GetParameter(0);
    float f2_err_var1 = fit_var1->GetParError(0);

    TF1* fit_var2 = new TF1("fit_var2","1-2*[0]*x",0,2.5);
    fit_var2->SetLineColor(kRed);
    fit_var2->SetLineStyle(2);
    g_var_2->Fit(fit_var2);
    float f2_var2 = fit_var2->GetParError(0);
    float f2_err_var2 = fit_var2->GetParError(0);


    // fit function to determine significant difference in slope
    TF1 *func = new TF1("fit_y","(1+2*[0]*x)", 0, 2.5);
    func->SetLineColor(kBlue);
    func->SetLineStyle(2);
    gr_ratio->Fit(func);

    float fitSlopeVal = func->GetParameter(0);
    float fitSlopeErr = func->GetParError(0);
    // calculate significance of deviation from null hyp (no slope)
    float z_score = fabs(fitSlopeVal)/fitSlopeErr;
    float pValue = 2.*(1.-0.5*(1+TMath::Erf(z_score/sqrt(2))));

    TCanvas* c1 = new TCanvas("c1","New Canvas",50,50,700,600);
    TPad *pad2 = new TPad("pad2", "",0.,0.0,1.0,0.3);
    TPad *pad1 = new TPad("pad1", "",0.0,0.3,1.0,1.0);
    pad1->Draw();
    pad2->Draw();

    pad1->cd();
    gPad->SetBottomMargin(0);

    h_frame_r2->Draw("AXIS");

    g_var_1->SetMarkerColor(4);
    g_var_1->SetLineColor(4);
    g_var_1->SetMarkerStyle(21);
    g_var_1->Draw("PSAME");

    g_var_2->SetMarkerColor(2);
    g_var_2->SetLineColor(2);
    g_var_2->SetMarkerStyle(20);
    g_var_2->Draw("PSAME");


    line1->Draw("SAME");

    myText(           0.2,0.90,1,"#font[72]{ATLAS} Internal",0.06);
    myText(          0.2,0.83,1,"2017 5 TeV #it{pp} Track-Cluster Correlation",0.06);
    myText(          0.2,0.76,1,"0.5 < p_{T}^{trk,clus} < 5 GeV, #||{#eta^{ref}} > 4.0, 80 < N_{ch} < 150",0.06);

    myMarkerLineText(0.25,0.26-0.13, 1.2, 4, 21, 4, 1,"cluster-track", 0.06, true);//fin1
    myMarkerLineText(0.25,0.19-0.13, 1.2, 2, 20, 2, 1,"tower-track", 0.06, true);//fin1
    myMarkerLineText(0.55,0.26-0.13, 0, 2, kBlue, kBlue, 2,Form("Fit #it{F}_{2}=%0.3f#pm%0.4f  ",f2_var1,f2_err_var1), 0.06,true);
    myMarkerLineText(0.55,0.19-0.13, 0, 2, kRed, kRed, 2,Form("Fit #it{F}_{2}=%0.3f#pm%0.4f  ",f2_var2,f2_err_var2), 0.06,true);


    //ratio
    pad2->cd();
    gPad->SetTopMargin(0);
    gPad->SetBottomMargin(0.30);


    h_frame_r2_ratio->Draw("AXIS");
    line1->Draw("SAME");

/*
    h_ratio->SetMarkerColor(1);
    h_ratio->SetLineColor(1);
    h_ratio->SetMarkerStyle(21);
    h_ratio->Draw("ex0 SAME");*/
//    h_ratio_m->Draw("PSAME");

    gr_ratio->Draw("PSAME");
    gr_ratio->SetMarkerColor(1);
    gr_ratio->SetLineColor  (1);
    gr_ratio->SetMarkerStyle(20);
    gr_ratio->SetLineWidth(2);


    myMarkerLineText(0.3,0.85, 0, 2, kBlue, kBlue, 2,Form("Fit (1+2*A*%s): A=%0.3f#pm%0.4f  #it{p}-val=%0.2f",etaAString.c_str(),fitSlopeVal,fitSlopeErr,pValue), 0.13,true);



    c1->SaveAs("../figures/ClusterTower_comp_r2_r2.pdf");




    ////////////////////////////////////////////
    // v22  plot
    //Sub
    TH1F* hden1_v22_sub = (TH1F*)fin1->Get("hNch0_den_v22Sub_deta");
    TH1F* hden2_v22_sub = (TH1F*)fin2->Get("hNch0_den_v22Sub_deta");
    TH1F* hnum1_v22_sub = (TH1F*)fin1->Get("hNch0_num_v22Sub_deta");
    TH1F* hnum2_v22_sub = (TH1F*)fin2->Get("hNch0_num_v22Sub_deta");

    //Raw
    TH1F* hden1_v22_raw = (TH1F*)fin1->Get("hNch0_den_v22Raw_deta");
    TH1F* hden2_v22_raw = (TH1F*)fin2->Get("hNch0_den_v22Raw_deta");
    TH1F* hnum1_v22_raw = (TH1F*)fin1->Get("hNch0_num_v22Raw_deta");
    TH1F* hnum2_v22_raw = (TH1F*)fin2->Get("hNch0_num_v22Raw_deta");

    TGraphAsymmErrors* gden1_v22_sub = new TGraphAsymmErrors(hden1_v22_sub);
    TGraphAsymmErrors* gden2_v22_sub = new TGraphAsymmErrors(hden2_v22_sub);
    TGraphAsymmErrors* gnum1_v22_sub = new TGraphAsymmErrors(hnum1_v22_sub);
    TGraphAsymmErrors* gnum2_v22_sub = new TGraphAsymmErrors(hnum2_v22_sub);

    TGraphAsymmErrors* gden_v22_sub_ratio = (TGraphAsymmErrors*) ratioHistAbsErr(hden1_v22_sub,hden2_v22_sub);
    TGraphAsymmErrors* gnum_v22_sub_ratio = (TGraphAsymmErrors*) ratioHistAbsErr(hnum1_v22_sub,hnum2_v22_sub);

    for (int ig=0; ig<gden1_v22_sub->GetN(); ig++) {
           gden1_v22_sub->GetX()[ig] -= 0.03;
           gden2_v22_sub->GetX()[ig] -= 0.03;
           gnum1_v22_sub->GetX()[ig] -= 0.00;
           gnum2_v22_sub->GetX()[ig] -= 0.03;
           gden1_v22_sub->GetEXlow()[ig] = 0;
           gden2_v22_sub->GetEXlow()[ig] = 0;
           gnum1_v22_sub->GetEXlow()[ig] = 0;
           gnum2_v22_sub->GetEXlow()[ig] = 0;
           gden1_v22_sub->GetEXhigh()[ig] = 0;
           gden2_v22_sub->GetEXhigh()[ig] = 0;
           gnum1_v22_sub->GetEXhigh()[ig] = 0;
           gnum2_v22_sub->GetEXhigh()[ig] = 0;
    }

    TGraphAsymmErrors* gden1_v22_raw = new TGraphAsymmErrors(hden1_v22_raw);
    TGraphAsymmErrors* gden2_v22_raw = new TGraphAsymmErrors(hden2_v22_raw);
    TGraphAsymmErrors* gnum1_v22_raw = new TGraphAsymmErrors(hnum1_v22_raw);
    TGraphAsymmErrors* gnum2_v22_raw = new TGraphAsymmErrors(hnum2_v22_raw);

    TGraphAsymmErrors* gden_v22_raw_ratio = (TGraphAsymmErrors*) ratioHistAbsErr(hden1_v22_raw,hden2_v22_raw);
    TGraphAsymmErrors* gnum_v22_raw_ratio = (TGraphAsymmErrors*) ratioHistAbsErr(hnum1_v22_raw,hnum2_v22_raw);

    for (int ig=0; ig<gden1_v22_raw->GetN(); ig++) {
          // gden1_v22_raw->GetX()[ig] -= 0.00;
          // gden2_v22_raw->GetX()[ig] -= 0.03;
          // gnum1_v22_raw->GetX()[ig] -= 0.00;
          // gnum2_v22_raw->GetX()[ig] -= 0.03;
           gden1_v22_raw->GetEXlow()[ig] = 0;
           gden2_v22_raw->GetEXlow()[ig] = 0;
           gnum1_v22_raw->GetEXlow()[ig] = 0;
           gnum2_v22_raw->GetEXlow()[ig] = 0;
           gden1_v22_raw->GetEXhigh()[ig] = 0;
           gden2_v22_raw->GetEXhigh()[ig] = 0;
           gnum1_v22_raw->GetEXhigh()[ig] = 0;
           gnum2_v22_raw->GetEXhigh()[ig] = 0;
           gnum_v22_raw_ratio->GetX()[ig] += 0.02;
           gden_v22_raw_ratio->GetX()[ig] += 0.02;
    }


    TCanvas* c2 = new TCanvas("c2","c2",50,50,700,600);
    TPad *pad3 = new TPad("pad3", "",0.,0.0,1.0,0.3);
    TPad *pad4 = new TPad("pad4", "",0.0,0.3,1.0,1.0);
    pad4->Draw();
    pad3->Draw();

    pad4->cd();
    gPad->SetBottomMargin(0);

    h_frame_v22->Draw("AXIS");

    gden1_v22_sub->SetMarkerColor(4);
    gden1_v22_sub->SetLineColor(4);
    gden1_v22_sub->SetMarkerStyle(21);
    gden1_v22_sub->Draw("PSAME");
    gnum1_v22_sub->SetMarkerColor(4);
    gnum1_v22_sub->SetLineColor(4);
    gnum1_v22_sub->SetMarkerStyle(21);
    gnum1_v22_sub->Draw("PSAME");

    gnum2_v22_sub->SetMarkerColor(2);
    gnum2_v22_sub->SetLineColor(2);
    gnum2_v22_sub->SetMarkerStyle(20);
    gnum2_v22_sub->Draw("PSAME");
    gden2_v22_sub->SetMarkerColor(2);
    gden2_v22_sub->SetLineColor(2);
    gden2_v22_sub->SetMarkerStyle(20);
    gden2_v22_sub->Draw("PSAME");


    gden1_v22_raw->SetMarkerColor(4);
    gden1_v22_raw->SetLineColor(4);
    gden1_v22_raw->SetMarkerStyle(25);
    gnum1_v22_raw->SetMarkerColor(4);
    gnum1_v22_raw->SetLineColor(4);
    gnum1_v22_raw->SetMarkerStyle(25);
    gden1_v22_raw->Draw("PSAME");
    gnum1_v22_raw->Draw("PSAME");

    gden2_v22_raw->SetMarkerColor(2);
    gden2_v22_raw->SetLineColor(2);
    gden2_v22_raw->SetMarkerStyle(24);
    gnum2_v22_raw->SetMarkerColor(2);
    gnum2_v22_raw->SetLineColor(2);
    gnum2_v22_raw->SetMarkerStyle(24);
    gden2_v22_raw->Draw("PSAME");
    gnum2_v22_raw->Draw("PSAME");


    myMarkerLineText(0.25,0.25-0.1, 1.2, 4, 21, 4, 1,"template fit, cluster-track", 0.06, true);
    myMarkerLineText(0.25,0.18-0.1, 1.2, 2, 20, 2, 1,"template fit, tower-track", 0.06, true);
    myMarkerLineText(0.65,0.25-0.1, 1.2, 4, 25, 4, 1,"raw fourier cluster-track", 0.06, true);
    myMarkerLineText(0.65,0.18-0.1, 1.2, 2, 24, 2, 1,"raw fourier tower-track", 0.06, true);

    myText(           0.2,0.90,1,"#font[72]{ATLAS} Internal",0.06);
    myText(          0.2,0.83,1,"2017 5 TeV #it{pp} Track-Cluster Correlation",0.06);
    myText(          0.2,0.76,1,"0.5 < p_{T}^{trk,clus} < 5 GeV, #||{#eta^{ref}} > 4.0, 80 < N_{ch} < 150",0.06);

    //ratio
    pad3->cd();
    gPad->SetTopMargin(0);
    gPad->SetBottomMargin(0.30);

    h_frame_v22_ratio->Draw("AXIS");
    line1->Draw("SAME");

    gden_v22_raw_ratio->SetMarkerColor(1);
    gden_v22_raw_ratio->SetLineColor(1);
    gden_v22_raw_ratio->SetMarkerStyle(25);
    gden_v22_raw_ratio->SetLineWidth(1);
    gden_v22_raw_ratio->Draw("PSAME");
    gnum_v22_raw_ratio->SetMarkerColor(1);
    gnum_v22_raw_ratio->SetLineColor(1);
    gnum_v22_raw_ratio->SetMarkerStyle(25);
    gnum_v22_raw_ratio->SetLineWidth(1);
    gnum_v22_raw_ratio->Draw("PSAME");

    gden_v22_sub_ratio->SetMarkerColor(1);
    gden_v22_sub_ratio->SetLineColor(1);
    gden_v22_sub_ratio->SetMarkerStyle(20);
    gden_v22_sub_ratio->SetLineWidth(1);
    gden_v22_sub_ratio->Draw("PSAME");
    gnum_v22_sub_ratio->SetMarkerColor(1);
    gnum_v22_sub_ratio->SetLineColor(1);
    gnum_v22_sub_ratio->SetMarkerStyle(20);
    gnum_v22_sub_ratio->SetLineWidth(1);
    gnum_v22_sub_ratio->Draw("PSAME");


    c2->SaveAs("../ClusterTower_comp_v22.pdf");


    }
