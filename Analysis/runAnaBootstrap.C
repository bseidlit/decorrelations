#include "CorrelationButton/CorrelationMaker.h"
#include "CorrelationButton/NonFlowSubtractor.C"
#include"figmaker/plotCommon.h"

int runAnaBootstrap() {

const float numExp = 2;

for (int iExp=0; iExp<numExp; iExp++){

    cout << endl << "-------------  exp " << iExp << "  -----------------";

    FlowAnaConfig* theConfig1 = new FlowAnaConfig();
    FlowAnaConfig* theConfig2 = new FlowAnaConfig();

    bool _saveFit = true;

    // configurate the analyss
    // input/output
    theConfig1->setInputPath("./input/");
    theConfig2->setInputPath("./input/");
    theConfig2->setOutputPath("./output/");
    theConfig1->setOutputPath("./output/");
    theConfig1->setOutputFigPath("./plots");
    theConfig2->setOutputFigPath("./plots");
    theConfig1->setInputFileName(Form("corr_randSplit%dof2_2",iExp));
    theConfig2->setInputFileName(Form("corr_randSplit%dof2_2",iExp));
    theConfig1->setOutputFileName(Form("Results_randSplit%dof2_2",iExp));

    const int Neta = 5;
    float detapos_out_range[Neta+1] = {0, 0.5, 1.0, 1.5, 2.0, 2.5};
    float detaneg_out_range[Neta+1] = {0,-0.5,-1.0,-1.5,-2.0,-2.5};

    theConfig1->setCorrHistPathSame("Correlation_2D_raw/h2pc_sig_A_");
    theConfig1->setCorrHistPathMix ("Correlation_2D_raw/h2pc_mix_A_");
    theConfig1->setTrigYieldHistName("h2_nsig_raw");
    theConfig1->setMixTrigYieldHistName("h2_nsig_raw");

    theConfig2->setCorrHistPathSame("Correlation_2D_raw/h2pc_sig_C_");
    theConfig2->setCorrHistPathMix ("Correlation_2D_raw/h2pc_mix_C_");
    theConfig2->setTrigYieldHistName("h2_nsig_raw");
    theConfig2->setMixTrigYieldHistName("h2_nsig_raw");

    // gap/referece/cut
    theConfig1->setReference (40, 60); // LM reference Nch selection
    //theConfig1->setReference2(30, 40); // LM2 selection
    theConfig1->setEtaRange (0.0, 8.0); // Gap
    theConfig1->setCorrEtaBoundary(8.0);
    theConfig1->setCorrEtaInterval(2.0);

    theConfig2->setReference (40, 60); // LM reference Nch selection
    //theConfig2->setReference2(30, 40); // LM2 selection
    theConfig2->setEtaRange (0.0, 8.0); // Gap
    theConfig2->setCorrEtaBoundary(8.0);
    theConfig2->setCorrEtaInterval(2.0);

    int iPtyMethod = 1;
    // addtional interface for configuring the whole analysis
    // pt/Nch/... binning of outputs
    // inconsistency between input and ouput binning will be checked since input bins can only be merged to produce output
    const int Nbins_out_Nch = 1;
    float dNch_out_range[Nbins_out_Nch+1] = {80,150};

    const int Nbins_out_pt = 9;
    float dpt_out_range[Nbins_out_pt+1] = {0.5,1,1.5,2,2.5,3,3.5,4,4.5,5};

    const int Nbins_out_eta = 10;
    float deta_out_range[Nbins_out_eta+1] = {-2.5, -2.0, -1.5, -1.0, -0.5, 0, 0.5, 1.0, 1.5, 2.0, 2.5};

    TH1F* heta_binning = new TH1F("heta_binning","",Nbins_out_eta,deta_out_range);

    theConfig1->setInputThirdBinningName("heta_binning");
    theConfig1->setThirdDimensionName("eta");
    theConfig1->setOutputMultiBinning(Nbins_out_Nch, dNch_out_range);
    theConfig1->setOutputPtBinning(Nbins_out_pt, dpt_out_range);
    theConfig1->setOutputThirdBinning(Nbins_out_eta, deta_out_range);
    theConfig2->setInputThirdBinningName("heta_binning");
    theConfig2->setThirdDimensionName("eta");
    theConfig2->setOutputMultiBinning(Nbins_out_Nch, dNch_out_range);
    theConfig2->setOutputPtBinning(Nbins_out_pt, dpt_out_range);
    theConfig2->setOutputThirdBinning(Nbins_out_eta, deta_out_range);

    CorrelationMaker* theMaker1 = new CorrelationMaker();
    CorrelationMaker* theMaker2 = new CorrelationMaker();



    if (!theConfig1->init() || !theConfig2->init()) {
        return 1;
    }
    theConfig1->setInputThirdBinning(heta_binning);
    theConfig2->setInputThirdBinning(heta_binning);


    theMaker1->setConfig(theConfig1);
    theMaker1->setStatCorrection(false);
    theMaker2->setConfig(theConfig2);
    theMaker2->setStatCorrection(false);

    theMaker1->init();
    theMaker2->init();

    TCanvas* c1;
    TCanvas* c2;


    // decorrelation
    for (int index_Nch = 1; index_Nch < 2; index_Nch++) {
        float _Nch_low  = 80;
        float _Nch_high = 150;
        float _pt_trig_low  = 0.5;
        float _pt_trig_high = 5.0;

        TH1F* h_den_v22Raw = new TH1F(Form("hNch0_den_v22Raw_deta"), "", Neta, detapos_out_range);
        TH1F* h_den_v33Raw = new TH1F(Form("hNch0_den_v33Raw_deta"), "", Neta, detapos_out_range);
        TH1F* h_den_v22Sub = new TH1F(Form("hNch0_den_v22Sub_deta"), "", Neta, detapos_out_range);
        TH1F* h_den_v33Sub = new TH1F(Form("hNch0_den_v33Sub_deta"), "", Neta, detapos_out_range);
        TH1F* h_den_v22Imp = new TH1F(Form("hNch0_den_v22Imp_deta"), "", Neta, detapos_out_range);
        TH1F* h_den_v33Imp = new TH1F(Form("hNch0_den_v33Imp_deta"), "", Neta, detapos_out_range);
        TH1F* h_num_v22Raw = new TH1F(Form("hNch0_num_v22Raw_deta"), "", Neta, detaneg_out_range);
        TH1F* h_num_v33Raw = new TH1F(Form("hNch0_num_v33Raw_deta"), "", Neta, detaneg_out_range);
        TH1F* h_num_v22Sub = new TH1F(Form("hNch0_num_v22Sub_deta"), "", Neta, detaneg_out_range);
        TH1F* h_num_v33Sub = new TH1F(Form("hNch0_num_v33Sub_deta"), "", Neta, detaneg_out_range);
        TH1F* h_num_v22Imp = new TH1F(Form("hNch0_num_v22Imp_deta"), "", Neta, detaneg_out_range);
        TH1F* h_num_v33Imp = new TH1F(Form("hNch0_num_v33Imp_deta"), "", Neta, detaneg_out_range);

        TH1F* h_num_v22Raw1 = new TH1F(Form("hNch0_num_v22Raw_deta1"), "", Neta, detapos_out_range);
        TH1F* h_num_v33Raw1 = new TH1F(Form("hNch0_num_v33Raw_deta1"), "", Neta, detapos_out_range);
        TH1F* h_num_v22Sub1 = new TH1F(Form("hNch0_num_v22Sub_deta1"), "", Neta, detapos_out_range);
        TH1F* h_num_v33Sub1 = new TH1F(Form("hNch0_num_v33Sub_deta1"), "", Neta, detapos_out_range);
        TH1F* h_num_v22Imp1 = new TH1F(Form("hNch0_num_v22Imp_deta1"), "", Neta, detapos_out_range);
        TH1F* h_num_v33Imp1 = new TH1F(Form("hNch0_num_v33Imp_deta1"), "", Neta, detapos_out_range);

        for (int index_eta = 1; index_eta < Neta+1; index_eta++) {
            float _eta_pos_low  = h_den_v22Raw->GetXaxis()->GetBinLowEdge(index_eta);
            float _eta_pos_high = h_den_v22Raw->GetXaxis()->GetBinUpEdge (index_eta);
            float _eta_neg_low  = -1*_eta_pos_high;
            float _eta_neg_high = -1*_eta_pos_low;

            NonFlowSubtractor subTool_den;
            NonFlowSubtractor subTool_num;
            subTool_den.init();
            subTool_num.init();

            TH1* hist_den1_LM = theMaker1->MakeCorr(_pt_trig_low, _pt_trig_high, theConfig1->getReferenceLow(),theConfig1->getReferenceHigh(), _eta_pos_low, _eta_pos_high, iPtyMethod);
            TH1* hist_den1_HM = theMaker1->MakeCorr(_pt_trig_low, _pt_trig_high, _Nch_low, _Nch_high, _eta_pos_low, _eta_pos_high, iPtyMethod);
            hist_den1_LM->SetName(Form("hist_den1_LM_eta%d", index_eta));
            hist_den1_HM->SetName(Form("hist_den1_HM_eta%d", index_eta));

            TH1* hist_den2_LM = theMaker2->MakeCorr(_pt_trig_low, _pt_trig_high, theConfig2->getReferenceLow(),theConfig2->getReferenceHigh(), _eta_neg_low, _eta_neg_high, iPtyMethod);
            TH1* hist_den2_HM = theMaker2->MakeCorr(_pt_trig_low, _pt_trig_high, _Nch_low, _Nch_high, _eta_neg_low, _eta_neg_high, iPtyMethod);
            hist_den2_LM->SetName(Form("hist_den2_LM_eta%d", index_eta));
            hist_den2_HM->SetName(Form("hist_den2_HM_eta%d", index_eta));

            TH1* hist_den_LM = (TH1*)hist_den1_LM->Clone(Form("hist_den_LM_eta%d", index_eta));
            TH1* hist_den_HM = (TH1*)hist_den1_HM->Clone(Form("hist_den_HM_eta%d", index_eta));
            hist_den_LM->Add(hist_den2_LM);
            hist_den_HM->Add(hist_den2_HM);

            TH1* hist_num1_LM = theMaker1->MakeCorr(_pt_trig_low, _pt_trig_high, theConfig1->getReferenceLow(),theConfig1->getReferenceHigh(), _eta_neg_low, _eta_neg_high, iPtyMethod);
            TH1* hist_num1_HM = theMaker1->MakeCorr(_pt_trig_low, _pt_trig_high, _Nch_low, _Nch_high, _eta_neg_low, _eta_neg_high, iPtyMethod);
            hist_num1_LM->SetName(Form("hist_num1_LM_eta%d", index_eta));
            hist_num1_HM->SetName(Form("hist_num1_HM_eta%d", index_eta));

            TH1* hist_num2_LM = theMaker2->MakeCorr(_pt_trig_low, _pt_trig_high, theConfig1->getReferenceLow(),theConfig1->getReferenceHigh(), _eta_pos_low, _eta_pos_high, iPtyMethod);
            TH1* hist_num2_HM = theMaker2->MakeCorr(_pt_trig_low, _pt_trig_high, _Nch_low, _Nch_high, _eta_pos_low, _eta_pos_high, iPtyMethod);
            hist_num2_LM->SetName(Form("hist_num2_LM_eta%d", index_eta));
            hist_num2_HM->SetName(Form("hist_num2_HM_eta%d", index_eta));

            TH1* hist_num_LM = (TH1*)hist_num1_LM->Clone(Form("hist_num_LM_eta%d", index_eta));
            TH1* hist_num_HM = (TH1*)hist_num1_HM->Clone(Form("hist_num_HM_eta%d", index_eta));
            hist_num_LM->Add(hist_num2_LM);
            hist_num_HM->Add(hist_num2_HM);


            // apply subtraction here
            subResult theResult_den = subTool_den.templateFit(hist_den_LM, hist_den_HM);
            subResult theResult_num = subTool_num.templateFit(hist_num_LM, hist_num_HM);

            h_den_v22Raw->SetBinContent(index_eta, theResult_den.getV22RawValue());
            h_den_v22Raw->SetBinError  (index_eta, theResult_den.getV22RawError());
            h_den_v22Sub->SetBinContent(index_eta, theResult_den.getV22SubValue());
            h_den_v22Sub->SetBinError  (index_eta, theResult_den.getV22SubError());
            h_den_v33Raw->SetBinContent(index_eta, theResult_den.getV33RawValue());
            h_den_v33Raw->SetBinError  (index_eta, theResult_den.getV33RawError());
            h_den_v33Sub->SetBinContent(index_eta, theResult_den.getV33SubValue());
            h_den_v33Sub->SetBinError  (index_eta, theResult_den.getV33SubError());

            h_num_v22Raw->SetBinContent(index_eta, theResult_num.getV22RawValue());
            h_num_v22Raw->SetBinError  (index_eta, theResult_num.getV22RawError());
            h_num_v22Sub->SetBinContent(index_eta, theResult_num.getV22SubValue());
            h_num_v22Sub->SetBinError  (index_eta, theResult_num.getV22SubError());
            h_num_v33Raw->SetBinContent(index_eta, theResult_num.getV33RawValue());
            h_num_v33Raw->SetBinError  (index_eta, theResult_num.getV33RawError());
            h_num_v33Sub->SetBinContent(index_eta, theResult_num.getV33SubValue());
            h_num_v33Sub->SetBinError  (index_eta, theResult_num.getV33SubError());

            h_num_v22Raw1->SetBinContent(index_eta, theResult_num.getV22RawValue());
            h_num_v22Raw1->SetBinError  (index_eta, theResult_num.getV22RawError());
            h_num_v22Sub1->SetBinContent(index_eta, theResult_num.getV22SubValue());
            h_num_v22Sub1->SetBinError  (index_eta, theResult_num.getV22SubError());
            h_num_v33Raw1->SetBinContent(index_eta, theResult_num.getV33RawValue());
            h_num_v33Raw1->SetBinError  (index_eta, theResult_num.getV33RawError());
            h_num_v33Sub1->SetBinContent(index_eta, theResult_num.getV33SubValue());
            h_num_v33Sub1->SetBinError  (index_eta, theResult_num.getV33SubError());

            delete hist_den_LM;
            delete hist_den_HM;
            delete hist_num_LM;
            delete hist_num_HM;
        }
        theConfig1->outputFile()->cd();
        h_den_v22Raw->Write();
        h_den_v33Raw->Write();
        h_den_v22Sub->Write();
        h_den_v33Sub->Write();
        h_num_v22Raw->Write();
        h_num_v33Raw->Write();
        h_num_v22Sub->Write();
        h_num_v33Sub->Write();

        h_num_v22Raw1->Write();
        h_num_v33Raw1->Write();
        h_num_v22Sub1->Write();
        h_num_v33Sub1->Write();

        TH1F* h_deco_v22Raw = (TH1F*)h_num_v22Raw1->Clone("h_deco_v22Raw");
        TH1F* h_deco_v33Raw = (TH1F*)h_num_v33Raw1->Clone("h_deco_v33Raw");
        TH1F* h_deco_v22Sub = (TH1F*)h_num_v22Sub1->Clone("h_deco_v22Sub");
        TH1F* h_deco_v33Sub = (TH1F*)h_num_v33Sub1->Clone("h_deco_v33Sub");

        h_deco_v22Raw->Divide(h_den_v22Raw);
        h_deco_v33Raw->Divide(h_den_v33Raw);
        h_deco_v22Sub->Divide(h_den_v22Sub);
        h_deco_v33Sub->Divide(h_den_v33Sub);

        TGraphAsymmErrors* g_deco_v22Raw_absErr = (TGraphAsymmErrors*) ratioHistAbsErr(h_num_v22Raw1,h_den_v22Raw);
        TGraphAsymmErrors* g_deco_v33Raw_absErr = (TGraphAsymmErrors*) ratioHistAbsErr(h_num_v33Raw1,h_den_v33Raw);
        TGraphAsymmErrors* g_deco_v22Sub_absErr = (TGraphAsymmErrors*) ratioHistAbsErr(h_num_v22Sub1,h_den_v22Sub);
        TGraphAsymmErrors* g_deco_v33Sub_absErr = (TGraphAsymmErrors*) ratioHistAbsErr(h_num_v33Sub1,h_den_v33Sub);

        g_deco_v22Raw_absErr->SetName("g_deco_v22Raw_absErr");
        g_deco_v33Raw_absErr->SetName("g_deco_v33Raw_absErr");
        g_deco_v22Sub_absErr->SetName("g_deco_v22Sub_absErr");
        g_deco_v33Sub_absErr->SetName("g_deco_v33Sub_absErr");



        h_deco_v22Raw->Write();
        h_deco_v33Raw->Write();
        h_deco_v22Sub->Write();
        h_deco_v33Sub->Write();

        g_deco_v22Raw_absErr->Write();
        g_deco_v33Raw_absErr->Write();
        g_deco_v22Sub_absErr->Write();
        g_deco_v33Sub_absErr->Write();
    }

    theConfig1->outputFile()->cd();
    theConfig1->getOutputMultiBinning()->Write();
    theConfig1->getOutputPtBinning()->Write();
    theConfig1->getOutputThirdBinning()->Write();
    theConfig1->outputFile()->Close();

    delete theConfig1,theConfig2,theMaker1,theMaker2;

  }

    return 0;
}
