#include "plotCommon.h"

void PlotPythia() {
    initStyle();
    TFile* fnom = new TFile("../output/Results_pythia.root","READ");
    string str_dataType = "#bf{PYTHIA} TRUTH #it{pp}";

    TH1F* h_nom = (TH1F*)fnom->Get("h_deco_v22Sub");
    TH1F* h_nom_raw = (TH1F*)fnom->Get("h_deco_v22Raw");


    TGraphAsymmErrors* g_nom = new TGraphAsymmErrors(h_nom);

    for (int ig=0; ig<g_nom->GetN(); ig++) {
           //g_nom->GetX()[ig] += 0.025;
           g_nom->GetEXlow ()[ig] = 0.0;
           g_nom->GetEXhigh()[ig] = 0.0;
    }


    TF1* fit_varN = new TF1("fit_varN","1-2.*[0]*x",0,2.5);
    fit_varN->SetLineColor(kBlack);
    fit_varN->SetLineStyle(2);
    g_nom->Fit(fit_varN);
    float f2_var2 = fit_varN->GetParameter(0);
    float f2_err_var2 = fit_varN->GetParError(0);

    TCanvas* c1 = new TCanvas("c1","New Canvas",50,50,700,600);
    h_frame_r2->Draw("AXIS");
    h_frame_r2->GetYaxis()->SetRangeUser(-0.1,1.7);

    g_nom->SetMarkerColor(1);
    g_nom->SetLineColor(1);
    g_nom->SetMarkerStyle(20);
    g_nom->Draw("PSAME");

    h_nom_raw->Draw("ex0 same");
    h_nom_raw->SetMarkerColor(4);
    h_nom_raw->SetLineColor(4);
    h_nom_raw->SetMarkerStyle(21);

    line1->Draw("SAME");

    myText(           0.2,0.90,1,"#font[72]{ATLAS} Internal",0.05);
    myText(          0.2,0.83,1,str_dataType.c_str(),0.05);
    myText(          0.2,0.76,1,"0.5 < p_{T}^{trk,clus} < 5 GeV, #||{#eta^{ref}} > 4.0, 80 < N_{ch} < 150",0.05);
    myText(          0.2,0.70,1,"80 < N_{ch} < 150",0.05);

    myMarkerLineText(0.25,0.35+0.02, 1.5, 4, 21, 4, 1,"w/o nonflow sub", 0.06, true);//fin1
    myMarkerLineText(0.25,0.27+0.02, 1.5, 1, 20, 1, 1,"Template fit", 0.06, true);//fin1
    myMarkerLineText(0.25,0.19+0.02, 0, 2, 1, 1, 2,Form("Fit #it{F}_{2}=%0.3f#pm%0.4f  ",f2_var2,f2_err_var2), 0.06,true);
  //  myMarkerLineText(0.65,0.26-0.13, 0, 2, kBlack, kBlack, 2,Form("Fit #it{f}_{2}=%0.3f#pm%0.4f  ",f2_var1,f2_err_var1), 0.06,true);



    c1->SaveAs("../figures/pythia/nominal_r2.pdf");




    ////////////////////////////////////////////
    // v22  plot
    //Sub
    TH1F* hdenN_v22_sub = (TH1F*)fnom->Get("hNch0_den_v22Sub_deta");
    TH1F* hnumN_v22_sub = (TH1F*)fnom->Get("hNch0_num_v22Sub_deta");

    //Raw
    TH1F* hdenN_v22_raw = (TH1F*)fnom->Get("hNch0_den_v22Raw_deta");
    TH1F* hnumN_v22_raw = (TH1F*)fnom->Get("hNch0_num_v22Raw_deta");

    TGraphAsymmErrors* gdenN_v22_sub = new TGraphAsymmErrors(hdenN_v22_sub);
    TGraphAsymmErrors* gnumN_v22_sub = new TGraphAsymmErrors(hnumN_v22_sub);

    for (int ig=0; ig<gdenN_v22_sub->GetN(); ig++) {
           gdenN_v22_sub->GetEXlow()[ig] = 0;
           gnumN_v22_sub->GetEXlow()[ig] = 0;
           gdenN_v22_sub->GetEXhigh()[ig] = 0;
           gnumN_v22_sub->GetEXhigh()[ig] = 0;
    }

    TGraphAsymmErrors* gdenN_v22_raw = new TGraphAsymmErrors(hdenN_v22_raw);
    TGraphAsymmErrors* gnumN_v22_raw = new TGraphAsymmErrors(hnumN_v22_raw);

    for (int ig=0; ig<gdenN_v22_raw->GetN(); ig++) {
           gdenN_v22_raw->GetEXlow()[ig] = 0;
           gnumN_v22_raw->GetEXlow()[ig] = 0;
           gdenN_v22_raw->GetEXhigh()[ig] = 0;
           gnumN_v22_raw->GetEXhigh()[ig] = 0;
    }


    TCanvas* c2 = new TCanvas("c2","c2",50,50,700,600);
    h_frame_v22->Draw("AXIS");
    h_frame_v22->GetYaxis()->SetRangeUser(-0.0005,0.004);
    line0->Draw("same");
    gnumN_v22_sub->SetMarkerColor(1);
    gnumN_v22_sub->SetLineColor(1);
    gnumN_v22_sub->SetMarkerStyle(20);
    gnumN_v22_sub->Draw("PSAME");
    gdenN_v22_sub->SetMarkerColor(1);
    gdenN_v22_sub->SetLineColor(1);
    gdenN_v22_sub->SetMarkerStyle(20);
    gdenN_v22_sub->Draw("PSAME");

    gdenN_v22_raw->SetMarkerColor(1);
    gdenN_v22_raw->SetLineColor(1);
    gdenN_v22_raw->SetMarkerStyle(24);
    gnumN_v22_raw->SetMarkerColor(1);
    gnumN_v22_raw->SetLineColor(1);
    gnumN_v22_raw->SetMarkerStyle(24);
    gdenN_v22_raw->Draw("PSAME");
    gnumN_v22_raw->Draw("PSAME");


    myMarkerLineText(0.25,0.18-0.1, 1.2, 1, 20, 1, 1,"template fit", 0.06, true);
    myMarkerLineText(0.65,0.18-0.1, 1.2, 1, 24, 1, 1,"raw fourier", 0.06, true);

    myText(           0.2,0.90,1,"#font[72]{ATLAS} Internal",0.05);
    myText(          0.2,0.83,1,str_dataType.c_str(),0.05);
    myText(          0.2,0.76,1,"0.5 < p_{T}^{trk,clus} < 5 GeV, #||{#eta^{ref}} > 4.0, 80 < N_{ch} < 150",0.05);
    myText(          0.2,0.69,1,"80 < N_{ch} < 150",0.05);

    c2->SaveAs("../figures/pythia/nominal_v22.pdf");


    }
