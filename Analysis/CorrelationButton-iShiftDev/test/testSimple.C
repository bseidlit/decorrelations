#include "../NonFlowSubtractor.C"
void testSimple() {

    TFile* fin = new TFile(Form("./correlationHistTest.root"),"READ");

    TH1F* h_correlation_LM  = (TH1F*)fin->Get( Form("PTYRaw/dphi_Nch_ref") );
    TH1F* h_correlation_HM  = (TH1F*)fin->Get( Form("PTYRaw/dphi_Nch6") );

    // Correlation for the second lowest LM bin 
    // will be used for correction
    TH1F* h_correlation_LM2 = (TH1F*)fin->Get( Form("PTYRaw/dphi_Nch2") );

    h_correlation_LM->Rebin(2);
    h_correlation_HM->Rebin(2);

    NonFlowSubtractor subTool;
    // change the fitter configure before call init() here
    //subTool.setAtlasFixedC3();
    //subTool.setAtlasFixedC4();
    subTool.setDebug();
    //subTool.setZYAM();
    subTool.init();
    subTool.setIshift(); // run test with I option shift
    //--------------------------------------------------
    // ATLAS template fit
    //--------------------------------------------------
    //subTool.setFtempOffset(0.05); // fractioanl offset for F_temp parameter, could be positive or negative 
    subResult theResult = subTool.templateFit(h_correlation_LM, h_correlation_HM);
    //subResult theResult = subTool.templateFit(h_correlation_LM, h_correlation_HM, 0.4, 0.6, 0.9);

    //subResult theResult = subTool.templateFit(h_correlation_LM, h_correlation_HM, h_correlation_LM2);
    //subResult theResult = subTool.referenceFit(h_correlation_LM, h_correlation_HM);
    //subResult theResult = subTool.templateHistFit(h_correlation_LM, h_correlation_HM, h_correlation_LM2);
    //subResult theResult = subTool.templateHistFit(h_correlation_LM, h_correlation_HM);

    //--------------------------------------------------
    // Test for using symmetrized dphi correlation
    // OK from first look
    // need further confirmation
    //--------------------------------------------------
    //TH1* hsym_LM = Symmetrize(h_correlation_LM);
    //hsym_LM->SetName("hs_LM");
    //TH1* hsym_HM = Symmetrize(h_correlation_HM);
    //hsym_HM->SetName("hs_HM");
    //subResult theResult = subTool.templateFit(hsym_LM, hsym_HM);

    /*
    */
    //--------------------------------------------------
    // Access the fitted results and plots
    //--------------------------------------------------
    cout << endl;
    cout << "======================================================" << endl;
    cout << "Test explicit interface for fitted c2 and c3: " << endl;
    cout << endl;
    cout << "Default  v22 = " << theResult.getV22SubValue()     << " +/- " << theResult.getV22SubError()    << endl;
    cout << "Improved v22 = " << theResult.getV22SubImpValue()  << " +/- " << theResult.getV22SubImpError() << endl;
    cout << "Default  v33 = " << theResult.getV33SubValue()     << " +/- " << theResult.getV33SubError()    << endl;
    cout << "Improved v33 = " << theResult.getV33SubImpValue()  << " +/- " << theResult.getV33SubImpError() << endl;

    //--------------------------------------------------
    // Access the fitted results via general function
    //--------------------------------------------------
    cout << endl;
    cout << "======================================================" << endl;
    cout << "Test general interface for fitted resutls: " << endl;
    cout << endl;
    cout << "Fourier  v22 = " << theResult.getCoeffRawValue(2)      << " +/- " << theResult.getCoeffRawError(2)    << endl;
    cout << "Default  v22 = " << theResult.getCoeffSubValue(2)      << " +/- " << theResult.getCoeffSubError(2)    << endl;
    cout << "Improved v22 = " << theResult.getCoeffSubImpValue(2)   << " +/- " << theResult.getCoeffSubImpError(2) << endl;
    cout << endl;
    cout << "Fourier  v33 = " << theResult.getCoeffRawValue(3)      << " +/- " << theResult.getCoeffRawError(3)    << endl;
    cout << "Default  v33 = " << theResult.getCoeffSubValue(3)      << " +/- " << theResult.getCoeffSubError(3)    << endl;
    cout << "Improved v33 = " << theResult.getCoeffSubImpValue(3)   << " +/- " << theResult.getCoeffSubImpError(3) << endl;
    cout << endl;
    cout << "Fourier  v44 = " << theResult.getCoeffRawValue(4)      << " +/- " << theResult.getCoeffRawError(4)    << endl;
    cout << "Default  v44 = " << theResult.getCoeffSubValue(4)      << " +/- " << theResult.getCoeffSubError(4)    << endl;
    cout << "Improved v44 = " << theResult.getCoeffSubImpValue(4)   << " +/- " << theResult.getCoeffSubImpError(4) << endl;


    TCanvas* c1 = new TCanvas("c1","scaling",50,50, 600,600);
    //TCanvas* c1 = new TCanvas("c1","scaling",50,50, 600,800);
    //subTool.plotAtlas3pHM(c1);
    //subTool.plotAtlasHistSubHM(c1);
    subTool.plotAtlasSubHM(c1);
    c1->cd();
    //c1->SaveAs("test.pdf");
    // add addtional lengends
    
    TCanvas* c2 = new TCanvas("c2","scaling",50,50, 600,600);
    subTool.plotAtlasLM(c2);
    c2->cd();
    // add addtional lengends

}
