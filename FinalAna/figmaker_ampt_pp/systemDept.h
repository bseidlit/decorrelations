
const int Nbins_out_Nch = 4;
float dNch_out_range[Nbins_out_Nch+1] = {0,10,40,80,160};

string sysTag = "AMPT #it{pp}";
string sysStr1 = "AMPT #it{pp} truth-turth";
string sysStr2 = "0.5 < #it{p}_{T}^{trk} < 5 GeV, #||{#it{#eta}^{twr}} > 4.0";

float LM_ref_plot_below = 50;
