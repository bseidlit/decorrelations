// Common plotting frames and strings


static TH1F *h_frame_r2;
static TH1F *h_frame_v22;
static TH1F *h_frame_v22_dCent;
static TH1F *h_frame_r2_ratio;
static TH1F *h_frame_v22_ratio;
static TH1F *h_frame_v22_dCent_ratio;
static TH1F *h_frame_v22_ref;
static TH1F *h_frame_d2Od1;
static TH1F* h_frame_vnn;
static TH1F* h_frame_Fn;
static TH1F* h_frame_Fn_pull;
static TH1F* h_frame_err_Fn;


static TGraph* line1;
static TGraph* line0;
static TGraph* line2;
static TGraph* linen2;


string etaAString = "#||{#it{#eta}^{a}}";

void initStyle(){
    h_frame_r2 = new TH1F("h_frame_r2","",100,0,10);
    h_frame_r2->GetXaxis()->SetRangeUser(0,2.5);
    h_frame_r2->GetXaxis()->SetTitle(etaAString.c_str());
    h_frame_r2->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_r2->GetYaxis()->SetRangeUser(0.67,1.3);
    h_frame_r2->GetYaxis()->SetTitle("#it{r}_{2}");
    h_frame_r2->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_r2->GetYaxis()->SetTitleSize(0.09);
    h_frame_r2->GetYaxis()->SetTitleOffset(0.7);
    h_frame_r2->GetYaxis()->SetLabelSize(0.07);


    h_frame_v22 = new TH1F("h_frame_v22","",100,-5,5);
    h_frame_v22->GetYaxis()->SetRangeUser(0.0,0.015);
    h_frame_v22->GetXaxis()->SetRangeUser(-2.5,2.5);
    h_frame_v22->GetXaxis()->SetTitle("#it{#eta^{a}}");
    h_frame_v22->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_v22->GetYaxis()->SetTitle("#it{v}_{2,2}");
    h_frame_v22->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22->GetYaxis()->SetTitleSize(0.09);
    h_frame_v22->GetYaxis()->SetLabelSize(0.07);
    h_frame_v22->GetYaxis()->SetTitleOffset(0.9);
    h_frame_v22->GetXaxis()->SetNdivisions(510,kTRUE);

    h_frame_vnn = new TH1F("h_frame_vnn","",100,-2.5,2.5);
    h_frame_vnn->GetYaxis()->SetRangeUser(-0.015,0.015);
    h_frame_vnn->GetXaxis()->SetRangeUser(-2.5,2.5);
    h_frame_vnn->GetXaxis()->SetTitle("#it{#eta^{a}}");
    h_frame_vnn->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_vnn->GetYaxis()->SetTitle("#it{v}_{n,n}");
    h_frame_vnn->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_vnn->GetYaxis()->SetTitleSize(0.09);
    h_frame_vnn->GetYaxis()->SetLabelSize(0.07);
    h_frame_vnn->GetYaxis()->SetTitleOffset(0.9);
    h_frame_vnn->GetXaxis()->SetNdivisions(510,kTRUE);

    h_frame_v22_dCent = new TH1F("h_frame_v22_dCent","",100,0,100);
    h_frame_v22_dCent->GetYaxis()->SetRangeUser(-0.005,0.025);
    h_frame_v22_dCent->GetXaxis()->SetRangeUser(0,100);
    h_frame_v22_dCent->GetXaxis()->SetTitle("Centrality [%]");
    h_frame_v22_dCent->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_v22_dCent->GetYaxis()->SetTitle("#it{v}_{2,2}");
    h_frame_v22_dCent->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22_dCent->GetYaxis()->SetTitleSize(0.09);
    h_frame_v22_dCent->GetYaxis()->SetLabelSize(0.07);
    h_frame_v22_dCent->GetYaxis()->SetTitleOffset(0.9);
    h_frame_v22_dCent->GetXaxis()->SetNdivisions(510,kTRUE);


    h_frame_Fn = new TH1F("h_frame_Fn","",2500,0,2500);
    h_frame_Fn->GetXaxis()->SetRangeUser(18,2500);
    h_frame_Fn->GetYaxis()->SetRangeUser(0.001,0.5);
    h_frame_Fn->GetXaxis()->SetTitle(etaAString.c_str());
    h_frame_Fn->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_Fn->GetYaxis()->SetTitle("#it{F}_{n}  ");
    h_frame_Fn->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_Fn->GetYaxis()->SetTitleSize(0.09);
    h_frame_Fn->GetYaxis()->SetTitleOffset(0.7);
    h_frame_Fn->GetYaxis()->SetLabelSize(0.07);



    line1 = new TGraph(2);
    line1->SetPoint(0,-100,1);
    line1->SetPoint(1,5000000,1);
    line1->SetLineStyle(2);
    line1->SetLineColor(1);
    line1->SetLineWidth(1);

    line0 = new TGraph(2);
    line0->SetPoint(0,-100,0);
    line0->SetPoint(1,500000,0);
    line0->SetLineStyle(2);
    line0->SetLineColor(1);
    line0->SetLineWidth(1);

    line2 = new TGraph(2);
    line2->SetPoint(0,-100,2);
    line2->SetPoint(1,500000,2);
    line2->SetLineStyle(2);
    line2->SetLineColor(1);
    line2->SetLineWidth(1);

    linen2 = new TGraph(2);
    linen2->SetPoint(0,-100,-2);
    linen2->SetPoint(1,5000000,-2);
    linen2->SetLineStyle(2);
    linen2->SetLineColor(1);
    linen2->SetLineWidth(1);

    h_frame_r2_ratio = new TH1F("h_frame_r2_ratio","",100,0,10);
    h_frame_r2_ratio->GetYaxis()->SetRangeUser(0.65,1.6);
    h_frame_r2_ratio->GetXaxis()->SetRangeUser(0,2.5);

    h_frame_r2_ratio->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_r2_ratio->GetYaxis()->SetLabelSize(0.14);
    h_frame_r2_ratio->GetYaxis()->SetTitleSize(0.13);
    h_frame_r2_ratio->GetYaxis()->CenterTitle(kTRUE);
    h_frame_r2_ratio->GetYaxis()->SetTitle("ratio");
    h_frame_r2_ratio->GetYaxis()->SetTitleOffset(0.6);

    h_frame_r2_ratio->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_r2_ratio->GetXaxis()->SetLabelSize(0.16);
    h_frame_r2_ratio->GetXaxis()->SetTickLength(0.10);
    h_frame_r2_ratio->GetXaxis()->SetTitleSize(0.13);
    h_frame_r2_ratio->GetXaxis()->SetTitleOffset(0.83);
    h_frame_r2_ratio->GetXaxis()->SetTitle(Form("%s      ",etaAString.c_str()));


    h_frame_v22_ratio = new TH1F("h_frame_v22_ratio","",100,-5,5);
    h_frame_v22_ratio->GetYaxis()->SetRangeUser(0.75,1.25);
    h_frame_v22_ratio->GetXaxis()->SetRangeUser(-2.5,2.5);

    h_frame_v22_ratio->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22_ratio->GetYaxis()->SetLabelSize(0.14);
    h_frame_v22_ratio->GetYaxis()->SetTitleSize(0.13);
    h_frame_v22_ratio->GetYaxis()->CenterTitle(kTRUE);
    h_frame_v22_ratio->GetYaxis()->SetTitle("ratio");
    h_frame_v22_ratio->GetYaxis()->SetTitleOffset(0.6);
    h_frame_v22_ratio->GetYaxis()->SetTitleSize(0.12);

    h_frame_v22_ratio->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_v22_ratio->GetXaxis()->SetLabelSize(0.16);
    h_frame_v22_ratio->GetXaxis()->SetTickLength(0.10);
    h_frame_v22_ratio->GetXaxis()->SetTitleSize(0.13);
    h_frame_v22_ratio->GetXaxis()->SetTitleOffset(1.03);
    h_frame_v22_ratio->GetXaxis()->SetTitle("#it{#eta^{a}}");

    h_frame_v22_dCent_ratio = new TH1F("h_frame_v22_dCent_ratio","",100,0,100);
    h_frame_v22_dCent_ratio->GetYaxis()->SetRangeUser(0.50,1.10);
    h_frame_v22_dCent_ratio->GetXaxis()->SetRangeUser(0,100);
    h_frame_v22_dCent_ratio->GetYaxis()->SetNdivisions(506,kTRUE);
    h_frame_v22_dCent_ratio->GetYaxis()->SetLabelSize(0.14);
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitleSize(0.13);
    h_frame_v22_dCent_ratio->GetYaxis()->CenterTitle(kTRUE);
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitle("ratio to raw");
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitleOffset(0.6);
    h_frame_v22_dCent_ratio->GetYaxis()->SetTitleSize(0.12);
    h_frame_v22_dCent_ratio->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_v22_dCent_ratio->GetXaxis()->SetLabelSize(0.16);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTickLength(0.10);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTitleSize(0.13);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTitleOffset(1.03);
    h_frame_v22_dCent_ratio->GetXaxis()->SetTitle("Centrality [%]");


    h_frame_Fn_pull = new TH1F("h_frame_Fn_pull","",2500,0,2500);
    h_frame_Fn_pull->GetYaxis()->SetRangeUser(-4.75,4.75);
    h_frame_Fn_pull->GetXaxis()->SetRangeUser(18,2500);

    h_frame_Fn_pull->GetYaxis()->SetNdivisions(505,kTRUE);
    h_frame_Fn_pull->GetYaxis()->SetLabelSize(0.14);
    h_frame_Fn_pull->GetYaxis()->SetTitleSize(0.13);
    h_frame_Fn_pull->GetYaxis()->CenterTitle(kTRUE);
    h_frame_Fn_pull->GetYaxis()->SetTitleOffset(0.6);
    h_frame_Fn_pull->GetYaxis()->SetTitle("pull");

    h_frame_Fn_pull->GetXaxis()->SetNdivisions(510,kTRUE);
    h_frame_Fn_pull->GetXaxis()->SetLabelSize(0.16);
    h_frame_Fn_pull->GetXaxis()->SetTickLength(0.10);
    h_frame_Fn_pull->GetXaxis()->SetTitleSize(0.13);
    h_frame_Fn_pull->GetXaxis()->SetTitleOffset(0.83);
    h_frame_Fn_pull->GetXaxis()->SetTitle("#it{N}_{ch}");



    h_frame_v22_ref = new TH1F("h_frame_v22_ref","",2500,0,2500);
    h_frame_v22_ref->GetYaxis()->SetRangeUser(0,0.015);
    h_frame_v22_ref->GetXaxis()->SetRangeUser(18,2500);
    h_frame_v22_ref->GetYaxis()->SetNdivisions(505);
    h_frame_v22_ref->GetYaxis()->SetTitle("#it{c}_{2} or  #it{a}_{2}  ");
    h_frame_v22_ref->GetXaxis()->SetTitle("#it{N}^{rec}_{ch}");

    h_frame_d2Od1 = new TH1F("h_frame_d2Od1","",2500,0,2500);
    h_frame_d2Od1->GetYaxis()->SetRangeUser(-0.1,0);
    h_frame_d2Od1->GetXaxis()->SetRangeUser(18,2500);
    h_frame_d2Od1->GetYaxis()->SetNdivisions(505);
    h_frame_d2Od1->GetYaxis()->SetTitle("#it{d}_{2}/#it{d}_{1}");
    h_frame_d2Od1->GetXaxis()->SetTitle("#it{N}_{ch}^{rec}");

    h_frame_err_Fn = new TH1F("h_frame_err_Fn","",2500,0,2500);
    h_frame_err_Fn->GetXaxis()->SetRangeUser(18,2500);
    h_frame_err_Fn->GetYaxis()->SetRangeUser(0.001,0.5);
    h_frame_err_Fn->SetXTitle("#it{N}_{ch}^{rec}");
    h_frame_err_Fn->SetYTitle("Absolute deviation");
    h_frame_err_Fn->GetYaxis()->SetTitleOffset(1.5);
    h_frame_err_Fn->GetYaxis()->SetNdivisions(505,kTRUE);
    h_frame_err_Fn->GetXaxis()->SetNdivisions(509,kTRUE);
    h_frame_err_Fn->Draw("AXIS");
  }




  TGraphAsymmErrors* ratioHistAbsErr(TH1* nom,TH1* den,int index){
    TGraphAsymmErrors* gr_ratio = new TGraphAsymmErrors();
    string name = nom->GetName();
    gr_ratio->SetName(Form("%s%s_%d",name.c_str(),"_ratioAbsErr",index));

    for (int ip=0; ip<nom->GetNbinsX(); ip++){
      float nomVal = nom->GetBinContent(ip+1);
      float denVal = den->GetBinContent(ip+1);
      float nomErr = nom->GetBinError(ip+1);
      float denErr = den->GetBinError(ip+1);
      // calc ratio
      float ratio = nomVal/denVal;
      float x = nom->GetBinCenter(ip+1);
      // calc error
      float sum = sqrt(pow(nomErr,2)+pow(denErr,2));
      float ratio_low  = fabs((nomVal-sum)/denVal-ratio);
      float ratio_high = fabs((nomVal+sum)/denVal-ratio);
      gr_ratio->SetPoint(ip,x,ratio);
      gr_ratio->SetPointError(ip,0,0,ratio_low,ratio_high);
    }
    return gr_ratio;
  }


  void shiftGraph(TGraphAsymmErrors* gr, float shift,float x_err){
    for (int ig=0; ig<gr->GetN(); ig++) {
           gr->GetX()[ig] += shift;
           gr->GetEXlow ()[ig] = x_err;
           gr->GetEXhigh()[ig] = x_err;
    }
    return;
  }

  void shiftGraphNoErr(TGraphAsymmErrors* gr, float shift){
    for (int ig=0; ig<gr->GetN(); ig++) {
           gr->GetX()[ig] += shift;
           gr->GetEXlow ()[ig] = 0.0;
           gr->GetEXhigh()[ig] = 0.0;
           gr->GetEYlow ()[ig] = 0.0;
           gr->GetEYhigh()[ig] = 0.0;
    }
    return;
  }


  /////////////////////////////////////////////////////////////
  // generate a synthetic histagram data based on errors and
  // values.  Guassin uncorrelated

  class BlairGenSynHist{
  public:
    void init();
    TH1F* genHist(TH1F*,int);
  private:
    TRandom3* rand;
  };

  void BlairGenSynHist::init(){
    rand = new TRandom3(0);
  }

  TH1F* BlairGenSynHist::genHist(TH1F* hist, int index){
    int numBins = hist->GetNbinsX();
    TH1F* histMC = (TH1F*) hist->Clone(Form("%s_MC%d",hist->GetName(),index));
    histMC->Reset();

    for(int ib=1; ib<numBins+1; ib++){
      float val = rand->Gaus(hist->GetBinContent(ib),hist->GetBinError(ib));
      histMC->SetBinContent(ib,val);
      histMC->SetBinError(ib,hist->GetBinError(ib));
    }

    return histMC;
  }

  float NtrkToCent(float Ntrk){
    TFile* fin = new TFile("../../Correlation/Correlation_XeXe/Ntrk_cent.root","READ");
    TGraph* gr_cent_ntrk = (TGraph*) fin->Get("gr_cent_ntrk");
    float cent = gr_cent_ntrk->Eval(Ntrk);
    return cent;
  }

class calcF{
  public:
    void init();
    void calc(TH1F*);
    void calcVnn(TH1F*);
    float f_val;
    float f_err;
    float f_chi2;
    float cn_midRap;
    float f_chi2Ondf;
    TF1* f_f;
    TF1* f_vnn;
    TGraphAsymmErrors* graph();
    float chi2(TH1F*);
    void calcFit(TH1F* hist);
  private:
};

void calcF::init(){
  f_f =  new TF1("f_f","1-2*[0]*x",0,2.5);
  f_vnn = new TF1("f_vnn","[0]*(1+[1]*x)",-2.5,2.5);
}

void calcF::calcFit(TH1F* hist){
  hist->Fit(f_f,"R","0",0,2.5);
  f_val = f_f->GetParameter(0);
  f_err = f_f->GetParError(0);
  f_chi2 = chi2(hist);
  f_chi2Ondf = f_chi2 / (hist->GetNbinsX()-1);
}


void calcF::calcVnn(TH1F* hist){
  hist->Fit(f_vnn,"RN","",-2.5,2.5);
  f_val = f_vnn->GetParameter(1);
  f_err = f_vnn->GetParError(1);
  cn_midRap = f_vnn->GetParameter(0);

  f_chi2 = chi2(hist);
  f_chi2Ondf = f_chi2/(hist->GetNbinsX()-1);
}


void calcF::calc(TH1F* hist){
  double par = 0;
  double norm = 0;
  double par_el = 0;
  double par_eh = 0;
  for (int ip=0; ip<hist->GetNbinsX(); ip++){
    double etaA = hist->GetBinCenter(ip+1);
    double r2 = hist->GetBinContent(ip+1);
    double r2_eh = hist->GetBinError(ip+1);
    double r2_el = hist->GetBinError(ip+1);
    par += etaA*(1-r2);
    norm += etaA*etaA;
    par_el = sqrt(par_el*par_el+etaA*etaA*r2_el*r2_el);
    par_eh = sqrt(par_eh*par_eh+etaA*etaA*r2_eh*r2_eh);
  }
  par /= 2*norm;
  par_el /= 2*norm;
  par_eh /= 2*norm;

  f_val = par;
  f_err = par_el;

  f_chi2 = chi2(hist);
  f_chi2Ondf = f_chi2/(hist->GetNbinsX()-1);
  //TGraphAsymmErrors* g_result = new TGraphAsymmErrors();
  //g_result->SetPoint(0,0,par);
  //g_result->SetPointError(0,0,0,par_el,par_eh);

  //return g_result;
}


TGraphAsymmErrors* calcF::graph(){
  TGraphAsymmErrors* g_result = new TGraphAsymmErrors();
  g_result->SetPoint(0,0,1);
  g_result->SetPointError(0,0,0,0,0);
  g_result->SetPoint(1,2.5,1-f_val*5);
  g_result->SetPointError(1,0,0,f_err*5,f_err*5);
  g_result->SetLineStyle(7);
  g_result->SetLineWidth(2);
  return g_result;
}

float calcF::chi2(TH1F* hist){
  float chi2 = 0;
  for (int ip=1; ip<hist->GetNbinsX()+1; ip++){
    float mod = 1-f_val*2*hist->GetBinCenter(ip);
    float dat = hist->GetBinContent(ip);
    float sig = hist->GetBinError(ip);
    chi2 += pow((dat-mod)/sig,2);
  }
  return chi2;
}

/*
class fitJetY{
public:
  static double_t fitf(double_t*,double_t*);
  void init(int);
private:
  TFile* fin;
  static TGraphErrors* gr;
};

void fitJetY::init(int i){
  fin = new TFile("../../centrality/rootFiles/JetY_cent.root");
  if (i==0) {
    gr = (TGraphErrors*) fin->Get("gr_npart_ncollOnpart2_pfx");
  }
  if (i==1){
    gr = (TGraphErrors*) fin->Get("gr_npart_1Oncoll_pfx");
  }
}

double_t fitJetY::fitf(double_t* x,double_t* par){
  double_t val = gr->Eval(*x);
  double_t fitval = par[0]*val;
  return val;
}
*/



  TGraphAsymmErrors* ratioHistAbsErr(TH1* nom,TH1* den){
    TGraphAsymmErrors* gr_ratio = new TGraphAsymmErrors();
    string name = nom->GetName();
    gr_ratio->SetName(Form("%s%s",name.c_str(),"_ratioAbsErr"));

    for (int ip=0; ip<nom->GetNbinsX(); ip++){
      float nomVal = nom->GetBinContent(ip+1);
      float denVal = den->GetBinContent(ip+1);
      float nomErr = nom->GetBinError(ip+1);
      float denErr = den->GetBinError(ip+1);
      // calc ratio
      float ratio = nomVal/denVal;
      float x = nom->GetBinCenter(ip+1);
      // calc error
      float sum = sqrt(pow(nomErr,2)+pow(denErr,2));
      float ratio_low  = fabs((nomVal-sum)/denVal-ratio);
      float ratio_high = fabs((nomVal+sum)/denVal-ratio);
      gr_ratio->SetPoint(ip,x,ratio);
      gr_ratio->SetPointError(ip,0,0,ratio_low,ratio_high);
    }
    return gr_ratio;
  }




double_t fitf_1ONcall(double_t* x,double_t* par){
  TFile fin("../../centrality/rootFiles/JetY_cent.root");
  TGraphErrors* gr = (TGraphErrors*) fin.Get("gr_npart_1Oncoll_pfx");
  double_t val = gr->Eval(*x);
  double_t fitval = par[0]*val;
  return fitval;
}

double_t fitf_NcollONpart2(double_t* x,double_t* par){
  TFile fin("../../centrality/rootFiles/JetY_cent.root");
  TGraphErrors* gr2 = (TGraphErrors*) fin.Get("gr_npart_ncollOnpart2_pfx");
  double_t val2= gr2->Eval(*x);
  double_t fitval = par[0]*val2;
  return fitval;
}


float quad(float v1,float v2){
  return sqrt(pow(v1,2)+pow(v2,2));
}


pair<float,float> calcAvgStd(int nh,TH1F* hist[nh]){
  float avg=0,x2=0;
  int counts = 0;
  for (int ih=0; ih<nh; ih++){
    for (int ib=0; ib<hist[ih]->GetNbinsX(); ib++){
      float val = hist[ih]->GetBinContent(ib);
      if (val == 0) continue;
      avg += val;
      x2  += val*val;
      counts++;
    }
  }
  avg /= counts;
  x2  /= counts;
  float std = sqrt(x2-avg*avg);
  pair<float,float> result(avg,std);
  return result;
}


TH1F* calc_d2Od1_ratio(TH1F* h_v11, TH1F* h_v22_raw, TH1F* h_v22_sub){

  TH1F* h_d2Od1 = (TH1F*) h_v11->Clone("h_d2Od1_temp");

  for (int in=1; in<h_v11->GetNbinsX()+1; in++){
    float temp = h_v22_raw->GetBinContent(in) - h_v22_sub->GetBinContent(in);
    float ratio = temp/h_v11->GetBinContent(in);
    float temp_error = quad(h_v22_raw->GetBinError(in),h_v22_sub->GetBinError(in));
    float ratio_error = ratio*quad(temp_error/temp,h_v11->GetBinError(in)/h_v11->GetBinContent(in));
    h_d2Od1->SetBinContent(in,ratio);
    h_d2Od1->SetBinError(in,ratio_error);
  }

return h_d2Od1;
}


TH1F* calc_rn(TH1F* h_vn){
  int Nbins = h_vn->GetNbinsX();
  float lim = h_vn->GetBinLowEdge(Nbins) + h_vn->GetBinWidth(Nbins);
  TH1F* h_rn = new TH1F(Form("%s_rn",h_vn->GetName()),"",Nbins/2,0,lim);

  for (int ib=1; ib<Nbins/2+1; ib++){
    float den_val = h_vn->GetBinContent(Nbins-ib+1);
    float num_val = h_vn->GetBinContent(ib);
    float den_err = h_vn->GetBinError(Nbins-ib+1);
    float num_err = h_vn->GetBinError(ib);
    float ratio   = num_val/den_val;
    float ratio_err = ratio*quad(num_err/num_val,den_err/den_val);
    h_rn->SetBinContent(Nbins/2-ib+1,ratio);
    h_rn->SetBinError  (Nbins/2-ib+1,ratio_err);
  }

  return h_rn;
}



TH1F* histPull(TH1F*htemp,TH1F*htest){
  string name = htemp->GetName();
  TH1F* h_pull = (TH1F*) htemp->Clone(Form("pull_%s",name.c_str()));
  const int xbins = h_pull->GetXaxis()->GetNbins();
  for (int ix=1; ix<xbins+1; ix++){
    float tempX = htemp->GetBinContent(ix);
    float tempEx= htemp->GetBinError(ix);
    float testX = htest->GetBinContent(ix);
    float testEx= htest->GetBinError(ix);
    float dif = tempX-testX;
    float difE = quad(testEx,tempEx);
    float pull = dif/difE;
    h_pull->SetBinContent(ix,pull);
    h_pull->SetBinError(ix,0.001);
  }
  return h_pull;
}

TH1F* D1subCorrected(TH1F* h_d1, TH1F* h_a2, float d2Od1,float Fstar){
  TH1F* h_c2 = (TH1F*) h_a2->Clone(Form("%s_sub",h_a2->GetName()));
  TH1F* h_d2 = (TH1F*) h_d1->Clone("temp_d1");
  h_d2->Scale(d2Od1);
  h_c2->Add(h_d2,-1);
  // get uncorrected value
  TH1F* h_d2_etaCorrection = (TH1F*) h_d1->Clone("h_d2_etaCorrection");
  h_d2_etaCorrection->Reset();
  for(int ib=1; ib<h_c2->GetNbinsX()+1; ib++){
    float eta  = h_c2->GetBinCenter(ib);
    float val  = h_d2->GetBinContent(ib)*Fstar*eta;
    float err = h_d2->GetBinError(ib)*Fstar*eta;
    h_d2_etaCorrection->SetBinContent(ib,val);
    h_d2_etaCorrection->SetBinError(ib,err);
  }
  h_c2->Add(h_d2_etaCorrection,-1);

  return h_c2;
}



class crtdTempFit{
public:
  float Ftemp_val = 0;
  float Ftemp_err = 0;
  float F2_HM_val = 0;
  float F2_HM_err = 0;
  float F2_LM_val = 0;
  float F2_LM_err = 0;
  float c2_LM_val = 1;
  float c2_LM_err = 0;
  float c2_HM_val = 1;
  float c2_HM_err = 0;
  pair<float,float> crtn;
  pair<float,float> calcCrtn();
};

pair<float,float> crtdTempFit::calcCrtn(){
  // term 1 = c^HM - c^LM*rho
  float term1_val = c2_HM_val - c2_LM_val*Ftemp_val;
  float term1_err = quad(c2_HM_err,c2_LM_err*Ftemp_val);
  // term 2 = rho*F^LM*c^LM
  float term2_val = Ftemp_val*c2_LM_val*F2_LM_val;
  float term2_err = term2_val*quad(Ftemp_err/Ftemp_val,quad(c2_LM_err/c2_LM_val,F2_LM_err/F2_LM_val));
  // term 1 * F^HM
  float normTemp_val = F2_HM_val*term1_val;
  float normTemp_err = normTemp_val*quad(term1_err/term1_val,F2_HM_err/F2_HM_val);
  float crtTemp_val  = normTemp_val + term2_val;
  float crtTemp_err  = quad(normTemp_err,term2_err);
  float crtn_val     = crtTemp_val/c2_HM_val;
  float crtn_err     = crtn_val*quad(crtTemp_err/crtTemp_val,c2_HM_err/c2_HM_val);
  crtn.first = crtn_val;
  crtn.second = crtn_err;
  return crtn;
}


TGraphAsymmErrors* calcSyst(TH1F* nom,TH1F* var){
  TGraphAsymmErrors* gr_syst = new TGraphAsymmErrors(nom);

  for(int ib=0; ib<nom->GetNbinsX(); ib++){
    float val = nom->GetBinContent(ib+1) - var->GetBinContent(ib+1);
    if(val != val) val = 0;
    float x = nom->GetBinCenter(ib+1);
    gr_syst->SetPoint(ib,x,val);
  }
  return gr_syst;
}

TGraphAsymmErrors* quadGraph(TGraphAsymmErrors* gr1,TGraphAsymmErrors* gr2){
  TGraphAsymmErrors* gr_syst = (TGraphAsymmErrors*) gr1->Clone("gr_syst");

  for(int ib=0; ib<gr1->GetN(); ib++){
    double_t x,y,x2,y2;
    gr1->GetPoint(ib,x,y);
     gr2->GetPoint(ib,x2,y2);
    float val = quad(y,y2);
    if(val != val) val = 0;
    gr_syst->SetPoint(ib,x,val);
    gr_syst->GetEYlow()[ib]=0;
    gr_syst->GetEYhigh()[ib]=0;
  }
  return gr_syst;
}


vector<TH1F*> calcSyst_2var(TH1F* h_nom,TH1F* h_var_1,TH1F* h_var_2,string sig){
      TH1F* h_sys      = (TH1F*) h_nom->Clone( Form("h_%s_sysSym",sig.c_str()) );
      TH1F* h_sys_low  = (TH1F*) h_nom->Clone( Form("h_%s_sysLow",sig.c_str()) );
      TH1F* h_sys_high = (TH1F*) h_nom->Clone( Form("h_%s_sysHigh",sig.c_str()) );
      h_sys     ->Reset();
      h_sys_low ->Reset();
      h_sys_high->Reset();

      for (int ig = 0; ig < h_var_1->GetNbinsX(); ig++) {
          float error_low = 0;
          float error_high = 0;

          float rel_error1 = h_var_1->GetBinContent(ig+1) - h_nom->GetBinContent(ig+1);
          if (rel_error1 != rel_error1) rel_error1 = 0;
          if (rel_error1 > 0) {
              error_high = fabs(rel_error1);
          } else {
              error_low = fabs(rel_error1);
          }
          // set rel_error2 if there is a second variation
          float rel_error2 = h_var_2->GetBinContent(ig+1) - h_nom->GetBinContent(ig+1);;
          if (rel_error2 != rel_error2) rel_error2 = 0;
          if (rel_error2 > 0) {
              error_high = TMath::Max(fabs(rel_error2), error_high);
          } else {
              error_low = TMath::Max(fabs(rel_error2), error_low);
          }

          float error = TMath::Max(fabs(rel_error1), fabs(rel_error2));
          // fill absolute error
          h_sys->SetBinContent(ig+1, error);
          h_sys->SetBinError  (ig+1, 0);
          h_sys_low->SetBinContent(ig+1, error_low);
          h_sys_low->SetBinError  (ig+1, 0);
          h_sys_high->SetBinContent(ig+1, error_high);
          h_sys_high->SetBinError  (ig+1, 0);
      }

      vector<TH1F*> hists;
      hists.push_back(h_sys);
      hists.push_back(h_sys_low);
      hists.push_back(h_sys_high);

      return hists;

}





vector<TH1F*> calcSyst_2var(TH1F* h_nom,TH1F* h_var_1,TH1F* h_var_2,string sig,bool sym){
      TH1F* h_sys      = (TH1F*) h_nom->Clone( Form("h_%s_sysSym",sig.c_str()) );
      TH1F* h_sys_low  = (TH1F*) h_nom->Clone( Form("h_%s_sysLow",sig.c_str()) );
      TH1F* h_sys_high = (TH1F*) h_nom->Clone( Form("h_%s_sysHigh",sig.c_str()) );
      h_sys     ->Reset();
      h_sys_low ->Reset();
      h_sys_high->Reset();

      for (int ig = 0; ig < h_var_1->GetNbinsX(); ig++) {
          float error_low = 0;
          float error_high = 0;

          float rel_error1 = h_var_1->GetBinContent(ig+1) - h_nom->GetBinContent(ig+1);
          if (rel_error1 != rel_error1) rel_error1 = 0;
          if (rel_error1 > 0) {
              error_high = fabs(rel_error1);
          } else {
              error_low = fabs(rel_error1);
          }
          // set rel_error2 if there is a second variation
          float rel_error2 = h_var_2->GetBinContent(ig+1) - h_nom->GetBinContent(ig+1);;
          if (rel_error2 != rel_error2) rel_error2 = 0;
          if (rel_error2 > 0) {
              error_high = TMath::Max(fabs(rel_error2), error_high);
          } else {
              error_low = TMath::Max(fabs(rel_error2), error_low);
          }

          float error = TMath::Max(fabs(rel_error1), fabs(rel_error2));
          // fill absolute error
          h_sys->SetBinContent(ig+1, error);
          h_sys->SetBinError  (ig+1, 0.000001);
          h_sys_low->SetBinContent(ig+1, error_low);
          h_sys_low->SetBinError  (ig+1, 0);
          h_sys_high->SetBinContent(ig+1, error_high);
          h_sys_high->SetBinError  (ig+1, 0);
      }

      TF1* f_pol = new TF1("f_pol","[0]+[1]*TMath::Log(x)+[2]*TMath::Log(x)*TMath::Log(x)",10,2500);
      h_sys->Fit("f_pol","","",0,2500);
      for (int ib=1; ib<h_sys->GetNbinsX(); ib++){
        cout << f_pol->Eval(h_sys->GetBinCenter(ib)) << " " << h_sys->GetBinContent(ib) << endl;
        h_sys->SetBinContent(ib,f_pol->Eval(h_sys->GetBinCenter(ib)));
      }

      vector<TH1F*> hists;
      hists.push_back(h_sys);
      hists.push_back(h_sys_low);
      hists.push_back(h_sys_high);

      return hists;

}


TH1F* calcDevToPull(TH1F* dev,TH1F* h_nom){
  TH1F* result = (TH1F*) h_nom->Clone(Form("%s_devpull",h_nom->GetName()));
  for(int ib=1; ib<h_nom->GetNbinsX(); ib++){
    result->SetBinContent(ib,dev->GetBinContent(ib)/h_nom->GetBinError(ib));
    result->SetBinError(ib,0);
  }
  return result;
}


void setHistError(TH1F* hist, float err){
  for(int ib=1; ib<hist->GetNbinsX()+1; ib++){
    hist->SetBinError(ib,err);
  }
  return;
}



void shiftGraph(TGraphAsymmErrors* gr, float shift,float x_err,bool setYerrZero){
  for (int ig=0; ig<gr->GetN(); ig++) {
         gr->GetX()[ig] += shift;
         gr->GetEXlow ()[ig] = x_err;
         gr->GetEXhigh()[ig] = x_err;
         if (setYerrZero){
           gr->GetEYlow ()[ig] = 0;
           gr->GetEYhigh()[ig] = 0;
         }
  }
  return;
}


TH1F* rmsExpErr(unsigned int numExp, TH1F* hist[numExp]){
  TH1F* result = (TH1F*) hist[0]->Clone(Form("%s_merge",hist[0]->GetName()));
  cout << endl << result->GetName();
  result->Reset();
  for(int ib=1; ib<result->GetNbinsX()+1; ib++){
    float x=0,x2=0;
    for (int iexp=1; iexp<numExp; iexp++){
      float val = hist[iexp]->GetBinContent(ib);
      x+=val;
      x2+=val*val;
    }
    float std = sqrt(fabs(x2/(numExp-1)-x*x/(numExp-1)/(numExp-1)));
    //cout << endl << x << "   " << std;
    result->SetBinContent(ib,hist[0]->GetBinContent(ib));
    cout  << "  " << std/hist[0]->GetBinError(ib);
    result->SetBinError(ib,std);
  }
  return result;
}


TH1F* fitPull(TH1F* hist,TF1* fit){
  TH1F* h_pull = (TH1F*) hist->Clone(Form("%s_pull",hist->GetName()));
  for(int ib=1; ib<hist->GetNbinsX()+1; ib++){
    if (hist->GetBinError(ib)==0){continue;cout<< endl << "Zero error in pull calc";}
    float val = hist->GetBinContent(ib)-fit->Eval(hist->GetBinCenter(ib));
    h_pull->SetBinContent(ib,val/hist->GetBinError(ib));
    h_pull->SetBinError(ib,1);
  }

  return h_pull;
}
