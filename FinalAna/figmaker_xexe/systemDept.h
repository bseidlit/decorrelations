
const int Nbins_out_Nch = 20;
float dNch_out_range[Nbins_out_Nch+1] = {0, 18, 28, 41, 59, 82, 112, 150, 196, 254, 323, 404, 500, 612, 740, 889, 1061, 1261, 1496, 1783,2500};

string sysTag = "5.44 TeV Xe+Xe";
string sysStr1 = "5.44 TeV Xe+XeTrk-Twr Corr.";
string sysStr2 = "0.5 < #it{p}_{T}^{trk} < 5 GeV, #||{#it{#eta}^{twr}} > 4.0";

float LM_ref_plot_below = 50;
