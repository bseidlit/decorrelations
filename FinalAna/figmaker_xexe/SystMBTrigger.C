#include "../figmaker_13TeV/plotCommon.h"
//#include "systemDept.h"

void SystMBTrigger(){

  const int Nbins_out_Nch = 8;
  float dNch_out_range[Nbins_out_Nch+1] = {0, 5 ,10 ,15,20,25,30,35,40};

  string sysStr1 = "13 TeV #it{pp} Track-Cluster Corr.";
  string sysStr2 = "0.5 < #it{p}_{T}^{trk,cls} < 5 GeV, #||{#it{#eta}^{cls}} > 4.0";

  float LM_ref_plot_below = 50;


initStyle();
string figPath = "../figures/13TeV/MBTrigger/";
string varStr = "Trg";
string varStr1 = "no L1";
string varStr2 = "MBTS_1";
string varStr3 = "MBTS_2";
string varStr4 = "MBTS_1_1";

TFile* fvar1 = new TFile("../output/13TeV/Results_mb_noL1.root");
TFile* fvar2 = new TFile("../output/13TeV/Results_mb_MBTS_1.root");
TFile* fvar3 = new TFile("../output/13TeV/Results_mb_MBTS_2.root");
TFile* fvar4 = new TFile("../output/13TeV/Results_mb_MBTS_1_1.root");

TH1F* h_dNch_v11_raw_ref_1  = (TH1F*) fvar1->Get("h_dNch_v11_raw_ref");
TH1F* h_dNch_v22_raw_ref_1  = (TH1F*) fvar1->Get("h_dNch_v22_raw_ref");
TH1F* h_dNch_v22_sub_ref_1  = (TH1F*) fvar1->Get("h_dNch_v22_sub_ref");
TH1F* h_dNch_v33_raw_ref_1  = (TH1F*) fvar1->Get("h_dNch_v33_raw_ref");
TH1F* h_dNch_v33_sub_ref_1  = (TH1F*) fvar1->Get("h_dNch_v33_sub_ref");
TH1F* h_dNch_v22_sub_ref2_1 = (TH1F*) fvar1->Get("h_dNch_v22_sub_ref2");

TH1F* h_dNch_v11_raw_ref_2  = (TH1F*) fvar2->Get("h_dNch_v11_raw_ref");
TH1F* h_dNch_v22_raw_ref_2  = (TH1F*) fvar2->Get("h_dNch_v22_raw_ref");
TH1F* h_dNch_v22_sub_ref_2  = (TH1F*) fvar2->Get("h_dNch_v22_sub_ref");
TH1F* h_dNch_v33_raw_ref_2  = (TH1F*) fvar2->Get("h_dNch_v33_raw_ref");
TH1F* h_dNch_v33_sub_ref_2  = (TH1F*) fvar2->Get("h_dNch_v33_sub_ref");
TH1F* h_dNch_v22_sub_ref2_2 = (TH1F*) fvar2->Get("h_dNch_v22_sub_ref2");

TH1F* h_dNch_v11_raw_ref_3  = (TH1F*) fvar3->Get("h_dNch_v11_raw_ref");
TH1F* h_dNch_v22_raw_ref_3  = (TH1F*) fvar3->Get("h_dNch_v22_raw_ref");
TH1F* h_dNch_v22_sub_ref_3  = (TH1F*) fvar3->Get("h_dNch_v22_sub_ref");
TH1F* h_dNch_v33_raw_ref_3  = (TH1F*) fvar3->Get("h_dNch_v33_raw_ref");
TH1F* h_dNch_v33_sub_ref_3  = (TH1F*) fvar3->Get("h_dNch_v33_sub_ref");
TH1F* h_dNch_v22_sub_ref2_3 = (TH1F*) fvar3->Get("h_dNch_v22_sub_ref2");

TH1F* h_dNch_v11_raw_ref_4  = (TH1F*) fvar4->Get("h_dNch_v11_raw_ref");
TH1F* h_dNch_v22_raw_ref_4  = (TH1F*) fvar4->Get("h_dNch_v22_raw_ref");
TH1F* h_dNch_v22_sub_ref_4  = (TH1F*) fvar4->Get("h_dNch_v22_sub_ref");
TH1F* h_dNch_v33_raw_ref_4  = (TH1F*) fvar4->Get("h_dNch_v33_raw_ref");
TH1F* h_dNch_v33_sub_ref_4  = (TH1F*) fvar4->Get("h_dNch_v33_sub_ref");
TH1F* h_dNch_v22_sub_ref2_4 = (TH1F*) fvar4->Get("h_dNch_v22_sub_ref2");

TH1F* h_d2Od1_1      = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_1,h_dNch_v22_raw_ref_1,h_dNch_v22_sub_ref_1);
h_d2Od1_1->SetName("h_d2Od1_1");
TH1F* h_d2Od1_ref2_1 = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_1,h_dNch_v22_raw_ref_1,h_dNch_v22_sub_ref2_1);
h_d2Od1_ref2_1->SetName("h_d2Od1_ref2_1");

TH1F* h_d2Od1_2      = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_2,h_dNch_v22_raw_ref_2,h_dNch_v22_sub_ref_2);
h_d2Od1_2->SetName("h_d2Od1_2");
TH1F* h_d2Od1_ref2_2 = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_2,h_dNch_v22_raw_ref_2,h_dNch_v22_sub_ref2_2);
h_d2Od1_ref2_2->SetName("h_d2Od1_ref2_2");


TH1F* h_d2Od1_3      = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_3,h_dNch_v22_raw_ref_3,h_dNch_v22_sub_ref_3);
h_d2Od1_3->SetName("h_d2Od1_3");
TH1F* h_d2Od1_ref2_3 = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_3,h_dNch_v22_raw_ref_3,h_dNch_v22_sub_ref2_3);
h_d2Od1_ref2_3->SetName("h_d2Od1_ref2_3");


TH1F* h_d2Od1_4      = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_4,h_dNch_v22_raw_ref_4,h_dNch_v22_sub_ref_4);
h_d2Od1_4->SetName("h_d2Od1_4");
TH1F* h_d2Od1_ref2_4 = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref_4,h_dNch_v22_raw_ref_4,h_dNch_v22_sub_ref2_4);
h_d2Od1_ref2_4->SetName("h_d2Od1_ref2_4");

TCanvas* c1 = new TCanvas("c1","c1",600,600);

h_frame_v22_ref->Draw("AXIS");
h_dNch_v22_sub_ref_1->Draw("ex0 same");
h_dNch_v22_sub_ref_1->SetBinContent(1,0);
h_dNch_v22_sub_ref_1->SetBinError(1,0);

h_dNch_v22_raw_ref_1->Draw("ex0 same");
h_dNch_v22_raw_ref_1->SetMarkerColor(kRed);
h_dNch_v22_raw_ref_1->SetMarkerStyle(24);

h_dNch_v22_sub_ref2_1->Draw("ex0 same");
h_dNch_v22_sub_ref2_1->SetMarkerColor(kBlue);
h_dNch_v22_sub_ref2_1->SetLineColor(kBlue);
h_dNch_v22_sub_ref2_1->SetMarkerStyle(24);
h_dNch_v22_sub_ref2_1->SetBinContent(2,0);
h_dNch_v22_sub_ref2_1->SetBinError(2,0);
h_dNch_v22_sub_ref2_1->SetBinContent(1,0);
h_dNch_v22_sub_ref2_1->SetBinError(1,0);

myText( 0.2,0.90,1,"#font[72]{ATLAS} Internal",0.04);
myText( 0.2,0.83,1,sysStr1.c_str(),0.04);
myText( 0.2,0.76,1,sysStr2.c_str(),0.04);

myMarkerLineText(0.5,0.70,1.5, kRed  , 24,kRed  , 1,"raw (#it{d}_{2}+#it{c}_{2})", 0.04, true);
myMarkerLineText(0.5,0.65,1.5, kBlack, 20,kBlack, 1,"sub (#it{c}_{2}) LM 0-10 ", 0.04, true);
myMarkerLineText(0.5,0.60,1.5, kBlue , 24,kBlue , 1,"sub (#it{c}_{2}) LM 10-20", 0.04, true);

c1->SaveAs(Form("%sv22_ref_%s.pdf",figPath.c_str(),varStr.c_str()));



TCanvas* c2 = new TCanvas("c2","c2",600,600);

h_frame_d2Od1->Draw("AXIS");

h_d2Od1_1->Draw("ex0 same");
h_d2Od1_1->SetMarkerStyle(25);
h_d2Od1_1->SetBinContent(1,0);
h_d2Od1_1->SetBinError(1,0);

h_d2Od1_2->Draw("ex0 same");
h_d2Od1_2->SetBinContent(1,0);
h_d2Od1_2->SetBinError(1,0);
h_d2Od1_2->SetMarkerStyle(25);
h_d2Od1_2->SetMarkerColor(kRed);
h_d2Od1_2->SetLineColor(kRed);

h_d2Od1_ref2_1->Draw("ex0 same");
h_d2Od1_ref2_1->SetMarkerStyle(24);
h_d2Od1_ref2_1->SetBinContent(2,0);
h_d2Od1_ref2_1->SetBinError  (2,0);
h_d2Od1_ref2_1->SetBinContent(1,0);
h_d2Od1_ref2_1->SetBinError  (1,0);

h_d2Od1_ref2_2->Draw("ex0 same");
h_d2Od1_ref2_2->SetMarkerStyle(24);
h_d2Od1_ref2_2->SetMarkerColor(kRed);
h_d2Od1_ref2_2->SetLineColor(kRed);
h_d2Od1_ref2_2->SetBinContent(2,0);
h_d2Od1_ref2_2->SetBinError  (2,0);
h_d2Od1_ref2_2->SetBinContent(1,0);
h_d2Od1_ref2_2->SetBinError  (1,0);

myText( 0.2,0.90,1,"#font[72]{ATLAS} Internal",0.04);
myText( 0.2,0.83,1,sysStr1.c_str(),0.04);
myText( 0.2,0.76,1,sysStr2.c_str(),0.04);

myMarkerLineText(0.25,0.70,1.5, kBlack, 24,kBlack, 1,Form("%s LM 0-10",varStr1.c_str()), 0.04, true);
myMarkerLineText(0.25,0.65,1.5, kBlack, 25,kBlack, 1,Form("%s LM 10-20",varStr1.c_str()), 0.04, true);
myMarkerLineText(0.65,0.70,1.5, kRed  , 24,kRed  , 1,Form("%s LM 0-10",varStr2.c_str()), 0.04, true);
myMarkerLineText(0.65,0.65,1.5, kRed  , 25,kRed  , 1,Form("%s LM 10-20",varStr2.c_str()), 0.04, true);


const int numRef = 2;
TH1F* h_d2Od1_comb_1[numRef];
h_d2Od1_comb_1[0] = h_d2Od1_1;
h_d2Od1_comb_1[1] = h_d2Od1_ref2_1;
pair<float,float> final_d2Od1_1 = calcAvgStd(numRef,h_d2Od1_comb_1);

myText(0.2,0.6,1,Form("#it{d}_{2}/#it{d}_{1} = %0.3f#pm%0.3f",final_d2Od1_1.first,final_d2Od1_1.second));


TH1F* h_d2Od1_comb_2[numRef];
h_d2Od1_comb_2[0] = h_d2Od1_2;
h_d2Od1_comb_2[1] = h_d2Od1_ref2_2;
pair<float,float> final_d2Od1_2 = calcAvgStd(numRef,h_d2Od1_comb_2);

myText(0.6,0.6,1,Form("#it{d}_{2}/#it{d}_{1} = %0.3f#pm%0.3f",final_d2Od1_2.first,final_d2Od1_2.second));

c2->SaveAs(Form("%sd2Od1_ref_%s.pdf",figPath.c_str(),varStr.c_str()));


TH1F* h_d2Od1_syst_1 = new TH1F("h_d2Od1_syst_1","",1,0,1);
h_d2Od1_syst_1->SetBinContent(1,final_d2Od1_1.first);
h_d2Od1_syst_1->SetBinError  (1,final_d2Od1_1.second);

TH1F* h_d2Od1_syst_2 = new TH1F("h_d2Od1_syst_2","",1,0,1);
h_d2Od1_syst_2->SetBinContent(1,final_d2Od1_2.first);
h_d2Od1_syst_2->SetBinContent(1,final_d2Od1_2.second);

TH1F* h_v11Raw_Fn_1   = new TH1F("h_v11Raw_Fn_1","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Raw_Fn_1   = new TH1F("h_v22Raw_Fn_1","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Sub_Fn_1   = new TH1F("h_v22Sub_Fn_1","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22SubD1_Fn_1 = new TH1F("h_v22SubD1_Fn_1","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Raw_Fn_1   = new TH1F("h_v33Raw_Fn_1","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Sub_Fn_1   = new TH1F("h_v33Sub_Fn_1","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Crt_Fn_1   = new TH1F("h_v22Crt_Fn_1","",Nbins_out_Nch,dNch_out_range);

TH1F* h_v11Raw_Fn_2   = new TH1F("h_v11Raw_Fn_2","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Raw_Fn_2   = new TH1F("h_v22Raw_Fn_2","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Sub_Fn_2   = new TH1F("h_v22Sub_Fn_2","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22SubD1_Fn_2 = new TH1F("h_v22SubD1_Fn_2","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Raw_Fn_2   = new TH1F("h_v33Raw_Fn_2","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Sub_Fn_2   = new TH1F("h_v33Sub_Fn_2","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Crt_Fn_2   = new TH1F("h_v22Crt_Fn_2","",Nbins_out_Nch,dNch_out_range);

TH1F* h_v11Raw_Fn_3   = new TH1F("h_v11Raw_Fn_3","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Raw_Fn_3   = new TH1F("h_v22Raw_Fn_3","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Sub_Fn_3   = new TH1F("h_v22Sub_Fn_3","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22SubD1_Fn_3 = new TH1F("h_v22SubD1_Fn_3","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Raw_Fn_3   = new TH1F("h_v33Raw_Fn_3","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Sub_Fn_3   = new TH1F("h_v33Sub_Fn_3","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Crt_Fn_3   = new TH1F("h_v22Crt_Fn_3","",Nbins_out_Nch,dNch_out_range);

TH1F* h_v11Raw_Fn_4   = new TH1F("h_v11Raw_Fn_4","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Raw_Fn_4   = new TH1F("h_v22Raw_Fn_4","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Sub_Fn_4   = new TH1F("h_v22Sub_Fn_4","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22SubD1_Fn_4 = new TH1F("h_v22SubD1_Fn_4","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Raw_Fn_4   = new TH1F("h_v33Raw_Fn_4","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Sub_Fn_4   = new TH1F("h_v33Sub_Fn_4","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Crt_Fn_4   = new TH1F("h_v22Crt_Fn_4","",Nbins_out_Nch,dNch_out_range);



for (int in=0; in<Nbins_out_Nch; in++){
  TH1F* h_v11Raw_1 = (TH1F*) fvar1->Get(Form("hNch%d_den_v11Raw_deta",in+1));
  TH1F* h_v22Raw_1 = (TH1F*) fvar1->Get(Form("hNch%d_den_v22Raw_deta",in+1));
  TH1F* h_v22Sub_1 = (TH1F*) fvar1->Get(Form("hNch%d_den_v22Sub_deta",in+1));
  TH1F* h_v33Raw_1 = (TH1F*) fvar1->Get(Form("hNch%d_den_v33Raw_deta",in+1));
  TH1F* h_v33Sub_1 = (TH1F*) fvar1->Get(Form("hNch%d_den_v33Sub_deta",in+1));
  TH1F* h_Ftemp_1  = (TH1F*) fvar1->Get(Form("hNch%d_Ftemp_deta",in+1));

  TH1F* h_v11Raw_2 = (TH1F*) fvar2->Get(Form("hNch%d_den_v11Raw_deta",in+1));
  TH1F* h_v22Raw_2 = (TH1F*) fvar2->Get(Form("hNch%d_den_v22Raw_deta",in+1));
  TH1F* h_v22Sub_2 = (TH1F*) fvar2->Get(Form("hNch%d_den_v22Sub_deta",in+1));
  TH1F* h_v33Raw_2 = (TH1F*) fvar2->Get(Form("hNch%d_den_v33Raw_deta",in+1));
  TH1F* h_v33Sub_2 = (TH1F*) fvar2->Get(Form("hNch%d_den_v33Sub_deta",in+1));
  TH1F* h_Ftemp_2  = (TH1F*) fvar2->Get(Form("hNch%d_Ftemp_deta",in+1));


  TH1F* h_v11Raw_3 = (TH1F*) fvar3->Get(Form("hNch%d_den_v11Raw_deta",in+1));
  TH1F* h_v22Raw_3 = (TH1F*) fvar3->Get(Form("hNch%d_den_v22Raw_deta",in+1));
  TH1F* h_v22Sub_3 = (TH1F*) fvar3->Get(Form("hNch%d_den_v22Sub_deta",in+1));
  TH1F* h_v33Raw_3 = (TH1F*) fvar3->Get(Form("hNch%d_den_v33Raw_deta",in+1));
  TH1F* h_v33Sub_3 = (TH1F*) fvar3->Get(Form("hNch%d_den_v33Sub_deta",in+1));
  TH1F* h_Ftemp_3  = (TH1F*) fvar3->Get(Form("hNch%d_Ftemp_deta",in+1));


  TH1F* h_v11Raw_4 = (TH1F*) fvar4->Get(Form("hNch%d_den_v11Raw_deta",in+1));
  TH1F* h_v22Raw_4 = (TH1F*) fvar4->Get(Form("hNch%d_den_v22Raw_deta",in+1));
  TH1F* h_v22Sub_4 = (TH1F*) fvar4->Get(Form("hNch%d_den_v22Sub_deta",in+1));
  TH1F* h_v33Raw_4 = (TH1F*) fvar4->Get(Form("hNch%d_den_v33Raw_deta",in+1));
  TH1F* h_v33Sub_4 = (TH1F*) fvar4->Get(Form("hNch%d_den_v33Sub_deta",in+1));
  TH1F* h_Ftemp_4  = (TH1F*) fvar4->Get(Form("hNch%d_Ftemp_deta",in+1));


  TH1F* h_v22SubD1_1 = (TH1F*) D1subCorrected(h_v11Raw_1,h_v22Raw_1,final_d2Od1_1.first,0.04);
  TH1F* h_v22SubD1_2 = (TH1F*) D1subCorrected(h_v11Raw_2,h_v22Raw_2,final_d2Od1_2.first,0.04);
  TH1F* h_v22SubD1_3 = (TH1F*) D1subCorrected(h_v11Raw_3,h_v22Raw_3,final_d2Od1_2.first,0.04);
  TH1F* h_v22SubD1_4 = (TH1F*) D1subCorrected(h_v11Raw_4,h_v22Raw_4,final_d2Od1_2.first,0.04);
  h_v22SubD1_1->SetName("hNch%d_den_v22SubD1_deta1");
  h_v22SubD1_2->SetName("hNch%d_den_v22SubD1_deta2");
  h_v22SubD1_3->SetName("hNch%d_den_v22SubD1_deta3");
  h_v22SubD1_4->SetName("hNch%d_den_v22SubD1_deta4");


  TH1F* h_v11Raw_rn_1   = (TH1F*) calc_rn(h_v11Raw_1);
  TH1F* h_v22Raw_rn_1   = (TH1F*) calc_rn(h_v22Raw_1);
  TH1F* h_v22Sub_rn_1   = (TH1F*) calc_rn(h_v22Sub_1);
  TH1F* h_v33Raw_rn_1   = (TH1F*) calc_rn(h_v33Raw_1);
  TH1F* h_v33Sub_rn_1   = (TH1F*) calc_rn(h_v33Sub_1);
  TH1F* h_v22SubD1_rn_1 = (TH1F*) calc_rn(h_v22SubD1_1);

  TH1F* h_v11Raw_rn_2   = (TH1F*) calc_rn(h_v11Raw_2);
  TH1F* h_v22Raw_rn_2   = (TH1F*) calc_rn(h_v22Raw_2);
  TH1F* h_v22Sub_rn_2   = (TH1F*) calc_rn(h_v22Sub_2);
  TH1F* h_v33Raw_rn_2   = (TH1F*) calc_rn(h_v33Raw_2);
  TH1F* h_v33Sub_rn_2   = (TH1F*) calc_rn(h_v33Sub_2);
  TH1F* h_v22SubD1_rn_2 = (TH1F*) calc_rn(h_v22SubD1_2);


  TH1F* h_v11Raw_rn_3   = (TH1F*) calc_rn(h_v11Raw_3);
  TH1F* h_v22Raw_rn_3   = (TH1F*) calc_rn(h_v22Raw_3);
  TH1F* h_v22Sub_rn_3   = (TH1F*) calc_rn(h_v22Sub_3);
  TH1F* h_v33Raw_rn_3   = (TH1F*) calc_rn(h_v33Raw_3);
  TH1F* h_v33Sub_rn_3   = (TH1F*) calc_rn(h_v33Sub_3);
  TH1F* h_v22SubD1_rn_3 = (TH1F*) calc_rn(h_v22SubD1_3);

  TH1F* h_v11Raw_rn_4   = (TH1F*) calc_rn(h_v11Raw_4);
  TH1F* h_v22Raw_rn_4   = (TH1F*) calc_rn(h_v22Raw_4);
  TH1F* h_v22Sub_rn_4   = (TH1F*) calc_rn(h_v22Sub_4);
  TH1F* h_v33Raw_rn_4   = (TH1F*) calc_rn(h_v33Raw_4);
  TH1F* h_v33Sub_rn_4   = (TH1F*) calc_rn(h_v33Sub_4);
  TH1F* h_v22SubD1_rn_4 = (TH1F*) calc_rn(h_v22SubD1_4);


  calcF Fcalc;
  Fcalc.init();

  Fcalc.calc(h_v11Raw_rn_1  );
  TGraphErrors* gr_v11Raw_Fn_1 = (TGraphErrors*) Fcalc.graph();
  h_v11Raw_Fn_1->SetBinContent(in+1,Fcalc.f_val);
  h_v11Raw_Fn_1->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Raw_rn_1  );
  TGraphErrors* gr_v22Raw_Fn_1 = (TGraphErrors*) Fcalc.graph();
  h_v22Raw_Fn_1->SetBinContent(in+1,Fcalc.f_val);
  h_v22Raw_Fn_1->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Sub_rn_1  );
  TGraphErrors* gr_v22Sub_Fn_1 = (TGraphErrors*) Fcalc.graph();
  h_v22Sub_Fn_1->SetBinContent(in+1,Fcalc.f_val);
  h_v22Sub_Fn_1->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v33Raw_rn_1  );
  TGraphErrors* gr_v33Raw_Fn_1 = (TGraphErrors*) Fcalc.graph();
  h_v33Raw_Fn_1->SetBinContent(in+1,Fcalc.f_val);
  h_v33Raw_Fn_1->SetBinError(in+1,Fcalc.f_err);
  gr_v33Raw_Fn_1->SetLineColor(kSpring);

  Fcalc.calc(h_v33Sub_rn_1);
  TGraphErrors* gr_v33Sub_Fn_1 = (TGraphErrors*) Fcalc.graph();
  h_v33Sub_Fn_1->SetBinContent(in+1,Fcalc.f_val);
  h_v33Sub_Fn_1->SetBinError(in+1,Fcalc.f_err);
  gr_v33Sub_Fn_1->SetLineColor(kSpring);


  Fcalc.calc(h_v11Raw_rn_2  );
  TGraphErrors* gr_v11Raw_Fn_2 = (TGraphErrors*) Fcalc.graph();
  h_v11Raw_Fn_2->SetBinContent(in+1,Fcalc.f_val);
  h_v11Raw_Fn_2->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Raw_rn_2  );
  TGraphErrors* gr_v22Raw_Fn_2 = (TGraphErrors*) Fcalc.graph();
  h_v22Raw_Fn_2->SetBinContent(in+1,Fcalc.f_val);
  h_v22Raw_Fn_2->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Sub_rn_2  );
  TGraphErrors* gr_v22Sub_Fn_2 = (TGraphErrors*) Fcalc.graph();
  h_v22Sub_Fn_2->SetBinContent(in+1,Fcalc.f_val);
  h_v22Sub_Fn_2->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v33Raw_rn_2  );
  TGraphErrors* gr_v33Raw_Fn_2 = (TGraphErrors*) Fcalc.graph();
  h_v33Raw_Fn_2->SetBinContent(in+1,Fcalc.f_val);
  h_v33Raw_Fn_2->SetBinError(in+1,Fcalc.f_err);
  gr_v33Raw_Fn_2->SetLineColor(kSpring);

  Fcalc.calc(h_v33Sub_rn_2);
  TGraphErrors* gr_v33Sub_Fn_2 = (TGraphErrors*) Fcalc.graph();
  h_v33Sub_Fn_2->SetBinContent(in+1,Fcalc.f_val);
  h_v33Sub_Fn_2->SetBinError(in+1,Fcalc.f_err);
  gr_v33Sub_Fn_2->SetLineColor(kSpring);

  Fcalc.calc(h_v22SubD1_rn_1);
  TGraphErrors* gr_v22SubD1_Fn_1 = (TGraphErrors*) Fcalc.graph();
  h_v22SubD1_Fn_1->SetBinContent(in+1,Fcalc.f_val);
  h_v22SubD1_Fn_1->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22SubD1_rn_2);
  TGraphErrors* gr_v22SubD1_Fn_2 = (TGraphErrors*) Fcalc.graph();
  h_v22SubD1_Fn_2->SetBinContent(in+1,Fcalc.f_val);
  h_v22SubD1_Fn_2->SetBinError(in+1,Fcalc.f_err);



  Fcalc.calc(h_v11Raw_rn_3  );
  TGraphErrors* gr_v11Raw_Fn_3 = (TGraphErrors*) Fcalc.graph();
  h_v11Raw_Fn_3->SetBinContent(in+1,Fcalc.f_val);
  h_v11Raw_Fn_3->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Raw_rn_3  );
  TGraphErrors* gr_v22Raw_Fn_3 = (TGraphErrors*) Fcalc.graph();
  h_v22Raw_Fn_3->SetBinContent(in+1,Fcalc.f_val);
  h_v22Raw_Fn_3->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Sub_rn_3  );
  TGraphErrors* gr_v22Sub_Fn_3 = (TGraphErrors*) Fcalc.graph();
  h_v22Sub_Fn_3->SetBinContent(in+1,Fcalc.f_val);
  h_v22Sub_Fn_3->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v33Raw_rn_3  );
  TGraphErrors* gr_v33Raw_Fn_3 = (TGraphErrors*) Fcalc.graph();
  h_v33Raw_Fn_3->SetBinContent(in+1,Fcalc.f_val);
  h_v33Raw_Fn_3->SetBinError(in+1,Fcalc.f_err);
  gr_v33Raw_Fn_3->SetLineColor(kSpring);

  Fcalc.calc(h_v33Sub_rn_3);
  TGraphErrors* gr_v33Sub_Fn_3 = (TGraphErrors*) Fcalc.graph();
  h_v33Sub_Fn_3->SetBinContent(in+1,Fcalc.f_val);
  h_v33Sub_Fn_3->SetBinError(in+1,Fcalc.f_err);
  gr_v33Sub_Fn_3->SetLineColor(kSpring);


  Fcalc.calc(h_v11Raw_rn_4  );
  TGraphErrors* gr_v11Raw_Fn_4 = (TGraphErrors*) Fcalc.graph();
  h_v11Raw_Fn_4->SetBinContent(in+1,Fcalc.f_val);
  h_v11Raw_Fn_4->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Raw_rn_4  );
  TGraphErrors* gr_v22Raw_Fn_4 = (TGraphErrors*) Fcalc.graph();
  h_v22Raw_Fn_4->SetBinContent(in+1,Fcalc.f_val);
  h_v22Raw_Fn_4->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Sub_rn_4  );
  TGraphErrors* gr_v22Sub_Fn_4 = (TGraphErrors*) Fcalc.graph();
  h_v22Sub_Fn_4->SetBinContent(in+1,Fcalc.f_val);
  h_v22Sub_Fn_4->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v33Raw_rn_4  );
  TGraphErrors* gr_v33Raw_Fn_4 = (TGraphErrors*) Fcalc.graph();
  h_v33Raw_Fn_4->SetBinContent(in+1,Fcalc.f_val);
  h_v33Raw_Fn_4->SetBinError(in+1,Fcalc.f_err);
  gr_v33Raw_Fn_4->SetLineColor(kSpring);

  Fcalc.calc(h_v33Sub_rn_4);
  TGraphErrors* gr_v33Sub_Fn_4 = (TGraphErrors*) Fcalc.graph();
  h_v33Sub_Fn_4->SetBinContent(in+1,Fcalc.f_val);
  h_v33Sub_Fn_4->SetBinError(in+1,Fcalc.f_err);
  gr_v33Sub_Fn_4->SetLineColor(kSpring);



  crtdTempFit crtdTempFit;
  crtdTempFit.Ftemp_val = h_Ftemp_1->GetBinContent(4);;
  crtdTempFit.Ftemp_err = h_Ftemp_1->GetBinError(4);;
  crtdTempFit.F2_HM_val = h_v22Sub_Fn_1->GetBinContent(in+1);
  crtdTempFit.F2_HM_err = h_v22Sub_Fn_1->GetBinError(in+1);
  crtdTempFit.F2_LM_val = h_v22SubD1_Fn_1->GetBinContent(4);
  crtdTempFit.F2_LM_err = 0;
  crtdTempFit.c2_LM_val = h_dNch_v22_sub_ref_1->GetBinContent(4);
  crtdTempFit.c2_LM_err = 0;
  crtdTempFit.c2_HM_val = h_dNch_v22_sub_ref_1->GetBinContent(in+1);
  crtdTempFit.c2_HM_err = h_dNch_v22_sub_ref_1->GetBinError(in+1);

  pair<float,float> crtn =  crtdTempFit.calcCrtn();
  h_v22Crt_Fn_1->SetBinContent(in+1,crtn.first);
  h_v22Crt_Fn_1->SetBinError  (in+1,crtn.second);

  crtdTempFit.Ftemp_val = h_Ftemp_2->GetBinContent(4);;
  crtdTempFit.Ftemp_err = h_Ftemp_2->GetBinError(4);;
  crtdTempFit.F2_HM_val = h_v22Sub_Fn_2->GetBinContent(in+1);
  crtdTempFit.F2_HM_err = h_v22Sub_Fn_2->GetBinError(in+1);
  crtdTempFit.F2_LM_val = h_v22SubD1_Fn_2->GetBinContent(4);
  crtdTempFit.c2_LM_val = h_dNch_v22_sub_ref_1->GetBinContent(4);

  pair<float,float> crtn2 =  crtdTempFit.calcCrtn();
  h_v22Crt_Fn_2->SetBinContent(in+1,crtn2.first);
  h_v22Crt_Fn_2->SetBinError  (in+1,crtn2.second);


  TCanvas* c3 = new TCanvas("c3","c3",600,600);

  h_v22Raw_1->Draw("ex0");
  h_v22Raw_1->GetYaxis()->SetRangeUser(0.0,0.002);
  h_v22Raw_1->GetXaxis()->SetRangeUser(-2.5,2.5);
  h_v22Raw_1->GetXaxis()->SetTitle("#it{#eta^{a}}");
  h_v22Raw_1->GetXaxis()->SetNdivisions(509,kTRUE);
  h_v22Raw_1->GetYaxis()->SetTitle("#it{v}_{2,2}");
  h_v22Raw_1->GetYaxis()->SetNdivisions(506,kTRUE);

  h_v22Raw_1->SetMarkerColor(kRed);
  h_v22Raw_1->SetMarkerStyle(22);
  h_v22Raw_1->SetLineColor(kRed);

  h_v22Raw_2->SetMarkerColor(kBlack);
  h_v22Raw_2->SetMarkerStyle(26);
  h_v22Raw_2->SetLineColor(kBlack);

  if (!(h_v22Sub_Fn_1->FindBin(LM_ref_plot_below)>=in+1)){
    h_v22Sub_1->Draw("ex0 same");
    h_v22Sub_1->SetMarkerColor(kSpring);
    h_v22Sub_1->SetMarkerStyle(20);
    h_v22Sub_1->SetLineColor(kSpring);

    h_v22Sub_2->Draw("ex0 same");
    h_v22Sub_2->SetMarkerColor(kBlack);
    h_v22Sub_2->SetMarkerStyle(24);
    h_v22Sub_2->SetLineColor(kBlack);
  }

  h_v22SubD1_1->Draw("ex0 same");
  h_v22SubD1_1->SetMarkerColor(kBlue);
  h_v22SubD1_1->SetMarkerStyle(33);
  h_v22SubD1_1->SetLineColor(kBlue);

  h_v22SubD1_2->Draw("ex0 same");
  h_v22SubD1_2->SetMarkerColor(kBlack);
  h_v22SubD1_2->SetMarkerStyle(27);
  h_v22SubD1_2->SetLineColor(kBlack);


  myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
  myText( 0.2,0.85,1,sysStr2.c_str(),0.04);

  myText( 0.2,0.80,1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]),0.04);

  myText          (0.20,     0.40+0.3,1,varStr1.c_str(),0.03);
  myMarkerLineText(0.23,     0.35+0.3,1.5, kRed    , 22,kRed    ,1,"raw #it{F}_{2}", 0.03, true);
  myMarkerLineText(0.23,     0.30+0.3,1.5, kSpring , 20,kSpring ,1,"temp. fit #it{F}_{2}", 0.03, true);
  myMarkerLineText(0.23,     0.25+0.3,1.5, kBlue    ,33,kBlue   ,1,"#it{d}_{1} sub #it{F}_{2}", 0.03, true);
  myText          (0.17+0.18,0.40+0.3,1,varStr2.c_str(),0.03);
  myMarkerLineText(0.23+0.18,0.35+0.3,1.5,kBlack, 26,kBlack,1,"", 0.03, true);
  myMarkerLineText(0.23+0.18,0.30+0.3,1.5,kBlack, 24,kBlack,1,"", 0.03, true);
  myMarkerLineText(0.23+0.18,0.25+0.3,1.5,kBlack, 27,kBlack,1,"", 0.03, true);

  c3->SaveAs(Form("%svnn_Nch%d_%s.pdf",figPath.c_str(),in,varStr.c_str()));
  delete c3;



  TCanvas* c4 = new TCanvas("c4","c4",600,600);

  h_v11Raw_rn_1->GetYaxis()->SetRangeUser(0.3,1.15);
  h_v11Raw_rn_1->GetXaxis()->SetRangeUser(0,2.5);
  h_v11Raw_rn_1->GetXaxis()->SetTitle("|#it{#eta^{a}}|");
  h_v11Raw_rn_1->GetXaxis()->SetNdivisions(509,kTRUE);
  h_v11Raw_rn_1->GetYaxis()->SetTitle("#it{r}_{n}");
  h_v11Raw_rn_1->GetYaxis()->SetNdivisions(506,kTRUE);

  h_v11Raw_rn_1 ->Draw("ex0");
  h_v11Raw_rn_1 ->SetLineColor(kMagenta);
  h_v11Raw_rn_1 ->SetMarkerColor(kMagenta);
  gr_v11Raw_Fn_1->SetLineColor(kMagenta);
  gr_v11Raw_Fn_1->SetLineStyle(1);
  h_v11Raw_rn_1 ->SetMarkerStyle(21);
  gr_v11Raw_Fn_1->Draw("l");

  h_v11Raw_rn_2 ->Draw("ex0 same");
  h_v11Raw_rn_2 ->SetLineColor(kBlack);
  h_v11Raw_rn_2 ->SetMarkerColor(kBlack);
  gr_v11Raw_Fn_2->SetLineColor(kBlack);
  h_v11Raw_rn_2 ->SetMarkerStyle(25);
  gr_v11Raw_Fn_2->Draw("l");

  h_v22Raw_rn_1 ->Draw("ex0 same");
  h_v22Raw_rn_1 ->SetLineColor(kRed);
  h_v22Raw_rn_1 ->SetMarkerColor(kRed);
  gr_v22Raw_Fn_1->SetLineColor(kRed);
  gr_v22Raw_Fn_1->SetLineStyle(1);
  h_v22Raw_rn_1 ->SetMarkerStyle(22);
  gr_v22Raw_Fn_1->Draw("l");

  h_v22Raw_rn_2 ->Draw("ex0 same");
  h_v22Raw_rn_2 ->SetLineColor(kBlack);
  h_v22Raw_rn_2 ->SetMarkerColor(kBlack);
  gr_v22Raw_Fn_2->SetLineColor(kBlack);
  h_v22Raw_rn_2 ->SetMarkerStyle(26);
  gr_v22Raw_Fn_2->Draw("l");

  if (!(h_v22Sub_Fn_1->FindBin(LM_ref_plot_below)>=in+1)){
    h_v22Sub_rn_1   ->Draw("ex0 same");
    h_v22Sub_rn_1   ->SetLineColor(kSpring);
    h_v22Sub_rn_1   ->SetMarkerColor(kSpring);
    gr_v22Sub_Fn_1->SetLineColor(kSpring);
    gr_v22Sub_Fn_1->SetLineStyle(1);
    h_v22Sub_rn_1   ->SetMarkerStyle(20);
    gr_v22Sub_Fn_1->Draw("l");

    h_v22Sub_rn_2   ->Draw("ex0 same");
    h_v22Sub_rn_2   ->SetLineColor(kBlack);
    h_v22Sub_rn_2   ->SetMarkerColor(kBlack);
    gr_v22Sub_Fn_2->SetLineColor(kBlack);
    h_v22Sub_rn_2   ->SetMarkerStyle(24);
    gr_v22Sub_Fn_2->Draw("l");
  }

  h_v22SubD1_rn_1 ->Draw("ex0 same");
  h_v22SubD1_rn_1 ->SetLineColor(kBlue);
  h_v22SubD1_rn_1 ->SetMarkerColor(kBlue);
  gr_v22SubD1_Fn_1->SetLineColor(kBlue);
  gr_v22SubD1_Fn_1->SetLineStyle(1);
  h_v22SubD1_rn_1 ->SetMarkerStyle(33);
  gr_v22SubD1_Fn_1->Draw("l");

  h_v22SubD1_rn_2 ->Draw("ex0 same");
  h_v22SubD1_rn_2 ->SetLineColor(kBlack);
  h_v22SubD1_rn_2 ->SetMarkerColor(kBlack);
  gr_v22SubD1_Fn_2->SetLineColor(kBlack);
  h_v22SubD1_rn_2 ->SetMarkerStyle(27);
  gr_v22SubD1_Fn_2->Draw("l");

  line1->Draw("l");

  myText( 0.2,0.91,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
  myText( 0.2,0.86,1,sysStr2.c_str(),0.04);
  myText( 0.2,0.45,1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]),0.04);

  myText          (0.20,0.45-0.05,1,varStr1.c_str(),0.03);
  myMarkerLineText(0.23,0.40-0.05,1.5, kMagenta, 21,kMagenta,1,"raw #it{F}_{1}", 0.03, true);
  myMarkerLineText(0.23,0.35-0.05,1.5, kRed    , 22,kRed    ,1,"raw #it{F}_{2}", 0.03, true);
  myMarkerLineText(0.23,0.30-0.05,1.5, kSpring , 20,kSpring ,1,"temp. fit #it{F}_{2}", 0.03, true);
  myMarkerLineText(0.23,0.25-0.05,1.5, kBlue    ,33,kBlue   ,1,"#it{d}_{1} sub #it{F}_{2}", 0.03, true);

  myText          (0.17+0.18,0.45-0.05,1,varStr2.c_str(),0.03);
  myMarkerLineText(0.23+0.18,0.40-0.05,1.5,kBlack, 25,kBlack,1,"", 0.03, true);
  myMarkerLineText(0.23+0.18,0.35-0.05,1.5,kBlack, 26,kBlack,1,"", 0.03, true);
  myMarkerLineText(0.23+0.18,0.30-0.05,1.5,kBlack, 24,kBlack,1,"", 0.03, true);
  myMarkerLineText(0.23+0.18,0.25-0.05,1.5,kBlack, 27,kBlack,1,"", 0.03, true);

  c4->SaveAs(Form("%srn_Nch%d_%s.pdf",figPath.c_str(),in,varStr.c_str()));
  delete c4;

}


TCanvas* c5 = new TCanvas("c5","c5",600,600);
TPad *pad2 = new TPad("pad2", "",0.,0.0,1.0,0.3);
TPad *pad1 = new TPad("pad1", "",0.0,0.3,1.0,1.0);
pad1->Draw();
pad2->Draw();

pad1->cd();
gPad->SetBottomMargin(0);
h_frame_Fn->Draw("AXIS");
h_frame_Fn->GetXaxis()->SetRangeUser(0,40);

h_v11Raw_Fn_1->Draw("ex0 same");
h_v11Raw_Fn_1->SetLineColor(kMagenta);
h_v11Raw_Fn_1->SetMarkerColor(kMagenta);
h_v11Raw_Fn_1->SetMarkerStyle(21);

h_v11Raw_Fn_2->Draw("ex0 same");
h_v11Raw_Fn_2->SetLineColor(kBlack);
h_v11Raw_Fn_2->SetMarkerColor(kBlack);
h_v11Raw_Fn_2->SetMarkerStyle(25);

//h_v11Raw_Fn_3->Draw("ex0 same");
h_v11Raw_Fn_3->SetLineColor(kBlack);
h_v11Raw_Fn_3->SetMarkerColor(kBlack);
h_v11Raw_Fn_3->SetMarkerStyle(25);

h_v11Raw_Fn_4->Draw("ex0 same");
h_v11Raw_Fn_4->SetLineColor(kBlack);
h_v11Raw_Fn_4->SetMarkerColor(kBlack);
h_v11Raw_Fn_4->SetMarkerStyle(25);


h_v22Raw_Fn_1->Draw("ex0 same");
h_v22Raw_Fn_1->SetLineColor(kRed);
h_v22Raw_Fn_1->SetMarkerColor(kRed);
h_v22Raw_Fn_1->SetMarkerStyle(22);

h_v22Raw_Fn_2->Draw("ex0 same");
h_v22Raw_Fn_2->SetLineColor(kBlack);
h_v22Raw_Fn_2->SetMarkerColor(kBlack);
h_v22Raw_Fn_2->SetMarkerStyle(26);

//h_v22Raw_Fn_3->Draw("ex0 same");
h_v22Raw_Fn_3->SetLineColor(kBlack);
h_v22Raw_Fn_3->SetMarkerColor(kBlack);
h_v22Raw_Fn_3->SetMarkerStyle(26);

h_v22Raw_Fn_4->Draw("ex0 same");
h_v22Raw_Fn_4->SetLineColor(kBlack);
h_v22Raw_Fn_4->SetMarkerColor(kBlack);
h_v22Raw_Fn_4->SetMarkerStyle(26);


/*
h_v22Sub_Fn_1->Draw("ex0 same");
h_v22Sub_Fn_1->SetLineColor(kSpring);
h_v22Sub_Fn_1->SetMarkerColor(kSpring);
h_v22Sub_Fn_1->SetMarkerStyle(20);
h_v22Sub_Fn_1->GetXaxis()->SetRangeUser(60,160);

h_v22Sub_Fn_2->Draw("ex0 same");
h_v22Sub_Fn_2->SetLineColor(kBlack);
h_v22Sub_Fn_2->SetMarkerColor(kBlack);
h_v22Sub_Fn_2->SetMarkerStyle(24);
h_v22Sub_Fn_2->GetXaxis()->SetRangeUser(60,160);

h_v22Crt_Fn_1->Draw("ex0 same");
h_v22Crt_Fn_1->SetLineColor(kBlack);
h_v22Crt_Fn_1->SetMarkerStyle(23);
h_v22Crt_Fn_1->SetMarkerColor(kCyan+2);
h_v22Crt_Fn_1->SetLineColor(kCyan+2);
h_v22Crt_Fn_1->GetXaxis()->SetRangeUser(60,160);

h_v22Crt_Fn_2->Draw("ex0 same");
h_v22Crt_Fn_2->SetMarkerColor(kBlack);
h_v22Crt_Fn_2->SetMarkerStyle(32);
h_v22Crt_Fn_2->GetXaxis()->SetRangeUser(60,160);

h_v22SubD1_Fn_1->Draw("ex0 same");
h_v22SubD1_Fn_1->SetLineColor(kBlue);
h_v22SubD1_Fn_1->SetMarkerColor(kBlue);
h_v22SubD1_Fn_1->SetMarkerStyle(33);

h_v22SubD1_Fn_2->Draw("ex0 same");
h_v22SubD1_Fn_2->SetLineColor(kBlack);
h_v22SubD1_Fn_2->SetMarkerColor(kBlack);
h_v22SubD1_Fn_2->SetMarkerStyle(27);


//h_v22SubD1_Fn_3 ->Draw("ex0 same");
h_v22SubD1_Fn_3 ->SetLineColor(kBlack);
h_v22SubD1_Fn_3 ->SetMarkerColor(kBlack);
h_v22SubD1_Fn_3 ->SetMarkerStyle(27);
*/

myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
myText( 0.2,0.85,1,sysStr2.c_str(),0.04);

myText          (0.20,0.45-0.15,1,varStr1.c_str(),0.04);
myMarkerLineText(0.23,0.40-0.15,1.5, kMagenta, 21,kMagenta,1,"raw #it{F}_{1}", 0.04, true);
myMarkerLineText(0.23,0.35-0.15,1.5, kRed    , 22,kRed    ,1,"raw #it{F}_{2}", 0.04, true);
myMarkerLineText(0.23,0.30-0.15,1.5, kSpring , 20,kSpring ,1,"temp. fit #it{F}_{2}", 0.04, true);
myMarkerLineText(0.23,0.25-0.15,1.5, kBlue    ,33,kBlue   ,1,"#it{d}_{1} sub #it{F}_{2}", 0.04, true);
myMarkerLineText(0.23,0.20-0.15,1.5, kCyan+2    ,23,kCyan+2   ,1,"crtd temp #it{F}_{2}", 0.04, true);

myText          (0.17+0.18,0.45-0.15,1,varStr2.c_str(),0.04);
myMarkerLineText(0.23+0.18,0.40-0.15,1.5,kBlack, 25,kBlack,1,"", 0.04, true);
myMarkerLineText(0.23+0.18,0.35-0.15,1.5,kBlack, 26,kBlack,1,"", 0.04, true);
myMarkerLineText(0.23+0.18,0.30-0.15,1.5,kBlack, 24,kBlack,1,"", 0.04, true);
myMarkerLineText(0.23+0.18,0.25-0.15,1.5,kBlack, 27,kBlack,1,"", 0.04, true);
myMarkerLineText(0.23+0.18,0.20-0.15,1.5,kBlack, 32,kBlack,1,"", 0.04, true);


TH1F* h_v11Raw_Fn_pull   = (TH1F*) histPull(h_v11Raw_Fn_1,h_v11Raw_Fn_2);
TH1F* h_v22Raw_Fn_pull   = (TH1F*) histPull(h_v22Raw_Fn_1,h_v22Raw_Fn_2);
TH1F* h_v22Sub_Fn_pull   = (TH1F*) histPull(h_v22Sub_Fn_1,h_v22Sub_Fn_2);
TH1F* h_v22SubD1_Fn_pull = (TH1F*) histPull(h_v22SubD1_Fn_1,h_v22SubD1_Fn_2);
TH1F* h_v22Crt_Fn_pull   = (TH1F*) histPull(h_v22Crt_Fn_1,h_v22Crt_Fn_2);

//ratio
pad2->cd();
gPad->SetTopMargin(0);
gPad->SetBottomMargin(0.30);

h_frame_Fn_pull->Draw("AXIS");
line0->Draw("l");
line2->Draw("l");
linen2->Draw("l");

h_v11Raw_Fn_pull ->Draw("ex0 same");
h_v22Sub_Fn_pull  ->Draw("ex0 same");
h_v22Raw_Fn_pull  ->Draw("ex0 same");
h_v22SubD1_Fn_pull->Draw("ex0 same");
h_v22Crt_Fn_pull  ->Draw("ex0 same");


c5->SaveAs(Form("%sFn_%s.pdf",figPath.c_str(),varStr.c_str()));
//delete c5;

TH1F* h_Fstar_1 = (TH1F*) h_v22Raw_Fn_1->Clone("h_Fstar_1");
h_Fstar_1->Add(h_v11Raw_Fn_1,-1);

TH1F* h_Fstar_2 = (TH1F*) h_v22Raw_Fn_2->Clone("h_Fstar_2");
h_Fstar_2->Add(h_v11Raw_Fn_2,-1);

TFile* fout = new TFile(Form("../output/13TeV/Syst_%s.root",varStr.c_str()),"recreate");

vector<TH1F*> vec;
vec = calcSyst_2var(h_v11Raw_Fn_1,h_v11Raw_Fn_2,h_v11Raw_Fn_2,"v11Raw_Fn");
vec[0]->Write();
vec[1]->Write();
vec[2]->Write();
vec  = calcSyst_2var(h_v22Raw_Fn_1,h_v22Raw_Fn_2,h_v22Raw_Fn_2,"v22Raw_Fn");
vec[0]->Write();
vec[1]->Write();
vec[2]->Write();
vec  = calcSyst_2var(h_v22Sub_Fn_1,h_v22Sub_Fn_2,h_v22Sub_Fn_2,"v22Sub_Fn");
vec[0]->Write();
vec[1]->Write();
vec[2]->Write();
vec  = calcSyst_2var(h_v22SubD1_Fn_1,h_v22SubD1_Fn_2,h_v22SubD1_Fn_2,"v22SubD1_Fn");
vec[0]->Write();
vec[1]->Write();
vec[2]->Write();
vec  = calcSyst_2var(h_v22Crt_Fn_1,h_v22Crt_Fn_2,h_v22Crt_Fn_2,"v22Crt_Fn");
vec[0]->Write();
vec[1]->Write();
vec[2]->Write();

h_d2Od1_syst_1->Write();
h_d2Od1_syst_2->Write();
h_Fstar_1->Write();
h_Fstar_2->Write();

}
