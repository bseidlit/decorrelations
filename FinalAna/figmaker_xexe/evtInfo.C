#include "plotCommon.h"
#include "systemDept.h"

void evtInfo(){

string figpath = "../figures/xexe";
TFile* fin = new TFile("../../Correlation/Correlation_XeXe/rootFiles/corr_nominal_dNch.root");

TH1F* h_Nch_raw = (TH1F*) fin->Get("h_Nch_raw");
TH2F* h2_Nch_sumET = (TH2F*) fin->Get("h2_Nch_sumET");
TH2F* h2_Nch_sumET_noPileup   = (TH2F*) fin->Get("h2_Nch_sumET_noPileup");


TCanvas* c1 = new TCanvas("c1","c1",600,600);
h_Nch_raw->Draw();
h_Nch_raw->SetLineColor(kBlack);
h_Nch_raw->SetXTitle("#it{N}_{ch}^{rec}");
h_Nch_raw->SetYTitle("Events");
h_Nch_raw->GetXaxis()->SetRangeUser(0,2500);

gPad->SetLogy();

myText(0.25,0.90,1,"#bf{#it{ATLAS}} Interal");
myText(0.25,0.85,1,sysStr1.c_str());
myText(0.66,0.80,1,Form("# of events"));
myText(0.66,0.75,1,Form("%0.0f",h_Nch_raw->GetEntries()));

c1->SaveAs(Form("%s/Nch_raw.pdf",figpath.c_str()));

/*
TH1F* h_ActMu = (TH1F*) fin->Get("h_ActMu");
TCanvas* c2 = new TCanvas("c2","c2",600,600);
h_ActMu->Draw();
h_ActMu->SetLineColor(kBlack);
h_ActMu->SetXTitle("Actual #mu");
h_ActMu->SetYTitle("Events");

gPad->SetLogy();

myText(0.50,0.90,1,"#bf{#it{ATLAS}} Interal");
myText(0.50,0.85,1,sysStr1.c_str());


c2->SaveAs(Form("%s/actualMu.pdf",figpath.c_str()));
*/


TProfile* prof_Nch_sumET = (TProfile*) h2_Nch_sumET->ProfileX("prof_Nch_sumET");

TCanvas* c3 = new TCanvas("c3","c3",600,600);
h2_Nch_sumET->Draw("COLZ");
h2_Nch_sumET->SetXTitle("#it{N}_{ch}^{rec}");
h2_Nch_sumET->SetYTitle("#Sigma #it{E}_{T}");
h2_Nch_sumET->GetXaxis()->SetNdivisions(505);

prof_Nch_sumET->Draw("same");
prof_Nch_sumET->SetMarkerSize(0.5);

myText(0.30,0.90-0.65,1,"#bf{#it{ATLAS}} Interal");
myText(0.30,0.85-0.65,1,sysStr1.c_str());

gPad->SetLogz();
gPad->SetRightMargin(0.15);

c3->SaveAs(Form("%s/Nch_sumET_pileup.pdf",figpath.c_str()));




TProfile* prof_Nch_sumET_noPileup = (TProfile*) h2_Nch_sumET_noPileup->ProfileX("prof_Nch_sumET_noPileup");

TCanvas* c4 = new TCanvas("c4","c4",600,600);
h2_Nch_sumET_noPileup->Draw("COLZ");
h2_Nch_sumET_noPileup->SetXTitle("#it{N}_{ch}^{rec}");
h2_Nch_sumET_noPileup->SetYTitle("#Sigma #it{E}_{T}");
h2_Nch_sumET_noPileup->GetXaxis()->SetNdivisions(505);

prof_Nch_sumET_noPileup->Draw("same");
prof_Nch_sumET_noPileup->SetMarkerSize(0.5);

myText(0.2,0.90,1,"#bf{#it{ATLAS}} Interal");
myText(0.2,0.85,1,sysStr1.c_str());

gPad->SetLogz();
gPad->SetRightMargin(0.15);

c4->SaveAs(Form("%s/Nch_sumET_NoPileup.pdf",figpath.c_str()));




}
