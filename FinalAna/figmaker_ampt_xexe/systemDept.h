
const int Nbins_out_Nch = 13;
float dNch_out_range[Nbins_out_Nch+1] = {0,10,20,30,40,50,60,70,80,90,95,98,99,100};

string sysTag = "AMPT 5.44 TeV Xe+Xe";
string sysStr1 = "AMPT 5.44 TeV Xe+XeTrk-Twr Corr.";
string sysStr2 = "0.5 < #it{p}_{T}^{trk} < 5 GeV, #||{#it{#eta}^{twr}} > 4.0";

float LM_ref_plot_below = 50;
