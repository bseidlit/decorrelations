
const int Nbins_out_Nch = 7;
float dNch_out_range[Nbins_out_Nch+1] = {0,10, 20 , 40,60,70,90,160};

string sysTag = "13 TeV #it{pp}";
string sysStr1 = "13 TeV #it{pp} Track-Cluster Corr.";
string sysStr2 = "0.5 < #it{p}_{T}^{trk,cls} < 5 GeV, #||{#it{#eta}^{cls}} > 4.0";

float LM_ref_plot_below = 50;
