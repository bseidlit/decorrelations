#include "../figmaker_13TeV/plotCommon.h"
#include "systemDept.h"

void PlotExample(){

  initStyle();
  string figPath = "../figures/13TeV/Exp/";



  TH1F* h_v11Raw = new TH1F("h_v11Raw","",20,-2.5,2.5);
  TH1F* h_v22Raw = new TH1F("h_v22Raw","",20,-2.5,2.5);
  TH1F* h_v22Sub1 = new TH1F("h_v11Sub1","",20,-2.5,2.5);
  TH1F* h_v22Sub2 = new TH1F("h_v11Sub2","",20,-2.5,2.5);

  float d10 = 0.1;
  float d20 = 0.08;
  float fd_1 = 0.10;
  float fd_2 = 0.2;

  for (int ib=1; ib<h_v11Raw->GetNbinsX()+1; ib++){
    float eta = h_v11Raw->GetBinCenter(ib);
    float d1 = d10*(1+fd_1*eta);
    float d2 = d20*(1+fd_2*eta);
    float d2Sub1 = d1*d20/d10;
    float d2Sub2 = d1*d20/d10*(1+(fd_2-fd_1)*eta);
    h_v11Raw->SetBinContent(ib,d1);
    h_v11Raw->SetBinError(ib,0.001);
    h_v22Raw->SetBinContent(ib,d2);
    h_v22Raw->SetBinError(ib,0.001);
    h_v22Sub1->SetBinContent(ib,d2Sub1);
    h_v22Sub1->SetBinError(ib,0.001);
    h_v22Sub2->SetBinContent(ib,d2Sub2);
    h_v22Sub2->SetBinError(ib,0.001);
  }


  TCanvas* c3 = new TCanvas("c3","c3",600,600);

  h_v22Raw->Draw("ex0");
  h_v22Raw->GetYaxis()->SetRangeUser(-0.00,0.15);
  h_v22Raw->GetXaxis()->SetRangeUser(-2.5,2.5);
  h_v22Raw->GetXaxis()->SetTitle("#it{#eta^{a}}");
  h_v22Raw->GetXaxis()->SetNdivisions(509,kTRUE);
  h_v22Raw->GetYaxis()->SetTitle(" #it{d}_{n}  ");
  h_v22Raw->GetYaxis()->SetNdivisions(506,kTRUE);

  h_v22Raw->SetMarkerColor(kRed);
  h_v22Raw->SetMarkerStyle(22);
  h_v22Raw->SetLineColor(kRed);

  h_v11Raw->Draw("ex0 same");
  h_v11Raw->SetMarkerColor(kMagenta);
  h_v11Raw->SetMarkerStyle(21);
  h_v11Raw->SetLineColor(kMagenta);

  h_v22Sub1->Draw("ex0 same");
  h_v22Sub1->SetMarkerColor(kBlack);
  h_v22Sub1->SetMarkerStyle(27);
  h_v22Sub1->SetLineColor(kBlack);

  //h_v22Sub2->Draw("ex0 same");
  h_v22Sub2->SetMarkerColor(kBlue);
  h_v22Sub2->SetMarkerStyle(33);
  h_v22Sub2->SetLineColor(kBlue);

/*
  h_v22SubD1->Draw("ex0 same");
  h_v22SubD1->SetMarkerColor(kBlue);
  h_v22SubD1->SetMarkerStyle(33);
  h_v22SubD1->SetLineColor(kBlue);
*/

  myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal "),0.04);

  myMarkerLineText(0.25,0.80,1.5, kMagenta  , 21,kMagenta  , 1,"#it{d}_{1}", 0.05, true);
  myMarkerLineText(0.25,0.75,1.5, kRed , 22,kRed , 1   ,"#it{d}_{2}", 0.05, true);

  myText          (0.29,0.77-0.4,1,"Approximations of #it{d}_{2}" ,0.05);
  myMarkerLineText(0.30,0.70-0.4,1.5, kBlack, 27,kBlack , 1   ,"#it{d}_{1}*(#it{d}_{2}|^{#eta=0}/#it{d}_{1}|^{#eta=0})", 0.05, true);
//  myMarkerLineText(0.30,0.63-0.4,1.5, kBlue, 33,kBlue , 1   ,"#it{d}_{1}*(#it{d}_{2}|^{#eta=0}/#it{d}_{1}|^{#eta=0})*(1+(#it{F}^{d}_{2}-#it{F}^{d}_{1})#eta)", 0.05, true);


  c3->SaveAs(Form("%sexp_vnn_2.pdf",figPath.c_str()));
  delete c3;




  TF1* f_v2 = new TF1("f_v2","1+2*[0]*TMath::Cos(2*x)",-TMath::PiOver2(),3*TMath::PiOver2());
TF1* f_v2_mes = new TF1("f_v2_mes","[1]*(1+2*[0]*TMath::Cos(2*x))",-TMath::PiOver2(),3*TMath::PiOver2());

double v2HM[1] = {0.1};
double v2LM[1] = {0.1};


TH1F* h_HM  = new TH1F("h_HM" ,"",66,-TMath::PiOver2(),3*TMath::PiOver2());
TH1F* h_LM  = new TH1F("h_LM" ,"",66,-TMath::PiOver2(),3*TMath::PiOver2());
TH1F* h_sub = new TH1F("h_sub","",66,-TMath::PiOver2(),3*TMath::PiOver2());
float F_temp = 0.3;

for (int ib=1; ib<h_HM->GetNbinsX()+1; ib++){
  float delPhi = h_HM->GetBinCenter(ib);
  f_v2->SetParameters(v2HM);
  float val_v2 = f_v2->Eval(delPhi);
  f_v2->SetParameters(v2LM);
  float val_v2_LM = f_v2->Eval(delPhi);
  h_HM->SetBinContent(ib,val_v2);
  h_HM->SetBinError(ib,0.0001);
  h_LM->SetBinContent(ib,val_v2_LM*F_temp);
  h_LM->SetBinError(ib,0.0001);
  h_sub->SetBinContent(ib,val_v2-val_v2_LM*F_temp);
  h_sub->SetBinError(ib,0.0001);

}


TCanvas* c1 = new TCanvas("c1","c1",600,600);
h_HM->Draw("hist");

h_HM->SetXTitle("#Delta#Phi");
h_HM->SetYTitle("Corr");
h_HM->GetYaxis()->SetRangeUser(0,1.3);

h_LM->Draw("same hist");
h_LM->SetLineColor(4);
h_sub->Draw("same hist");
h_sub->SetLineColor(2);

h_sub->Fit(f_v2_mes);
float sub_v2 = f_v2_mes->GetParameter(0);

myText(0.4,0.9,1,Form("#bf{Y^{HM}  c_{2}=%0.1f}",v2HM[0]),0.04);
myText(0.4,0.25,4,Form("#bf{F^{temp}Y^{LM}  c_{2}=%0.1f}",v2LM[0]),0.04);
myText(0.4,0.4,2,Form("#bf{Y^{HM}-F^{temp}Y^{LM}  c_{2}=%0.2f}",sub_v2),0.04);


c1->SaveAs(Form("%sexp_delphi_1.pdf",figPath.c_str()));
delete c1;



TH1F* h_c2_HM = new TH1F("h_c2_HM","",20,-2.5,2.5);
TH1F* h_c2_LM = new TH1F("h_c2_HM","",20,-2.5,2.5);


float c10 = 0.1;
float c20 = 0.1;
float fc_1 = 0.10;
float fc_2 = 0.2;

for (int ib=1; ib<h_v11Raw->GetNbinsX()+1; ib++){
  float eta = h_v11Raw->GetBinCenter(ib);
  float d1 = c10*(1+fc_1*eta);
  float d2 = c20*(1+fc_2*eta);
  h_c2_HM->SetBinContent(ib,d1);
  h_c2_HM->SetBinError(ib,0.001);
  h_c2_LM->SetBinContent(ib,d2);
  h_c2_LM->SetBinError(ib,0.001);
}



  TCanvas* c2 = new TCanvas("c2","c2",600,600);

  h_c2_HM->Draw("ex0");
  h_c2_HM->GetYaxis()->SetRangeUser(-0.00,0.15);
  h_c2_HM->GetXaxis()->SetRangeUser(-2.5,2.5);
  h_c2_HM->GetXaxis()->SetTitle("#it{#eta^{a}}");
  h_c2_HM->GetXaxis()->SetNdivisions(509,kTRUE);
  h_c2_HM->GetYaxis()->SetTitle(" #it{c}_{n}  ");
  h_c2_HM->GetYaxis()->SetNdivisions(506,kTRUE);

  h_c2_HM->SetMarkerColor(kBlack);
  h_c2_HM->SetMarkerStyle(20);
  h_c2_HM->SetLineColor(kBlack);

  h_c2_LM->Draw("ex0 same");
  h_c2_LM->SetMarkerColor(kBlue);
  h_c2_LM->SetMarkerStyle(20);
  h_c2_LM->SetLineColor(kBlue);

  myText(0.4,0.68,1,"#bf{c_{2}^{HM}}",0.05);
  myText(0.4,0.55,4,"#bf{c_{2}^{LM}}",0.05);

  c2->SaveAs(Form("%sexp_c2_eta.pdf",figPath.c_str()));
  delete c2;

}
