void mergeMuSyst(){

  TFile* fin_Mu   = new TFile("../output/5TeV/Syst_Mu.root", "READ");
  TFile* fin_HMR  = new TFile("../output/5TeV/Syst_HighMuRuns.root", "READ");
  TFile* fout     = new TFile("../output/5TeV/Syst_MuComb.root","RECREATE");

  TH1F* hsys_Mu_low;
  TH1F* hsys_Mu_high;
  TH1F* hsys_HMR_low;
  TH1F* hsys_HMR_high;
  TH1F* hsys_Mu_low_comb  ;
  TH1F* hsys_Mu_high_comb ;

  // v11Raw_Fn
  {
      string obs = "v11Raw_Fn";

      hsys_Mu_low  = (TH1F*)fin_Mu->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_Mu_high = (TH1F*)fin_Mu->Get(Form("h_%s_sysHigh",obs.c_str()) );

      hsys_HMR_low  = (TH1F*)fin_HMR->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_HMR_high = (TH1F*)fin_HMR->Get(Form("h_%s_sysHigh",obs.c_str()) );

      fout->cd();
      hsys_Mu_low_comb  = (TH1F*) hsys_Mu_low ->Clone(Form("h_%s_sysLow", obs.c_str()));
      hsys_Mu_high_comb = (TH1F*) hsys_Mu_high->Clone(Form("h_%s_sysHigh",obs.c_str()));

      hsys_Mu_low_comb ->SetBinContent(1,hsys_HMR_low->GetBinContent(1));
      hsys_Mu_high_comb->SetBinContent(1,hsys_HMR_high->GetBinContent(1));
      hsys_Mu_low_comb ->SetBinContent(2,hsys_HMR_low->GetBinContent(2));
      hsys_Mu_high_comb->SetBinContent(2,hsys_HMR_high->GetBinContent(2));
      hsys_Mu_low_comb ->SetBinContent(3,hsys_HMR_low->GetBinContent(3));
      hsys_Mu_high_comb->SetBinContent(3,hsys_HMR_high->GetBinContent(3));

      hsys_Mu_low_comb ->Write();
      hsys_Mu_high_comb->Write();
  }

  // v22Raw_Fn
  {
      string obs = "v22Raw_Fn";

      hsys_Mu_low  = (TH1F*)fin_Mu->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_Mu_high = (TH1F*)fin_Mu->Get(Form("h_%s_sysHigh",obs.c_str()) );

      hsys_HMR_low  = (TH1F*)fin_HMR->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_HMR_high = (TH1F*)fin_HMR->Get(Form("h_%s_sysHigh",obs.c_str()) );

      fout->cd();
      hsys_Mu_low_comb  = (TH1F*) hsys_Mu_low ->Clone(Form("h_%s_sysLow", obs.c_str()));
      hsys_Mu_high_comb = (TH1F*) hsys_Mu_high->Clone(Form("h_%s_sysHigh",obs.c_str()));

      hsys_Mu_low_comb ->SetBinContent(1,hsys_HMR_low->GetBinContent(1));
      hsys_Mu_high_comb->SetBinContent(1,hsys_HMR_high->GetBinContent(1));
      hsys_Mu_low_comb ->SetBinContent(2,hsys_HMR_low->GetBinContent(2));
      hsys_Mu_high_comb->SetBinContent(2,hsys_HMR_high->GetBinContent(2));
      hsys_Mu_low_comb ->SetBinContent(3,hsys_HMR_low->GetBinContent(3));
      hsys_Mu_high_comb->SetBinContent(3,hsys_HMR_high->GetBinContent(3));

      hsys_Mu_low_comb ->Write();
      hsys_Mu_high_comb->Write();
  }

  // v22Sub_Fn
  {
      string obs = "v22Sub_Fn";

      hsys_Mu_low  = (TH1F*)fin_Mu->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_Mu_high = (TH1F*)fin_Mu->Get(Form("h_%s_sysHigh",obs.c_str()) );

      hsys_HMR_low  = (TH1F*)fin_HMR->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_HMR_high = (TH1F*)fin_HMR->Get(Form("h_%s_sysHigh",obs.c_str()) );

      fout->cd();
      hsys_Mu_low_comb  = (TH1F*) hsys_Mu_low ->Clone(Form("h_%s_sysLow", obs.c_str()));
      hsys_Mu_high_comb = (TH1F*) hsys_Mu_high->Clone(Form("h_%s_sysHigh",obs.c_str()));
      hsys_Mu_low_comb ->Write();
      hsys_Mu_high_comb->Write();
  }

  // v22SubD1_Fn
  {
      string obs = "v22SubD1_Fn";

      hsys_Mu_low  = (TH1F*)fin_Mu->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_Mu_high = (TH1F*)fin_Mu->Get(Form("h_%s_sysHigh",obs.c_str()) );

      hsys_HMR_low  = (TH1F*)fin_HMR->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_HMR_high = (TH1F*)fin_HMR->Get(Form("h_%s_sysHigh",obs.c_str()) );

      fout->cd();
      hsys_Mu_low_comb  = (TH1F*) hsys_Mu_low ->Clone(Form("h_%s_sysLow", obs.c_str()));
      hsys_Mu_high_comb = (TH1F*) hsys_Mu_high->Clone(Form("h_%s_sysHigh",obs.c_str()));
      hsys_Mu_low_comb ->Write();
      hsys_Mu_high_comb->Write();
  }

  // v22Crt_Fn
  {
      string obs = "v22Crt_Fn";

      hsys_Mu_low  = (TH1F*)fin_Mu->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_Mu_high = (TH1F*)fin_Mu->Get(Form("h_%s_sysHigh",obs.c_str()) );

      hsys_HMR_low  = (TH1F*)fin_HMR->Get(Form("h_%s_sysLow", obs.c_str()) );
      hsys_HMR_high = (TH1F*)fin_HMR->Get(Form("h_%s_sysHigh",obs.c_str()) );

      fout->cd();
      hsys_Mu_low_comb  = (TH1F*) hsys_Mu_low ->Clone(Form("h_%s_sysLow", obs.c_str()));
      hsys_Mu_high_comb = (TH1F*) hsys_Mu_high->Clone(Form("h_%s_sysHigh",obs.c_str()));
      hsys_Mu_low_comb ->Write();
      hsys_Mu_high_comb->Write();
  }

}
