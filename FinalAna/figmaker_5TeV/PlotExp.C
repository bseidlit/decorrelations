#include "../figmaker_13TeV/plotCommon.h"
#include "systemDept.h"

void PlotExp(){

initStyle();
string figPath = "../figures/5TeV/Exp/";

TFile* fnom = new TFile("../output/5TeV/Results_nominal.root");


TH1F* h_dNch_v11_raw_ref  = (TH1F*) fnom->Get("h_dNch_v11_raw_ref");
TH1F* h_dNch_v22_raw_ref  = (TH1F*) fnom->Get("h_dNch_v22_raw_ref");
TH1F* h_dNch_v22_sub_ref  = (TH1F*) fnom->Get("h_dNch_v22_sub_ref");
TH1F* h_dNch_v33_raw_ref  = (TH1F*) fnom->Get("h_dNch_v33_raw_ref");
TH1F* h_dNch_v33_sub_ref  = (TH1F*) fnom->Get("h_dNch_v33_sub_ref");
TH1F* h_dNch_v22_sub_ref2 = (TH1F*) fnom->Get("h_dNch_v22_sub_ref2");
TH1F* h_dNch_Fremp        = (TH1F*) fnom->Get("h_dNch_Fremp");
TH1F* h_dNch_Fremp_ref2   = (TH1F*) fnom->Get("h_dNch_Fremp_ref2");

TH1F* h_d2Od1      = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref,h_dNch_v22_raw_ref,h_dNch_v22_sub_ref);
h_d2Od1->SetName("h_d2Od1");

TH1F* h_d2Od1_ref2 = (TH1F*)
  calc_d2Od1_ratio(h_dNch_v11_raw_ref,h_dNch_v22_raw_ref,h_dNch_v22_sub_ref2);
h_d2Od1_ref2->SetName("h_d2Od1_ref2");




TCanvas* c1 = new TCanvas("c1","c1",600,600);

h_frame_v22_ref->Draw("AXIS");
h_frame_v22_ref->GetYaxis()->SetTitle("#it{a}_{2} or #it{c}_{2}  ");
h_dNch_v22_sub_ref->Draw("ex0 same");
h_dNch_v22_sub_ref->SetBinContent(1,0);
h_dNch_v22_sub_ref->SetBinError(1,0);
h_dNch_v22_sub_ref->SetBinContent(2,0);
h_dNch_v22_sub_ref->SetBinError(2,0);

h_dNch_v22_raw_ref->Draw("ex0 same");
h_dNch_v22_raw_ref->SetMarkerColor(kRed);
h_dNch_v22_raw_ref->SetMarkerStyle(24);

h_dNch_v22_sub_ref2->Draw("ex0 same");
h_dNch_v22_sub_ref2->SetMarkerColor(kBlue);
h_dNch_v22_sub_ref2->SetLineColor(kBlue);
h_dNch_v22_sub_ref2->SetMarkerStyle(25);
h_dNch_v22_sub_ref2->SetBinContent(2,0);
h_dNch_v22_sub_ref2->SetBinError(2,0);
h_dNch_v22_sub_ref2->SetBinContent(1,0);
h_dNch_v22_sub_ref2->SetBinError(1,0);
h_dNch_v22_sub_ref2->SetBinContent(3,0);
h_dNch_v22_sub_ref2->SetBinError (3,0);

myText( 0.2,0.90,1,"#font[72]{ATLAS} Internal",0.04);
myText( 0.2,0.83,1,sysStr1.c_str(),0.04);
myText( 0.2,0.76,1,sysStr2.c_str(),0.04);

myMarkerLineText(0.5,0.70,1.5, kRed  , 24,kRed  , 1,"raw (#it{d}_{2}+#it{c}_{2})", 0.04, true);
myMarkerLineText(0.5,0.65,1.5, kBlack, 20,kBlack, 1,"temp. fit (#it{c}_{2}) LM 0-20 ", 0.04, true);
myMarkerLineText(0.5,0.60,1.5, kBlue , 25,kBlue , 1,"temp. fit (#it{c}_{2}) LM 40-60", 0.04, true);

c1->SaveAs(Form("%sv22_ref.pdf",figPath.c_str()));

zeroRefd2Od1(h_d2Od1,1);
zeroRefd2Od1(h_d2Od1_ref2,4);

TCanvas* c2 = new TCanvas("c2","c2",600,600);

h_frame_d2Od1->Draw("AXIS");
h_d2Od1->Draw("ex0 same");

h_d2Od1_ref2->Draw("ex0 same");
h_d2Od1_ref2->SetMarkerColor(kBlue);
h_d2Od1_ref2->SetLineColor(kBlue);
h_d2Od1_ref2->SetMarkerStyle(25);
h_d2Od1_ref2->SetMarkerStyle(25);

myText( 0.2,0.90,1,"#font[72]{ATLAS} Internal",0.04);
myText( 0.2,0.83,1,sysStr1.c_str(),0.04);
myText( 0.2,0.76,1,sysStr2.c_str(),0.04);

myMarkerLineText(0.5,0.70,1.5, kBlack, 20,kBlack, 1,"LM 0-20", 0.04, true);
myMarkerLineText(0.5,0.65,1.5, kBlue , 25,kBlue , 1,"LM 40-60", 0.04, true);


const int numRef = 2;
TH1F* h_d2Od1_comb[numRef];
h_d2Od1_comb[0] = h_d2Od1;
h_d2Od1_comb[1] = h_d2Od1_ref2;
pair<float,float> final_d2Od1 = calcAvgStd(numRef,h_d2Od1_comb);

myText(0.3,0.5,1,Form("derived #it{d}_{2}/#it{d}_{1} = %0.3f#pm%0.3f",final_d2Od1.first,final_d2Od1.second));

c2->SaveAs(Form("%sd2Od1_ref.pdf",figPath.c_str()));



TH1F* h_v11Raw_Fn   = new TH1F("h_v11Raw_Fn","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Raw_Fn   = new TH1F("h_v22Raw_Fn","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Sub_Fn   = new TH1F("h_v22Sub_Fn","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22SubD1_Fn = new TH1F("h_v22SubD1_Fn","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Raw_Fn   = new TH1F("h_v33Raw_Fn","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v33Sub_Fn   = new TH1F("h_v33Sub_Fn","",Nbins_out_Nch,dNch_out_range);
TH1F* h_v22Crt_Fn   = new TH1F("h_v22Crt_Fn","",Nbins_out_Nch,dNch_out_range);

TH1F* h_v11Raw_plot[Nbins_out_Nch];
TH1F* h_Ftemp_plot[Nbins_out_Nch];

cout << "\\hline" << endl;
cout << "\\Nrec"<<  " & " <<
    "$c_{2}^\\text{LM}$" << " & " <<
    "$F_{2}^\\text{LM}$" << " & "<<
    "$c_{2}^\\text{HM}$" << " & " <<
    "$F_{2}^\\text{temp}$" <<" & " <<
    "$\\rho$" << " \\\\" << endl;
cout << "\\hline" << endl;

for (int in=0; in<Nbins_out_Nch; in++){
  TH1F* h_v11Raw = (TH1F*) fnom->Get(Form("hNch%d_den_v11Raw_deta",in+1));
  TH1F* h_v22Raw = (TH1F*) fnom->Get(Form("hNch%d_den_v22Raw_deta",in+1));
  TH1F* h_v22Sub = (TH1F*) fnom->Get(Form("hNch%d_den_v22Sub_deta",in+1));
  TH1F* h_v33Raw = (TH1F*) fnom->Get(Form("hNch%d_den_v33Raw_deta",in+1));
  TH1F* h_v33Sub = (TH1F*) fnom->Get(Form("hNch%d_den_v33Sub_deta",in+1));
  TH1F* h_Ftemp  = (TH1F*) fnom->Get(Form("hNch%d_Ftemp_deta",in+1));

  h_v11Raw_plot[in] = (TH1F*) fnom->Get(Form("hNch%d_den_v11Raw_deta",in+1));
  h_Ftemp_plot[in] = (TH1F*) fnom->Get(Form("hNch%d_Ftemp_deta",in+1));

  TH1F* h_v22SubD1 = (TH1F*) D1subCorrected(h_v11Raw,h_v22Raw,final_d2Od1.first,0.04);
  h_v22SubD1->SetName("hNch%d_den_v22SubD1_deta");

  TH1F* h_v11Raw_rn   = (TH1F*) calc_rn(h_v11Raw);
  TH1F* h_v22Raw_rn   = (TH1F*) calc_rn(h_v22Raw);
  TH1F* h_v22Sub_rn   = (TH1F*) calc_rn(h_v22Sub);
  TH1F* h_v33Raw_rn   = (TH1F*) calc_rn(h_v33Raw);
  TH1F* h_v33Sub_rn   = (TH1F*) calc_rn(h_v33Sub);
  TH1F* h_v22SubD1_rn = (TH1F*) calc_rn(h_v22SubD1);

  calcF Fcalc;
  Fcalc.init();

  Fcalc.calc(h_v11Raw_rn  );
  TGraphErrors* gr_v11Raw_Fn = (TGraphErrors*) Fcalc.graph();
  h_v11Raw_Fn->SetBinContent(in+1,Fcalc.f_val);
  h_v11Raw_Fn->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Raw_rn  );
  TGraphErrors* gr_v22Raw_Fn = (TGraphErrors*) Fcalc.graph();
  h_v22Raw_Fn->SetBinContent(in+1,Fcalc.f_val);
  h_v22Raw_Fn->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22Sub_rn  );
  TGraphErrors* gr_v22Sub_Fn = (TGraphErrors*) Fcalc.graph();
  h_v22Sub_Fn->SetBinContent(in+1,Fcalc.f_val);
  h_v22Sub_Fn->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v22SubD1_rn);
  TGraphErrors* gr_v22SubD1_Fn = (TGraphErrors*) Fcalc.graph();
  h_v22SubD1_Fn->SetBinContent(in+1,Fcalc.f_val);
  h_v22SubD1_Fn->SetBinError(in+1,Fcalc.f_err);

  Fcalc.calc(h_v33Raw_rn  );
  TGraphErrors* gr_v33Raw_Fn = (TGraphErrors*) Fcalc.graph();
  h_v33Raw_Fn->SetBinContent(in+1,Fcalc.f_val);
  h_v33Raw_Fn->SetBinError(in+1,Fcalc.f_err);
  gr_v33Raw_Fn->SetLineColor(kSpring);

  Fcalc.calc(h_v33Sub_rn  );
  TGraphErrors* gr_v33Sub_Fn = (TGraphErrors*) Fcalc.graph();
  h_v33Sub_Fn->SetBinContent(in+1,Fcalc.f_val);
  h_v33Sub_Fn->SetBinError(in+1,Fcalc.f_err);
  gr_v33Sub_Fn->SetLineColor(kSpring);

  crtdTempFit crtdTempFit;
  crtdTempFit.Ftemp_val = h_Ftemp->GetBinContent(4);;
  crtdTempFit.Ftemp_err = h_Ftemp->GetBinError(4);;
  crtdTempFit.F2_HM_val = h_v22Sub_Fn->GetBinContent(in+1);
  crtdTempFit.F2_HM_err = h_v22Sub_Fn->GetBinError(in+1);
  crtdTempFit.F2_LM_val = h_v22SubD1_Fn->GetBinContent(4);
  crtdTempFit.F2_LM_err = 0;
  crtdTempFit.c2_LM_val = h_dNch_v22_sub_ref->GetBinContent(4);
  crtdTempFit.c2_LM_err = 0;
  crtdTempFit.c2_HM_val = h_dNch_v22_sub_ref->GetBinContent(in+1);
  crtdTempFit.c2_HM_err = h_dNch_v22_sub_ref->GetBinError(in+1);

  pair<float,float> crtn =  crtdTempFit.calcCrtn();
  h_v22Crt_Fn->SetBinContent(in+1,crtn.first);
  h_v22Crt_Fn->SetBinError  (in+1,crtn.second);

  if (in>=4){
    cout << dNch_out_range[in] << "-" <<  dNch_out_range[in+1]<<  " & " <<
     crtdTempFit.c2_LM_val << " & " <<
     crtdTempFit.F2_LM_val << " & "<<
     crtdTempFit.c2_HM_val << " & " <<
     crtdTempFit.F2_HM_val <<" & " <<
     crtdTempFit.Ftemp_val << " \\\\" << endl;
 }


  TCanvas* c3 = new TCanvas("c3","c3",600,600);
  h_v22Raw->Draw("ex0");
  h_v22Raw->GetYaxis()->SetRangeUser(0.00,0.003);
  h_v22Raw->GetXaxis()->SetRangeUser(-2.5,2.5);
  h_v22Raw->GetXaxis()->SetTitle("#it{#eta^{a}}");
  h_v22Raw->GetXaxis()->SetNdivisions(509,kTRUE);
  h_v22Raw->GetYaxis()->SetTitle(" #it{a}_{2}");
  h_v22Raw->GetYaxis()->SetNdivisions(506,kTRUE);

  h_v22Raw->SetMarkerColor(kRed);
  h_v22Raw->SetMarkerStyle(22);
  h_v22Raw->SetLineColor(kRed);
/*
  TLine* mr_v22_raw = new TLine(-1.0,h_dNch_v22_raw_ref->GetBinContent(in+1),1.0,h_dNch_v22_raw_ref->GetBinContent(in+1));
  mr_v22_raw->SetLineColor(kRed);
  mr_v22_raw->SetLineWidth(2);
  mr_v22_raw->Draw("same");

  TLine* mr_v22_sub = new TLine(-1.0,h_dNch_v22_sub_ref->GetBinContent(in+1),1.0,h_dNch_v22_sub_ref->GetBinContent(in+1));
  mr_v22_sub->SetLineColor(kRed);
  //mr_v22_sub->Draw("same");

  if (in >= 4) h_v22Sub->Draw("ex0 same");
  h_v22Sub->SetMarkerColor(kSpring);
  h_v22Sub->SetMarkerStyle(20);
  h_v22Sub->SetLineColor(kSpring);

  h_v22SubD1->Draw("ex0 same");
  h_v22SubD1->SetMarkerColor(kBlue);
  h_v22SubD1->SetMarkerStyle(33);
  h_v22SubD1->SetLineColor(kBlue);

  line0->Draw("l");
*/
  myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
  myText( 0.2,0.85,1,sysStr2.c_str(),0.04);
  myText( 0.47,0.77,1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]),0.04);

  myMarkerLineText(0.25,0.70+0.1,1.5, kRed  , 22,kRed  , 1,"raw (#it{a}_{2})", 0.04, true);
  //if (in > 3) myMarkerLineText(0.25,0.65+0.1,1.5, kSpring, 20,kSpring, 1,"temp. fit (#it{c}_{2}) ", 0.04, true);
  //myMarkerLineText(0.25,0.60+0.1,1.5, kBlue , 33,kBlue , 1,"#it{d}_{1} sub (#it{c}_{2})", 0.04, true);
  //myMarkerLineText(0.25,0.53+0.1,0, kRed , 0,kRed , 1,"#int Y(#Delta#Phi,#eta^{a})d#eta^{a} #rightarrow #it{a}_{2} ", 0.04, true);

  c3->SaveAs(Form("%svnn_Nch%d.pdf",figPath.c_str(),in));
  delete c3;


  TCanvas* c4 = new TCanvas("c4","c4",600,600);

  h_v11Raw_rn->GetYaxis()->SetRangeUser(0.0,1.15);
  h_v11Raw_rn->GetXaxis()->SetRangeUser(0,2.5);
  h_v11Raw_rn->GetXaxis()->SetTitle("|#it{#eta^{a}}|");
  h_v11Raw_rn->GetXaxis()->SetNdivisions(509,kTRUE);
  h_v11Raw_rn->GetYaxis()->SetTitle("#it{r}_{2}");
  h_v11Raw_rn->GetYaxis()->SetNdivisions(506,kTRUE);

 h_v11Raw_rn   ->Draw("AXIS");
  h_v11Raw_rn   ->SetLineColor(kMagenta);
  h_v11Raw_rn   ->SetMarkerColor(kMagenta);
  gr_v11Raw_Fn->SetLineColor(kMagenta);
  h_v11Raw_rn   ->SetMarkerStyle(21);
  //gr_v11Raw_Fn->Draw("l");


  h_v22Raw_rn   ->Draw("ex0 same");
  h_v22Raw_rn   ->SetLineColor(kRed);
  h_v22Raw_rn   ->SetMarkerColor(kRed);
  gr_v22Raw_Fn->SetLineColor(kRed);
  h_v22Raw_rn   ->SetMarkerStyle(23);
  gr_v22Raw_Fn->Draw("l");
/*
  if (in >= 4) h_v22Sub_rn   ->Draw("ex0 same");
  h_v22Sub_rn   ->SetLineColor(kSpring);
  h_v22Sub_rn   ->SetMarkerColor(kSpring);
  gr_v22Sub_Fn->SetLineColor(kSpring);
  h_v22Sub_rn   ->SetMarkerStyle(20);
    if (in >= 4) gr_v22Sub_Fn->Draw("l");

  TGraphAsymmErrors* gr_v22SubD1_rn = new TGraphAsymmErrors(h_v22SubD1_rn);
  shiftGraph(gr_v22SubD1_rn,0.02,0);
  gr_v22SubD1_rn ->Draw("p");
  gr_v22SubD1_rn ->SetLineColor(kBlue);
  gr_v22SubD1_rn ->SetMarkerColor(kBlue);
  gr_v22SubD1_Fn->SetLineColor(kBlue);
  gr_v22SubD1_rn ->SetMarkerStyle(33);
  gr_v22SubD1_Fn->Draw("l");
*/
  line1->Draw("l");

  myText( 0.2,0.91,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
  myText( 0.2,0.86,1,sysStr2.c_str(),0.04);
  myText( 0.2,0.45,1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]),0.04);

  //myMarkerLineText(0.25,0.40-0.04,1.5, kMagenta, 21,kMagenta, 1,"raw #it{r}_{1}", 0.04, true);
  myMarkerLineText(0.25,0.35-0.04,1.5, kRed  , 23,kRed  , 1,"raw #it{r}_{2}", 0.04, true);
  myMarkerLineText(0.25,0.30-0.04,0, kRed  , 1,kRed  , 7,"#it{F}_{2}", 0.04, true);
  //if (in > 3) myMarkerLineText(0.25,0.30-0.04,1.5, kSpring, 20,kSpring, 1,"temp. fit #it{r}_{2}", 0.04, true);
  //myMarkerLineText(0.25,0.25-0.04,1.5, kBlue , 33,kBlue , 1,"#it{d}_{1} sub #it{r}_{2}", 0.04, true);


  c4->SaveAs(Form("%srn_Nch%d.pdf",figPath.c_str(),in));
  delete c4;

}


TCanvas* cFtemp = new TCanvas("cFtemp","cFtemp",600,600);
h_Ftemp_plot[4]->Draw("ex0");
h_Ftemp_plot[4]->GetYaxis()->SetRangeUser(0,1);
h_Ftemp_plot[4]->GetXaxis()->SetRangeUser(-2.5,2.5);
h_Ftemp_plot[4]->GetXaxis()->SetTitle("#it{#eta^{a}}");
h_Ftemp_plot[4]->GetXaxis()->SetNdivisions(509,kTRUE);
h_Ftemp_plot[4]->GetYaxis()->SetTitle(" #it{#rho}");
h_Ftemp_plot[4]->GetYaxis()->SetNdivisions(506,kTRUE);

TLine* line_Ftemp[Nbins_out_Nch];

for (int in=4; in<Nbins_out_Nch; in++){
  bool even = in%2==0;
  h_Ftemp_plot[in]->Draw("ex0 same");
  if (even) h_Ftemp_plot[in]->SetLineColor  (kRed-3+in);
  if (even) h_Ftemp_plot[in]->SetMarkerColor(kRed-3+in);
  if (even) h_Ftemp_plot[in]->SetMarkerStyle(22);
  if (even) myMarkerLineText(0.22,0.55-in*0.04,1.5,kRed-3+in, 22,kRed-3+in, 1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]), 0.03, true);
  //if (even) line_Ftemp[in]->SetLineColor(kRed-3+in);

  if (!even) h_Ftemp_plot[in]->SetLineColor  (kBlue+3-in);
  if (!even) h_Ftemp_plot[in]->SetMarkerColor(kBlue+3-in);
  if (!even) h_Ftemp_plot[in]->SetMarkerStyle(34);
  if (!even) myMarkerLineText(0.22,0.55-in*0.04,1.5,kBlue+3-in, 34,kBlue+3-in, 1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]), 0.03, true);
  //if (!even) line_Ftemp[in]->SetLineColor(kBlue+3-in);


  float mr_val_Ftemp = h_dNch_Fremp_ref2->GetBinContent(in+1);
  line_Ftemp[in] = new TLine(-2.5,mr_val_Ftemp,2.5,mr_val_Ftemp);
  line_Ftemp[in]->SetLineColor(kRed-3+in);
  line_Ftemp[in]->SetLineWidth(2);
  //line_Ftemp[in]->Draw("same");

}

myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
myText( 0.2,0.85,1,sysStr2.c_str(),0.04);

myMarkerLineText(0.22,0.20,0,kBlack, 0,kBlack, 1,"#intY(#Delta#Phi,#it{#eta}^{a})d#it{#eta}^{a} #rightarrow #it{#rho} ", 0.04, true);


cFtemp->SaveAs(Form("%sFtemp.pdf",figPath.c_str()));
delete cFtemp;




TCanvas* c7 = new TCanvas("c7","c7",600,600);
h_v11Raw_plot[0]->Draw("ex0");
h_v11Raw_plot[0]->GetYaxis()->SetRangeUser(-0.06,0.01);
h_v11Raw_plot[0]->GetXaxis()->SetRangeUser(-2.5,2.5);
h_v11Raw_plot[0]->GetXaxis()->SetTitle("#it{#eta^{a}}");
h_v11Raw_plot[0]->GetXaxis()->SetNdivisions(509,kTRUE);
h_v11Raw_plot[0]->GetYaxis()->SetTitle(" #it{a}_{1}");
h_v11Raw_plot[0]->GetYaxis()->SetNdivisions(506,kTRUE);

TLine* line_v11[Nbins_out_Nch];

for (int in=0; in<Nbins_out_Nch; in++){

      float mr_val_v11 = h_dNch_v11_raw_ref->GetBinContent(in+1);
      line_v11[in] = new TLine(-1.0,mr_val_v11,1.0,mr_val_v11);
      line_v11[in]->SetLineWidth(2);
      line_v11[in]->Draw("same");

    bool even = in%2==0;
    h_v11Raw_plot[in]->Draw("ex0 same");
    if (even) h_v11Raw_plot[in]->SetLineColor  (kRed-3+in);
    if (even) h_v11Raw_plot[in]->SetMarkerColor(kRed-3+in);
    if (even) h_v11Raw_plot[in]->SetMarkerStyle(22);
    if (even) myMarkerLineText(0.22,0.55-in*0.04,1.5,kRed-3+in, 22,kRed-3+in, 1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]), 0.03, true);
    if (even) line_v11[in]->SetLineColor(kRed-3+in);

    if (!even) h_v11Raw_plot[in]->SetLineColor  (kBlue+3-in);
    if (!even) h_v11Raw_plot[in]->SetMarkerColor(kBlue+3-in);
    if (!even) h_v11Raw_plot[in]->SetMarkerStyle(34);
    if (!even) myMarkerLineText(0.22,0.55-in*0.04,1.5,kBlue+3-in, 34,kBlue+3-in, 1,Form("#it{N}_{ch}=[%0.0f,%0.0f]",dNch_out_range[in],dNch_out_range[in+1]), 0.03, true);
    if (!even) line_v11[in]->SetLineColor(kBlue+3-in);


}

myMarkerLineText(0.22,0.20,0,kBlack, 0,kBlack, 1,"#intY(#Delta#Phi,#it{#eta}^{a})d#it{#eta}^{a} #rightarrow #it{a}_{1} ", 0.04, true);

line0->Draw("l");
TLine* vert0 = new TLine(0,-0.06,0,0.01);
vert0->SetLineStyle(7);
vert0->Draw("same");

myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
myText( 0.2,0.85,1,sysStr2.c_str(),0.04);

c7->SaveAs(Form("%sv11Raw.pdf",figPath.c_str()));
delete c7;




TCanvas* c5 = new TCanvas("c5","c5",600,600);

h_v11Raw_Fn->GetYaxis()->SetRangeUser(0.0,0.3);
h_v11Raw_Fn->GetXaxis()->SetTitle("#it{N}_{ch}");
h_v11Raw_Fn->GetXaxis()->SetNdivisions(509,kTRUE);
h_v11Raw_Fn->GetYaxis()->SetTitle("#it{F}_{2}");
h_v11Raw_Fn->GetYaxis()->SetNdivisions(506,kTRUE);


//h_v11Raw_Fn   ->Draw("ex0");
h_v11Raw_Fn   ->SetLineColor(kMagenta);
h_v11Raw_Fn   ->SetMarkerColor(kMagenta);
h_v11Raw_Fn   ->SetMarkerStyle(21);

h_v22Raw_Fn   ->Draw("ex0 same");
h_v22Raw_Fn   ->SetLineColor(kRed);
h_v22Raw_Fn   ->SetMarkerColor(kRed);
h_v22Raw_Fn   ->SetMarkerStyle(23);
/*
h_v22Sub_Fn   ->Draw("ex0 same");
h_v22Sub_Fn   ->SetLineColor(kSpring);
h_v22Sub_Fn   ->SetMarkerColor(kSpring);
h_v22Sub_Fn   ->SetMarkerStyle(20);
h_v22Sub_Fn->GetXaxis()->SetRangeUser(60,160);

h_v22Crt_Fn ->Draw("ex0 same ");
h_v22Crt_Fn ->SetLineColor(kCyan+2);
h_v22Crt_Fn ->SetMarkerColor(kCyan+2);
h_v22Crt_Fn ->SetMarkerStyle(22);
h_v22Crt_Fn->GetXaxis()->SetRangeUser(60,160);

h_v22SubD1_Fn ->Draw("ex0 same");
h_v22SubD1_Fn ->SetLineColor(kBlue);
h_v22SubD1_Fn ->SetMarkerColor(kBlue);
h_v22SubD1_Fn ->SetMarkerStyle(33);
*/
myText( 0.2,0.90,1,Form("#font[72]{ATLAS} Internal  %s",sysStr1.c_str()),0.04);
myText( 0.2,0.85,1,sysStr2.c_str(),0.04);
//
//myMarkerLineText(0.22,0.40-0.0,1.5,kMagenta, 21,kMagenta, 1,"raw #it{F}_{1}", 0.04, true);
myMarkerLineText(0.22,0.35-0.0,1.5,kRed    , 23,kRed    , 1,"raw #it{F}_{2}", 0.04, true);
//myMarkerLineText(0.22,0.30-0.0,1.5,kSpring , 20,kSpring, 1,"temp. fit #it{F}_{2}", 0.04, true);
//myMarkerLineText(0.22,0.25-0.0,1.5,kBlue   , 33,kBlue  , 1,"#it{d}_{1} sub #it{F}_{2}", 0.04, true);
//myMarkerLineText(0.22,0.20-0.0,1.5,kCyan+2 , 22,kCyan+2, 1,"crtd. temp. #it{F}_{2}", 0.04, true);


c5->SaveAs(Form("%sFn.pdf",figPath.c_str()));
delete c5;

/*
TFile* fout = new TFile("../output/5TeV/ANA_Results_nominal.root","RECREATE");
h_v22Sub_Fn->Write();
h_v22Raw_Fn->Write();
h_v22SubD1_Fn->Write();
h_v22Crt_Fn->Write();
h_v11Raw_Fn->Write();
*/
}
