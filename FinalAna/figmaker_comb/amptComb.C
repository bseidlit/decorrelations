#include "../figmaker_xexe/plotCommon.h"

void shiftLogGraph(TGraphAsymmErrors* gr, float shift,float x_err){
  for (int ig=1; ig<gr->GetN(); ig++) {
         gr->GetX()[ig] += shift*gr->GetX()[ig]/25.;
         gr->GetEXlow ()[ig] =  x_err*gr->GetX()[ig]/25.;
         gr->GetEXhigh()[ig] =  x_err*gr->GetX()[ig]/25.;
  }
  return;
}

pair <TGraphAsymmErrors*,TGraphAsymmErrors*> mergeStatSyst(TGraphAsymmErrors* stat1,TGraphAsymmErrors* syst1, TGraphAsymmErrors* stat2, TGraphAsymmErrors* syst2){
  pair <TGraphAsymmErrors*,TGraphAsymmErrors*> result;
  result.first  = new TGraphAsymmErrors();
  result.second = new TGraphAsymmErrors();
  for (int ip=0; ip<stat1->GetN(); ip++){
    double x1,x2,y1,y2,el1,el2,eh1,eh2,exl,exh;
    stat1->GetPoint(ip,x1,y1);
    stat2->GetPoint(ip,x2,y2);
    result.first ->SetPoint(ip,(x1+x2)/2,(y1+y2)/2);
    result.second->SetPoint(ip,(x1+x2)/2,(y1+y2)/2);

    float estatl = quad(stat1->GetErrorYlow(ip) ,stat2->GetErrorYlow(ip))/2;
    float estath = quad(stat1->GetErrorYhigh(ip),stat2->GetErrorYhigh(ip))/2;
    result.first->SetPointError(ip,stat1->GetErrorXlow(ip),stat1->GetErrorXhigh(ip),estatl,estath);
    float valsystl = min(y1-syst1->GetErrorYlow(ip) ,y2-syst2->GetErrorYlow(ip));
    float valsysth = max(y1+syst1->GetErrorYhigh(ip),y2+syst2->GetErrorYhigh(ip));
    float esystl = (y1+y2)/2 - valsystl;
    float esysth = valsysth  - (y1+y2)/2;
    result.second->SetPointError(ip,syst1->GetErrorXlow(ip),syst1->GetErrorXhigh(ip),esystl,esysth);
  }
  return result;
}

void amptComb() {
    initStyle();
    TFile* fin = new TFile("../output/xexe/Results_withSyst.root","Read");

    TGraphAsymmErrors* gr_v11Raw_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v11Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v11Raw_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v11Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Sub_Fn_StatError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Sub_Fn_SystError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22SubD1_Fn_StatError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22SubD1_Fn_SystError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Stat_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Crt_Fn_StatError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Syst_xexe = (TGraphAsymmErrors*) fin->Get("g_v22Crt_Fn_SystError");

    float plotShift = 0.5;

    pair <TGraphAsymmErrors*,TGraphAsymmErrors*> gr_v22SubComb_xexe;
    gr_v22SubComb_xexe = mergeStatSyst(gr_v22Crt_Fn_Stat_xexe,gr_v22Crt_Fn_Syst_xexe, gr_v22SubD1_Fn_Stat_xexe,gr_v22SubD1_Fn_Syst_xexe);

    shiftLogGraph(gr_v22SubComb_xexe.first ,0,0);
    shiftLogGraph(gr_v22SubComb_xexe.second,0,1);


    shiftLogGraph(gr_v11Raw_Fn_Stat_xexe,0,0);
    shiftLogGraph(gr_v11Raw_Fn_Syst_xexe,0,1);
    shiftLogGraph(gr_v22Raw_Fn_Stat_xexe,0,0);
    shiftLogGraph(gr_v22Raw_Fn_Syst_xexe,0,1);
    shiftLogGraph(gr_v22Sub_Fn_Stat_xexe,0,0);
    shiftLogGraph(gr_v22Sub_Fn_Syst_xexe,0,1);
    shiftLogGraph(gr_v22SubD1_Fn_Stat_xexe,   plotShift,0);
    shiftLogGraph(gr_v22SubD1_Fn_Syst_xexe,   plotShift,1);
    shiftLogGraph(gr_v22Crt_Fn_Stat_xexe  ,-1*plotShift,0);
    shiftLogGraph(gr_v22Crt_Fn_Syst_xexe  ,-1*plotShift,1);


    gr_v22Crt_Fn_Stat_xexe->SetPoint(0,-8,0);
    gr_v22Crt_Fn_Syst_xexe->SetPoint(0,-8,0);
    gr_v22Sub_Fn_Stat_xexe->SetPoint(0,-8,0);
    gr_v22Sub_Fn_Syst_xexe->SetPoint(0,-8,0);



    TFile* fin_5TeV = new TFile("../output/5TeV/Results_withSyst.root","Read");

    TGraphAsymmErrors* gr_v11Raw_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v11Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v11Raw_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v11Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Sub_Fn_StatError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Sub_Fn_SystError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Stat_5TeV = (TGraphAsymmErrors*) fin_5TeV->Get("g_v22SubD1_Fn_StatError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Syst_5TeV = (TGraphAsymmErrors*) fin_5TeV->Get("g_v22SubD1_Fn_SystError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Stat_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Crt_Fn_StatError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Syst_5TeV = (TGraphAsymmErrors*)   fin_5TeV->Get("g_v22Crt_Fn_SystError");

    pair <TGraphAsymmErrors*,TGraphAsymmErrors*> gr_v22SubComb_5TeV;
    gr_v22SubComb_5TeV = mergeStatSyst(gr_v22Crt_Fn_Stat_5TeV,gr_v22Crt_Fn_Syst_5TeV, gr_v22SubD1_Fn_Stat_5TeV,gr_v22SubD1_Fn_Syst_5TeV);

    shiftLogGraph(gr_v22SubComb_5TeV.first ,0,0);
    shiftLogGraph(gr_v22SubComb_5TeV.second,0,1);



    shiftLogGraph(gr_v11Raw_Fn_Stat_5TeV,0,0);
    shiftLogGraph(gr_v11Raw_Fn_Syst_5TeV,0,1);
    shiftLogGraph(gr_v22Raw_Fn_Stat_5TeV,0,0);
    shiftLogGraph(gr_v22Raw_Fn_Syst_5TeV,0,1);
    shiftLogGraph(gr_v22Sub_Fn_Stat_5TeV,0,0);
    shiftLogGraph(gr_v22Sub_Fn_Syst_5TeV,0,1);
    shiftLogGraph(gr_v22SubD1_Fn_Stat_5TeV,   plotShift,0);
    shiftLogGraph(gr_v22SubD1_Fn_Syst_5TeV,   plotShift,1);
    shiftLogGraph(gr_v22Crt_Fn_Stat_5TeV  ,-1*plotShift,0);
    shiftLogGraph(gr_v22Crt_Fn_Syst_5TeV  ,-1*plotShift,1);

    gr_v22SubComb_5TeV.first->SetPoint(0,-8,0);
    gr_v22SubComb_5TeV.first->SetPoint(1,-8,0);
    gr_v22SubComb_5TeV.first->SetPoint(2,-8,0);
    gr_v22SubComb_5TeV.first->SetPoint(3,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(0,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(1,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(2,-8,0);
    gr_v22SubComb_5TeV.second->SetPoint(3,-8,0);





    TFile* fin_13TeV = new TFile("../output/13TeV/Results_withSyst.root","Read");

    TGraphAsymmErrors* gr_v11Raw_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v11Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v11Raw_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v11Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Raw_Fn_StatError");
    TGraphAsymmErrors* gr_v22Raw_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Raw_Fn_SystError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Sub_Fn_StatError");
    TGraphAsymmErrors* gr_v22Sub_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Sub_Fn_SystError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Stat_13TeV = (TGraphAsymmErrors*) fin_13TeV->Get("g_v22SubD1_Fn_StatError");
    TGraphAsymmErrors* gr_v22SubD1_Fn_Syst_13TeV = (TGraphAsymmErrors*) fin_13TeV->Get("g_v22SubD1_Fn_SystError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Stat_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Crt_Fn_StatError");
    TGraphAsymmErrors* gr_v22Crt_Fn_Syst_13TeV = (TGraphAsymmErrors*)   fin_13TeV->Get("g_v22Crt_Fn_SystError");

    pair <TGraphAsymmErrors*,TGraphAsymmErrors*> gr_v22SubComb_13TeV;
    gr_v22SubComb_13TeV = mergeStatSyst(gr_v22Crt_Fn_Stat_13TeV,gr_v22Crt_Fn_Syst_13TeV, gr_v22SubD1_Fn_Stat_13TeV,gr_v22SubD1_Fn_Syst_13TeV);

    shiftLogGraph(gr_v22SubComb_13TeV.first ,0,0);
    shiftLogGraph(gr_v22SubComb_13TeV.second,0,1);



    shiftLogGraph(gr_v11Raw_Fn_Stat_13TeV,0,0);
    shiftLogGraph(gr_v11Raw_Fn_Syst_13TeV,0,1);
    shiftLogGraph(gr_v22Raw_Fn_Stat_13TeV,0,0);
    shiftLogGraph(gr_v22Raw_Fn_Syst_13TeV,0,1);
    shiftLogGraph(gr_v22Sub_Fn_Stat_13TeV,0,0);
    shiftLogGraph(gr_v22Sub_Fn_Syst_13TeV,0,1);
    shiftLogGraph(gr_v22SubD1_Fn_Stat_13TeV,   plotShift,0);
    shiftLogGraph(gr_v22SubD1_Fn_Syst_13TeV,   plotShift,1);
    shiftLogGraph(gr_v22Crt_Fn_Stat_13TeV  ,-1*plotShift,0);
    shiftLogGraph(gr_v22Crt_Fn_Syst_13TeV  ,-1*plotShift,1);


    gr_v22SubComb_13TeV.first->SetPoint(0,-8,0);
    gr_v22SubComb_13TeV.first->SetPoint(1,-8,0);
    gr_v22SubComb_13TeV.first->SetPoint(2,-8,0);
    gr_v22SubComb_13TeV.first->SetPoint(3,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(0,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(1,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(2,-8,0);
    gr_v22SubComb_13TeV.second->SetPoint(3,-8,0);



    TFile* f_ampt_xexe = new TFile("../output/ampt/ANA_Results_xexe_nominal.root");
    TH1F* h_v22Raw_Fn_ampt = (TH1F*) f_ampt_xexe->Get("h_v22Raw_Fn_mult");
    TH1F* h_geo2_Fn_ampt = (TH1F*) f_ampt_xexe->Get("h_geo2_Fn_mult");

    TFile* f_ampt_pp = new TFile("../output/ampt_pp/ANA_Results_nominal.root");
    TH1F* h_v22Raw_Fn_ampt_pp = (TH1F*) f_ampt_pp->Get("h_v22Raw_Fn");
    TH1F* h_geo2_Fn_ampt_pp = (TH1F*) f_ampt_pp->Get("h_geo2_Fn");



    TCanvas* c1 = new TCanvas("c1","New Canvas",50,50,700,600);
    h_frame_Fn->Draw("AXIS");
    h_frame_Fn->GetXaxis()->SetTitle("#it{N}_{ch}^{rec}");
    h_frame_Fn->GetYaxis()->SetTitle("#it{F}_{2}");
    h_frame_Fn->GetYaxis()->SetRangeUser(5e-3,0.5);
    h_frame_Fn->GetXaxis()->SetRangeUser(20,2500);

    float alphaF = 0.6;

/*
    gr_v11Raw_Fn_Syst_xexe->Draw("2");
    gr_v11Raw_Fn_Syst_xexe->SetFillColorAlpha(kMagenta-7,alphaF);
    gr_v11Raw_Fn_Syst_xexe->SetFillColorAlpha(kMagenta-7,alphaF);
    gr_v11Raw_Fn_Stat_xexe->Draw("PSAME");
    gr_v11Raw_Fn_Stat_xexe->SetMarkerColor(kMagenta+1);
    gr_v11Raw_Fn_Stat_xexe->SetLineColor(kMagenta+1);
    gr_v11Raw_Fn_Stat_xexe->SetMarkerStyle(21);
*/
/*
    gr_v22Raw_Fn_Syst_xexe->Draw("2");
    gr_v22Raw_Fn_Syst_xexe->SetFillColorAlpha(kBlue-7,alphaF);
    gr_v22Raw_Fn_Syst_xexe->SetFillColorAlpha(kBlue-7,alphaF);
    gr_v22Raw_Fn_Stat_xexe->Draw("PSAME");
    gr_v22Raw_Fn_Stat_xexe->SetMarkerColor(kBlue+1);
    gr_v22Raw_Fn_Stat_xexe->SetLineColor(kBlue+1);
    gr_v22Raw_Fn_Stat_xexe->SetMarkerStyle(24);
    */
    gr_v22Raw_Fn_Syst_5TeV->Draw("2");
    gr_v22Raw_Fn_Syst_5TeV->SetFillColorAlpha(kGray+1,alphaF);
    gr_v22Raw_Fn_Stat_5TeV->Draw("PSAME");
    gr_v22Raw_Fn_Stat_5TeV->SetMarkerColor(kBlack);
    gr_v22Raw_Fn_Stat_5TeV->SetLineColor  (kBlack);
    gr_v22Raw_Fn_Stat_5TeV->SetMarkerStyle(24);

    /*
    gr_v22Raw_Fn_Syst_13TeV->Draw("2");
    gr_v22Raw_Fn_Syst_13TeV->SetFillColorAlpha(kRed-7,alphaF);
    gr_v22Raw_Fn_Syst_13TeV->SetFillColorAlpha(kRed-7,alphaF);
    gr_v22Raw_Fn_Stat_13TeV->Draw("PSAME");
    gr_v22Raw_Fn_Stat_13TeV->SetMarkerColor(kRed+1);
    gr_v22Raw_Fn_Stat_13TeV->SetLineColor(kRed+1);
    gr_v22Raw_Fn_Stat_13TeV->SetMarkerStyle(24);

*/
/*
    gr_v22Sub_Fn_Syst_xexe->Draw("2");
    gr_v22Sub_Fn_Syst_xexe->SetFillColorAlpha(kGreen-7,alphaF);
    gr_v22Sub_Fn_Syst_xexe->SetFillColorAlpha(kGreen-7,alphaF);
    gr_v22Sub_Fn_Stat_xexe->Draw("PSAME");
    gr_v22Sub_Fn_Stat_xexe->SetMarkerColor(kGreen+2);
    gr_v22Sub_Fn_Stat_xexe->SetLineColor(kGreen+2);
    gr_v22Sub_Fn_Stat_xexe->SetMarkerStyle(20);
    */
/*
    gr_v22Crt_Fn_Syst_xexe->Draw("2");
    gr_v22Crt_Fn_Syst_xexe->SetFillColorAlpha(kCyan-7,alphaF);
    gr_v22Crt_Fn_Syst_xexe->SetLineColor(kCyan-7);
    gr_v22Crt_Fn_Stat_xexe->Draw("PSAME");
    gr_v22Crt_Fn_Stat_xexe->SetMarkerColor(kCyan+2);
    gr_v22Crt_Fn_Stat_xexe->SetLineColor(kCyan+2);
    gr_v22Crt_Fn_Stat_xexe->SetMarkerStyle(22);
*/
/*
    gr_v22SubComb_xexe.second->Draw("2");
    gr_v22SubComb_xexe.second->SetFillColorAlpha(kBlue-7,alphaF);
    gr_v22SubComb_xexe.second->SetLineColor(kBlue-7);
    gr_v22SubComb_xexe.first->Draw("PSAME");
    gr_v22SubComb_xexe.first->SetMarkerColor(kBlue+2);
    gr_v22SubComb_xexe.first->SetLineColor(kBlue+2);
    gr_v22SubComb_xexe.first->SetMarkerStyle(33);
    */
    gr_v22SubComb_5TeV.second->Draw("2");
    gr_v22SubComb_5TeV.second->SetFillColorAlpha(kGray+1,alphaF);
    gr_v22SubComb_5TeV.second->SetLineColor(kGray);
    gr_v22SubComb_5TeV.first->Draw("PSAME");
    gr_v22SubComb_5TeV.first->SetMarkerColor(kBlack);
    gr_v22SubComb_5TeV.first->SetLineColor  (kBlack);
    gr_v22SubComb_5TeV.first->SetMarkerStyle(33);

    /*
    gr_v22SubComb_13TeV.second->Draw("2");
    gr_v22SubComb_13TeV.second->SetFillColorAlpha(kRed-7,alphaF);
    gr_v22SubComb_13TeV.second->SetLineColor(kRed-7);
    gr_v22SubComb_13TeV.first->Draw("PSAME");
    gr_v22SubComb_13TeV.first->SetMarkerColor(kRed+2);
    gr_v22SubComb_13TeV.first->SetLineColor(kRed+2);
    gr_v22SubComb_13TeV.first->SetMarkerStyle(33);
*/
/*
    h_v22Raw_Fn_ampt->Draw("ex0 same");
    h_v22Raw_Fn_ampt->SetMarkerStyle(20);
    h_v22Raw_Fn_ampt->SetMarkerColor(kBlue);
    h_v22Raw_Fn_ampt->SetLineColor(kBlue);

    h_geo2_Fn_ampt->Draw("same lhist");
    h_geo2_Fn_ampt->SetLineWidth(7);
    h_geo2_Fn_ampt->SetLineColor  (kBlue-7);
*/

  h_v22Raw_Fn_ampt_pp->Draw("ex0 same");
  h_v22Raw_Fn_ampt_pp->SetMarkerStyle(20);
  h_v22Raw_Fn_ampt_pp->SetMarkerColor(kBlue);
  h_v22Raw_Fn_ampt_pp->SetLineColor(kBlue);
  h_geo2_Fn_ampt_pp->Draw("same lhist");
  h_geo2_Fn_ampt_pp->SetLineWidth(7);
  h_geo2_Fn_ampt_pp->SetLineColor  (kBlue-7);

    h_frame_Fn->Draw("AXIS same");


    gPad->SetLogx();
    gPad->SetLogy();

    myText( 0.27,0.91,1,"#font[72]{ATLAS} Internal",0.04);
    myText( 0.27,0.86,1,"|#it{#eta}^{a}|<2.5, 0.5<#it{p}_{T}^{a}<5.0",0.04);
    myText( 0.27,0.81,1,"|#it{#eta}^{ref}|>4.0",0.04);
    myText( 0.20,0.20,1,"#it{v}_{2,2} = 1 + 2#times#it{F}_{n}#times#it{#eta}^{a}",0.04);
/*
    myMarkerLineText(0.22,0.40-0.0,1.5,kMagenta, 21,kMagenta, 1,"raw #it{F}_{1}", 0.04, true);
    myMarkerLineText(0.22,0.35-0.0,1.5,kRed    , 23,kRed    , 1,"raw #it{F}_{2}", 0.04, true);
    myMarkerLineText(0.22,0.30-0.0,1.5,kSpring , 20,kSpring, 1,"temp. fit #it{F}_{2}", 0.04, true);
    */
    myText          (0.65-0.05,0.90 ,1,"AMPT pp",0.04);
    myMarkerLineText(0.65     ,0.85 ,1.5,kBlue, 20,kBlue, 1,"Raw", 0.04, true);
  //  myMarkerLineText(0.65     ,0.80 ,1.5,kBlack, 33,kBlack, 1,"Sub", 0.04, true);
    myOnlyBoxText   (0.65     , 0.80,1.2,kBlue-7  ,0,1,"initial-state partons", 0.04, 1001);

    myText          (0.65-0.05,0.90-0.15 ,1,"#it{pp} 13 TeV trk-clus",0.04);
    myMarkerLineText(0.65     ,0.85-0.15 ,1.5,kBlack, 24,kBlack, 1,"Raw", 0.04, true);
    myMarkerLineText(0.65     ,0.80-0.15 ,1.5,kBlack, 33,kBlack, 1,"Sub", 0.04, true);
  //  myText          (0.65-0.05,0.90-0.15 ,1,"Xe+Xe 5.44 TeV trk-twr",0.04);
  //  myMarkerLineText(0.65     ,0.85-0.15 ,1.5,kBlue+2, 24,kBlue+2, 1,"Raw", 0.04, true);
  //  myMarkerLineText(0.65     ,0.80-0.15 ,1.5,kBlue+2, 33,kBlue+2, 1,"Sub", 0.04, true);


  c1->SaveAs("../figures/comb/ampt_finalResults_Fn.pdf");


}
