
const int Nbins_out_Nch = 1;
float dNch_out_range[Nbins_out_Nch+1] = {0,150};

string sysTag = "Jewel 5 TeV #it{pp}";
string sysStr1 = "5 TeV #it{pp} Truth-Truth Corr.";
string sysStr2 = "0.5 < #it{p}_{T} < 5 GeV, #||{#it{#eta}^{cls}} > 4.0";

float LM_ref_plot_below = 50;
