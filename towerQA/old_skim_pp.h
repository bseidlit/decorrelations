//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Feb 23 13:45:33 2020 by ROOT version 6.18/04
// from TTree analysis/My analysis ntuple
// found on file: ../RacfFile/user.qhu.20651067._000013.ANALYSIS.root
//////////////////////////////////////////////////////////

#ifndef skim_pp_h
#define skim_pp_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class skim_pp {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   int syst = 0;

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           RunNumber;
   Int_t           EventNumber;
   Int_t           LumiBlock;
   Float_t         AvgMu;
   Float_t         ActMu;
   Float_t         PV_x;
   Float_t         PV_y;
   Float_t         PV_z;
   Int_t           nVtx;
   Bool_t          L1_MBTS_1;
   Int_t           Ntrk_MB;
   vector<TString> *TriggerObject_Chain;
   vector<float>   *TriggerObject_Ps;
   vector<int>     *Vertex_nTrk;
   vector<float>   *Vertex_sumPt;
   vector<float>   *Vertex_sumPt2;
   vector<int>     *Vertex_type;
   vector<float>   *Vertex_x;
   vector<float>   *Vertex_y;
   vector<float>   *Vertex_z;
   vector<float>   *Track_pt;
   vector<float>   *Track_eta;
   vector<float>   *Track_phi;
   vector<float>   *Track_eff;
   vector<float>   *Track_d0Signif;
   vector<float>   *Track_z0SinTheta;
   vector<float>   *Cluster_et;
   vector<float>   *Cluster_eta;
   vector<float>   *Cluster_phi;
   vector<float>   *Cluster_size;
   vector<int>     *Cluster_status;
   vector<float>   *tower_pt;
   vector<float>   *tower_Phi;
   Int_t            ntower;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_LumiBlock;   //!
   TBranch        *b_AvgMu;   //!
   TBranch        *b_ActMu;   //!
   TBranch        *b_PV_x;   //!
   TBranch        *b_PV_y;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_L1_MBTS_1;   //!
   TBranch        *b_Ntrk_MB;   //!
   TBranch        *b_TriggerObject_Chain;   //!
   TBranch        *b_TriggerObject_Ps;   //!
   TBranch        *b_Vertex_nTrk;   //!
   TBranch        *b_Vertex_sumPt;   //!
   TBranch        *b_Vertex_sumPt2;   //!
   TBranch        *b_Vertex_type;   //!
   TBranch        *b_Vertex_x;   //!
   TBranch        *b_Vertex_y;   //!
   TBranch        *b_Vertex_z;   //!
   TBranch        *b_Track_pt;   //!
   TBranch        *b_Track_eta;   //!
   TBranch        *b_Track_phi;   //!
   TBranch        *b_Track_eff;   //!
   TBranch        *b_Track_d0Signif;   //!
   TBranch        *b_Track_z0SinTheta;   //!
   TBranch        *b_Cluster_et;   //!
   TBranch        *b_Cluster_eta;   //!
   TBranch        *b_Cluster_phi;   //!
   TBranch        *b_Cluster_size;   //!
   TBranch        *b_Cluster_status;   //!
   TBranch        *b_tower_pt;   //!
   TBranch        *b_tower_Phi;   //!
   TBranch        *b_ntower; //! 


   skim_pp(TTree *tree=0);
   virtual ~skim_pp();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual float    trkEff(float,float);

   TH2F *h2_trkEff;
};

#endif

#ifdef skim_pp_cxx
skim_pp::skim_pp(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
/*   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../../input/user.qhu.20651067._000002.ANALYSIS.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../../input/user.qhu.20651067._000002.ANALYSIS.root");
      }
      f->GetObject("analysis",tree);

   } */
   Init(tree);
}

skim_pp::~skim_pp()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t skim_pp::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t skim_pp::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void skim_pp::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   TriggerObject_Chain = 0;
   TriggerObject_Ps = 0;
   Vertex_nTrk = 0;
   Vertex_sumPt = 0;
   Vertex_sumPt2 = 0;
   Vertex_type = 0;
   Vertex_x = 0;
   Vertex_y = 0;
   Vertex_z = 0;
   Track_pt = 0;
   Track_eta = 0;
   Track_phi = 0;
   Track_eff = 0;
   Track_d0Signif = 0;
   Track_z0SinTheta = 0;
   Cluster_et = 0;
   Cluster_eta = 0;
   Cluster_phi = 0;
   Cluster_size = 0;
   Cluster_status = 0;
   tower_pt = 0;
   tower_Phi = 0;
   ntower = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("LumiBlock", &LumiBlock, &b_LumiBlock);
   fChain->SetBranchAddress("AvgMu", &AvgMu, &b_AvgMu);
   fChain->SetBranchAddress("ActMu", &ActMu, &b_ActMu);
   fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
   fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("L1_MBTS_1", &L1_MBTS_1, &b_L1_MBTS_1);
   fChain->SetBranchAddress("Ntrk_MB", &Ntrk_MB, &b_Ntrk_MB);
   fChain->SetBranchAddress("TriggerObject_Chain", &TriggerObject_Chain, &b_TriggerObject_Chain);
   fChain->SetBranchAddress("TriggerObject_Ps", &TriggerObject_Ps, &b_TriggerObject_Ps);
   fChain->SetBranchAddress("Vertex_nTrk", &Vertex_nTrk, &b_Vertex_nTrk);
   fChain->SetBranchAddress("Vertex_sumPt", &Vertex_sumPt, &b_Vertex_sumPt);
   fChain->SetBranchAddress("Vertex_sumPt2", &Vertex_sumPt2, &b_Vertex_sumPt2);
   fChain->SetBranchAddress("Vertex_type", &Vertex_type, &b_Vertex_type);
   fChain->SetBranchAddress("Vertex_x", &Vertex_x, &b_Vertex_x);
   fChain->SetBranchAddress("Vertex_y", &Vertex_y, &b_Vertex_y);
   fChain->SetBranchAddress("Vertex_z", &Vertex_z, &b_Vertex_z);
   fChain->SetBranchAddress("Track_pt", &Track_pt, &b_Track_pt);
   fChain->SetBranchAddress("Track_eta", &Track_eta, &b_Track_eta);
   fChain->SetBranchAddress("Track_phi", &Track_phi, &b_Track_phi);
/*   fChain->SetBranchAddress("Track_eff", &Track_eff, &b_Track_eff);
   fChain->SetBranchAddress("Track_d0Signif", &Track_d0Signif, &b_Track_d0Signif);
   fChain->SetBranchAddress("Track_z0SinTheta", &Track_z0SinTheta, &b_Track_z0SinTheta);
   fChain->SetBranchAddress("Cluster_et", &Cluster_et, &b_Cluster_et);
   fChain->SetBranchAddress("Cluster_eta", &Cluster_eta, &b_Cluster_eta);
   fChain->SetBranchAddress("Cluster_phi", &Cluster_phi, &b_Cluster_phi);
   fChain->SetBranchAddress("Cluster_size", &Cluster_size, &b_Cluster_size);
   fChain->SetBranchAddress("Cluster_status", &Cluster_status, &b_Cluster_status);
*/

   fChain->SetBranchAddress("tower_pt", &tower_pt, &b_tower_pt);
   fChain->SetBranchAddress("tower_phi", &tower_Phi, &b_tower_Phi);
   fChain->SetBranchAddress("ntower", &ntower, &b_ntower);
   
   TFile* f_trkEff = new TFile("trkEff_nominal.root");
   h2_trkEff = (TH2F*) f_trkEff->Get("eff_eta_pt");


   Notify();
}

float skim_pp::trkEff(float eta,float pt){
  int bin  = h2_trkEff->FindBin(eta,pt);
  float track_efficiency = h2_trkEff->GetBinContent(bin);
  return track_efficiency;
}

Bool_t skim_pp::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void skim_pp::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t skim_pp::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef skim_pp_cxx
