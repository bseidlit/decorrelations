#define skim_pp_cxx
#include "skim_pp.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "pp_towers_gt4.0_array_phi.txt"
#include "pp_towers_gt4.0_array.txt"
#include "pp_towers_calib_cuts.txt"

// trigger particle: tracks of eta selection -2.5 to 2.5
// associated particle: clusters of eta selection 4.0 to 4.9

void skim_pp::Loop() {
    if (fChain == 0) return;
    
    std::cout.imbue(std::locale(""));


    TFile* fout = new TFile("skim_pp.root","RECREATE");

    // Basic setting
    const float PVzCut = 100.;
    const int NchCut = 150;
    const float ActMuCut_low  = 0.0;// 0.54 split
    const float ActMuCut_high = 2.5;//2.5 max  0.452 split
    const float cluster_pt_low  = 0.5;
    const float cluster_pt_high = 5.0;
    const float cluster_eta_low = 4.0;
    const int poolSize = 21; // pool size

    // varation
    bool req_1vtx = 0;
    bool weightClusterPt = 0;
    bool splitData = 0; const int splitFac = 1;
    bool poisFluc = 0;
    bool randSplit = 0; const int randIntSplitFac = 1;
    bool nearbyVertexSyst = 0;  const float nearbyVertexCut = 10;//mm

    


    Long64_t nentries = fChain->GetEntries();
    //Long64_t nentries = 10000; //for testing
    Long64_t nbytes = 0, nb = 0;

    //////////////////////////////////////////////////////////////////////
   //Track roconstuction efficiency tool
   // poisson fluctuation for synthetic data
   TRandom3 rnd = TRandom3();
   rnd.SetSeed();
   if (randSplit) rnd.SetSeed(nentries+1);


    //-----------------------------------------------------------------------------

    const int Nch_interval = 5;
    const int Nbins_Nch = NchCut / Nch_interval;
    double dNch_range[Nbins_Nch+1];
    for (int i=0; i<Nbins_Nch+1; i++) {
        dNch_range[i] = i*Nch_interval;
    }


    // Nch slices
    TH1F* hNch_binning = new TH1F("hNch_binning","",Nbins_Nch,dNch_range);
    TH1F* h_pt_rec_trk60 = new TH1F("h_pt_rec_trk60","",46,0.4,5.0);

    const float dPVz_interval = 20;// default 20
    const int Nbins_PVz = 2*PVzCut/dPVz_interval;


  std::vector< std::string > m_AnalysisTriggers;

  // 2015 low-mu run trigger list
  m_AnalysisTriggers.push_back("HLT_mb_sptrk");
  m_AnalysisTriggers.push_back("HLT_noalg_mb_L1MBTS_1");
  m_AnalysisTriggers.push_back("HLT_mb_sp900_trk60_hmt_L1MBTS_1_1");
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE10");

  // 2017 low-mu run trigger list
  //m_AnalysisTriggers.push_back("HLT_mb_sptrk");
  m_AnalysisTriggers.push_back("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10");
  m_AnalysisTriggers.push_back("HLT_mb_sp1100_pusup450_trk70_hmt_L1TE20");
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_pusup550_trk90_hmt_L1TE30");
  m_AnalysisTriggers.push_back("HLT_mb_sp1600_pusup600_trk100_hmt_L1TE40");
  m_AnalysisTriggers.push_back("HLT_mb_sp1700_pusup650_trk110_hmt_L1TE50");

  // 2017 5 TeV run trigger list
  // no pileup suppression
  // mb trigger HLT_mb_sptrk
  m_AnalysisTriggers.push_back("HLT_mb_sp1400_trk90_hmt_L1TE25");
  m_AnalysisTriggers.push_back("HLT_mb_sp1200_trk80_hmt_L1TE20");
  m_AnalysisTriggers.push_back("HLT_mb_sp1100_trk70_hmt_L1TE15");
  m_AnalysisTriggers.push_back("HLT_mb_sp700_trk50_hmt_L1TE10");
  m_AnalysisTriggers.push_back("HLT_mb_sp600_trk40_hmt_L1TE5");

  //////////////////////////////////////////////////
  //  HIstograms
  TH2F* h_Ntrk_Nclus_barrel = new TH2F("h_Ntrk_Nclus_barrel","",3000,0,3000,500,0,500);
  TH2F* h_Ntrk_Nclus_FCAL = new TH2F("h_Ntrk_Nclus_FCAL","",3000,0,3000,500,0,500);
  TH1F* h_Ntrk = new TH1F("h_Ntrk","",3000,0,3000);
  TH2F* h_Ntrk_FCAL_sumET = new TH2F("h_Ntrk_FCAL_sumET","",3000,0,3000,3000,0,3000);

  const int numTrig = m_AnalysisTriggers.size();

  const int n_towers = 1171;
  TH1F* h_tower_pt[n_towers];
  for (int it=0; it<n_towers; it++) h_tower_pt[it] = new TH1F(Form("h_tower_pt%d",it),"",5e4,-1,5);
   TH1F* h_tower_phi = new TH1F("h_tower_phi","",1000,-TMath::Pi(),TMath::Pi());

  TH1F* h_phi_check = new TH1F("h_phi_check","",64,-TMath::Pi(),TMath::Pi());
  TH1F* h_same_dphi = new TH1F("h_same_dphi","",64,-TMath::Pi()/2.-TMath::Pi()/64.,3.*TMath::Pi()/2.-TMath::Pi()/64.);
  TH1F* h_mix_dphi = new TH1F("h_mix_dphi","",64,-TMath::Pi()/2.-TMath::Pi()/64.,3.*TMath::Pi()/2.-TMath::Pi()/64.);
  float tower_pt_mix[n_towers] = {0};
  bool filledPool = 0;
TH2F* h2_tower_y_pt_calib = new TH2F("h2_tower_y_pt_calib","",100,-5,5,64,-TMath::Pi(),TMath::Pi());

    const int n_ed = 50;
    int n_ds = 0; // counter 
    TH2F* h2_eventDisplays[n_ed];
    for (int ied=0; ied<n_ed; ied++)
      h2_eventDisplays[ied] = new TH2F(Form("h2_eventDisplays%d",ied),"",100,-5,5,64,-TMath::Pi(),TMath::Pi());


    //TH2F* h2_nsig_raw = new TH2F("h2_nsig_raw", "", Nbins_pt, dpt_range, Nbins_Nch, dNch_range);
    //TH1F* h_Nch_raw = new TH1F("h_Nch_raw", "", 450, 0, 450);
    TH1F* h_ActMu = new TH1F("h_ActMu","",500,0,5);

    cout << "total event number: " << nentries << endl;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        //if (jentry > 1e4) continue;

        // split data for systematic checks
        if (splitData && jentry%2 != splitFac) continue;
        //if (jentry > 4.*nentries/4. || jentry < 3.*nentries/4.)continue;
        int randInt = rnd.Integer(2);
        if (randSplit &&  randInt != randIntSplitFac) continue;

        nb = fChain->GetEntry(jentry);   nbytes += nb;
      	if (jentry%50000 == 0)  cout << "------------------running event " << jentry << "------------------" << endl;

        ///////////////////////////////
        // bad events 
        if (EventNumber == 1005029851 ) continue;
        if (EventNumber ==  -689257889) continue;
        if (EventNumber ==  -351900998) continue;
        if (EventNumber ==  620626355) continue;
        if (EventNumber ==  319335) continue;
        if (EventNumber ==  -1509348982) continue;
        if (EventNumber == 223514985 ) continue;
        if (EventNumber ==  1533853005) continue;
        if (EventNumber ==  -1282177608) continue;
        if (EventNumber ==  -2086729361) continue;
        if (EventNumber ==  1005029851) continue;
        if (EventNumber ==   547902208) continue;
        if (EventNumber ==   547903633) continue;        


        //cout << endl << TriggerObject_Chain->size(); 
        //cout << endl << EventNumber << " "  << Track_pt->size() << " " << Track_phi->size() << " " << Track_eta->size() << " " << Cluster_eta->size() << " " << Cluster_et->size() << " " << Cluster_phi->size() << " " ;

        ///////////////////////////////
        // trigger selection
        bool _passTrig = false;
        for (int itrig = 0; itrig < TriggerObject_Chain->size(); itrig++) {
            if ((TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sptrk"))||(TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp1400_trk90_hmt_L1TE25") && Ntrk_MB >= 90) || (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp1200_trk80_hmt_L1TE20") && Ntrk_MB >= 80) || (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp1100_trk70_hmt_L1TE15") && Ntrk_MB >= 70) || ((TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp700_trk50_hmt_L1TE10") || TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp700_pusup350_trk50_hmt_L1TE10")) && Ntrk_MB >= 50) || (TriggerObject_Chain->at(itrig).EqualTo("HLT_mb_sp600_trk40_hmt_L1TE5") && Ntrk_MB >= 40))
               _passTrig = true;
       }
       if (!_passTrig) continue;

       //////////////////////////////////
       // Calc event level quantities
       float min_Del_r = 100000;
       if (nearbyVertexSyst){
          for (int iv=0; iv<Vertex_x->size(); iv++){
            float dR = sqrt( pow(PV_x-Vertex_x->at(iv),2) +  pow(PV_y-Vertex_y->at(iv),2) + pow(PV_z-Vertex_z->at(iv),2));
            if ( dR < min_Del_r && Vertex_type->at(iv) > 1) min_Del_r = dR;
          }
        }

       Ntrk_MB = 0;
       for (int itrk=0; itrk<Track_pt->size(); itrk++){
          if (Track_pt->at(itrk) > 400) Ntrk_MB++;
       }

       //cout << Track_pt->size() << " " << Track_eta->size() <<  endl;

       //////////////////////////////////
       // event selection
        if (req_1vtx && nVtx <= 2) continue;
        if (fabs(PV_z) > PVzCut) continue;
        if (Ntrk_MB == 0) continue;
        if (ActMu > ActMuCut_high || ActMu <  ActMuCut_low) continue;
        if (nearbyVertexSyst && min_Del_r < nearbyVertexCut ) continue;

        //////////////////////////////////////////////////////
        // generate random number of resamplings for bootstrap
        int poisEvtW = 1;
        if (poisFluc) poisEvtW = (int) rnd.Poisson(1);

        ///////////////////////////////
        // event binning
        int Vz_bin = (int)((PV_z + PVzCut)/dPVz_interval);
        int Nch_bin = hNch_binning->FindBin(Ntrk_MB)-1;
        //h_Nch_raw->Fill(Ntrk_MB);
        h_ActMu->Fill(ActMu);

        int trk_400 = Ntrk_MB;

        /////////////////////////////////////////////////////////
        // Fill histograms
 
        h_Ntrk->Fill(trk_400);
  
        int multDef = trk_400;

        // tower hists 
        for (int it=0; it<tower_pt->size() && it<n_towers; it++){
          h_tower_pt[it]->Fill(tower_pt->at(it));
         // if ( tower_pt->at(it) > 0.2) cout << " " << tower_pt->at(it);
        }
        //h_tower_phi->Fill(tower_Phi->at(333));

        // event displays 
        //if (n_ds < n_ed){
        //  for (int it=0; it<n_towers; it++){
        //     if (tower_pt->at(it) > 0.5) h2_eventDisplays[n_ds]->Fill(tower_eta[it],tower_phi[it]);
        //  }
        //  n_ds++;
        //}


        //////////////////////////
        // tower-tower correlation
/*
       for (int it=0; it<tower_pt->size() && it<n_towers; it++){        
         if (tower_pt->at(it) > cuts[it]){
           h2_tower_y_pt_calib->Fill(tower_eta[it],tower_phi[it]);
         }
       }

       if (filledPool==1){
        for(int ipos=0; ipos<n_towers; ipos++){
          if (tower_eta[ipos] < 0)continue; 
          if (tower_pt->at(ipos) < 0.5) continue;
          h_phi_check->Fill(tower_phi[ipos]);
          for(int ineg=0; ineg<n_towers; ineg++){
            if (tower_eta[ineg] > 0) continue;
            if (tower_pt->at(ineg) < 0.5) continue;
            double dphi = tower_phi[ipos] - tower_phi[ineg];
            while (dphi >  3*TMath::PiOver2()-0.0001) dphi -= 2*TMath::Pi();
            while (dphi < -1*TMath::PiOver2()-0.0001) dphi += 2*TMath::Pi();
            h_same_dphi->Fill(dphi);
          }
       }

        for(int ipos=0; ipos<n_towers; ipos++){
          if (tower_eta[ipos] < 0 )continue;
          if (tower_pt->at(ipos) < 0.5) continue;
          for(int ineg=0; ineg<n_towers; ineg++){
            if (tower_eta[ineg] > 0 ) continue;
            if (tower_pt_mix[ineg] < 0.5) continue;
            double dphi = tower_phi[ipos] - tower_phi[ineg];
            while (dphi >  3*TMath::PiOver2()-0.0001) dphi -= 2*TMath::Pi();
            while (dphi < -1*TMath::PiOver2()-0.0001) dphi += 2*TMath::Pi();
            h_mix_dphi->Fill(dphi);
          }
       } 
       }

       for(int it=0; it<n_towers; it++) tower_pt_mix[it] = tower_pt->at(it);
       filledPool=1;
       */
//cout << endl << "----------------------";
            
        
    }// event loop



    for (int it=0; it<n_towers; it++) h_tower_pt[it]->Write();

    for (int ids=0; ids<n_ed; ids++) h2_eventDisplays[ids]->Write();

    h_Ntrk_Nclus_barrel->Write();
    h_Ntrk_Nclus_FCAL->Write();
    h_Ntrk->Write();
    h_Ntrk_FCAL_sumET->Write();
    
     h_phi_check->Write();
     h_same_dphi->Write();
     h_mix_dphi->Write();

     h_tower_phi->Write();

      h2_tower_y_pt_calib->Write();

    fout->Close();
}
