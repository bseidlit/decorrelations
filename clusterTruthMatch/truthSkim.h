//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri May  5 17:47:38 2023 by ROOT version 6.18/04
// from TTree analysis/My analysis ntuple
// found on file: /sphenix/u/bseidlitz/work/trees/pp_13TeV_MB_pythia/user.bseidlit.ppDecorrelation_ppMBMC_trkClus_13TeV_01.361203.e3639_s2601_s2132_r6616_r6270_tid05411175_00_ANALYSIS.root/user.bseidlit.33235767._000005.ANALYSIS.root
//////////////////////////////////////////////////////////

#ifndef truthSkim_h
#define truthSkim_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class truthSkim {
public :
   TChain          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           RunNumber;
   Long64_t        EventNumber;
   Int_t           LumiBlock;
   Float_t         AvgMu;
   Float_t         ActMu;
   Float_t         fcalet_A;
   Float_t         fcalet_C;
   Float_t         PV_x;
   Float_t         PV_y;
   Float_t         PV_z;
   Int_t           nVtx;
   Bool_t          L1_MBTS_1;
   Int_t           Ntrk_MB;
   Int_t           N_barrelClus;
   Int_t           L1_energyT;
   Float_t         sumGapAC;
   vector<TString> *TriggerObject_Chain;
   vector<float>   *TriggerObject_Ps;
   Int_t           nTrkOnline;
   UInt_t          nTrk_AR;
   vector<int>     *Vertex_nTrk;
   vector<float>   *Vertex_sumPt;
   vector<float>   *Vertex_sumPt2;
   vector<int>     *Vertex_type;
   vector<float>   *Vertex_x;
   vector<float>   *Vertex_y;
   vector<float>   *Vertex_z;
   vector<float>   *Track_pt;
   vector<float>   *Track_eta;
   vector<float>   *Track_phi;
   vector<bool>    *Track_quality1;
   vector<bool>    *Track_quality2;
   vector<float>   *Track_truthMatchingProb;
   vector<float>   *Track_truthPt;
   vector<float>   *Track_truthEta;
   vector<float>   *Track_truthPhi;
   vector<float>   *Track_truthType;
   vector<float>   *Track_truthOrig;
   vector<float>   *Track_truthPdgID;
   vector<float>   *Track_truthz;
   vector<float>   *Track_truthnIn;
   vector<float>   *Track_truthBarcode;
   vector<float>   *Cluster_et;
   vector<float>   *Cluster_eta;
   vector<float>   *Cluster_phi;
   vector<float>   *truth_pt;
   vector<float>   *truth_eta;
   vector<float>   *truth_phi;
   vector<int>     *truth_pid;
   vector<int>     *truth_barcode;
   vector<bool>    *truth_isCharged;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_LumiBlock;   //!
   TBranch        *b_AvgMu;   //!
   TBranch        *b_ActMu;   //!
   TBranch        *b_fcalet_A;   //!
   TBranch        *b_fcalet_C;   //!
   TBranch        *b_PV_x;   //!
   TBranch        *b_PV_y;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_L1_MBTS_1;   //!
   TBranch        *b_Ntrk_MB;   //!
   TBranch        *b_N_barrelClus;   //!
   TBranch        *b_L1_energyT;   //!
   TBranch        *b_sumGapAC;   //!
   TBranch        *b_TriggerObject_Chain;   //!
   TBranch        *b_TriggerObject_Ps;   //!
   TBranch        *b_nTrkOnline;   //!
   TBranch        *b_nTrk_AR;   //!
   TBranch        *b_Vertex_nTrk;   //!
   TBranch        *b_Vertex_sumPt;   //!
   TBranch        *b_Vertex_sumPt2;   //!
   TBranch        *b_Vertex_type;   //!
   TBranch        *b_Vertex_x;   //!
   TBranch        *b_Vertex_y;   //!
   TBranch        *b_Vertex_z;   //!
   TBranch        *b_Track_pt;   //!
   TBranch        *b_Track_eta;   //!
   TBranch        *b_Track_phi;   //!
   TBranch        *b_Track_quality1;   //!
   TBranch        *b_Track_quality2;   //!
   TBranch        *b_Track_truthMatchingProb;   //!
   TBranch        *b_Track_truthPt;   //!
   TBranch        *b_Track_truthEta;   //!
   TBranch        *b_Track_truthPhi;   //!
   TBranch        *b_Track_truthType;   //!
   TBranch        *b_Track_truthOrig;   //!
   TBranch        *b_Track_truthPdgID;   //!
   TBranch        *b_Track_truthz;   //!
   TBranch        *b_Track_truthnIn;   //!
   TBranch        *b_Track_truthBarcode;   //!
   TBranch        *b_Cluster_et;   //!
   TBranch        *b_Cluster_eta;   //!
   TBranch        *b_Cluster_phi;   //!
   TBranch        *b_truth_pt;   //!
   TBranch        *b_truth_eta;   //!
   TBranch        *b_truth_phi;   //!
   TBranch        *b_truth_pid;   //!
   TBranch        *b_truth_barcode;   //!
   TBranch        *b_truth_isCharged;   //!

   truthSkim(TChain *tree=0);
   virtual ~truthSkim();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TChain *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef truthSkim_cxx
truthSkim::truthSkim(TChain* tree) : fChain(0) 
{
   
   std::ifstream list("inputFiles.txt");
   std::string filename;
   tree = new TChain("analysis");
//   if (tree == 0) {
      while(getline(list,filename)){
           tree->Add(Form("/sphenix/u/bseidlitz/work/trees/pp_13TeV_MB_pythia/user.bseidlit.ppDecorrelation_ppMBMC_trkClus_13TeV_01.361203.e3639_s2601_s2132_r6616_r6270_tid05411175_00_ANALYSIS.root/%s",filename.c_str()));
      }
/*
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/sphenix/u/bseidlitz/work/trees/pp_13TeV_MB_pythia/user.bseidlit.ppDecorrelation_ppMBMC_trkClus_13TeV_01.361203.e3639_s2601_s2132_r6616_r6270_tid05411175_00_ANALYSIS.root/user.bseidlit.33235767._000005.ANALYSIS.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/sphenix/u/bseidlitz/work/trees/pp_13TeV_MB_pythia/user.bseidlit.ppDecorrelation_ppMBMC_trkClus_13TeV_01.361203.e3639_s2601_s2132_r6616_r6270_tid05411175_00_ANALYSIS.root/user.bseidlit.33235767._000005.ANALYSIS.root");
      }
      f->GetObject("analysis",tree);
*/
  // }
   Init(tree);
}

truthSkim::~truthSkim()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t truthSkim::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t truthSkim::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void truthSkim::Init(TChain *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   TriggerObject_Chain = 0;
   TriggerObject_Ps = 0;
   Vertex_nTrk = 0;
   Vertex_sumPt = 0;
   Vertex_sumPt2 = 0;
   Vertex_type = 0;
   Vertex_x = 0;
   Vertex_y = 0;
   Vertex_z = 0;
   Track_pt = 0;
   Track_eta = 0;
   Track_phi = 0;
   Track_quality1 = 0;
   Track_quality2 = 0;
   Track_truthMatchingProb = 0;
   Track_truthPt = 0;
   Track_truthEta = 0;
   Track_truthPhi = 0;
   Track_truthType = 0;
   Track_truthOrig = 0;
   Track_truthPdgID = 0;
   Track_truthz = 0;
   Track_truthnIn = 0;
   Track_truthBarcode = 0;
   Cluster_et = 0;
   Cluster_eta = 0;
   Cluster_phi = 0;
   truth_pt = 0;
   truth_eta = 0;
   truth_phi = 0;
   truth_pid = 0;
   truth_barcode = 0;
   truth_isCharged = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("LumiBlock", &LumiBlock, &b_LumiBlock);
   fChain->SetBranchAddress("AvgMu", &AvgMu, &b_AvgMu);
   fChain->SetBranchAddress("ActMu", &ActMu, &b_ActMu);
   fChain->SetBranchAddress("fcalet_A", &fcalet_A, &b_fcalet_A);
   fChain->SetBranchAddress("fcalet_C", &fcalet_C, &b_fcalet_C);
   fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
   fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("L1_MBTS_1", &L1_MBTS_1, &b_L1_MBTS_1);
   fChain->SetBranchAddress("Ntrk_MB", &Ntrk_MB, &b_Ntrk_MB);
   fChain->SetBranchAddress("N_barrelClus", &N_barrelClus, &b_N_barrelClus);
   fChain->SetBranchAddress("L1_energyT", &L1_energyT, &b_L1_energyT);
   fChain->SetBranchAddress("sumGapAC", &sumGapAC, &b_sumGapAC);
   fChain->SetBranchAddress("TriggerObject_Chain", &TriggerObject_Chain, &b_TriggerObject_Chain);
   fChain->SetBranchAddress("TriggerObject_Ps", &TriggerObject_Ps, &b_TriggerObject_Ps);
   fChain->SetBranchAddress("nTrkOnline", &nTrkOnline, &b_nTrkOnline);
   fChain->SetBranchAddress("nTrk_AR", &nTrk_AR, &b_nTrk_AR);
   fChain->SetBranchAddress("Vertex_nTrk", &Vertex_nTrk, &b_Vertex_nTrk);
   fChain->SetBranchAddress("Vertex_sumPt", &Vertex_sumPt, &b_Vertex_sumPt);
   fChain->SetBranchAddress("Vertex_sumPt2", &Vertex_sumPt2, &b_Vertex_sumPt2);
   fChain->SetBranchAddress("Vertex_type", &Vertex_type, &b_Vertex_type);
   fChain->SetBranchAddress("Vertex_x", &Vertex_x, &b_Vertex_x);
   fChain->SetBranchAddress("Vertex_y", &Vertex_y, &b_Vertex_y);
   fChain->SetBranchAddress("Vertex_z", &Vertex_z, &b_Vertex_z);
   fChain->SetBranchAddress("Track_pt", &Track_pt, &b_Track_pt);
   fChain->SetBranchAddress("Track_eta", &Track_eta, &b_Track_eta);
   fChain->SetBranchAddress("Track_phi", &Track_phi, &b_Track_phi);
   fChain->SetBranchAddress("Track_quality1", &Track_quality1, &b_Track_quality1);
   fChain->SetBranchAddress("Track_quality2", &Track_quality2, &b_Track_quality2);
   fChain->SetBranchAddress("Track_truthMatchingProb", &Track_truthMatchingProb, &b_Track_truthMatchingProb);
   fChain->SetBranchAddress("Track_truthPt", &Track_truthPt, &b_Track_truthPt);
   fChain->SetBranchAddress("Track_truthEta", &Track_truthEta, &b_Track_truthEta);
   fChain->SetBranchAddress("Track_truthPhi", &Track_truthPhi, &b_Track_truthPhi);
   fChain->SetBranchAddress("Track_truthType", &Track_truthType, &b_Track_truthType);
   fChain->SetBranchAddress("Track_truthOrig", &Track_truthOrig, &b_Track_truthOrig);
   fChain->SetBranchAddress("Track_truthPdgID", &Track_truthPdgID, &b_Track_truthPdgID);
   fChain->SetBranchAddress("Track_truthz", &Track_truthz, &b_Track_truthz);
   fChain->SetBranchAddress("Track_truthnIn", &Track_truthnIn, &b_Track_truthnIn);
   fChain->SetBranchAddress("Track_truthBarcode", &Track_truthBarcode, &b_Track_truthBarcode);
   fChain->SetBranchAddress("Cluster_et", &Cluster_et, &b_Cluster_et);
   fChain->SetBranchAddress("Cluster_eta", &Cluster_eta, &b_Cluster_eta);
   fChain->SetBranchAddress("Cluster_phi", &Cluster_phi, &b_Cluster_phi);
   fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
   fChain->SetBranchAddress("truth_eta", &truth_eta, &b_truth_eta);
   fChain->SetBranchAddress("truth_phi", &truth_phi, &b_truth_phi);
   fChain->SetBranchAddress("truth_pid", &truth_pid, &b_truth_pid);
   fChain->SetBranchAddress("truth_barcode", &truth_barcode, &b_truth_barcode);
   fChain->SetBranchAddress("truth_isCharged", &truth_isCharged, &b_truth_isCharged);
   Notify();
}

Bool_t truthSkim::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void truthSkim::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t truthSkim::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef truthSkim_cxx
