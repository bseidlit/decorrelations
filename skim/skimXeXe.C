#include "../XeXeFileNames.C"
//#include "ppFileNames.C"

void skim(int fileNumber){

  TChain* fChain = new TChain("analysis");

  //string filePath = "/usatlas/scratch/bseidlit/pp_decor_WO_towers/user.bseidlit.ppDecorrelation_05.00341123.r11215_p3764_ANALYSIS.root/user.bseidlit.22676139._000001.ANALYSIS.root";
//  string filePath = "/usatlas/scratch/bseidlit/pp_decor_WO_towers/user.bseidlit/user.bseidlit.22565521._000011.ANALYSIS.root";
  

  //for (int ifile=0; ifile<nFiles; ifile++)fChain->Add((filePath+fileNames[ifile]).c_str());
  //fChain->Add(filePath.c_str());
  if (fileNumber == -1) for (int ifile=0; ifile<fileNames.size(); ifile++)  fChain->Add(fileNames[ifile].c_str());
  if (fileNumber != -1) fChain->Add(fileNames[fileNumber].c_str());

  Int_t           fCurrent;

  // Declaration of leaf types
   Int_t           RunNumber;
   Int_t           EventNumber;
   Int_t           LumiBlock;
   Float_t         AvgMu;
   Float_t         ActMu;
   Float_t         PV_x;
   Float_t         PV_y;
   Float_t         PV_z;
   Int_t           nVtx;
   Bool_t          L1_MBTS_1;
   Int_t           Ntrk_MB;
   vector<TString> *TriggerObject_Chain;
   vector<float>   *TriggerObject_Ps;
   vector<int>     *Vertex_nTrk;
   vector<float>   *Vertex_sumPt;
   vector<float>   *Vertex_sumPt2;
   vector<int>     *Vertex_type;
   vector<float>   *Vertex_x;
   vector<float>   *Vertex_y;
   vector<float>   *Vertex_z;
   vector<float>   *Track_pt;
   vector<float>   *Track_eta;
   vector<float>   *Track_phi;
   vector<float>   *Track_d0Signif;
   vector<float>   *Track_z0SinTheta;
   vector<float>   *Cluster_et;
   vector<float>   *Cluster_eta;
   vector<float>   *Cluster_phi;
   vector<float>   *Cluster_size;
   vector<int>     *Cluster_status;
   float fcalet_A;
   float fcalet_C;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_LumiBlock;   //!
   TBranch        *b_AvgMu;   //!
   TBranch        *b_ActMu;   //!
   TBranch        *b_PV_x;   //!
   TBranch        *b_PV_y;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_L1_MBTS_1;   //!
   TBranch        *b_Ntrk_MB;   //!
   TBranch        *b_TriggerObject_Chain;   //!
   TBranch        *b_TriggerObject_Ps;   //!
   TBranch        *b_Vertex_nTrk;   //!
   TBranch        *b_Vertex_sumPt;   //!
   TBranch        *b_Vertex_sumPt2;   //!
   TBranch        *b_Vertex_type;   //!
   TBranch        *b_Vertex_x;   //!
   TBranch        *b_Vertex_y;   //!
   TBranch        *b_Vertex_z;   //!
   TBranch        *b_Track_pt;   //!
   TBranch        *b_Track_eta;   //!
   TBranch        *b_Track_phi;   //!
   TBranch        *b_Track_d0Signif;   //!
   TBranch        *b_Track_z0SinTheta;   //!
   TBranch        *b_Cluster_et;   //!
   TBranch        *b_Cluster_eta;   //!
   TBranch        *b_Cluster_phi;   //!
   TBranch        *b_Cluster_size;   //!
   TBranch        *b_Cluster_status;   //!
   TBranch        *b_fcalet_A; //!
   TBranch        *b_fcalet_C; //!

   // Set object pointer
   TriggerObject_Chain = 0;
   TriggerObject_Ps = 0;
   Vertex_nTrk = 0;
   Vertex_sumPt = 0;
   Vertex_sumPt2 = 0;
   Vertex_type = 0;
   Vertex_x = 0;
   Vertex_y = 0;
   Vertex_z = 0;
   Track_pt = 0;
   Track_eta = 0;
   Track_phi = 0;
   Track_d0Signif = 0;
   Track_z0SinTheta = 0;
   Cluster_et = 0;
   Cluster_eta = 0;
   Cluster_phi = 0;
   Cluster_size = 0;
   Cluster_status = 0;
   // Set branch addresses and branch pointers
  // if (!tree) return;
   //fChain = tree;
  // fCurrent = -1;
   fChain->SetMakeClass(1);


   fChain->SetBranchAddress("RunNumber", &RunNumber );
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("LumiBlock", &LumiBlock, &b_LumiBlock);
   fChain->SetBranchAddress("AvgMu", &AvgMu, &b_AvgMu);
   fChain->SetBranchAddress("ActMu", &ActMu, &b_ActMu);
   fChain->SetBranchAddress("PV_x", &PV_x, &b_PV_x);
   fChain->SetBranchAddress("PV_y", &PV_y, &b_PV_y);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("L1_MBTS_1", &L1_MBTS_1, &b_L1_MBTS_1);
   fChain->SetBranchAddress("Ntrk_MB", &Ntrk_MB, &b_Ntrk_MB);
   fChain->SetBranchAddress("TriggerObject_Chain", &TriggerObject_Chain, &b_TriggerObject_Chain);
   fChain->SetBranchAddress("TriggerObject_Ps", &TriggerObject_Ps, &b_TriggerObject_Ps);
   fChain->SetBranchAddress("Vertex_nTrk", &Vertex_nTrk, &b_Vertex_nTrk);
   fChain->SetBranchAddress("Vertex_sumPt", &Vertex_sumPt, &b_Vertex_sumPt);
   fChain->SetBranchAddress("Vertex_sumPt2", &Vertex_sumPt2, &b_Vertex_sumPt2);
   fChain->SetBranchAddress("Vertex_type", &Vertex_type, &b_Vertex_type);
   fChain->SetBranchAddress("Vertex_x", &Vertex_x, &b_Vertex_x);
   fChain->SetBranchAddress("Vertex_y", &Vertex_y, &b_Vertex_y);
   fChain->SetBranchAddress("Vertex_z", &Vertex_z, &b_Vertex_z);
   fChain->SetBranchAddress("Track_pt", &Track_pt, &b_Track_pt);
   fChain->SetBranchAddress("Track_eta", &Track_eta, &b_Track_eta);
   fChain->SetBranchAddress("Track_phi", &Track_phi, &b_Track_phi);
   fChain->SetBranchAddress("Track_d0Signif", &Track_d0Signif, &b_Track_d0Signif);
   fChain->SetBranchAddress("Track_z0SinTheta", &Track_z0SinTheta, &b_Track_z0SinTheta);
   fChain->SetBranchAddress("Cluster_et", &Cluster_et, &b_Cluster_et);
   fChain->SetBranchAddress("Cluster_eta", &Cluster_eta, &b_Cluster_eta);
   fChain->SetBranchAddress("Cluster_phi", &Cluster_phi, &b_Cluster_phi);
   fChain->SetBranchAddress("Cluster_size", &Cluster_size, &b_Cluster_size);
   fChain->SetBranchAddress("Cluster_status", &Cluster_status, &b_Cluster_status);
   fChain->SetBranchAddress("fcalet_A", &fcalet_A, &b_fcalet_A);
   fChain->SetBranchAddress("fcalet_C", &fcalet_C, &b_fcalet_C);

  //////////////////////////////////////////////////
  // triggers 

  std::vector< std::string > m_AnalysisTriggers;

  // 2015 low-mu run trigger list
  m_AnalysisTriggers.push_back("HLT_mb_sptrk");


  //////////////////////////////////////////////////
  //  HIstograms
  TH2F* h_Ntrk_Nclus_barrel = new TH2F("h_Ntrk_Nclus_barrel","",3000,0,3000,500,0,500);
  TH2F* h_Ntrk_Nclus_FCAL = new TH2F("h_Ntrk_Nclus_FCAL","",3000,0,3000,500,0,500);
  TH1F* h_Ntrk = new TH1F("h_Ntrk","",3000,0,3000);
  TH2F* h_Ntrk_FCAL_sumET = new TH2F("h_Ntrk_FCAL_sumET","",3000,0,3000,3000,0,3000);
   

  /////////////////////////////////////////////////////////////
  // secEvent loop
  std::cout.imbue(std::locale(""));

  cout << "Number of events = " << fChain->GetEntries()<< endl;

  for(Long64_t iEL = 0; iEL < fChain->GetEntries(); iEL++){

    // if (iEL > 100) continue;

    if ((iEL%100000) == 0) cout << " -- processing event " << iEL << endl;
        Long64_t ientry = fChain->LoadTree(iEL);
        if (ientry < 0) break;

    //if ((iEL%10) != 0) continue;

    fChain->GetEntry(iEL);
  // cout << endl << "got entry" << endl;

    //-////////////////////
    // Event level quants
    int trk_400 = 0;
    bool useVtx = 0;
    int trk_n = Track_pt->size();
    for (int itrk=0; itrk<trk_n; itrk++){
      if (Track_pt->at(itrk) > 400){
          trk_400++;
      }
    }

    //cout << trk_400 << " grid def " << Ntrk_MB << endl;
    float FCAL_sumET = fcalet_A + fcalet_C;

    //////////////////////////////////
    //Clusters into vector
    int Nclus_barrel=0;
    int Nclus_FCAL =0;
    
    for (int iclus = 0;iclus<Cluster_eta->size();iclus++){
	if (fabs(Cluster_eta->at(iclus)) < 2.5 && Cluster_et->at(iclus) > 400.) Nclus_barrel++; 
	if (fabs(Cluster_eta->at(iclus)) > 3.2 && Cluster_et->at(iclus) > 400.) Nclus_FCAL++; 
     }

     ////////////////////////////////////////////////
    //track into Vector and quality cut
/*
    for (int ipart = 0; ipart<trk_n; ipart++){
      if (Track_pt->at(ipart) < 400) continue;
      if (fabs(Track_eta->at(ipart)) >= 2.5) continue;
      float track_eff = 1;

    }
*/

    /////////////////////////////////////////////////////////
    // Fill histograms

    h_Ntrk_Nclus_barrel->Fill(trk_400,Nclus_barrel);
    h_Ntrk_Nclus_FCAL->Fill(trk_400,Nclus_FCAL);
    h_Ntrk->Fill(trk_400);
    h_Ntrk_FCAL_sumET->Fill(trk_400,FCAL_sumET);

 /*   cout << endl;
    for (int iT=0; iT<TriggerObject_Chain->size(); iT++)
      cout << endl << TriggerObject_Chain->at(iT);
*/
    // trigger eff


  } // end EL

  TFile *fout = new TFile("out_skimXeXe.root","RECREATE");

  h_Ntrk_Nclus_barrel->Write();
  h_Ntrk_Nclus_FCAL->Write();
  h_Ntrk->Write();
  h_Ntrk_FCAL_sumET->Write();



  cout << endl;

}
